/**
 ** This file is part of the sila_cpp project.
 ** Copyright 2021 SiLA2
 **
 ** Permission is hereby granted, free of charge, to any person obtaining a copy
 ** of this software and associated documentation files (the "Software"), to deal
 ** in the Software without restriction, including without limitation the rights
 ** to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 ** copies of the Software, and to permit persons to whom the Software is
 ** furnished to do so, subject to the following conditions:
 **
 ** The above copyright notice and this permission notice shall be included in all
 ** copies or substantial portions of the Software.
 **
 ** THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 ** IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 ** FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 ** AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 ** LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 ** OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 ** SOFTWARE.
 **/

//============================================================================
/// \file   DynamicClient.cpp
/// \author Florian Meinicke (florian.meinicke@cetoni.de)
/// \date   20.01.2021
/// \brief  A minimal dynamic client example
//============================================================================

//============================================================================
//                                  INCLUDES
//============================================================================
#include <sila_cpp/client/DynamicSiLAClient.h>
#include <sila_cpp/client/ServerManager.h>
#include <sila_cpp/common/FullyQualifiedCommandID.h>
#include <sila_cpp/common/ServerAddress.h>
#include <sila_cpp/common/logging.h>
#include <sila_cpp/data_types.h>
#include <sila_cpp/framework/error_handling/FrameworkError.h>
#include <sila_cpp/framework/error_handling/ValidationError.h>

#include <QCoreApplication>
#include <QFutureWatcher>
#include <QThread>

#include <grpcpp/grpcpp.h>

using namespace std;
using SiLA2::CDynamicSiLAClient;
using SiLA2::CServerAddress;
using SiLA2::CServerInformation;
using SiLA2::CServerManager;

//============================================================================
int main(int argc, char* argv[])
{
    QCoreApplication App{argc, argv};
    QCoreApplication::setApplicationName("DynamicClient");
    QCoreApplication::setApplicationVersion("0.1.0");

    // specify whether to accept untrusted certificates
    CDynamicSiLAClient::setAcceptUntrustedServerCertificateHandler(
        [](const QSslCertificate&, const CDynamicSiLAClient*) { return true; });

    QEventLoop DiscoveryLoop;
    CServerManager Manager;
    Manager.start();
    QUuid ServerUUID;
    QObject::connect(
        &Manager, &CServerManager::serverDiscovered,
        [&DiscoveryLoop, &ServerUUID](const QUuid& DiscoveredUUID,
                                      const CServerAddress& Address) {
            qDebug() << "Discovered Server" << DiscoveredUUID << "on" << Address;
            ServerUUID = DiscoveredUUID;
            DiscoveryLoop.quit();
        });
    DiscoveryLoop.exec();  // executes until a server is found
    const auto Client = Manager.connect(ServerUUID);
    if (!Client)
    {
        qCritical() << "Could not connect to Server";
        return 1;
    }

    // Greeting Provider (Unobservable) ======================================
    const auto GreetingProviderStub =  // index 0 is SiLAService Feature
        Client->featureStub(Client->featureIdentifiers().at(1));

    // SayHello Command ------------------------------------------------------
    // valid call
    const auto SayHelloCommand = GreetingProviderStub->command(
        GreetingProviderStub->commandIdentifiers().front());
    //    get Parameters from Command and set value(s)
    auto SayHelloParameters = SayHelloCommand->parameters();
    SayHelloParameters["Name"] = "World";
    const auto SayHelloResponse = SayHelloCommand->call(SayHelloParameters);
    qDebug() << "Received greeting:" << SayHelloResponse;

    try
    {
        // throws Validation Error because of Parameter == "error"
        SayHelloParameters[0] = "error";
        // SayHelloParameters[0].setValue("error"); // does the same
        qInfo() << SayHelloCommand->call(SayHelloParameters);
    }
    catch (const SiLA2::CFrameworkError& err)
    {
        qInfo() << "Caught expected Framework Error:" << err.what();
    }

    // StartYear Property ----------------------------------------------------
    const auto StartYearProperty = GreetingProviderStub->property(
        GreetingProviderStub->propertyIdentifiers().front());
    qDebug() << "Received StartYear value" << StartYearProperty->get();

    // Temperature Controller (Observable) ===================================
    const auto TemperatureControllerStub =
        Client->featureStub(Client->featureIdentifiers().at(2));

    // CurrentTemperature Property -------------------------------------------
    const auto CurrentTemperatureProperty = TemperatureControllerStub->property(
        TemperatureControllerStub->propertyIdentifiers().front());
    //    option 1: get a single value
    qInfo() << "Single CurrentTemperature value"
            << CurrentTemperatureProperty->get();

    //    option 2: get notified whenever the value changes
    //    (i.e. subscribe to the Property)
    const auto CurrentTemperature = CurrentTemperatureProperty->subscribe();
    QFutureWatcher<SiLA2::CDynamicValue> CurrentTemperatureWatcher{};
    QObject::connect(&CurrentTemperatureWatcher,
                     &QFutureWatcher<SiLA2::CDynamicValue>::resultReadyAt,
                     [CurrentTemperature](int index) {
                         qInfo() << "Received CurrentTemperature value"
                                 << CurrentTemperature.resultAt(index);
                     });
    //    set the Future **after** connecting to the Watcher's signal(s)
    CurrentTemperatureWatcher.setFuture(CurrentTemperature);

    // ControlTemperature Command --------------------------------------------
    const auto ControlTemperatureCommand = TemperatureControllerStub->command(
        TemperatureControllerStub->commandIdentifiers().front());
    auto ControlTemperatureParameters = ControlTemperatureCommand->parameters();
    // 'TargetTemperature' is of type Real, so we need to explicitly pass a
    // double/float; passing an int would set the Parameter type to Integer which
    // would result in an error when we call the Command
    ControlTemperatureParameters["TargetTemperature"] = 360.0;
    try
    {
        //    option 1: let sila_cpp handle the Execution and just get the result
        ControlTemperatureCommand->call(ControlTemperatureParameters);
    }
    catch (const SiLA2::CSiLAError& err)
    {
        qInfo() << "Caught unexpected Error:" << err.what();
    }

    //    option 2: just start the call to get the Execution UUID...
    try
    {
        ControlTemperatureParameters["TargetTemperature"] = 280.0;
        const auto UUID =
            ControlTemperatureCommand->start(ControlTemperatureParameters);
        // ... to manually retrieve Execution Info (or Intermediate Responses)
        //      get single Execution Info value
        qDebug() << "Single Execution Info value"
                 << ControlTemperatureCommand->getExecutionInfo(UUID);
        try
        {
            // premature call to get Result when Command is not finished yet will
            // yield a Framework Error but Command Execution continues
            ControlTemperatureCommand->result(UUID);
        }
        catch ([[maybe_unused]] const SiLA2::CFrameworkError& err)
        {
            qInfo() << "Caught expected Framework Error";
        }

        //      subscribe to Execution Info
        auto ExecutionInfo =
            ControlTemperatureCommand->subscribeExecutionInfo(UUID);
        QFutureWatcher<SiLA2::CExecutionInfo> ExecInfoWatcher;
        QEventLoop EventLoop;
        QObject::connect(
            &ExecInfoWatcher,
            &QFutureWatcher<SiLA2::CExecutionInfo>::resultReadyAt,
            [ExecutionInfo, &EventLoop](int index) {
                try
                {
                    // this will throw if the Execution Info cannot be obtained
                    const auto ExecInfo = ExecutionInfo.resultAt(index);
                    qDebug() << "Received ExecutionInfo value" << ExecInfo;
                    switch (ExecInfo.commandStatus())
                    {
                    case SiLA2::CommandStatus::Waiting:
                    case SiLA2::CommandStatus::Running:
                        break;
                    case SiLA2::CommandStatus::FinishedSuccessfully:
                    case SiLA2::CommandStatus::FinishedWithError:
                        EventLoop.quit();
                        break;
                    }
                }
                catch (const SiLA2::CSiLAError& err)
                {
                    qInfo() << "Caught unexpected Error:" << err.what();
                    EventLoop.quit();
                }
            });
        QObject::connect(&ExecInfoWatcher,
                         &QFutureWatcher<SiLA2::CExecutionInfo>::finished,
                         &EventLoop, &QEventLoop::quit);
        ExecInfoWatcher.setFuture(ExecutionInfo);
        EventLoop.exec();  // executes until Command is finished (i.e. until no
                           // more Execution Info can be read), or an error occurs

        //      just wait until Execution is finished
        ExecutionInfo.waitForFinished();

        // retrieve final result (must happen *after* no more Execution Info can
        // be read or else you'll get a Framework Error like above)
        ControlTemperatureCommand->result(UUID);
    }
    catch (const SiLA2::CSiLAError& err)
    {
        qInfo() << "Caught unexpected Error:" << err.what();
    }

    return App.exec();
}
