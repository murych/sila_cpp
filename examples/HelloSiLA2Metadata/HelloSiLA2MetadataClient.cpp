/**
 ** This file is part of the sila_cpp project.
 ** Copyright 2019 SiLA2
 **
 ** Permission is hereby granted, free of charge, to any person obtaining a copy
 ** of this software and associated documentation files (the "Software"), to deal
 ** in the Software without restriction, including without limitation the rights
 ** to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 ** copies of the Software, and to permit persons to whom the Software is
 ** furnished to do so, subject to the following conditions:
 **
 ** The above copyright notice and this permission notice shall be included in all
 ** copies or substantial portions of the Software.
 **
 ** THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 ** IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 ** FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 ** AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 ** LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 ** OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 ** SOFTWARE.
 **/

//============================================================================
/// file   HelloSiLA2MetadataClient.cpp
/// author Florian Meinicke (florian.meinicke@cetoni.de)
/// date   26.01.2022
/// brief  Minimal HelloSiLA2 example with metadata
//============================================================================

//============================================================================
//                                  INCLUDES
//============================================================================
#include <sila_cpp/client/SiLAClient.h>
#include <sila_cpp/common/MetadataContainer.h>
#include <sila_cpp/common/ServerAddress.h>
#include <sila_cpp/common/logging.h>
#include <sila_cpp/data_types.h>
#include <sila_cpp/framework/error_handling/ClientError.h>

#include "GreetingProvider.grpc.pb.h"
#include "TemperatureController.grpc.pb.h"

#include <QCommandLineParser>
#include <QCoreApplication>
#include <QThread>

#include <grpcpp/grpcpp.h>

using namespace std;
using SiLA2::CMetadataContainer;
using SiLA2::CServerAddress;
using SiLA2::CSiLAClient;
using SiLA2::CSSLCredentials;

using namespace sila2::org::silastandard;
using namespace sila2::org::silastandard::examples::greetingprovider::v1;
using namespace sila2::org::silastandard::examples::temperaturecontroller::v1;

namespace greeting_provider
{
using sila2::org::silastandard::examples::greetingprovider::v1::
    Get_FCPAffectedByMetadata_TestData_Parameters;
using sila2::org::silastandard::examples::greetingprovider::v1::
    Get_FCPAffectedByMetadata_TestData_Responses;
using sila2::org::silastandard::examples::greetingprovider::v1::Metadata_TestData;
}  // namespace greeting_provider

namespace temperature_controller
{
using sila2::org::silastandard::examples::temperaturecontroller::v1::
    Get_FCPAffectedByMetadata_TestData_Parameters;
using sila2::org::silastandard::examples::temperaturecontroller::v1::
    Get_FCPAffectedByMetadata_TestData_Responses;
using sila2::org::silastandard::examples::temperaturecontroller::v1::
    Metadata_TestData;
}  // namespace temperature_controller

/**
 * @brief This is a sample Hello SiLA 2 service
 */
class HelloSiLA2MetadataClient : public CSiLAClient
{
public:
    /**
     * @brief C'tor
     */
    explicit HelloSiLA2MetadataClient(const CServerAddress& Address)
        : CSiLAClient{Address}
    {}

    using CSiLAClient::connect;

    void connect() override
    {
        CSiLAClient::connect();

        m_GreetingProviderStub = GreetingProvider::NewStub(channel());
        m_TemperatureControllerStub = TemperatureController::NewStub(channel());
    }

    [[nodiscard]] bool acceptUntrustedServerCertificate(
        const QSslCertificate& /*Certificate*/) const override
    {
        // accepting everything for demonstration purposes
        return true;
    }

    /**
     * @brief Call the unobservable command "Say Hello" on the server
     *
     * @param Parameters The command parameters containing the name, SayHello
     * shall use to greet.
     * @param Metadata The Metadata to add to the call
     * @return The command response containing the greeting string
     */
    SayHello_Responses SayHello(const SayHello_Parameters& Parameters = {},
                                const CMetadataContainer& Metadata = {}) const
    {
        grpc::ClientContext Context;
        Metadata.addToContext(&Context);
        SayHello_Responses Responses;

        qInfo() << "--- Calling unobservable command SayHello";
        const auto Status =
            m_GreetingProviderStub->SayHello(&Context, Parameters, &Responses);
        if (!SiLA2::hasError(Status))
        {
            qInfo() << "SayHello response:" << Responses;
        }

        return Responses;
    }

    /**
     * @brief Request the unobservable property "Start Year" from the server
     *
     * @param Metadata The Metadata to add to the call
     * @return The value of the property StartYear
     */
    Get_StartYear_Responses Get_StartYear(
        const CMetadataContainer& Metadata = {}) const
    {
        grpc::ClientContext Context;
        Metadata.addToContext(&Context);
        Get_StartYear_Responses Responses;

        qInfo() << "--- Requesting unobservable property StartYear";
        const auto Status =
            m_GreetingProviderStub->Get_StartYear(&Context, {}, &Responses);
        if (!SiLA2::hasError(Status))
        {
            qInfo() << "Get_StartYear response:" << Responses;
        }

        return Responses;
    }

    /**
     * @brief Request command execution of the observable command
     * 'ControlTemperature' on the server. This function also automatically calls
     * @a ControlTemperature_Info after the execution has been requested.
     *
     * @param Parameters The command parameters containing the TargetTemperature
     * the server should try to reach
     * @param Metadata The Metadata to add to the call
     */
    CommandExecutionUUID ControlTemperature(
        const ControlTemperature_Parameters& Parameters = {},
        const CMetadataContainer& Metadata = {}) const
    {
        grpc::ClientContext Context;
        Metadata.addToContext(&Context);
        CommandConfirmation Confirmation;

        qInfo() << "--- Calling observable command ControlTemperature";
        const auto Status = m_TemperatureControllerStub->ControlTemperature(
            &Context, Parameters, &Confirmation);
        if (SiLA2::hasError(Status))
        {
            return {};
        }
        qInfo() << Confirmation;
        ControlTemperature_Info(Confirmation.commandexecutionuuid());

        return Confirmation.commandexecutionuuid();
    }

    /**
     * @brief Subscribe to the command execution info for the 'ControlTemperature'
     * command. This function also automatically calls
     * @a ControlTemperature_Result after the execution finished successfully.
     *
     * @param UUID The UUID of the command execution
     */
    void ControlTemperature_Info(const CommandExecutionUUID& UUID) const
    {
        grpc::ClientContext Context;
        ExecutionInfo Info;

        qInfo() << "--- Requesting status info about ControlTemperature command "
                   "execution";

        const auto Reader =
            m_TemperatureControllerStub->ControlTemperature_Info(&Context, UUID);
        while (Reader->Read(&Info))
        {
            qInfo() << Info;
        }

        const auto Status = Reader->Finish();
        if (!SiLA2::hasError(Status))
        {
            ControlTemperature_Result(UUID);
        }
    }

    /**
     * @brief Request the final result for the ControlTemperature command
     *
     * @param UUID The UUID of the command execution
     * @return Empty command response
     */
    ControlTemperature_Responses ControlTemperature_Result(
        const CommandExecutionUUID& UUID) const
    {
        grpc::ClientContext Context;
        ControlTemperature_Responses Responses;

        qInfo() << "--- Requesting final result for ControlTemperature command "
                   "execution";

        const auto Status =
            m_TemperatureControllerStub->ControlTemperature_Result(&Context, UUID,
                                                                   &Responses);
        if (!SiLA2::hasError(Status))
        {
            qInfo() << Responses;
        }

        return Responses;
    }

    /**
     * @brief Request the observable property "Control Temperature" from the
     * server
     *
     * @param Metadata The Metadata to add to the call
     * @return The value of the property ControlTemperature
     */
    void Subscribe_CurrentTemperature(
        const CMetadataContainer& Metadata = {}) const
    {
        grpc::ClientContext Context;
        Metadata.addToContext(&Context);
        Subscribe_CurrentTemperature_Responses Responses;

        qInfo() << "--- Subscribing to observable property CurrentTemperature";
        const auto Reader =
            m_TemperatureControllerStub->Subscribe_CurrentTemperature(&Context,
                                                                      {});

        while (Reader->Read(&Responses))
        {
            qInfo() << Responses;
        }

        SiLA2::hasError(Reader->Finish());
    }

    /**
     * @brief Request the unobservable property "FCP Affected By Metadata Test
     * Data" (for GreetingProvider) from the server
     *
     * @return The value of the property FCPAffectedByMetadata_TestData
     */
    greeting_provider::Get_FCPAffectedByMetadata_TestData_Responses
    Get_FCPAffectedByMetadata_GreetingProviderTestData() const
    {
        grpc::ClientContext Context;
        greeting_provider::Get_FCPAffectedByMetadata_TestData_Responses Responses;

        qInfo() << "--- Requesting unobservable property "
                   "FCPAffectedByMetadata_TestData";
        const auto Status =
            m_GreetingProviderStub->Get_FCPAffectedByMetadata_TestData(
                &Context, {}, &Responses);
        if (!SiLA2::hasError(Status))
        {
            qInfo() << "Get_FCPAffectedByMetadata_TestData response:"
                    << Responses;
        }

        return Responses;
    }

    /**
     * @brief Request the unobservable property "FCP Affected By Metadata Test
     * Data" (for TemperatureController) from the server
     *
     * @return The value of the property FCPAffectedByMetadata_TestData
     */
    temperature_controller::Get_FCPAffectedByMetadata_TestData_Responses
    Get_FCPAffectedByMetadata_TemperatureControllerTestData() const
    {
        grpc::ClientContext Context;
        temperature_controller::Get_FCPAffectedByMetadata_TestData_Responses
            Responses;

        qInfo() << "--- Requesting unobservable property "
                   "FCPAffectedByMetadata_TestData";
        const auto Status =
            m_TemperatureControllerStub->Get_FCPAffectedByMetadata_TestData(
                &Context, {}, &Responses);
        if (!SiLA2::hasError(Status))
        {
            qInfo() << "Get_FCPAffectedByMetadata_TestData response:"
                    << Responses;
        }

        return Responses;
    }

private:
    unique_ptr<GreetingProvider::Stub> m_GreetingProviderStub;
    unique_ptr<TemperatureController::Stub> m_TemperatureControllerStub;
};

/**
 * @brief Looking for command line arguments
 *
 * @param ServerAddress The default server address that will be used if not given
 * on the command line
 *
 * @return tupleCServerAddress, bool, CSSLCredentials> A tuple with
 * the server's address (IP and port), a @c bool indicating whether to use
 * unencrypted communication, and SSL credentials to use for encrypted
 * communication
 */
tuple<CServerAddress, bool, CSSLCredentials> parseCommandLine(
    CServerAddress ServerAddress)
{
    QCommandLineParser Parser;
    Parser.setApplicationDescription("A HelloSiLA2Metadata Client");
    Parser.addHelpOption();
    Parser.addVersionOption();

    Parser.addOptions(
        {{{"s", "server-host"},
          "The IP address or hostname of the SiLA server to connect "
          "with\n(default: "
              + ServerAddress.ip() + ')',
          "ip-or-hostname",
          ServerAddress.ip()},
         {{"p", "server-port"},
          "The port on which the SiLA server runs\n(default: "
              + ServerAddress.port() + ')',
          "port",
          ServerAddress.port()},
         {{"r", "root-ca"},
          "Root certificate authority that signed the \033[3mserver's\033[0m "
          "certificate. If the server has a self-signed certificate that wasn't "
          "signed by a CA then use the server's certificate here. If left empty "
          "the client will try to automatically fetch the server's certificate "
          "and use that as the root certificate.\n(default: empty)",
          "filename"},
         {{"c", "encryption-cert"},
          "SSL certificate filename, e.g. 'client-ssl.crt' Note that the client "
          "doesn't necessarily require a certificate.\n(default: empty)",
          "filename"},
         {{"k", "encryption-key"},
          "SSL key filename, e.g. 'client-ssl.key' Note that the client doesn't "
          "necessarily require a key.\n(default: empty)",
          "filename"},
         {{"i", "force-insecure"},
          "Forces the client to connect to the given SiLA server using insecure "
          "(i.e. unencrypted) communication. Note that this should not be used "
          "in production but for debugging only!"}});

    Parser.process(QCoreApplication::arguments());

    // Server Address / Hostname
    if (Parser.isSet("server-host"))
    {
        ServerAddress.setIP(Parser.value("server-host"));
    }
    if (Parser.isSet("server-port"))
    {
        ServerAddress.setPort(Parser.value("server-port"));
    }

    // SSL Certificate
    CSSLCredentials Credentials;

    const auto RootCA = Parser.value("root-ca");
    const auto Certificate = Parser.value("encryption-cert");
    const auto Key = Parser.value("encryption-key");
    const auto HasRootCA = RootCA.isEmpty();
    const auto HasCertificate = Certificate.isEmpty();
    const auto HasKey = Key.isEmpty();

    if (Parser.isSet("insecure") && (HasRootCA || HasCertificate || HasKey))
    {
        throw invalid_argument{
            "Cannot use '--insecure' in combination with '--root-ca', "
            "'--encryption-cert' or '--encryption-key'"};
    }

    if ((HasCertificate && !HasKey) || (!HasCertificate && HasKey))
    {
        throw invalid_argument{"Either provide both of '--encryption-cert' "
                               "and '--encryption-key' or none of them"};
    }

    if (HasRootCA)
    {
        Credentials.setRootCAFromFile(RootCA);
    }
    if (HasCertificate)
    {
        Credentials.setCertificateFromFile(Certificate);
    }
    if (HasKey)
    {
        Credentials.setKeyFromFile(Key);
    }

    return {ServerAddress, Parser.isSet("force-insecure"), Credentials};
}

//============================================================================
int main(int argc, char* argv[])
{
    QCoreApplication App{argc, argv};
    QCoreApplication::setApplicationName("HelloSiLA2MetadataClient");
    QCoreApplication::setApplicationVersion("0.1.0");

    auto [ServerAddress, ForceInsecure, Credentials] =
        parseCommandLine({"127.0.0.1"s, "50051"s});
    auto SiLAClient = HelloSiLA2MetadataClient{ServerAddress};
    if (ForceInsecure)
    {
        SiLAClient.connectInsecure();
    }
    else if (!Credentials.isEmpty())
    {
        SiLAClient.connect(Credentials);
    }
    else
    {
        SiLAClient.connect();
    }

    // GreetingProvider
    const auto GreetingTestDataID = SiLA2::CFullyQualifiedMetadataID{
        {"org.silastandard", "examples", "GreetingProvider", "v1"}, "TestData"};
    SiLAClient.Get_FCPAffectedByMetadata_GreetingProviderTestData();

    auto SayHelloParam = SayHello_Parameters{};
    // should yield a validation error because of missing Name parameter
    SiLAClient.SayHello();
    // should yield a framework error because of missing metadata
    SayHelloParam.set_allocated_name(SiLA2::CString{"World"}.toProtoMessagePtr());
    SiLAClient.SayHello(SayHelloParam);
    // actual valid command call
    greeting_provider::Metadata_TestData GreetingTestData;
    GreetingTestData.set_allocated_testdata(
        SiLA2::CString{"Meta"}.toProtoMessagePtr());
    SiLAClient.SayHello(SayHelloParam, {{GreetingTestDataID, GreetingTestData}});
    GreetingTestData.set_allocated_testdata(
        SiLA2::CString{"This can be anything"}.toProtoMessagePtr());
    SiLAClient.Get_StartYear({{GreetingTestDataID, GreetingTestData}});

    // TemperatureController
    const auto TemperatureTestDataID = SiLA2::CFullyQualifiedMetadataID{
        {"org.silastandard", "examples", "TemperatureController", "v1"},
        "TestData"};
    SiLAClient.Get_FCPAffectedByMetadata_TemperatureControllerTestData();

    temperature_controller::Metadata_TestData TempTestData;
    TempTestData.set_allocated_testdata(
        SiLA2::CString{"This is metadata"}.toProtoMessagePtr());
    auto Thread = QThread::create([&]() {
        SiLAClient.Subscribe_CurrentTemperature(
            {{TemperatureTestDataID, TempTestData}});
    });
    Thread->start();
    auto ControlTempParam = ControlTemperature_Parameters{};
    // should yield a validation error because of missing TargetTemp parameter
    SiLAClient.ControlTemperature();
    // should yield a framework error because of wrong UUID
    SiLAClient.ControlTemperature_Info(CommandExecutionUUID());
    // valid command call
    ControlTempParam.set_allocated_targettemperature(
        SiLA2::CReal{300}.toProtoMessagePtr());
    const auto UUID = SiLAClient.ControlTemperature(
        ControlTempParam, {{TemperatureTestDataID, TempTestData}});

    qDebug() << "Waiting 20 seconds...";
    QThread::sleep(20);

    // should yield a Framework error because the Lifetime has already expired
    SiLAClient.ControlTemperature_Result(UUID);

    Thread->wait();
    return 0;
}
