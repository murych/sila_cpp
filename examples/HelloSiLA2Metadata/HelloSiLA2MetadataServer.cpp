/**
 ** This file is part of the sila_cpp project.
 ** Copyright 2019 SiLA2
 **
 ** Permission is hereby granted, free of charge, to any person obtaining a copy
 ** of this software and associated documentation files (the "Software"), to deal
 ** in the Software without restriction, including without limitation the rights
 ** to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 ** copies of the Software, and to permit persons to whom the Software is
 ** furnished to do so, subject to the following conditions:
 **
 ** The above copyright notice and this permission notice shall be included in all
 ** copies or substantial portions of the Software.
 **
 ** THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 ** IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 ** FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 ** AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 ** LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 ** OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 ** SOFTWARE.
 **/

//============================================================================
/// file   HelloSiLA2MetadataServer.cpp
/// author Florian Meinicke (florian.meinicke@cetoni.de)
/// date   26.01.2022
/// brief  Minimal HelloSiLA2 example with metadata
//============================================================================

//============================================================================
//                                  INCLUDES
//============================================================================
#include <sila_cpp/common/FullyQualifiedFeatureID.h>
#include <sila_cpp/common/SSLCredentials.h>
#include <sila_cpp/common/ServerAddress.h>
#include <sila_cpp/common/ServerInformation.h>
#include <sila_cpp/common/logging.h>
#include <sila_cpp/server/SiLAServer.h>

#include <QCommandLineParser>
#include <QCoreApplication>

// include SiLA features
#include "GreetingProvider/GreetingProviderImpl.h"
#include "TemperatureController/TemperatureControllerImpl.h"

using namespace std;
using SiLA2::CFullyQualifiedFeatureID;
using SiLA2::CServerAddress;
using SiLA2::CServerInformation;
using SiLA2::CSiLAServer;
using SiLA2::CSSLCredentials;
using sila2::org::silastandard::examples::greetingprovider::v1::GreetingProvider;
using sila2::org::silastandard::examples::temperaturecontroller::v1::
    TemperatureController;

/**
 * @brief This is a sample Hello SiLA 2 service
 */
class HelloSiLA2MetadataServer : public CSiLAServer
{
public:
    /**
     * @brief C'tor
     */
    HelloSiLA2MetadataServer(const CServerInformation& ServerInfo,
                             const CServerAddress& Address)
        : CSiLAServer{ServerInfo, Address}
    {
        qDebug() << "Registering features...";
        registerFeature(new CGreetingProviderImpl(this));
        registerFeature(new CTemperatureControllerImpl(this));
    }
};

/**
 * @brief Looking for command line arguments
 *
 * @return tuple<CServerInformation, CServerAddress, CSSLCredentials> A tuple with
 * the server information (name, type, ...), the server's address (IP and port), a
 * @c bool indicating whether to use unencrypted communication, and SSL
 * credentials to use for encrypted communication if no automatically generated
 * self-signed certificate should be used
 */
tuple<CServerInformation, const CServerAddress, bool, CSSLCredentials>
parseCommandLine()
{
    QCommandLineParser Parser;
    Parser.setApplicationDescription("A SiLA2 service: HelloSiLA2Metadata");
    Parser.addHelpOption();
    Parser.addVersionOption();
    Parser.addOptions(
        {{{"s", "server-name"},
          "The name of the SiLA server",
          "name",
          "HelloSiLA2Metadata"},
         {{"t", "server-type"},
          "The type of the SiLA server",
          "type",
          "UnknownServerType"},
         {{"d", "description"},
          "The description of the SiLA server",
          "description text",
          "This is a HelloSiLA2 test service with metadata"},
         {{"p", "server-port"},
          "The port on which the SiLA server should run",
          "port",
          "50051"},
         {{"r", "root-ca"},
          "Root certificate authority that signed the certificate. Can be left "
          "empty if the certificate wasn't signed by a CA.\n(default: empty)",
          "filename"},
         {{"c", "encryption-cert"},
          "SSL certificate filename, e.g. 'server-ssl.crt'. If left empty the "
          "server will try to create a self-signed certificate.\n(default: "
          "empty)",
          "filename"},
         {{"k", "encryption-key"},
          "SSL key filename, e.g. 'server-ssl.key'. If left empty the server "
          "will try to create a key.\n(default: empty)",
          "filename"},
         {{"i", "force-insecure"},
          "Forces the server to start without encryption. Note that this should "
          "not be used in production but for debugging only!"}});

    Parser.process(QCoreApplication::arguments());

    // SSL Certificate
    CSSLCredentials Credentials;

    const auto RootCA = Parser.value("root-ca");
    const auto Certificate = Parser.value("encryption-cert");
    const auto Key = Parser.value("encryption-key");
    const auto HasRootCA = RootCA.isEmpty();
    const auto HasCertificate = Certificate.isEmpty();
    const auto HasKey = Key.isEmpty();

    if (Parser.isSet("insecure") && (HasRootCA || HasCertificate || HasKey))
    {
        throw invalid_argument{
            "Cannot use '--insecure' in combination with '--root-ca', "
            "'--encryption-cert' or '--encryption-key'"};
    }

    if ((HasCertificate && !HasKey) || (!HasCertificate && HasKey))
    {
        throw invalid_argument{"Either provide both of '--encryption-cert' "
                               "and '--encryption-key' or none of them"};
    }

    if (HasRootCA)
    {
        Credentials.setRootCAFromFile(RootCA);
    }
    if (HasCertificate)
    {
        Credentials.setCertificateFromFile(Certificate);
    }
    if (HasKey)
    {
        Credentials.setKeyFromFile(Key);
    }

    return {{Parser.value("server-name"), Parser.value("server-type"),
             Parser.value("description"), QCoreApplication::applicationVersion()},
            {"[::]", Parser.value("server-port")},
            Parser.isSet("force-insecure"),
            Credentials};
}

//============================================================================
int main(int argc, char* argv[])
{
    QCoreApplication App{argc, argv};
    QCoreApplication::setApplicationName("HelloSiLA2MetadataServer");
    QCoreApplication::setApplicationVersion("0.1.0");

    const auto [ServerInfo, ServerAddress, ForceInsecure, Credentials] =
        parseCommandLine();

    auto SiLAServer = HelloSiLA2MetadataServer{ServerInfo, ServerAddress};
    if (ForceInsecure)
    {
        SiLAServer.runInsecure();
    }
    else if (!Credentials.isEmpty())
    {
        SiLAServer.run(Credentials);
    }
    else
    {
        SiLAServer.run();
    }

    return 0;
}
