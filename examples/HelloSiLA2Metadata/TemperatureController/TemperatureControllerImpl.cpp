/**
 ** This file is part of the sila_cpp project.
 ** Copyright 2020 SiLA2
 **
 ** Permission is hereby granted, free of charge, to any person obtaining a copy
 ** of this software and associated documentation files (the "Software"), to deal
 ** in the Software without restriction, including without limitation the rights
 ** to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 ** copies of the Software, and to permit persons to whom the Software is
 ** furnished to do so, subject to the following conditions:
 **
 ** The above copyright notice and this permission notice shall be included in all
 ** copies or substantial portions of the Software.
 **
 ** THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 ** IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 ** FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 ** AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 ** LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 ** OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 ** SOFTWARE.
 **/

//============================================================================
/// \file   TemperatureControllerImpl.cpp
/// \author Florian Meinicke (florian.meinicke@cetoni.de)
/// \date   07.05.2020
/// \brief  Implementation of the CTemperatureControllerImpl class
//============================================================================

//============================================================================
//                                  INCLUDES
//============================================================================
#include "TemperatureControllerImpl.h"

#include <sila_cpp/common/logging.h>
#include <sila_cpp/framework/data_types/SiLAString.h>
#include <sila_cpp/framework/error_handling/ExecutionError.h>
#include <sila_cpp/framework/error_handling/ValidationError.h>

#include <QRandomGenerator>
#include <QThread>

using namespace std;
using namespace sila2::org::silastandard;
using namespace sila2::org::silastandard::examples::temperaturecontroller::v1;

//=============================================================================
CTemperatureControllerImpl::CTemperatureControllerImpl(SiLA2::CSiLAServer* parent)
    : CSiLAFeature{parent},
      m_ControlTempCommand{this, 100 /*sec*/},
      m_CurrentTempProperty{this, 293.0,
                            [this](const SiLA2::CMetadataContainer& Metadata) {
                                qDebug() << "Received Metadata" << Metadata;
                                return m_CurrentTempProperty.value();
                            }},
      // the whole Feature is affected by this metadata
      m_AffectedByTestDataProperty{this,
                                   {{fullyQualifiedIdentifier().toString()}}}
{
    m_ControlTempCommand.setExecutor(
        this, &CTemperatureControllerImpl::ControlTemperature);
}

//============================================================================
ControlTemperature_Responses CTemperatureControllerImpl::ControlTemperature(
    ControlTemperatureWrapper* Command)
{
    const auto Request = Command->parameters();
    qDebug() << "Request contains:" << Request;

    static const SiLA2::CFullyQualifiedCommandParameterID ParamID{
        {fullyQualifiedIdentifier(), "ControlTemperature"}, "TargetTemperature"};
    if (!Request.has_targettemperature())
    {
        throw SiLA2::CValidationError{ParamID,
                                      "ControlTemperature has been called "
                                      "without a TargetTemperature. Set "
                                      "a valid TargetTemperature to start "
                                      "controlling the temperature"};
    }
    if (m_TargetTemp > 0)
    {
        // we're already running - interrupt running Execution before
        // starting a new one
        m_ControlTempCommand.interruptAll();
    }

    try
    {
        const auto Metadata = Command->metadata().value<Metadata_TestData>(
            {fullyQualifiedIdentifier(), "TestData"});
        qDebug() << "Received metadata" << Metadata;
    }
    catch ([[maybe_unused]] const std::out_of_range& err)
    {
        qWarning() << "Didn't receive required 'TestData' metadata";
        using FrameworkErrorType = SiLA2::CFrameworkError::FrameworkErrorType;
        throw SiLA2::CFrameworkError{FrameworkErrorType::InvalidMetadata};
    }

    Command->setValidationSuccessful();

    m_TargetTemp = Request.targettemperature().value();
    // validate parameter constraint
    if (m_TargetTemp <= 277.0 || m_TargetTemp > 363.0)
    {
        throw SiLA2::CValidationError{
            ParamID, "ControlTemperature has been called with an invalid "
                     "TargetTemperature. Specify a TargetTemperature in the "
                     "range 277.0 K < T <= 363.0 K."};
    }

    Command->setValidationSuccessful();

    const auto TotalDiff = m_TargetTemp - m_CurrentTempProperty;
    while (abs(m_TargetTemp - m_CurrentTempProperty) > 1)
    {
        m_CurrentTempProperty += (TotalDiff < 0 ? -1 : 1)
                                 * QRandomGenerator::global()->generateDouble()
                                 * 2;
        qDebug() << "Current temp:" << m_CurrentTempProperty;

        const auto Diff = m_TargetTemp - m_CurrentTempProperty;
        const auto Progress = qBound(0.0, 1 - Diff / TotalDiff, 1.0);
        Command->setExecutionInfo(Progress);
        QThread::msleep(1000);  // simulate long-running command

        if (Command->isInterruptionRequested())
        {
            static const SiLA2::CFullyQualifiedDefinedErrorID ErrorID{
                fullyQualifiedIdentifier(), "ControlInterrupted"};
            // in case we get interrupted notify the client about this
            // with an Error
            throw SiLA2::CDefinedExecutionError{
                ErrorID,
                "The control of temperature could not be finished as it has been "
                "interrupted by another 'Control Temperature' command."};
        }
    }
    // NOTE: a TargetTemp of 0 indicates we're not already running
    //  (because only one Command Execution is allowed)
    m_TargetTemp = 0;
    return {};
}

//============================================================================
SiLA2::CFullyQualifiedFeatureID
CTemperatureControllerImpl::fullyQualifiedIdentifier() const
{
    static const SiLA2::CFullyQualifiedFeatureID FeatureID{
        "org.silastandard", "examples", "TemperatureController", "v1"};
    return FeatureID;
}
