[requires]
sila_cpp/0.0.1@sila2/testing

[generators]
cmake

[imports]
bin, *.dll -> ./bin # Copies all dll files from the package "bin" folder to this project's "bin" folder
