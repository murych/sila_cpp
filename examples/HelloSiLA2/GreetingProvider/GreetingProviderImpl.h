/**
 ** This file is part of the sila_cpp project.
 ** Copyright 2020 SiLA2
 **
 ** Permission is hereby granted, free of charge, to any person obtaining a copy
 ** of this software and associated documentation files (the "Software"), to deal
 ** in the Software without restriction, including without limitation the rights
 ** to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 ** copies of the Software, and to permit persons to whom the Software is
 ** furnished to do so, subject to the following conditions:
 **
 ** The above copyright notice and this permission notice shall be included in all
 ** copies or substantial portions of the Software.
 **
 ** THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 ** IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 ** FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 ** AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 ** LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 ** OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 ** SOFTWARE.
 **/

//============================================================================
/// \file   GreetingProviderImpl.h
/// \author Florian Meinicke (florian.meinicke@cetoni.de)
/// \date   07.01.2020
/// \brief  Declaration of the CGreetingProviderImpl class
//============================================================================
#ifndef GREETINGPROVIDERIMPL_H
#define GREETINGPROVIDERIMPL_H

//============================================================================
//                                  INCLUDES
//============================================================================
#include <sila_cpp/framework/data_types/SiLAInteger.h>
#include <sila_cpp/server/SiLAFeature.h>
#include <sila_cpp/server/command/UnobservableCommand.h>
#include <sila_cpp/server/property/UnobservableProperty.h>

#include "GreetingProvider.grpc.pb.h"

/**
 * @brief The CGreetingProviderImpl class implements the GreetingProvider feature
 */
class CGreetingProviderImpl final :
    public SiLA2::CSiLAFeature<
        sila2::org::silastandard::examples::greetingprovider::v1::GreetingProvider>
{
    // Using declarations for the Feature's Commands and Properties
    using SayHelloCommand =
        SiLA2::CUnobservableCommandManager<&CGreetingProviderImpl::RequestSayHello>;
    using SayHelloWrapper = SiLA2::CUnobservableCommandWrapper<
        sila2::org::silastandard::examples::greetingprovider::v1::SayHello_Parameters,
        sila2::org::silastandard::examples::greetingprovider::v1::
            SayHello_Responses>;
    using StartYearProperty = SiLA2::CUnobservablePropertyWrapper<
        SiLA2::CInteger, &CGreetingProviderImpl::RequestGet_StartYear>;

public:
    /**
     * @brief C'tor
     *
     * @param parent The SiLA server instance that contains this Feature
     */
    explicit CGreetingProviderImpl(SiLA2::CSiLAServer* parent);

    /**
     * @brief Say Hello Command
     *
     * @details Does what it says: returns "Hello SiLA 2 + [Name]" to the client.
     *
     * @param Command The current Say Hello Command Execution Wrapper
     * Contains the @b Name Command Parameter (The name, SayHello shall use to
     * greet.)
     *
     * @return SayHello_Responses The greeting string, returned to the SiLA
     * Client.
     *
     * @throw Validation Error if no Name is given
     */
    sila2::org::silastandard::examples::greetingprovider::v1::SayHello_Responses
    SayHello(SayHelloWrapper* Command);

    /**
     * @override
     * @brief Get the Fully Qualified Feature Identifier of this Feature
     *
     * @return This Feature's Fully Qualified Feature Identifier
     */
    [[nodiscard]] SiLA2::CFullyQualifiedFeatureID fullyQualifiedIdentifier()
        const override;

private:
    SayHelloCommand m_SayHelloCommand;  ///< Manager for the `SayHello` Command
    const StartYearProperty m_StartYearProperty;  ///< The server's start year
};
#endif  // GREETINGPROVIDERIMPL_H
