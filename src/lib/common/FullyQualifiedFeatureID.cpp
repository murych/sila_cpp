/**
 ** This file is part of the sila_cpp project.
 ** Copyright 2020 SiLA2
 **
 ** Permission is hereby granted, free of charge, to any person obtaining a copy
 ** of this software and associated documentation files (the "Software"), to deal
 ** in the Software without restriction, including without limitation the rights
 ** to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 ** copies of the Software, and to permit persons to whom the Software is
 ** furnished to do so, subject to the following conditions:
 **
 ** The above copyright notice and this permission notice shall be included in all
 ** copies or substantial portions of the Software.
 **
 ** THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 ** IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 ** FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 ** AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 ** LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 ** OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 ** SOFTWARE.
 **/

//============================================================================
/// \file   FullyQualifiedFeatureID.cpp
/// \author Florian Meinicke (florian.meinicke@cetoni.de)
/// \date   23.01.2020
/// \brief  Implementation of the CFullyQualifiedFeatureID class
//============================================================================

//============================================================================
//                                  INCLUDES
//============================================================================
#include <sila_cpp/common/FullyQualifiedFeatureID.h>
#include <sila_cpp/common/logging.h>

#include <QStringList>

//============================================================================
static const auto FULLY_QUALIFIED_MESSAGE_NAME_TEMPLATE =
    // 1: originator
    // 2: category
    // 3: feature identifier (lower case)
    // 4: major feature version
    // 5: message name
    QStringLiteral("sila2.%1.%2.%3.%4.%5");

using namespace std;

namespace SiLA2
{
//============================================================================
CFullyQualifiedFeatureID::CFullyQualifiedFeatureID(string_view Originator,
                                                   string_view Category,
                                                   string_view Identifier,
                                                   string_view Version)
    : m_Originator{Originator.data()},
      m_Category{Category.data()},
      m_Identifier{Identifier.data()},
      m_Version{Version.data()}
{}

//============================================================================
CFullyQualifiedFeatureID::CFullyQualifiedFeatureID(QStringView Originator,
                                                   QStringView Category,
                                                   QStringView Identifier,
                                                   QStringView Version)
    : m_Originator{Originator.toString()},
      m_Category{Category.toString()},
      m_Identifier{Identifier.toString()},
      m_Version{Version.toString()}
{}

//============================================================================
CFullyQualifiedFeatureID::~CFullyQualifiedFeatureID() = default;

//============================================================================
CFullyQualifiedFeatureID CFullyQualifiedFeatureID::fromString(const QString& from)
{
    return fromStdString(from.toStdString());
}

//============================================================================
CFullyQualifiedFeatureID CFullyQualifiedFeatureID::fromStdString(
    const string& from)
{
    using PartIndex = FullyQualifiedIdentifierPartIndex;
    const auto List = QString::fromStdString(from).split('/');
    if (List.size() != PartIndex::FullyQualifiedFeatureIdentifierLength)
    {
        qCWarning(sila_cpp_common).nospace()
            << "The given string \"" << from
            << "\" is not in the correct format of a Fully Qualified Feature "
               "Identifier";
        return CFullyQualifiedFeatureID{};
    }
    return CFullyQualifiedFeatureID{
        List.at(+PartIndex::Originator), List.at(+PartIndex::Category),
        List.at(+PartIndex::FeatureIdentifier), List.at(+PartIndex::Version)};
}

//============================================================================
bool CFullyQualifiedFeatureID::operator==(
    const CFullyQualifiedFeatureID& rhs) const
{
    return m_Originator.toLower() == rhs.m_Originator.toLower()
           && m_Category.toLower() == rhs.m_Category.toLower()
           && m_Identifier.toLower() == rhs.m_Identifier.toLower()
           && m_Version.toLower() == rhs.m_Version.toLower();
}

//=============================================================================
QString CFullyQualifiedFeatureID::originator() const
{
    return m_Originator;
}

//=============================================================================
QString CFullyQualifiedFeatureID::category() const
{
    return m_Category;
}

//=============================================================================
QString CFullyQualifiedFeatureID::version() const
{
    return m_Version;
}

//=============================================================================
CFullyQualifiedFeatureID CFullyQualifiedFeatureID::featureIdentifier() const
{
    return *this;
}

//=============================================================================
QString CFullyQualifiedFeatureID::identifier() const
{
    return m_Identifier;
}

//============================================================================
bool CFullyQualifiedFeatureID::isValid() const
{
    return !m_Originator.isEmpty() && !m_Category.isEmpty()
           && !m_Identifier.isEmpty() && !m_Version.isEmpty();
}

//============================================================================
QString CFullyQualifiedFeatureID::toString() const
{
    return QStringList{m_Originator, m_Category, m_Identifier, m_Version}.join(
        "/");
}

//============================================================================
string CFullyQualifiedFeatureID::toStdString() const
{
    return toString().toStdString();
}

//============================================================================
string CFullyQualifiedFeatureID::toServiceName() const
{
    return QStringList{"sila2",    m_Originator,
                       m_Category, m_Identifier.toLower(),
                       m_Version,  m_Identifier}
        .join('.')
        .toStdString();
}

//============================================================================
string CFullyQualifiedFeatureID::toFullyQualifiedMessageName(
    const QString& MessageName) const
{
    return FULLY_QUALIFIED_MESSAGE_NAME_TEMPLATE
        .arg(m_Originator, m_Category, m_Identifier.toLower(), m_Version,
             MessageName)
        .toStdString();
}

//============================================================================
bool operator<(const CFullyQualifiedFeatureID& lhs,
               const CFullyQualifiedFeatureID& rhs)
{
    return lhs.toString().toLower() < rhs.toString().toLower();
}

//============================================================================
QDebug operator<<(QDebug dbg, const CFullyQualifiedFeatureID& rhs)
{
    return dbg << rhs.toStdString();
}

//============================================================================
std::ostream& operator<<(std::ostream& os, const CFullyQualifiedFeatureID& rhs)
{
    return os << rhs.toStdString();
}
}  // namespace SiLA2
