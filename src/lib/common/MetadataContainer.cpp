/*******************************************************************************
 ** This file is part of the sila_cpp project.
 ** Copyright 2022 SiLA 2
 **
 ** Permission is hereby granted, free of charge, to any person obtaining a copy
 ** of this software and associated documentation files (the "Software"), to deal
 ** in the Software without restriction, including without limitation the rights
 ** to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 ** copies of the Software, and to permit persons to whom the Software is
 ** furnished to do so, subject to the following conditions:
 **
 ** The above copyright notice and this permission notice shall be included in all
 ** copies or substantial portions of the Software.
 **
 ** THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 ** IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 ** FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 ** AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 ** LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 ** OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 ** SOFTWARE.
 *****************************************************************************/

//============================================================================
/// \file   MetadataContainer.cpp
/// \author Florian Meinicke (florian.meinicke@cetoni.de)
/// \date   25.01.2022
/// \brief  Implementation of the CMetadataContainer class
//============================================================================

//============================================================================
//                                  INCLUDES
//============================================================================
#include <sila_cpp/common/MetadataContainer.h>
#include <sila_cpp/common/logging.h>

#include <grpcpp/impl/codegen/client_context.h>
#include <grpcpp/impl/codegen/string_ref.h>

#include <map>

using namespace std;
using namespace isocpp_p0201;

namespace SiLA2
{
//============================================================================
CMetadataContainer::CMetadataContainer() = default;

//============================================================================
CMetadataContainer::CMetadataContainer(
    map<CFullyQualifiedMetadataID, string> Metadata)
    : map<CFullyQualifiedMetadataID, string>{std::move(Metadata)}
{}

//============================================================================
CMetadataContainer::~CMetadataContainer() = default;

//============================================================================
CMetadataContainer CMetadataContainer::fromRawMetadata(
    const grpc_metadata_container_t& RawMetadata)
{
    CMetadataContainer Metadata;
    std::for_each(
        std::begin(RawMetadata), std::end(RawMetadata),
        [&Metadata](const auto& Metadatum) {
            const auto Key = QString::fromLocal8Bit(
                Metadatum.first.data(),
                static_cast<int>(Metadatum.first.length()));
            const auto Value =
                std::string{Metadatum.second.data(), Metadatum.second.length()};
            qCDebug(sila_cpp_server) << "current key" << Key << "value" << Value;
            if (!Key.startsWith("sila-") || !Key.endsWith("-bin"))
            {
                return;
            }
            const auto ID = CFullyQualifiedMetadataID::fromHeaderName(Key);
            const auto Inserted = Metadata.emplace(ID, Value).second;
            if (!Inserted)
            {
                qCWarning(sila_cpp_server).nospace()
                    << "Received multiple values for " << ID
                    << "! Only the first value will be available.";
            }
        });
    return Metadata;
}

//============================================================================
CMetadataContainer::CMetadataContainer(
    std::initializer_list<
        std::pair<CFullyQualifiedMetadataID, const google::protobuf::Message&>>
        Metadata)
{
    transform(::cbegin(Metadata), ::cend(Metadata), inserter(*this, begin()),
              [](const auto& Pair) {
                  return pair{Pair.first, Pair.second.SerializeAsString()};
              });
}

//============================================================================
void CMetadataContainer::addToContext(grpc::ClientContext* Context) const
{
    for_each(cbegin(), cend(), [&Context](const auto& Pair) {
        Context->AddMetadata(Pair.first.toHeaderName(), Pair.second);
    });
}

//============================================================================
ostream& operator<<(ostream& os, const CMetadataContainer& rhs)
{
    os << "CMetadataContainer(";
    auto it = cbegin(rhs);
    const auto end = cend(rhs);
    if (it != end)
    {
        os << '(' << it->first.toStdString() << it->second << ')';
        ++it;
    }
    while (it != end)
    {
        os << ", (" << it->first.toStdString() << it->second << ')';
        ++it;
    }
    return os << ')';
}
}  // namespace SiLA2
