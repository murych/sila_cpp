/**
 ** This file is part of the sila_cpp project.
 ** Copyright 2020 SiLA2
 **
 ** Permission is hereby granted, free of charge, to any person obtaining a copy
 ** of this software and associated documentation files (the "Software"), to deal
 ** in the Software without restriction, including without limitation the rights
 ** to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 ** copies of the Software, and to permit persons to whom the Software is
 ** furnished to do so, subject to the following conditions:
 **
 ** The above copyright notice and this permission notice shall be included in all
 ** copies or substantial portions of the Software.
 **
 ** THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 ** IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 ** FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 ** AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 ** LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 ** OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 ** SOFTWARE.
 **/

//============================================================================
/// \file   logging.cpp
/// \author Florian Meinicke (florian.meinicke@cetoni.de)
/// \date   09.01.2020
/// \brief  Implementation of helper functions for logging
//============================================================================

//============================================================================
//                                  INCLUDES
//============================================================================
#include <sila_cpp/common/logging.h>

#include <QProcessEnvironment>
#include <QThread>

#include <iostream>
#ifdef Q_OS_WIN
#    include <windows.h>
#    ifndef ENABLE_VIRTUAL_TERMINAL_PROCESSING
#        define ENABLE_VIRTUAL_TERMINAL_PROCESSING 0x0004
#    endif
#    ifndef DISABLE_NEWLINE_AUTO_RETURN
#        define DISABLE_NEWLINE_AUTO_RETURN 0x0008
#    endif
#endif  // Q_OS_WIN

#include <QMutex>
#include <QTime>

#include <google/protobuf/message.h>
#include <grpcpp/impl/codegen/status.h>
#include <grpcpp/impl/codegen/string_ref.h>

Q_LOGGING_CATEGORY(sila_cpp, "sila_cpp")
Q_LOGGING_CATEGORY(sila_cpp_client, "sila_cpp.client")
Q_LOGGING_CATEGORY(sila_cpp_heartbeat, "sila_cpp.client.heartbeat")
Q_LOGGING_CATEGORY(sila_cpp_codegen, "sila_cpp.codegen")
Q_LOGGING_CATEGORY(sila_cpp_common, "sila_cpp.common")
Q_LOGGING_CATEGORY(sila_cpp_discovery, "sila_cpp.discovery")
Q_LOGGING_CATEGORY(sila_cpp_constraints, "sila_cpp.framework.constraints")
Q_LOGGING_CATEGORY(sila_cpp_data_types, "sila_cpp.framework.data_types")
Q_LOGGING_CATEGORY(sila_cpp_errors, "sila_cpp.framework.errors")
Q_LOGGING_CATEGORY(sila_cpp_internal, "sila_cpp.internal")
Q_LOGGING_CATEGORY(sila_cpp_server, "sila_cpp.server")

using namespace std;

namespace SiLA2::logging
{
//============================================================================
static const QMap<QString, CLogManager::LoggingLevel> LOGGING_LEVELS = {
    {"debug", CLogManager::LoggingLevel::Debug},
    {"info", CLogManager::LoggingLevel::Info},
    {"warning", CLogManager::LoggingLevel::Warning},
    {"critical", CLogManager::LoggingLevel::Critical},
    {"fatal", CLogManager::LoggingLevel::Fatal}};

//============================================================================
static QMap<QString, CLogManager::LoggingLevel> CATEGORY_LOGGING_LEVELS = {
    {"sila_cpp", CLogManager::LoggingLevel::Info},
    {"sila_cpp.client", CLogManager::LoggingLevel::Info},
    {"sila_cpp.client.heartbeat", CLogManager::LoggingLevel::Info},
    {"sila_cpp.codegen", CLogManager::LoggingLevel::Info},
    {"sila_cpp.common", CLogManager::LoggingLevel::Info},
    {"sila_cpp.discovery", CLogManager::LoggingLevel::Info},
    {"sila_cpp.framework.data_types", CLogManager::LoggingLevel::Info},
    {"sila_cpp.framework.errors", CLogManager::LoggingLevel::Info},
    {"sila_cpp.internal", CLogManager::LoggingLevel::Info},
    {"sila_cpp.server", CLogManager::LoggingLevel::Info}};

#ifdef Q_OS_WIN
/**
 * @brief Enable the virtual terminal (VT) output mode of the Windows console
 * window
 * @todo[FM] Better react on errors
 */
void enableWinConsoleVTMode()
{
    for (const auto Handle : vector{STD_OUTPUT_HANDLE, STD_ERROR_HANDLE})
    {
        auto* const hOut = GetStdHandle(Handle);
        if (hOut == INVALID_HANDLE_VALUE)
        {
            return;
        }

        DWORD dwOriginalOutMode = 0;
        if (GetConsoleMode(hOut, &dwOriginalOutMode) == 0)
        {
            return;
        }

        DWORD dwRequestedOutModes = ENABLE_VIRTUAL_TERMINAL_PROCESSING
                                    | DISABLE_NEWLINE_AUTO_RETURN;

        if (auto dwOutMode = dwOriginalOutMode | dwRequestedOutModes;
            SetConsoleMode(hOut, dwOutMode) == 0)
        {
            // we failed to set both modes, try to step down mode gracefully.
            dwRequestedOutModes = ENABLE_VIRTUAL_TERMINAL_PROCESSING;
            dwOutMode = dwOriginalOutMode | dwRequestedOutModes;
            if (SetConsoleMode(hOut, dwOutMode) == 0)
            {
                // Failed to set any VT mode, can't do anything here.
                return;
            }
        }
    }
}
#endif  // Q_OS_WIN

/**
 * @brief ANSI color codes
 */
[[maybe_unused]] static const char* const BLACK{"\033[31m"};
[[maybe_unused]] static const char* const RED{"\033[31m"};
[[maybe_unused]] static const char* const GREEN{"\033[32m"};
[[maybe_unused]] static const char* const YELLOW{"\033[33m"};
[[maybe_unused]] static const char* const BLUE{"\033[34m"};
[[maybe_unused]] static const char* const MAGENTA{"\033[35m"};
[[maybe_unused]] static const char* const CYAN{"\033[36m"};
[[maybe_unused]] static const char* const WHITE{"\033[37m"};
[[maybe_unused]] static const char* const DEFAULT{"\033[39m"};
[[maybe_unused]] static const char* const GREY{"\033[90m"};

/**
 * @brief ANSI formatting codes
 */
[[maybe_unused]] static const char* const RESET{"\033[0m"};
[[maybe_unused]] static const char* const BOLD{"\033[1m"};
[[maybe_unused]] static const char* const DIM{"\033[2m"};
[[maybe_unused]] static const char* const UNDERLINE{"\033[4m"};
[[maybe_unused]] static const char* const BLINK{"\033[5m"};
[[maybe_unused]] static const char* const REVERSE{"\033[7m"};
[[maybe_unused]] static const char* const HIDDEN{"\033[8m"};

/**
 * @brief A custom message handler for formatting console output.
 */
void messageHandler(QtMsgType Type, const QMessageLogContext& Context,
                    const QString& Msg)
{
    static QMutex Mutex;
    QMutexLocker Lock{&Mutex};

    // Since we actually only want to format our own sila_cpp internal log
    // messages, we delegate everything else from other categories to the previous
    // message handler (see https://gitlab.com/SiLA2/sila_cpp/-/issues/40).
    if (!CATEGORY_LOGGING_LEVELS.contains(Context.category))
    {
        if (const auto previousMessageHandler =
                CLogManager::instance()->previousMessageHandler();
            Q_LIKELY(previousMessageHandler))
        {
            previousMessageHandler(Type, Context, Msg);
            return;
        }
    }

    const auto Time = QTime::currentTime().toString(Qt::ISODateWithMs);

    switch (Type)
    {
    case QtDebugMsg:
        cout << GREY << BOLD << '(' << QThread::currentThread() << ") "
             << Time.toStdString() << ": [" << Context.category << "] "
             << "DEBUG: " << Msg.toStdString() << "\n"
             << DIM << '(' << Context.file << ':' << Context.line << ")\n";
        break;

    case QtInfoMsg:
        cout << BOLD << '(' << QThread::currentThread() << ") "
             << Time.toStdString() << ": [" << Context.category << "] "
             << "INFO: " << Msg.toStdString() << "\n"
             << DIM << '(' << Context.file << ':' << Context.line << ")\n";
        break;

    case QtWarningMsg:
        cerr << YELLOW << BOLD << '(' << QThread::currentThread() << ") "
             << Time.toStdString() << ": [" << Context.category << "] "
             << "WARNING: " << Msg.toStdString() << "\n"
             << DIM << '(' << Context.file << ':' << Context.line << ")\n";
        break;

    case QtCriticalMsg:
        cerr << RED << BOLD << '(' << QThread::currentThread() << ") "
             << Time.toStdString() << ": [" << Context.category << "] "
             << "CRITICAL: " << Msg.toStdString() << "\n"
             << DIM << '(' << Context.file << ':' << Context.line << ")\n";
        break;

    case QtFatalMsg:
        cerr << RED << BOLD << '(' << QThread::currentThread() << ") "
             << Time.toStdString() << ": [" << Context.category << "] "
             << "FATAL: " << Msg.toStdString() << "\n"
             << DIM << '(' << Context.file << ':' << Context.line << ")\n"
             << RESET << flush;
        abort();
    }

    cerr << RESET << flush;
    cout << RESET << flush;
}

/**
 * @brief Filter function used to determine which categories and message types
 * should be enabled.
 *
 * @param category The logging category to configure
 */
void loggingCategoryFilter(QLoggingCategory* category)
{
    using LoggingLevel = CLogManager::LoggingLevel;
    const auto it = CATEGORY_LOGGING_LEVELS.find(category->categoryName());

    if (it == CATEGORY_LOGGING_LEVELS.end())
    {
        if (const auto previousFilter = CLogManager::instance()->previousFilter();
            Q_LIKELY(previousFilter))
        {
            previousFilter(category);
        }
        return;
    }

    for (auto i = LoggingLevel::Debug; i < LoggingLevel::Fatal; ++i)
    {
        category->setEnabled(CLogManager::loggingLevelToQtMsgType(i),
                             it.value() <= i);
    }
}

/**
 * @brief Pre-increment operator for @c CLogManager::LoggingLevel
 */
CLogManager::LoggingLevel operator++(CLogManager::LoggingLevel& level)
{
    return level = static_cast<CLogManager::LoggingLevel>(
               static_cast<underlying_type_t<CLogManager::LoggingLevel>>(level)
               + 1);
}

//============================================================================
std::shared_ptr<CLogManager> CLogManager::m_Instance{};

//============================================================================
void CLogManager::initialize()
{
#ifdef Q_OS_WIN
    enableWinConsoleVTMode();
#endif  // Q_OS_WIN

    installMessageHandler(messageHandler);
    m_PreviousFilter = QLoggingCategory::installFilter(loggingCategoryFilter);
    for_each(CATEGORY_LOGGING_LEVELS.keyBegin(), CATEGORY_LOGGING_LEVELS.keyEnd(),
             [](const auto& CategoryName) { setLoggingLevel(CategoryName); });
}

//============================================================================
std::shared_ptr<CLogManager> CLogManager::instance() noexcept
{
    if (Q_UNLIKELY(!m_Instance))
    {
        m_Instance.reset(new CLogManager);
        m_Instance->initialize();
    }
    return m_Instance;
}

//============================================================================
CLogManager::~CLogManager()
{
    qCDebug(sila_cpp)
        << "CLogManager destroyed. Installing previous message handler";
    qInstallMessageHandler(m_PreviousMessageHandler);
}

//============================================================================
QtMessageHandler CLogManager::installMessageHandler(QtMessageHandler CustomHandler)
{
    const auto PreviousHandler =
        qInstallMessageHandler(CustomHandler ? CustomHandler : messageHandler);
    if (PreviousHandler != messageHandler)
    {
        m_PreviousMessageHandler = PreviousHandler;
    }

    return PreviousHandler;
}

//============================================================================
void CLogManager::setLoggingLevel(LoggingLevel DefaultLevel)
{
    const auto Level = qEnvironmentVariable("SILA_CPP_LOGGING_LEVEL").toLower();

    for_each(begin(CATEGORY_LOGGING_LEVELS), end(CATEGORY_LOGGING_LEVELS),
             [&Level, DefaultLevel](auto& CategoryLevel) {
                 CategoryLevel = LOGGING_LEVELS.value(Level, DefaultLevel);
             });
    // re-apply the filter rules
    QLoggingCategory::installFilter(loggingCategoryFilter);
}

//============================================================================
void CLogManager::setLoggingLevel(const QString& CategoryName,
                                  LoggingLevel DefaultLevel)
{
    DefaultLevel = LOGGING_LEVELS.value(
        qEnvironmentVariable("SILA_CPP_LOGGING_LEVEL").toLower(), DefaultLevel);

    const auto EnvVarCatName =
        CategoryName.section('.', 1).toUpper().replace('.', '_');
    const auto EnvVarName =
        QStringLiteral("SILA_CPP_%1_LOGGING_LEVEL").arg(EnvVarCatName);
    const auto Level =
        qEnvironmentVariable(EnvVarName.toLatin1().constData()).toLower();

    CATEGORY_LOGGING_LEVELS[CategoryName] =
        LOGGING_LEVELS.value(Level, DefaultLevel);
    // re-apply the filter rules
    QLoggingCategory::installFilter(loggingCategoryFilter);
}

//============================================================================
QtMsgType CLogManager::loggingLevelToQtMsgType(LoggingLevel level)
{
    switch (level)
    {
    case LoggingLevel::Debug:
        return QtDebugMsg;
    case LoggingLevel::Info:
        return QtInfoMsg;
    case LoggingLevel::Warning:
        return QtWarningMsg;
    case LoggingLevel::Critical:
        return QtCriticalMsg;
    case LoggingLevel::Fatal:
        return QtFatalMsg;
    }

    qCCritical(sila_cpp_common)
        << "Unhandled LoggingLevel" << static_cast<int>(level);
    return QtInfoMsg;
}
}  // namespace SiLA2::logging

//=============================================================================
QDebug operator<<(QDebug dbg, const std::string& rhs)
{
    if (rhs.empty())
    {
        return dbg;
    }
    return dbg << rhs.c_str();
}

//=============================================================================
QDebug operator<<(QDebug dbg, const grpc::StatusCode& rhs)
{
    switch (rhs)
    {
    case grpc::StatusCode::OK:
        return dbg << "OK";
    case grpc::StatusCode::CANCELLED:
        return dbg << "CANCELLED";
    case grpc::StatusCode::UNKNOWN:
        return dbg << "UNKNOWN";
    case grpc::StatusCode::INVALID_ARGUMENT:
        return dbg << "INVALID_ARGUMENT";
    case grpc::StatusCode::DEADLINE_EXCEEDED:
        return dbg << "DEADLINE_EXCEEDED";
    case grpc::StatusCode::NOT_FOUND:
        return dbg << "NOT_FOUND";
    case grpc::StatusCode::ALREADY_EXISTS:
        return dbg << "ALREADY_EXISTS";
    case grpc::StatusCode::PERMISSION_DENIED:
        return dbg << "PERMISSION_DENIED";
    case grpc::StatusCode::UNAUTHENTICATED:
        return dbg << "UNAUTHENTICATED";
    case grpc::StatusCode::RESOURCE_EXHAUSTED:
        return dbg << "RESOURCE_EXHAUSTED";
    case grpc::StatusCode::FAILED_PRECONDITION:
        return dbg << "FAILED_PRECONDITION";
    case grpc::StatusCode::ABORTED:
        return dbg << "ABORTED";
    case grpc::StatusCode::OUT_OF_RANGE:
        return dbg << "OUT_OF_RANGE";
    case grpc::StatusCode::UNIMPLEMENTED:
        return dbg << "UNIMPLEMENTED";
    case grpc::StatusCode::INTERNAL:
        return dbg << "INTERNAL";
    case grpc::StatusCode::UNAVAILABLE:
        return dbg << "UNAVAILABLE";
    case grpc::StatusCode::DATA_LOSS:
        return dbg << "DATA_LOSS";
    case grpc::StatusCode::DO_NOT_USE:
        return dbg << "DO_NOT_USE";
    }

    qCCritical(sila_cpp_common)
        << "Unhandled grpc::StatusCode" << static_cast<int>(rhs);
    return dbg;
}

//=============================================================================
QDebug operator<<(QDebug dbg, const grpc::Status& rhs)
{
    return dbg << "\n\tStatus Code:" << rhs.error_code()
               << "\n\tError Message:" << rhs.error_message();
}

//=============================================================================
QDebug operator<<(QDebug dbg, const google::protobuf::Message& rhs)
{
    auto s = QDebugStateSaver{dbg};
    dbg.nospace() << '\n' << '\t' << rhs.GetDescriptor()->name() << " {\n\t  ";
    auto MsgString =
        QString::fromStdString(rhs.DebugString()).replace('\n', "\n\t  ");
    return dbg.noquote() << MsgString.remove(MsgString.size() - 2, 2) << '}';
}

//=============================================================================
QDebug operator<<(QDebug dbg, const google::protobuf::Message* rhs)
{
    return rhs ? (dbg << *rhs) : dbg;
}

//============================================================================
QDebug operator<<(QDebug dbg, const grpc::string_ref& rhs)
{
    return dbg << std::string{rhs.data(), rhs.length()};
}

//============================================================================
std::ostream& operator<<(std::ostream& os, const QString& rhs)
{
    return os << qPrintable(rhs);
}

//=============================================================================
std::ostream& operator<<(std::ostream& os, const google::protobuf::Message& rhs)
{
    os << '\n' << '\t' << rhs.GetDescriptor()->name() << " {\n\t  ";
    auto MsgString =
        QString::fromStdString(rhs.DebugString()).replace('\n', "\n\t  ");

    return os << MsgString.remove(MsgString.size() - 2, 2) << '}';
}

//=============================================================================
std::ostream& operator<<(std::ostream& os, const google::protobuf::Message* rhs)
{
    return os << *rhs;
}

//============================================================================
std::ostream& operator<<(std::ostream& os, const grpc::string_ref& rhs)
{
    return os << std::string_view{rhs.data(), rhs.length()};
}
