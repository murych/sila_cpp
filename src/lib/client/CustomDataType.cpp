/**
 ** This file is part of the sila_cpp project.
 ** Copyright (c) 2021 SiLA2
 **
 ** Permission is hereby granted, free of charge, to any person obtaining a copy
 ** of this software and associated documentation files (the "Software"), to deal
 ** in the Software without restriction, including without limitation the rights
 ** to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 ** copies of the Software, and to permit persons to whom the Software is
 ** furnished to do so, subject to the following conditions:
 **
 ** The above copyright notice and this permission notice shall be included in all
 ** copies or substantial portions of the Software.
 **
 ** THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 ** IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 ** FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 ** AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 ** LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 ** OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 ** SOFTWARE.
 **/

//============================================================================
/// \file   CustomDataType.cpp
/// \author Florian Meinicke (florian.meinicke@cetoni.de)
/// \date   01.03.2021
/// \brief  Implementation of the CCustomDataType class
//============================================================================

//============================================================================
//                                  INCLUDES
//============================================================================
#include <sila_cpp/client/CustomDataType.h>
#include <sila_cpp/client/DynamicStructure.h>
#include <sila_cpp/codegen/fdl/DataTypeDefinition.h>
#include <sila_cpp/codegen/fdl/DataTypeIdentifier.h>

#include "DynamicValue_p.h"

#include <utility>

using namespace std;
using namespace isocpp_p0201;
using namespace SiLA2::codegen;

namespace SiLA2
{
/**
 * @brief Private data of the CCustomDataType class - pimpl
 */
class CCustomDataType::PrivateImpl : public CDynamicValue::PrivateImpl
{
public:
    /**
     * @brief C'tor
     */
    PrivateImpl(CCustomDataType* parent, CFullyQualifiedDataTypeID ID,
                const fdl::CDataTypeDefinition& DataTypeFDL,
                shared_ptr<proto::CDynamicMessageFactory> DMF,
                CDynamicFeatureStub* FeatureStub);

    const CFullyQualifiedDataTypeID Identifier{{"", "", "", ""}, ""};
    CDynamicFeatureStub* Feature;
};

//============================================================================
CCustomDataType::PrivateImpl::PrivateImpl(
    CCustomDataType* parent, CFullyQualifiedDataTypeID ID,
    const fdl::CDataTypeDefinition& DataTypeFDL,
    shared_ptr<proto::CDynamicMessageFactory> DMF,
    CDynamicFeatureStub* FeatureStub)
    : CDynamicValue::PrivateImpl{parent, DataTypeFDL.dataType(), std::move(DMF)},
      Identifier{std::move(ID)},
      Feature{FeatureStub}
{}

///===========================================================================
CCustomDataType::CCustomDataType(
    CFullyQualifiedDataTypeID Identifier,
    const fdl::CDataTypeDefinition& DataTypeFDL,
    [[maybe_unused]] const shared_ptr<grpc::Channel>& Channel,
    shared_ptr<proto::CDynamicMessageFactory> DMF, CDynamicFeatureStub* Feature)
    : CDynamicValue{
        make_polymorphic_value<CDynamicValue::PrivateImpl, PrivateImpl>(
            this, std::move(Identifier), DataTypeFDL, std::move(DMF), Feature)}
{}

//============================================================================
CFullyQualifiedDataTypeID CCustomDataType::identifier() const
{
    PIMPL_D(const CCustomDataType);
    return d->Identifier;
}

//============================================================================
CDynamicFeatureStub* CCustomDataType::feature() const
{
    PIMPL_D(const CCustomDataType);
    return d->Feature;
}

//============================================================================
google::protobuf::Message* CCustomDataType::toProtoMessagePtr() const
{
    PIMPL_D(const CCustomDataType);
    return d->toProtoMessagePtr(d->Identifier.messageName(),
                                d->Identifier.identifier().toStdString());
}

//============================================================================
CDynamicStructure CCustomDataType::structure() const
{
    PIMPL_D(const CCustomDataType);

    if (const auto DataTypeFDL = d->getUnderlyingDataType();
        DataTypeFDL.type() == fdl::IDataType::Type::Structure)
    {
        return CDynamicStructure{DataTypeFDL.structure(),
                                 d->Identifier.messageName(),
                                 d->Identifier.identifier().toStdString(),
                                 d->DynamicMessageFactory, d->Feature};
    }
    qCDebug(sila_cpp_client) << "Underlying Data Type is not a Structure!";
    return {};
}

//============================================================================
void CCustomDataType::fillWithDefaultValue()
{
    PIMPL_D(CCustomDataType);
    PrivateImpl::setEmptyNestedValue(d->Feature, *this);
}

//============================================================================
QDebug operator<<(QDebug dbg, const CCustomDataType& rhs)
{
    QDebugStateSaver s{dbg};
    dbg.nospace() << "CCustomDataType(" << rhs.identifier() << '=';
    dbg << dynamic_cast<const CDynamicValue&>(rhs);
    return dbg << ')';
}

//============================================================================
ostream& operator<<(ostream& os, const CCustomDataType& rhs)
{
    os << "CCustomDataType(" << rhs.identifier() << '=';
    os << dynamic_cast<const CDynamicValue&>(rhs);
    return os << ')';
}
}  // namespace SiLA2
