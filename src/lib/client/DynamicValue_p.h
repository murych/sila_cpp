/**
 ** This file is part of the sila_cpp project.
 ** Copyright (c) 2021 SiLA2
 **
 ** Permission is hereby granted, free of charge, to any person obtaining a copy
 ** of this software and associated documentation files (the "Software"), to deal
 ** in the Software without restriction, including without limitation the rights
 ** to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 ** copies of the Software, and to permit persons to whom the Software is
 ** furnished to do so, subject to the following conditions:
 **
 ** The above copyright notice and this permission notice shall be included in all
 ** copies or substantial portions of the Software.
 **
 ** THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 ** IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 ** FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 ** AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 ** LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 ** OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 ** SOFTWARE.
 **/

//============================================================================
/// \file   DynamicValue_p.h
/// \author Florian Meinicke (florian.meinicke@cetoni.de)
/// \date   02.03.2021
/// \brief  Declaration of the CDynamicValue::PrivateImpl class
//============================================================================
#ifndef DYNAMICVALUE_P_H
#define DYNAMICVALUE_P_H

//============================================================================
//                                  INCLUDES
//============================================================================
#include <sila_cpp/client/DynamicValue.h>
#include <sila_cpp/codegen/fdl/SiLAElement.h>

namespace SiLA2
{
/**
 * @brief Private data of the CDynamicValue class - pimpl
 */
class CDynamicValue::PrivateImpl
{
public:
    /**
     * @brief C'tor
     */
    explicit PrivateImpl(
        CDynamicValue* parent, codegen::fdl::CDataType DataTypeFDL = {},
        std::shared_ptr<codegen::proto::CDynamicMessageFactory> DMF = nullptr);

    /**
     * @brief Copy c'tor
     */
    PrivateImpl(const PrivateImpl& rhs);

    /**
     * @brief Move c'tor
     */
    PrivateImpl(PrivateImpl&& rhs) noexcept = default;

    /**
     * @brief Copy assignment operator
     */
    PrivateImpl& operator=(const PrivateImpl& rhs);

    /**
     * @brief Move assignment operator
     */
    PrivateImpl& operator=(PrivateImpl&& rhs) noexcept = default;

    /**
     * @brief D'tor
     */
    virtual ~PrivateImpl() = default;

    /**
     * @brief Convert the @a Value to a list of a SiLA Framework Type (e.g.
     * String, Integer, ...)
     *
     * @tparam T The desired SiLA Framework Type
     * @return A list of the specified type @a T
     */
    template<typename T>
    std::vector<T> toList() const;

    /**
     * @brief Checks if the given @a DataType is a Basic SiLA Type and if
     * so if it has the given @a Identifier (i.e. if @a Identifier is @a "String"
     * this function checks if this Value is of Basic Type String). If no
     * @a Identifier is given then this function only checks if the @a DataType is
     * a Basic SiLA Type
     *
     * @param DataType The Data Type to check
     * @param Identifier (optional) The Basic Type Identifier to check for
     * @returns true, if this Value is of Basic Type with the given @a Identifier
     * @returns false, otherwise
     */
    [[nodiscard]] static bool isBasic(const codegen::fdl::CDataType& DataType,
                                      const QString& Identifier = "");

    /**
     * @brief Checks if the given @a DataType is a Constrained SiLA Type
     *
     * @param DataType The Data Type to check
     * @returns true, if this Value is of Constrained Type
     * @returns false, otherwise
     */
    [[nodiscard]] static bool isConstrained(
        const codegen::fdl::CDataType& DataType);

    /**
     * @brief Checks if the given @a DataType is a List SiLA Type
     *
     * @param DataType The Data Type to check
     * @returns true, if this Value is of List Type
     * @returns false, otherwise
     */
    [[nodiscard]] static bool isList(const codegen::fdl::CDataType& DataType);

    /**
     * @brief Checks if the given @a DataType's underlying type is a SiLA Basic
     * Type with the given @a Identifier (i.e. the @a DataType might simply be a
     * Basic Type already or it might be a List of a Basic Type or a Constrained
     * Basic Type)
     *
     * @param DataType The Data Type to check
     * @param Identifier The Basic Type Identifier to check for
     * @returns true, if this Value has a Basic underlying type with the given
     * @a Identifier
     * @returns false, otherwise
     */
    [[nodiscard]] static bool isUnderlyingBasic(
        const codegen::fdl::CDataType& DataType, const QString& Identifier);

    /**
     * @brief Get the underlying SiLA Basic type of the given @a DataType
     *
     * @param DataType The Data Type of which to get the underlying Basic Type
     *
     * @return The underlying Basic Data Type if @a DataType has one or a default
     * constructed @c CDataType otherwise
     */
    [[nodiscard]] static codegen::fdl::CDataType getUnderlyingBasicType(
        const codegen::fdl::CDataType& DataType);

    /**
     * @brief Get the underlying Data Type of the @a DataType of this Value
     *
     * @return The underlying Data Type of this value
     */
    [[nodiscard]] codegen::fdl::CDataType getUnderlyingDataType() const;

    /**
     * @brief Copies all data members from @a from
     *
     * @param from The object to copy from
     */
    void copyFrom(const PrivateImpl& from);

    /**
     * @brief Sets an empty (default) value for @a Value if it is a
     * @c CCustomDataType or a @c CDynamicStructure
     *
     * @param Feature The @c CDynamicFeatureStub to use for resolving Data Type
     * Definitions
     * @param Value The Dynamic Value to set to an empty (default) value
     */
    static void setEmptyNestedValue(const CDynamicFeatureStub* Feature,
                                    CDynamicValue& Value);

    /**
     * @brief Returns if the given protobuf @c Message @a Value has the expected
     * field type of the @a Field named @a FieldName
     *
     * @param Value The Value (@c Message) to check for the field type
     * @param Field The field having the correct (expected) type
     * @param FieldName The name of the Field (only used for logging a message in
     * case the types don't match)
     * @returns @c true of the field types match, @c false otherwise
     */
    [[nodiscard]] static bool valueHasFieldType(
        const std::unique_ptr<google::protobuf::Message>& Value,
        const google::protobuf::FieldDescriptor* Field,
        const std::string& FieldName);

    /**
     * @brief Convert this Dynamic Value into its corresponding protobuf
     * @c Message
     *
     * @param MessageName The name of the protobuf @c Message to return
     * @param FieldName The name of the field to set in the @c Message to be
     * returned
     * @return A pointer to the protobuf @c Message for this Value
     */
    [[nodiscard]] google::protobuf::Message* toProtoMessagePtr(
        const std::string& MessageName, const std::string& FieldName) const;

    /**
     * @brief Constructs the parsed Data Type FDL description to be used for
     * Dynamic Structures from the given @a ElementsMap
     *
     * @param ElementsMap A @c QVariantMap with multiple "Element" keys (i.e.
     * technically a @c QMultiMap) that describe the Data Type of the Structure
     * Elements
     * @param MightHaveNestedStruct Whether an Element might have a nested
     * Structure as its Data Type
     * @return The parsed Data Type FDL description for a Structure with the
     * Elements from @a ElementsMap
     */
    [[nodiscard]] static codegen::fdl::CDataType makeStructDataTypeFDL(
        QMultiMap<QString, QVariant> ElementsMap,
        bool MightHaveNestedStruct = false);

    /**
     * @brief Constructs the pretty string representation for the given @c Message
     * @a Val using protobuf Reflection
     *
     * @param Val The protobuf @c Message to get the pretty string for
     * @return The pretty string for @a Val
     */
    [[nodiscard]] static std::string prettyString(google::protobuf::Message* Val);

    [[nodiscard]] static QString prettyString(
        const std::unique_ptr<google::protobuf::Message>& Val)
    {
        return QString::fromStdString(prettyString(Val.get()));
    }

    std::unique_ptr<google::protobuf::Message> Value{nullptr};
    std::vector<std::unique_ptr<google::protobuf::Message>> Values{};
    codegen::fdl::CDataType DataType;
    DataTypeType DataTypeType_{DataTypeType::Unknown};
    static const QHash<QString, CDynamicValue::BasicTypeType>
        StringToBasicTypeType;
    std::shared_ptr<codegen::proto::CDynamicMessageFactory> DynamicMessageFactory;

    CDynamicValue* q_ptr{nullptr};

    PIMPL_DECLARE_PUBLIC(CDynamicValue)
};

//============================================================================
template<typename T>
std::vector<T> CDynamicValue::PrivateImpl::toList() const
{
    std::vector<T> Result;
    Result.reserve(Values.size());
    std::transform(
        std::begin(Values), std::end(Values), std::back_inserter(Result),
        [](const auto& EmbMsg) { return T::fromProtoMessage(*EmbMsg); });
    return Result;
}
}  // namespace SiLA2

#endif  // DYNAMICVALUE_P_H
