/*******************************************************************************
 ** This file is part of the sila_cpp project.
 ** Copyright 2021 SiLA 2
 **
 ** Permission is hereby granted, free of charge, to any person obtaining a copy
 ** of this software and associated documentation files (the "Software"), to deal
 ** in the Software without restriction, including without limitation the rights
 ** to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 ** copies of the Software, and to permit persons to whom the Software is
 ** furnished to do so, subject to the following conditions:
 **
 ** The above copyright notice and this permission notice shall be included in all
 ** copies or substantial portions of the Software.
 **
 ** THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 ** IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 ** FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 ** AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 ** LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 ** OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 ** SOFTWARE.
 *****************************************************************************/

//============================================================================
/// \file   HeartbeatThread.h
/// \author Florian Meinicke (florian.meinicke@cetoni.de)
/// \date   26.10.2021
/// \brief  Declaration of the CHeartbeatThread class
//============================================================================
#ifndef HEARTBEATTHREAD_H
#define HEARTBEATTHREAD_H

//============================================================================
//                                  INCLUDES
//============================================================================
#include <QThread>

#include <memory>

namespace SiLA2
{
class CSiLAClient;

/**
 * @brief The @c CHeartbeatThread class represents a thread that monitors a
 * connection of a SiLA Client to a Server. When the thread is started
 * (@c start()) it will periodically check the @c grpc::Channel of the Client and
 * also request the @b ServerUUID Property of the Server to be sure that the
 * Server is still available on the network. The thread will exit and emit its
 * @c finished() signal when
 * @li the @c Channel's state indicates that the communication is about to be
 * shutdown or
 * @li the @b ServerUUID cannot be retrieved or
 * @li the @c CSiLAClient instance that is monitored has been deleted (e.g.
 * because the user explicitly told the Server Manager to disconnect from the
 * Server)
 */
class CHeartbeatThread : public QThread
{
    Q_OBJECT
public:
    /**
     * @brief Constructor
     *
     * @param Client The @c CSiLAClient that should be monitored by this heartbeat
     * thread
     */
    explicit CHeartbeatThread(const std::shared_ptr<CSiLAClient>& Client);

public slots:

    /**
     * @brief Performs one heartbeat
     *
     * If the heartbeat is not successful this function will result in the thread
     * being stopped indicating that the connection to the monitored client has
     * been lost.
     *
     * @returns @c true, if the heartbeat was successful, @c false otherwise
     */
    bool beat();

    /**
     * @brief Stops the heartbeat
     */
    void stop();

protected:
    /**
     * @brief Runs the main loop of this thread that periodically checks the state
     * of the channel of the monitored client and requests the client's
     * @b ServerUUID property to test if the communication is still up
     */
    void run() override;

private:
    const int m_Timeout;  ///< timeout between checks in seconds
    std::weak_ptr<CSiLAClient> m_Client;
};
}  // namespace SiLA2
#endif  // HEARTBEATTHREAD_H
