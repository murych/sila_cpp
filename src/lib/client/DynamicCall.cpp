/**
 ** This file is part of the sila_cpp project.
 ** Copyright 2021 SiLA2
 **
 ** Permission is hereby granted, free of charge, to any person obtaining a copy
 ** of this software and associated documentation files (the "Software"), to deal
 ** in the Software without restriction, including without limitation the rights
 ** to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 ** copies of the Software, and to permit persons to whom the Software is
 ** furnished to do so, subject to the following conditions:
 **
 ** The above copyright notice and this permission notice shall be included in all
 ** copies or substantial portions of the Software.
 **
 ** THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 ** IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 ** FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 ** AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 ** LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 ** OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 ** SOFTWARE.
 **/

//============================================================================
/// \file   DynamicCall.cpp
/// \author Florian Meinicke (florian.meinicke@cetoni.de)
/// \date   12.02.2021
/// \brief  Implementation of the CDynamicCall class
//============================================================================

//============================================================================
//                                  INCLUDES
//============================================================================
#include "DynamicCall.h"

#include <sila_cpp/common/logging.h>

#include <google/protobuf/message.h>
#include <grpcpp/generic/generic_stub.h>
#include <grpcpp/grpcpp.h>

#include <utility>

using namespace std;
using namespace isocpp_p0201;

namespace SiLA2
{
using CallOperationType = CDynamicCallTimeoutError::CallOperationType;

/**
 * @brief Simple helper function to create tags for gRPC's Completion Queue
 */
void* tag(intptr_t t)
{
    return reinterpret_cast<void*>(t);
}

/**
 * @brief Private data of the CDynamicCall class - pimpl
 */
class CDynamicCall::PrivateImpl
{
public:
    /**
     * @brief C'tor
     */
    PrivateImpl(shared_ptr<grpc::Channel> SharedChannel, string MethodName);

    ~PrivateImpl() { delete Call; }

    /**
     * @brief Copy the @a ReceivedMetadata into the @a TargetMetadata
     * @param ReceivedMetadata The metadata that has been received from the server
     * @param TargetMetadata A pointer to the target metadata container
     */
    static void copyMetadata(
        const multimap<grpc::string_ref, grpc::string_ref>& ReceivedMetadata,
        MetadataContainer* TargetMetadata);

    /**
     * @brief Helper to create a @c gpr_timespec from the number of
     * @a TimeoutSeconds
     * All times returned will use @c GPR_TIMESPAN clock type
     *
     * @return The @a TimeoutSeconds converted to a @c gpr_timespec
     */
    [[nodiscard]] gpr_timespec gprTimeout() const
    {
        return TimeoutSeconds > 0 ?
                   gpr_time_from_seconds(TimeoutSeconds, GPR_TIMESPAN) :
                   gpr_inf_future(GPR_TIMESPAN);
    }

    int TimeoutSeconds{5};
    const string Method;
    const shared_ptr<grpc::Channel> Channel;
    grpc::GenericStub Stub;
    shared_ptr<grpc::CompletionQueue> CompletionQueue;  ///< non-copyable
    grpc::GenericClientAsyncReaderWriter* Call{nullptr};
    shared_ptr<grpc::ClientContext> Context;  ///< non-copyable
};

//============================================================================
CDynamicCall::PrivateImpl::PrivateImpl(shared_ptr<grpc::Channel> SharedChannel,
                                       string MethodName)
    : Method{std::move(MethodName)},
      Channel{std::move(SharedChannel)},
      Stub{Channel},
      CompletionQueue{make_shared<grpc::CompletionQueue>()},
      Context{make_shared<grpc::ClientContext>()}
{}

//============================================================================
void CDynamicCall::PrivateImpl::copyMetadata(
    const multimap<grpc::string_ref, grpc::string_ref>& ReceivedMetadata,
    MetadataContainer* TargetMetadata)
{
    TargetMetadata->clear();
    for_each(begin(ReceivedMetadata), end(ReceivedMetadata),
             [&TargetMetadata](const auto& Metadatum) {
                 TargetMetadata->emplace(
                     std::piecewise_construct,
                     std::forward_as_tuple(Metadatum.first.data(),
                                           Metadatum.first.length()),
                     std::forward_as_tuple(Metadatum.second.data(),
                                           Metadatum.second.length()));
             });
}

//============================================================================
CDynamicCall::CDynamicCall(std::shared_ptr<grpc::Channel> Channel,
                           string MethodName, const MetadataContainer& Metadata)
    : d_ptr{make_polymorphic_value<PrivateImpl>(std::move(Channel),
                                                std::move(MethodName))}
{
    PIMPL_D(CDynamicCall);
    qCDebug(sila_cpp_client) << "Dynamic Call" << d->Method << "start"
                             << "\nMetadata" << Metadata;

    for_each(begin(Metadata), end(Metadata), [&d](const auto& Metadatum) {
        d->Context->AddMetadata(Metadatum.first, Metadatum.second);
    });

    void* Tag;
    bool ok;

    d->Call =
        d->Stub.PrepareCall(d->Context.get(), d->Method, d->CompletionQueue.get())
            .release();
    if (!d->Call)
    {
        qFatal("Could not prepare Dynamic Call to %s", d->Method.c_str());
    }
    d->Call->StartCall(tag(1));
    const auto Event = d->CompletionQueue->AsyncNext(&Tag, &ok, d->gprTimeout());
    switch (Event)
    {
    case grpc::CompletionQueue::NextStatus::GOT_EVENT:
        if (!ok)
        {
            qCCritical(sila_cpp_client)
                << "Could not start Dynamic Call to" << d->Method;
        }
        qCDebug(sila_cpp_client)
            << "Dynamic Call" << d->Method << "start complete";
        return;
    case grpc::CompletionQueue::NextStatus::TIMEOUT:
        throw CDynamicCallTimeoutError{CallOperationType::Start, this};
    case grpc::CompletionQueue::NextStatus::SHUTDOWN:
        qCWarning(sila_cpp_client)
            << "Could not start Dynamic Call to" << d->Method
            << "because the stream was closed";
        return;
    }
    qCWarning(sila_cpp_client) << "Could not start Dynamic Call to" << d->Method
                               << "because of unhandled event" << Event;
}

//============================================================================
grpc::Status CDynamicCall::call(
    shared_ptr<grpc::Channel> Channel, string MethodName,
    const unique_ptr<google::protobuf::Message>& Request,
    unique_ptr<google::protobuf::Message>& Response,
    const MetadataContainer& Metadata, MetadataContainer* InitialMetadata,
    MetadataContainer* TrailingMetadata)
{
    CDynamicCall DynamicCall{std::move(Channel), std::move(MethodName), Metadata};
    DynamicCall.write(Request);
    DynamicCall.writesDone();
    DynamicCall.read(Response, InitialMetadata);
    return DynamicCall.finish(TrailingMetadata);
}

//============================================================================
bool CDynamicCall::write(const unique_ptr<google::protobuf::Message>& Message)
{
    PIMPL_D(CDynamicCall);
    qCDebug(sila_cpp_client)
        << "Dynamic Call" << d->Method << "write\nMessage:" << Message;

    const auto SerializedMessage = Message->SerializeAsString();
    const grpc::Slice Slice{SerializedMessage};
    const grpc::ByteBuffer Buffer{&Slice, 1};

    void* Tag;
    bool ok;

    d->Call->Write(Buffer, tag(2));
    const auto Event = d->CompletionQueue->AsyncNext(&Tag, &ok, d->gprTimeout());
    switch (Event)
    {
    case grpc::CompletionQueue::NextStatus::GOT_EVENT:
        if (!ok)
        {
            qCCritical(sila_cpp_client)
                << "Could not write Message for Dynamic Call to" << d->Method;
            return false;
        }
        qCDebug(sila_cpp_client)
            << "Dynamic Call" << d->Method << "write complete";
        return true;
    case grpc::CompletionQueue::NextStatus::TIMEOUT:
        throw CDynamicCallTimeoutError{CallOperationType::Write, this};
    case grpc::CompletionQueue::NextStatus::SHUTDOWN:
        qCWarning(sila_cpp_client)
            << "Could not write Message for Dynamic Call to" << d->Method
            << "because the stream was closed";
        return false;
    }
    qCWarning(sila_cpp_client)
        << "Could not write Message for Dynamic Call to" << d->Method
        << "because of unhandled event" << Event;
    return false;
}

//============================================================================
bool CDynamicCall::writesDone()
{
    PIMPL_D(CDynamicCall);
    qCDebug(sila_cpp_client) << "Dynamic Call" << d->Method << "writesDone";

    void* Tag;
    bool ok;

    d->Call->WritesDone(tag(3));
    const auto Event = d->CompletionQueue->AsyncNext(&Tag, &ok, d->gprTimeout());
    switch (Event)
    {
    case grpc::CompletionQueue::NextStatus::GOT_EVENT:
        if (!ok)
        {
            qCCritical(sila_cpp_client)
                << "Could not complete writesDone for Dynamic Call to"
                << d->Method;
            return false;
        }
        qCDebug(sila_cpp_client)
            << "Dynamic Call" << d->Method << "writesDone complete";
        return true;
    case grpc::CompletionQueue::NextStatus::TIMEOUT:
        throw CDynamicCallTimeoutError{CallOperationType::WritesDone, this};
    case grpc::CompletionQueue::NextStatus::SHUTDOWN:
        qCWarning(sila_cpp_client)
            << "Could not complete writesDone for Dynamic Call to" << d->Method
            << "because the stream was closed";
        return false;
    }
    qCWarning(sila_cpp_client)
        << "Could not complete writesDone for Dynamic Call to" << d->Method
        << "because of unhandled event" << Event;
    return false;
}

//============================================================================
bool CDynamicCall::read(unique_ptr<google::protobuf::Message>& Message,
                        MetadataContainer* InitialMetadata)
{
    PIMPL_D(CDynamicCall);
    qCDebug(sila_cpp_client) << "Dynamic Call" << d->Method << "read";

    void* Tag;
    bool ok;

    grpc::ByteBuffer Buffer;
    d->Call->Read(&Buffer, tag(4));
    const auto Event = d->CompletionQueue->AsyncNext(&Tag, &ok, d->gprTimeout());
    switch (Event)
    {
    case grpc::CompletionQueue::NextStatus::GOT_EVENT:
    {
        if (!ok)
        {
            qCDebug(sila_cpp_client) << "Dynamic Call to" << d->Method
                                     << "has ended - no more data to read!";
            return false;
        }
        
        vector<grpc::Slice> Slices;
        if (!Buffer.Dump(&Slices).ok())
        {
            qCCritical(sila_cpp_client)
                << "Could not parse server response for Dynamic Call to"
                << d->Method;
            return false;
        }

        string Response;
        for_each(cbegin(Slices), cend(Slices), [&Response](const auto& Slice) {
            Response.append(reinterpret_cast<const char*>(Slice.begin()),
                            Slice.size());
        });
        Message->ParseFromString(Response);

        qCDebug(sila_cpp_client) << "Received Message" << Message;

        if (InitialMetadata)
        {
            d->copyMetadata(d->Context->GetServerTrailingMetadata(),
                            InitialMetadata);
            qCDebug(sila_cpp_client)
                << "Received Initial Metadata" << *InitialMetadata;
        }
        qCDebug(sila_cpp_client)
            << "Dynamic Call" << d->Method << "read complete";
        return true;
    }
    case grpc::CompletionQueue::NextStatus::TIMEOUT:
        throw CDynamicCallTimeoutError{CallOperationType::Read, this};
    case grpc::CompletionQueue::NextStatus::SHUTDOWN:
        qCWarning(sila_cpp_client)
            << "Could not read from Dynamic Call to" << d->Method
            << "because the stream was closed";
        return false;
    }
    qCWarning(sila_cpp_client)
        << "Could not read from Dynamic Call to" << d->Method
        << "because of unhandled event" << Event;
    return false;
}

//============================================================================
void CDynamicCall::cancel()
{
    PIMPL_D(CDynamicCall);
    qCDebug(sila_cpp_client) << "Cancelling Dynamic Call to" << d->Method;
    d->Context->TryCancel();
}

//============================================================================
grpc::Status CDynamicCall::finish(MetadataContainer* TrailingMetadata)
{
    PIMPL_D(CDynamicCall);
    qCDebug(sila_cpp_client) << "Dynamic Call" << d->Method << "finish";

    void* Tag;
    bool ok;

    grpc::Status Status;
    d->Call->Finish(&Status, tag(5));
    const auto Event = d->CompletionQueue->AsyncNext(&Tag, &ok, d->gprTimeout());
    switch (Event)
    {
    case grpc::CompletionQueue::NextStatus::GOT_EVENT:
        if (!ok)
        {
            qCCritical(sila_cpp_client)
                << "Could not finish Dynamic Call to" << d->Method;
        }
        break;
    case grpc::CompletionQueue::NextStatus::TIMEOUT:
        throw CDynamicCallTimeoutError{CallOperationType::Finish, this};
    case grpc::CompletionQueue::NextStatus::SHUTDOWN:
        qCWarning(sila_cpp_client)
            << "Could not finish Dynamic Call to" << d->Method
            << "because the stream was closed";
        break;
    default:
        qCWarning(sila_cpp_client)
            << "Could not finish Dynamic Call to" << d->Method
            << "because of unhandled event" << Event;
        break;
    }

    if (TrailingMetadata)
    {
        d->copyMetadata(d->Context->GetServerTrailingMetadata(),
                        TrailingMetadata);
        qCDebug(sila_cpp_client)
            << "Received Trailing Metadata" << *TrailingMetadata;
    }
    qCDebug(sila_cpp_client)
        << "Dynamic Call" << d->Method << "finish complete" << Status;
    return Status;
}

//============================================================================
int CDynamicCall::timeoutSeconds() const
{
    PIMPL_D(const CDynamicCall);
    return d->TimeoutSeconds;
}

//============================================================================
void CDynamicCall::setTimeoutSeconds(int Seconds)
{
    PIMPL_D(CDynamicCall);
    d->TimeoutSeconds = Seconds;
}

//============================================================================
string CDynamicCall::method() const
{
    PIMPL_D(const CDynamicCall);
    return d->Method;
}

///===========================================================================
CDynamicCallTimeoutError::CDynamicCallTimeoutError(CallOperationType CallOpType,
                                                   CDynamicCall* Call)
    : m_Message{"Could not perform " + callOperationTypeToString(CallOpType)
                + " for Dynamic Call to " + Call->method()
                + " because the operation timed out (took longer than "
                + to_string(Call->timeoutSeconds()) + "s)"}
{
    qCWarning(sila_cpp_client) << m_Message;
}

//============================================================================
string CDynamicCallTimeoutError::callOperationTypeToString(CallOperationType Type)
{
    switch (Type)
    {
    case CallOperationType::Start:
        return "start";
    case CallOperationType::Write:
        return "write";
    case CallOperationType::WritesDone:
        return "writesDone";
    case CallOperationType::Read:
        return "read";
    case CallOperationType::Finish:
        return "finish";
    }
    qCCritical(sila_cpp_client)
        << "Unhandled CallOperationType" << static_cast<int>(Type);
    return "unknown";
}

//============================================================================
const char* CDynamicCallTimeoutError::what() const noexcept
{
    return m_Message.c_str();
}
}  // namespace SiLA2
