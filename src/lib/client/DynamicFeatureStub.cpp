/**
 ** This file is part of the sila_cpp project.
 ** Copyright 2021 SiLA2
 **
 ** Permission is hereby granted, free of charge, to any person obtaining a copy
 ** of this software and associated documentation files (the "Software"), to deal
 ** in the Software without restriction, including without limitation the rights
 ** to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 ** copies of the Software, and to permit persons to whom the Software is
 ** furnished to do so, subject to the following conditions:
 **
 ** The above copyright notice and this permission notice shall be included in all
 ** copies or substantial portions of the Software.
 **
 ** THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 ** IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 ** FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 ** AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 ** LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 ** OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 ** SOFTWARE.
 **/

//============================================================================
/// \file   DynamicFeatureStub.cpp
/// \author Florian Meinicke (florian.meinicke@cetoni.de)
/// \date   11.02.2021
/// \brief  Implementation of the CDynamicFeatureStub class
//============================================================================

//============================================================================
//                                  INCLUDES
//============================================================================
#include <sila_cpp/client/DynamicFeatureStub.h>
#include <sila_cpp/codegen/fdl/Feature.h>
#include <sila_cpp/codegen/proto/DynamicMessageFactory.h>
#include <sila_cpp/common/FullyQualifiedPropertyID.h>
#include <sila_cpp/framework/error_handling/ClientError.h>

#include "DynamicCall.h"

#include <utility>

using namespace std;
using namespace isocpp_p0201;
using namespace SiLA2::codegen::proto;
using namespace SiLA2::codegen::fdl;

namespace SiLA2
{
/**
 * @brief Private data of the CDynamicFeatureStub class - pimpl
 */
class CDynamicFeatureStub::PrivateImpl
{
public:
    /**
     * @brief C'tor
     */
    PrivateImpl(CDynamicFeatureStub* parent,
                const CFullyQualifiedFeatureID& FQFeatureID,
                const CFeature& ParsedFeature,
                shared_ptr<grpc::Channel> SharedChannel,
                shared_ptr<CDynamicMessageFactory> DMF);

    /**
     * @brief Given the list of FDL objects @a FDLList construct the corresponding
     * stub-like objects and map them to their Identifiers
     *
     * @tparam F The type of the Fully Qualified Identifier
     * @tparam T The type of the FDL objects (e.g. @c codegen::fdl::CCommand)
     * @tparam U The type of the stub-like objects (e.g @c CDynamicCommand)
     * @param FDLList The list of FDL objects
     * @param IdentifierList The list of identifiers of the stub-like objects
     * @param TargetList The target list in which to put the stub-like objects
     */
    template<typename T, typename F, typename U>
    void createStubs(const QList<T>& FDLList, QList<F>& IdentifierList,
                     QList<U>& TargetList);

    CDynamicFeatureStub* q_ptr;

    const CFullyQualifiedFeatureID FeatureID;
    const CFeature Feature;
    QList<CFullyQualifiedMetadataID> MetadataIDs;
    QList<CClientMetadata> Metadata;
    QList<CFullyQualifiedDataTypeID> DataTypeIDs;
    QList<CCustomDataType> DataTypes;
    QList<CFullyQualifiedCommandID> CommandIDs;
    QList<shared_ptr<CDynamicCommand>> Commands;
    QList<CFullyQualifiedPropertyID> PropertyIDs;
    QList<shared_ptr<CDynamicProperty>> Properties;
    const shared_ptr<grpc::Channel> Channel;
    const shared_ptr<CDynamicMessageFactory> DynamicMessageFactory;

    PIMPL_DECLARE_PUBLIC(CDynamicFeatureStub)
};

//============================================================================
CDynamicFeatureStub::PrivateImpl::PrivateImpl(
    CDynamicFeatureStub* parent, const CFullyQualifiedFeatureID& FQFeatureID,
    const CFeature& ParsedFeature, shared_ptr<grpc::Channel> SharedChannel,
    shared_ptr<CDynamicMessageFactory> DMF)
    : q_ptr{parent},
      FeatureID{FQFeatureID},
      Feature{ParsedFeature},
      Channel{std::move(SharedChannel)},
      DynamicMessageFactory{std::move(DMF)}
{}

//============================================================================
template<typename T, typename F, typename U>
void CDynamicFeatureStub::PrivateImpl::createStubs(const QList<T>& FDLList,
                                                   QList<F>& IdentifierList,
                                                   QList<U>& TargetList)
{
    PIMPL_Q(CDynamicFeatureStub);

    IdentifierList.clear();
    TargetList.clear();
    for_each(begin(FDLList), end(FDLList), [this, &q, &IdentifierList, &TargetList](const auto& Object) {
        const auto ID = F{FeatureID, Object.identifier()};
        IdentifierList.append(ID);
        if constexpr (is_same_v<U, CClientMetadata>
                      || is_same_v<U, CCustomDataType>)
        {
            TargetList.append(U{ID, Object, Channel, DynamicMessageFactory, q});
        }
        else
        {
            TargetList.append(make_shared<typename U::element_type>(
                ID, Object, Channel, DynamicMessageFactory, q));
        }
    });
}

///===========================================================================
CDynamicFeatureStub::CDynamicFeatureStub(
    const CFullyQualifiedFeatureID& FeatureID, const CFeature& Feature,
    shared_ptr<grpc::Channel> Channel, shared_ptr<CDynamicMessageFactory> DMF)
    : d_ptr{make_polymorphic_value<PrivateImpl>(
        this, FeatureID, Feature, std::move(Channel), std::move(DMF))}
{
    PIMPL_D(CDynamicFeatureStub);

    d->createStubs(Feature.dataTypeDefinitions(), d->DataTypeIDs, d->DataTypes);
    for_each(begin(d->DataTypes), end(d->DataTypes),
             mem_fn(&CCustomDataType::fillWithDefaultValue));
    d->createStubs(Feature.metadata(), d->MetadataIDs, d->Metadata);
    d->createStubs(Feature.commands(), d->CommandIDs, d->Commands);
    d->createStubs(Feature.properties(), d->PropertyIDs, d->Properties);
}

//============================================================================
CFullyQualifiedFeatureID CDynamicFeatureStub::identifier() const
{
    PIMPL_D(const CDynamicFeatureStub);
    return d->FeatureID;
}

//============================================================================
QString CDynamicFeatureStub::displayName() const
{
    PIMPL_D(const CDynamicFeatureStub);
    return d->Feature.displayName();
}

//============================================================================
QString CDynamicFeatureStub::description() const
{
    PIMPL_D(const CDynamicFeatureStub);
    return d->Feature.description().simplified();
}

//============================================================================
QString CDynamicFeatureStub::fdlString() const
{
    PIMPL_D(const CDynamicFeatureStub);
    return CFDLSerializer::serialize(d->Feature);
}

//============================================================================
CFeature CDynamicFeatureStub::fdl() const
{
    PIMPL_D(const CDynamicFeatureStub);
    return d->Feature;
}

//============================================================================
QList<CFullyQualifiedCommandID> CDynamicFeatureStub::commandIdentifiers() const
{
    PIMPL_D(const CDynamicFeatureStub);
    return d->CommandIDs;
}

//============================================================================
QList<shared_ptr<CDynamicCommand>> CDynamicFeatureStub::commands() const
{
    PIMPL_D(const CDynamicFeatureStub);
    return d->Commands;
}

//============================================================================
shared_ptr<CDynamicCommand> CDynamicFeatureStub::command(
    const CFullyQualifiedCommandID& CommandID) const
{
    PIMPL_D(const CDynamicFeatureStub);
    if (const auto Index = d->CommandIDs.indexOf(CommandID); Index >= 0)
    {
        return d->Commands.at(Index);
    }
    return nullptr;
}

//============================================================================
shared_ptr<CDynamicCommand> CDynamicFeatureStub::command(
    string_view CommandID) const
{
    PIMPL_D(const CDynamicFeatureStub);
    return command({d->FeatureID, CommandID});
}

//============================================================================
shared_ptr<CDynamicCommand> CDynamicFeatureStub::command(
    QStringView CommandID) const
{
    PIMPL_D(const CDynamicFeatureStub);
    return command({d->FeatureID, CommandID});
}

//============================================================================
QList<CFullyQualifiedPropertyID> CDynamicFeatureStub::propertyIdentifiers() const
{
    PIMPL_D(const CDynamicFeatureStub);
    return d->PropertyIDs;
}

//============================================================================
QList<shared_ptr<CDynamicProperty>> CDynamicFeatureStub::properties() const
{
    PIMPL_D(const CDynamicFeatureStub);
    return d->Properties;
}

//============================================================================
shared_ptr<CDynamicProperty> CDynamicFeatureStub::property(
    const CFullyQualifiedPropertyID& PropertyID) const
{
    PIMPL_D(const CDynamicFeatureStub);
    if (const auto Index = d->PropertyIDs.indexOf(PropertyID); Index >= 0)
    {
        return d->Properties.at(Index);
    }
    return nullptr;
}

//============================================================================
shared_ptr<CDynamicProperty> CDynamicFeatureStub::property(
    string_view PropertyID) const
{
    PIMPL_D(const CDynamicFeatureStub);
    return property({d->FeatureID, PropertyID});
}

//============================================================================
shared_ptr<CDynamicProperty> CDynamicFeatureStub::property(
    QStringView PropertyID) const
{
    PIMPL_D(const CDynamicFeatureStub);
    return property({d->FeatureID, PropertyID});
}

//============================================================================
QList<CFullyQualifiedMetadataID> CDynamicFeatureStub::metadataIdentifiers() const
{
    PIMPL_D(const CDynamicFeatureStub);
    return d->MetadataIDs;
}

//============================================================================
QList<CClientMetadata> CDynamicFeatureStub::metadata() const
{
    PIMPL_D(const CDynamicFeatureStub);
    return d->Metadata;
}

//============================================================================
CClientMetadata CDynamicFeatureStub::metadata(
    const CFullyQualifiedMetadataID& MetadataID) const
{
    PIMPL_D(const CDynamicFeatureStub);

    if (const auto Index = d->MetadataIDs.indexOf(MetadataID); Index >= 0)
    {
        return d->Metadata.at(Index);
    }
    throw invalid_argument{MetadataID.toStdString()};
}

//============================================================================
CClientMetadata CDynamicFeatureStub::metadata(string_view MetadataID) const
{
    PIMPL_D(const CDynamicFeatureStub);
    return metadata({d->FeatureID, MetadataID});
}

//============================================================================
CClientMetadata CDynamicFeatureStub::metadata(QStringView MetadataID) const
{
    PIMPL_D(const CDynamicFeatureStub);
    return metadata({d->FeatureID, MetadataID});
}

//============================================================================
QList<CFullyQualifiedDataTypeID> CDynamicFeatureStub::dataTypeIdentifiers() const
{
    PIMPL_D(const CDynamicFeatureStub);
    return d->DataTypeIDs;
}

//============================================================================
QList<CCustomDataType> CDynamicFeatureStub::dataTypes() const
{
    PIMPL_D(const CDynamicFeatureStub);
    return d->DataTypes;
}

//============================================================================
CCustomDataType CDynamicFeatureStub::dataType(
    const CFullyQualifiedDataTypeID& DataTypeID) const
{
    PIMPL_D(const CDynamicFeatureStub);

    qCDebug(sila_cpp_client) << "Data Types" << d->DataTypeIDs;
    const auto Index = d->DataTypeIDs.indexOf(DataTypeID);
    qCDebug(sila_cpp_client) << "index" << Index;
    if (Index >= 0)
    {
        return d->DataTypes.at(Index);
    }
    throw invalid_argument{DataTypeID.toStdString()};
}

//============================================================================
CCustomDataType CDynamicFeatureStub::dataType(string_view DataTypeID) const
{
    PIMPL_D(const CDynamicFeatureStub);
    return dataType({d->FeatureID, DataTypeID});
}

//============================================================================
CCustomDataType CDynamicFeatureStub::dataType(QStringView DataTypeID) const
{
    PIMPL_D(const CDynamicFeatureStub);
    return dataType({d->FeatureID, DataTypeID});
}
}  // namespace SiLA2
