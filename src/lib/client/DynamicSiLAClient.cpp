/**
 ** This file is part of the sila_cpp project.
 ** Copyright (c) 2021 SiLA2
 **
 ** Permission is hereby granted, free of charge, to any person obtaining a copy
 ** of this software and associated documentation files (the "Software"), to deal
 ** in the Software without restriction, including without limitation the rights
 ** to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 ** copies of the Software, and to permit persons to whom the Software is
 ** furnished to do so, subject to the following conditions:
 **
 ** The above copyright notice and this permission notice shall be included in all
 ** copies or substantial portions of the Software.
 **
 ** THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 ** IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 ** FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 ** AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 ** LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 ** OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 ** SOFTWARE.
 **/

//============================================================================
/// \file   DynamicSiLAClient.cpp
/// \author Florian Meinicke (florian.meinicke@cetoni.de)
/// \date   22.01.2021
/// \brief  Implementation of the CDynamicSiLAClient class
//============================================================================

//============================================================================
//                                  INCLUDES
//============================================================================
#include <sila_cpp/client/DynamicSiLAClient.h>
#include <sila_cpp/codegen/fdl/FDLSerializer.h>
#include <sila_cpp/codegen/fdl/Feature.h>
#include <sila_cpp/codegen/proto/DynamicMessageFactory.h>
#include <sila_cpp/codegen/proto/ProtobufFile.h>
#include <sila_cpp/codegen/proto/ProtobufGenerator.h>
#include <sila_cpp/common/FullyQualifiedFeatureID.h>

#include "SiLAClient_p.h"

using namespace std;
using namespace isocpp_p0201;
using namespace SiLA2::codegen::proto;

namespace SiLA2
{
/**
 * @brief Private data of the CDynamicSiLAClient class - pimpl
 */
class CDynamicSiLAClient::PrivateImpl : public CSiLAClient::PrivateImpl
{
public:
    /**
     * @brief C'tor
     */
    PrivateImpl(CDynamicSiLAClient* parent, const CServerAddress& Address);

    /**
     * @brief Retrieves all implemented Features from the Server and parses their
     * FDL description. The parsed @c CFeature objects are then stored in
     * @a Features.
     */
    void generateFeatureStubs();

    /**
     * @brief Update all Commands and Properties of all Feature Stubs by giving
     * them the Metadata that they're affected by
     */
    void crosslinkMetadata() const;

    /**
     * @brief Add the given @a Metadata to the given objects @a Objects if it
     * affects them
     *
     * @tparam T The type of the objects (@c CDynamicCommand or
     * @c CDynamicProperty)
     * @param Objects The list of objects to add the affecting Metadata to
     * @param Metadata The Metadata to add if it affects the @a Objects
     */
    template<typename T>
    void addAffectingMetadata(const QList<T>& Objects,
                              const CClientMetadata& Metadata) const;

    shared_ptr<CDynamicMessageFactory> DynamicMessageFactory;
    QList<CFullyQualifiedFeatureID> FeatureIDs;
    QList<shared_ptr<CDynamicFeatureStub>> Stubs;

    QStringList Errors;

    PIMPL_DECLARE_PUBLIC(CDynamicSiLAClient)
};

//============================================================================
CDynamicSiLAClient::PrivateImpl::PrivateImpl(CDynamicSiLAClient* parent,
                                             const CServerAddress& Address)
    : CSiLAClient::PrivateImpl{parent, Address},
      DynamicMessageFactory{make_shared<CDynamicMessageFactory>()}
{}

//============================================================================
void CDynamicSiLAClient::PrivateImpl::generateFeatureStubs()
{
    PIMPL_Q(CDynamicSiLAClient);
    const auto ImplementedFeatures = q->Get_ImplementedFeatures();

    for_each(
        begin(ImplementedFeatures), end(ImplementedFeatures),
        [&](const auto& FeatureIDString) {
            const auto FeatureID =
                CFullyQualifiedFeatureID::fromStdString(FeatureIDString);
            if (!FeatureID.isValid())
            {
                QString Error{"Failed to add Feature"
                              + FeatureIDString.toQString()
                              + "because the Identifier is not Fully Qualified"};
                Errors << Error;
                qCWarning(sila_cpp) << Error;
                return;
            }

            const auto FDL =
                QString::fromStdString(q->GetFeatureDefinition(FeatureID));
            if (!codegen::fdl::CFDLSerializer::validate(FeatureID, FDL))
            {
                QString Error{"Failed to add Feature" + FeatureID.toString()
                              + "because the Feature Description is invalid"};
                Errors << Error;
                qCWarning(sila_cpp) << Error;
                return;
            }
            codegen::fdl::CFeature Feature;
            codegen::fdl::CFDLSerializer::deserialize(Feature, FDL);
            // The Feature ID from `ImplementedFeatures` might be valid but
            // can differ from what is actually specified in the FDL. Since
            // the Protobuf messages are all dependent on the FDL we use the
            // Feature ID from the FDL from here on.
            const auto FDLFeatureID = CFullyQualifiedFeatureID{
                Feature.originator(), Feature.category(), Feature.identifier(),
                'v' + Feature.featureVersion().split('.').constFirst()};
            const auto Protobuf = CProtobufGenerator::generate(Feature);
            DynamicMessageFactory->addProtoFile(
                {FDLFeatureID.identifier() + ".proto", Protobuf});
            FeatureIDs.append(FeatureID);
            Stubs.append(q->makeFeatureStub(FDLFeatureID, Feature));
        });
}

//============================================================================
void CDynamicSiLAClient::PrivateImpl::crosslinkMetadata() const
{
    CClientMetadataList AllMetadata;
    for_each(begin(Stubs), end(Stubs), [&AllMetadata](const auto& Stub) {
        AllMetadata.append(Stub->metadata());
    });

    for_each(begin(AllMetadata), end(AllMetadata), [this](const auto& Metadata) {
        for_each(begin(Stubs), end(Stubs), [this, &Metadata](const auto& Stub) {
            addAffectingMetadata(Stub->commands(), Metadata);
            addAffectingMetadata(Stub->properties(), Metadata);
        });
    });
}

//============================================================================
template<typename T>
void CDynamicSiLAClient::PrivateImpl::addAffectingMetadata(
    const QList<T>& Objects, const CClientMetadata& Metadata) const
{
    for_each(begin(Objects), end(Objects), [&](const auto& Object) {
        const auto AffectedCalls = Metadata.affectedCalls();
        const auto Identifier = Object->identifier();
        for_each(begin(AffectedCalls), end(AffectedCalls), [&](const auto& Call) {
            if (Call == Identifier.featureIdentifier().toString()
                || Call == Identifier.toString())
            {
                Object->addAffectingMetadata(Metadata);
            }
        });
    });
}

///===========================================================================
CDynamicSiLAClient::AcceptUntrustedServerCertificateHandler
    CDynamicSiLAClient::m_AcceptUntrustedServerCertificateHandler{nullptr};

//============================================================================
CDynamicSiLAClient::CDynamicSiLAClient(const CServerAddress& Address)
    : CSiLAClient{make_polymorphic_value<CSiLAClient::PrivateImpl, PrivateImpl>(
        this, Address)}
{}

//============================================================================
void CDynamicSiLAClient::connect()
{
    PIMPL_D(CDynamicSiLAClient);

    CSiLAClient::connect();
    d->generateFeatureStubs();
    d->crosslinkMetadata();
}

//============================================================================
QStringList CDynamicSiLAClient::errors() const
{
    PIMPL_D(const CDynamicSiLAClient);
    return d->Errors;
}

//============================================================================
QStringList CDynamicSiLAClient::fdlStrings() const
{
    PIMPL_D(const CDynamicSiLAClient);

    QStringList Result;
    Result.reserve(d->Stubs.size());
    transform(begin(d->Stubs), end(d->Stubs), back_inserter(Result),
              [](const auto& Feature) { return Feature->fdlString(); });
    return Result;
}

//============================================================================
QList<codegen::fdl::CFeature> CDynamicSiLAClient::fdl() const
{
    PIMPL_D(const CDynamicSiLAClient);

    QList<codegen::fdl::CFeature> Result;
    Result.reserve(d->Stubs.size());
    transform(begin(d->Stubs), end(d->Stubs), back_inserter(Result),
              [](const auto& Feature) { return Feature->fdl(); });
    return Result;
}

//============================================================================
QList<CFullyQualifiedFeatureID> CDynamicSiLAClient::featureIdentifiers() const
{
    PIMPL_D(const CDynamicSiLAClient);
    return d->FeatureIDs;
}

//============================================================================
QList<std::shared_ptr<CDynamicFeatureStub>> CDynamicSiLAClient::featureStubs()
    const
{
    PIMPL_D(const CDynamicSiLAClient);
    return d->Stubs;
}

//============================================================================
std::shared_ptr<CDynamicFeatureStub> CDynamicSiLAClient::featureStub(
    const CFullyQualifiedFeatureID& FeatureID) const
{
    PIMPL_D(const CDynamicSiLAClient);
    if (const auto Index = d->FeatureIDs.indexOf(FeatureID); Index >= 0)
    {
        return d->Stubs.at(Index);
    }
    return nullptr;
}

//============================================================================
bool CDynamicSiLAClient::acceptUntrustedServerCertificate(
    const QSslCertificate& Certificate) const
{
    if (m_AcceptUntrustedServerCertificateHandler)
    {
        return m_AcceptUntrustedServerCertificateHandler(Certificate, this);
    }
    return CSiLAClient::acceptUntrustedServerCertificate(Certificate);
}

//============================================================================
CDynamicSiLAClient::AcceptUntrustedServerCertificateHandler
CDynamicSiLAClient::setAcceptUntrustedServerCertificateHandler(
    AcceptUntrustedServerCertificateHandler Handler)
{
    auto PreviousHandler = m_AcceptUntrustedServerCertificateHandler;
    m_AcceptUntrustedServerCertificateHandler = std::move(Handler);
    return PreviousHandler;
}

//============================================================================
shared_ptr<CDynamicFeatureStub> CDynamicSiLAClient::makeFeatureStub(
    const CFullyQualifiedFeatureID& FeatureID,
    const codegen::fdl::CFeature& Feature) const
{
    PIMPL_D(const CDynamicSiLAClient);
    return shared_ptr<CDynamicFeatureStub>{new CDynamicFeatureStub{
        FeatureID, Feature, channel(), d->DynamicMessageFactory}};
}
}  // namespace SiLA2

//============================================================================
QDebug operator<<(QDebug dbg, const SiLA2::CDynamicSiLAClient& rhs)
{
    QDebugStateSaver s{dbg};
    return dbg.nospace() << "CDynamicSiLAClient(" << rhs.serverAddress() << ')';
}
