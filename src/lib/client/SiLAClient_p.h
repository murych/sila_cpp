/**
 ** This file is part of the sila_cpp project.
 ** Copyright (c) 2021 SiLA2
 **
 ** Permission is hereby granted, free of charge, to any person obtaining a copy
 ** of this software and associated documentation files (the "Software"), to deal
 ** in the Software without restriction, including without limitation the rights
 ** to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 ** copies of the Software, and to permit persons to whom the Software is
 ** furnished to do so, subject to the following conditions:
 **
 ** The above copyright notice and this permission notice shall be included in all
 ** copies or substantial portions of the Software.
 **
 ** THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 ** IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 ** FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 ** AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 ** LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 ** OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 ** SOFTWARE.
 **/

//============================================================================
/// \file   SiLAClient_p.h
/// \author Florian Meinicke (florian.meinicke@cetoni.de)
/// \date   22.01.2021
/// \brief  Declaration of the CSiLAClient::PrivateImpl class
//============================================================================
#ifndef SILACLIENT_P_H
#define SILACLIENT_P_H

//============================================================================
//                                  INCLUDES
//============================================================================
#include <sila_cpp/client/SiLAClient.h>
#include <sila_cpp/common/SSLCredentials.h>
#include <sila_cpp/common/ServerAddress.h>
#include <sila_cpp/common/ServerInformation.h>

#include "SiLAService.grpc.pb.h"

#include <QSslSocket>

namespace SiLA2
{
/**
 * @brief Private data of the CSiLAClient class - pimpl
 */
class CSiLAClient::PrivateImpl
{
public:
    /**
     * @brief C'tor
     */
    explicit PrivateImpl(CSiLAClient* parent, CServerAddress Address);

    /**
     * @brief "Slot" for the @c peerVerifyError signal of @c QSslSocket
     * If the @a Errors include @c HostNameMismatch, @c SelfSignedCertificate, or
     * @c CertificateUntrusted errors these will be discarded since they're
     * non-fatal for the handshaking process or can be resolved in some way.
     *
     * @param Socket The SSL socket that is connected to the server an through
     * which the handshake is performed
     * @param Error The error that occurred during the SSL handshake with the
     * server
     */
    void onPeerVerifyError(QSslSocket& Socket, const QSslError& Error) const;

    /**
     * @brief "Slot" for the @c sslErrors signal of @c QSslSocket
     * If the @a Errors include @c HostNameMismatch, @c SelfSignedCertificate, or
     * @c CertificateUntrusted errors these will be discarded since they're
     * non-fatal for the handshaking process or can be resolved in some way.
     *
     * @param Socket The SSL socket on which the errors occurred
     * @param Errors The errors that occurred during the SSL handshake with the
     * server
     */
    void onSSLErrors(QSslSocket& Socket, const QList<QSslError>& Errors) const;

    /**
     * @brief Tries to get the certificate of the server at @a ServerAddress
     *
     * @return CSSLCredentials The credentials containing the server's certificate
     * if it could be obtained, or empty credentials otherwise
     *
     * @throws SiLA2::CConnectionError if there are errors during obtaining the
     * certificate
     */
    [[nodiscard]] CSSLCredentials getServerCertificate() const;

    /**
     * @brief Connect to the server at @a ServerAddress using @a Credentials (if
     * non-empty) for secure communication
     *
     * If @a Credentials is not empty this function first tries to use secure
     * communication. If that fails or @a Credentials is empty, falls back to
     * insecure communication if it was allowed by the
     * @b allow-insecure-communication CLI flag.
     *
     * @throws std::runtime_error if the connection could not be established
     */
    void connectToServer();

    /**
     * @brief Throws a @c std::runtime_error to indicate that a connection to the
     * server could not be established.
     *
     * @param Message (optional) Additional info to provide to the user that might
     * indicate why the connection failed
     */
    void throwCannotConnect(const std::string& Message = "") const;

    CSiLAClient* q_ptr;

    CServerInformation ServerInfo{};  ///< server info such as name, type, ...
    CServerAddress ServerAddress{};   ///< The server's address
    CSSLCredentials Credentials{};    ///< for secure communication
    bool ForceInsecure{false};        ///< force insecure communication?
    std::shared_ptr<grpc::Channel> Channel;  ///< channel to connect to the server
    using SiLAService =
        sila2::org::silastandard::core::silaservice::v1::SiLAService;
    std::shared_ptr<SiLAService::Stub> SiLAServiceStub;  ///< SiLAService stub

    PIMPL_DECLARE_PUBLIC(CSiLAClient)
};
}  // namespace SiLA2
#endif  // SILACLIENT_P_H
