/**
 ** This file is part of the sila_cpp project.
 ** Copyright 2021 SiLA2
 **
 ** Permission is hereby granted, free of charge, to any person obtaining a copy
 ** of this software and associated documentation files (the "Software"), to deal
 ** in the Software without restriction, including without limitation the rights
 ** to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 ** copies of the Software, and to permit persons to whom the Software is
 ** furnished to do so, subject to the following conditions:
 **
 ** The above copyright notice and this permission notice shall be included in all
 ** copies or substantial portions of the Software.
 **
 ** THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 ** IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 ** FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 ** AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 ** LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 ** OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 ** SOFTWARE.
 **/

//============================================================================
/// \file   DynamicProperty.cpp
/// \author Florian Meinicke (florian.meinicke@cetoni.de)
/// \date   18.02.2021
/// \brief  Implementation of the CDynamicProperty class
//============================================================================

//============================================================================
//                                  INCLUDES
//============================================================================
#include <sila_cpp/client/CustomDataType.h>
#include <sila_cpp/client/DynamicFeatureStub.h>
#include <sila_cpp/client/DynamicProperty.h>
#include <sila_cpp/client/DynamicStructure.h>
#include <sila_cpp/codegen/fdl/DataTypeIdentifier.h>
#include <sila_cpp/codegen/fdl/Property.h>
#include <sila_cpp/codegen/proto/DynamicMessageFactory.h>
#include <sila_cpp/framework/error_handling/ClientError.h>
#include <sila_cpp/framework/error_handling/SiLAError.h>

#include "DynamicValue_p.h"
#include "Subscription.h"

#include <QEventLoop>
#include <QFutureWatcher>
#include <QHash>

#include <google/protobuf/reflection.h>
#include <grpcpp/grpcpp.h>

#include <utility>

// winuser.h defines GetMessage which interferes with
// google::protobuf::Reflection::GetMessage
#ifdef GetMessage
#    undef GetMessage
#endif

using namespace std;
using namespace isocpp_p0201;
using namespace SiLA2::codegen;

namespace SiLA2
{
/**
 * @brief Private data of the CDynamicProperty class - pimpl
 */
class CDynamicProperty::PrivateImpl
{
public:
    /**
     * @brief C'tor
     */
    PrivateImpl(CFullyQualifiedPropertyID PropertyID,
                const fdl::CProperty& PropertyFDL,
                shared_ptr<grpc::Channel> SharedChannel,
                shared_ptr<proto::CDynamicMessageFactory> DMF,
                CDynamicFeatureStub* FeatureStub);

    /**
     * @brief Get the default (prototype) protobuf @c Message for the Parameters
     * of this Property. This will return different @c Messages depending on
     * whether this Property @c isObservable() or not.
     *
     * @return An empty protobuf @a Message of the Property's Parameters type
     */
    [[nodiscard]] std::unique_ptr<google::protobuf::Message>
    getParameterPrototype() const;

    /**
     * @brief Get the default (prototype) protobuf @c Message for the Responses of
     * this Property. This will return different @c Messages depending on whether
     * this Property @c isObservable() or not.
     *
     * @return An empty protobuf @a Message of the Property's Responses type
     */
    [[nodiscard]] std::unique_ptr<google::protobuf::Message>
    getResponsePrototype() const;

    /**
     * @brief Convert the @a Response given as a protobuf @c Message to the actual
     * @c CDynamicValue it represents
     *
     * @param Response The @c Message to convert
     * @return The @c CDynamicValue that @a Response contains
     */
    [[nodiscard]] CDynamicValue responseToDynamicValue(
        const unique_ptr<google::protobuf::Message>& Response) const;

    const CFullyQualifiedPropertyID Identifier;
    const fdl::CProperty Property;
    const shared_ptr<grpc::Channel> Channel;
    const shared_ptr<proto::CDynamicMessageFactory> DynamicMessageFactory;
    CClientMetadataList Metadata;
    shared_ptr<CSubscription<CDynamicValue>> Subscription;  ///< non-copyable
    CDynamicFeatureStub* Feature;
};

//============================================================================
CDynamicProperty::PrivateImpl::PrivateImpl(
    CFullyQualifiedPropertyID PropertyID, const fdl::CProperty& PropertyFDL,
    shared_ptr<grpc::Channel> SharedChannel,
    shared_ptr<proto::CDynamicMessageFactory> DMF,
    CDynamicFeatureStub* FeatureStub)
    : Identifier{std::move(PropertyID)},
      Property{PropertyFDL},
      Channel{std::move(SharedChannel)},
      DynamicMessageFactory{std::move(DMF)},
      Feature{FeatureStub}
{
    if (Property.isObservable())
    {
        Subscription.reset(new CSubscription<CDynamicValue>{
            Channel, Identifier.toMethodName(true), getResponsePrototype(), this,
            &PrivateImpl::responseToDynamicValue});
    }
}

//============================================================================
unique_ptr<google::protobuf::Message>
CDynamicProperty::PrivateImpl::getParameterPrototype() const
{
    return DynamicMessageFactory->getPrototype(
        Identifier.parameterMessageName(Property.isObservable()));
}

//============================================================================
unique_ptr<google::protobuf::Message>
CDynamicProperty::PrivateImpl::getResponsePrototype() const
{
    return DynamicMessageFactory->getPrototype(
        Identifier.responseMessageName(Property.isObservable()));
}

//============================================================================
CDynamicValue CDynamicProperty::PrivateImpl::responseToDynamicValue(
    const unique_ptr<google::protobuf::Message>& Response) const
{
    const auto* const Field = Response->GetDescriptor()->field(0);
    if (!Field)
    {
        qCCritical(sila_cpp_client)
            << "Received message for" << Identifier.toString()
            << "has no field. Something is wrong with this Property!";
        return {};
    }

    const auto* const Reflection = Response->GetReflection();
    if (!Field->is_repeated())
    {
        return {Reflection->MutableMessage(Response.get(), Field),
                Property.dataType(), DynamicMessageFactory};
    }

    // Field->is_repeated();
    namespace gp = google::protobuf;
    vector<gp::Message*> Messages;
    // can't directly iterate over `Ref` because that would require to
    // allocate an object of abstract `Message`
    const auto Ref =
        Reflection->GetRepeatedFieldRef<gp::Message>(*Response, Field);
    for (int i = 0; i < Ref.size(); ++i)
    {
        const auto& Val =
            Reflection->MutableRepeatedMessage(Response.get(), Field, i);
        auto* tmp = Val->New();
        tmp->CopyFrom(*Val);
        Messages.push_back(tmp);
    }
    return {Messages, Property.dataType(), DynamicMessageFactory};
}

///===========================================================================
CDynamicProperty::CDynamicProperty(const CFullyQualifiedPropertyID& Identifier,
                                   const fdl::CProperty& PropertyFDL,
                                   shared_ptr<grpc::Channel> Channel,
                                   shared_ptr<proto::CDynamicMessageFactory> DMF,
                                   CDynamicFeatureStub* Feature)
    : d_ptr{make_polymorphic_value<PrivateImpl>(
        Identifier, PropertyFDL, std::move(Channel), std::move(DMF), Feature)}
{}

//============================================================================
CFullyQualifiedPropertyID CDynamicProperty::identifier() const
{
    PIMPL_D(const CDynamicProperty);
    return d->Identifier;
}

//============================================================================
QString CDynamicProperty::displayName() const
{
    PIMPL_D(const CDynamicProperty);
    return d->Property.displayName();
}

//============================================================================
QString CDynamicProperty::description() const
{
    PIMPL_D(const CDynamicProperty);
    return d->Property.description().simplified();
}

//============================================================================
bool CDynamicProperty::isObservable() const
{
    PIMPL_D(const CDynamicProperty);
    return d->Property.isObservable();
}

//============================================================================
CDynamicValue CDynamicProperty::defaultValue() const
{
    PIMPL_D(const CDynamicProperty);

    auto Result = CDynamicValue{nullptr, d->Property.dataType()};
    const auto DataType = d->Property.dataType();
    switch (DataType.type())
    {
    case fdl::IDataType::Type::Basic:
    case fdl::IDataType::Type::Constrained:
    case fdl::IDataType::Type::List:
        CDynamicValue::PrivateImpl::setEmptyNestedValue(d->Feature, Result);
        return Result;
    case fdl::IDataType::Type::Identifier:
        Result.setValue(d->Feature->dataType(DataType.identifier().identifier()));
        return Result;
    case fdl::IDataType::Type::Structure:
        Result.setValue(CDynamicStructure{
            DataType.structure(),
            d->Identifier.responseMessageName(d->Property.isObservable()),
            d->Identifier.identifier().toStdString(), d->DynamicMessageFactory,
            d->Feature});
        return Result;
    case fdl::IDataType::Type::Invalid:
        qCWarning(sila_cpp_client)
            << "Invalid Data Type Type for Property" << d->Identifier.toString();
        return Result;
    }
    qCCritical(sila_cpp_client)
        << "Unhandled Data Type Type" << static_cast<int>(DataType.type())
        << "for Property" << d->Identifier.toString();
    return Result;
}

//============================================================================
CClientMetadataList CDynamicProperty::metadata() const
{
    PIMPL_D(const CDynamicProperty);
    return d->Metadata;
}

//============================================================================
void CDynamicProperty::addAffectingMetadata(const CClientMetadata& Metadata)
{
    PIMPL_D(CDynamicProperty);
    d->Metadata.append(Metadata);
}

//============================================================================
CDynamicFeatureStub* CDynamicProperty::feature() const
{
    PIMPL_D(const CDynamicProperty);
    return d->Feature;
}

//============================================================================
QString CDynamicProperty::propertyFDLString() const
{
    PIMPL_D(const CDynamicProperty);
    return fdl::CFDLSerializer::serialize(d->Property);
}

//============================================================================
fdl::CProperty CDynamicProperty::propertyFDL() const
{
    PIMPL_D(const CDynamicProperty);
    return d->Property;
}

//============================================================================
CDynamicValue CDynamicProperty::get(const CClientMetadataList& Metadata)
{
    PIMPL_D(CDynamicProperty);

    if (isObservable())
    {
        auto Future = subscribe(Metadata);
        auto Result = Future.result();
        Future.cancel();
        // in case this function is executed in a thread w/o an event loop the
        // cancellation of the `Future` would not be processed, so we need this
        // local event loop
        QEventLoop EventLoop;
        EventLoop.processEvents();
        qCDebug(sila_cpp_client) << d->Identifier << "Responses:" << Result;
        return Result;
    }

    // !isObservable();
    qCInfo(sila_cpp_client) << "--- Requesting Unobservable Property"
                            << d->Identifier;

    auto Responses = d->getResponsePrototype();
    const auto Status = CDynamicCall::call(
        d->Channel, d->Identifier.toMethodName(), d->getParameterPrototype(),
        Responses, Metadata.toMultiMap());

    throwOnError(Status);
    qCDebug(sila_cpp_client) << d->Identifier << "Responses:" << *Responses;

    return d->responseToDynamicValue(Responses);
}

//============================================================================
QFuture<CDynamicValue> CDynamicProperty::subscribe(
    const CClientMetadataList& Metadata)
{
    PIMPL_D(CDynamicProperty);

    if (!isObservable())
    {
        throw logic_error{"This Property ("s + d->Identifier.toStdString() + ") "
                          + "is not Observable and cannot be subscribed to! "
                            "Use `CDynamicProperty::get()` instead."};
    }

    qCInfo(sila_cpp_client) << "--- Starting Subscription to"
                            << d->Identifier.toString();
    try
    {
        return d->Subscription->start(d->getParameterPrototype(), Metadata);
    }
    catch (const CThreadPoolMaxThreadCountReachedError& err)
    {
        throw runtime_error{
            "Cannot start Subscription to " + d->Identifier.toStdString()
            + " because there are too may Subscriptions ("
            + to_string(err.maxThreadCount()) + ") running at the same time."};
    }
}

//============================================================================
QDebug operator<<(QDebug dbg, const CDynamicProperty& rhs)
{
    QDebugStateSaver s{dbg};
    return dbg.nospace() << "CDynamicProperty(" << rhs.identifier()
                         << "\n\tMetadata: " << rhs.metadata() << ')';
}

//============================================================================
ostream& operator<<(ostream& os, const CDynamicProperty& rhs)
{
    return os << "CDynamicProperty(" << rhs.identifier().toStdString()
              << "\n\tMetadata: " << rhs.metadata() << ')';
}
}  // namespace SiLA2
