/**
 ** This file is part of the sila_cpp project.
 ** Copyright 2021 SiLA2
 **
 ** Permission is hereby granted, free of charge, to any person obtaining a copy
 ** of this software and associated documentation files (the "Software"), to deal
 ** in the Software without restriction, including without limitation the rights
 ** to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 ** copies of the Software, and to permit persons to whom the Software is
 ** furnished to do so, subject to the following conditions:
 **
 ** The above copyright notice and this permission notice shall be included in all
 ** copies or substantial portions of the Software.
 **
 ** THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 ** IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 ** FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 ** AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 ** LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 ** OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 ** SOFTWARE.
 **/

//============================================================================
/// \file   DynamicSourceTree.cpp
/// \author Florian Meinicke (florian.meinicke@cetoni.de)
/// \date   13.01.2021
/// \brief  Implementation of the CDynamicSourceTree and CFile classes
//============================================================================

//============================================================================
//                                  INCLUDES
//============================================================================
#include "DynamicSourceTree.h"

#include <sila_cpp/common/logging.h>

#include <google/protobuf/io/zero_copy_stream_impl.h>

#include <sstream>

using namespace std;
using namespace google::protobuf;

namespace SiLA2::codegen::proto
{
//============================================================================
CDynamicSourceTree::CDynamicSourceTree() = default;

//============================================================================
CDynamicSourceTree::CDynamicSourceTree(const CProtobufFile& File)
{
    m_Files.push_back(File);
}

//============================================================================
void CDynamicSourceTree::addFile(const CProtobufFile& File)
{
    m_Files.push_back(File);
}

//============================================================================
io::ZeroCopyInputStream* CDynamicSourceTree::Open(const string& Filename)
{
    io::IstreamInputStream* Stream = nullptr;
    qCDebug(sila_cpp_codegen)
        << "Trying to open proto file with name" << Filename;

    for_each(begin(m_Files), end(m_Files), [&](const auto& File) {
        if (File.fileName().toStdString() == Filename)
        {
            Stream = new io::IstreamInputStream{
                new istringstream{File.content().toStdString()}};
        }
    });

    if (!Stream)
    {
        m_LastError = "Could not find file with name \"" + Filename + "\"";
        qCDebug(sila_cpp_codegen) << m_LastError;
    }

    return Stream;
}
}  // namespace SiLA2::codegen::proto
