/**
 ** This file is part of the sila_cpp project.
 ** Copyright 2021 SiLA2
 **
 ** Permission is hereby granted, free of charge, to any person obtaining a copy
 ** of this software and associated documentation files (the "Software"), to deal
 ** in the Software without restriction, including without limitation the rights
 ** to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 ** copies of the Software, and to permit persons to whom the Software is
 ** furnished to do so, subject to the following conditions:
 **
 ** The above copyright notice and this permission notice shall be included in all
 ** copies or substantial portions of the Software.
 **
 ** THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 ** IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 ** FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 ** AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 ** LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 ** OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 ** SOFTWARE.
 **/

//============================================================================
/// \file   ErrorCollector.h
/// \author Florian Meinicke (florian.meinicke@cetoni.de)
/// \date   13.01.2021
/// \brief  Declaration of the CErrorCollector class
//============================================================================
#ifndef CODEGEN_ERRORCOLLECTOR_H
#define CODEGEN_ERRORCOLLECTOR_H

//============================================================================
//                                  INCLUDES
//============================================================================
#include <google/protobuf/compiler/importer.h>

namespace SiLA2::codegen::proto
{
/**
 * @brief The CErrorCollector class collects all errors and warnings that may
 * occur during proto file parsing
 */
class CErrorCollector : public google::protobuf::compiler::MultiFileErrorCollector
{
public:
    /**
     * @override
     * @brief Add an error/warning to the collector
     *
     * @note Line and column numbers are zero-based. A line number of -1
     * indicates  an error with the entire file (e.g. "not found").
     *
     * @param Filename The file where the error/warning occurred
     * @param Line The line number in the file where the error/warning occurred
     * @param Column The column in the line where the error/warning occurred
     * @param Message The error/warning message
     */
    void AddError(const std::string& Filename, int Line, int Column,
                  const std::string& Message) override;
    void AddWarning(const std::string& Filename, int Line, int Column,
                    const std::string& Message) override;
};
}  // namespace SiLA2::codegen::proto

#endif  // CODEGEN_ERRORCOLLECTOR_H
