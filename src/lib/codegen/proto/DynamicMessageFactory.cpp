/**
 ** This file is part of the sila_cpp project.
 ** Copyright 2021 SiLA2
 **
 ** Permission is hereby granted, free of charge, to any person obtaining a copy
 ** of this software and associated documentation files (the "Software"), to deal
 ** in the Software without restriction, including without limitation the rights
 ** to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 ** copies of the Software, and to permit persons to whom the Software is
 ** furnished to do so, subject to the following conditions:
 **
 ** The above copyright notice and this permission notice shall be included in all
 ** copies or substantial portions of the Software.
 **
 ** THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 ** IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 ** FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 ** AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 ** LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 ** OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 ** SOFTWARE.
 **/

//============================================================================
/// \file   DynamicMessageFactory.cpp
/// \author Florian Meinicke (florian.meinicke@cetoni.de)
/// \date   10.02.2021
/// \brief  Implementation of the CDynamicMessageFactory class
//============================================================================

//============================================================================
//                                  INCLUDES
//============================================================================
#include <sila_cpp/codegen/proto/DynamicMessageFactory.h>
#include <sila_cpp/common/logging.h>

#include "DynamicSourceTree.h"
#include "ErrorCollector.h"

#include <google/protobuf/dynamic_message.h>
#include <google/protobuf/service.h>

#include <set>

using namespace google::protobuf;
using namespace isocpp_p0201;
using namespace std;

namespace SiLA2::codegen::proto
{
/**
 * @brief Private implementation of the CDynamicMessageFactory class - pimpl
 */
class CDynamicMessageFactory::PrivateImpl
{
public:
    /**
     * @brief C'tor
     */
    PrivateImpl();

    /**
     * @brief D'tor
     */
    ~PrivateImpl() noexcept;

    DynamicMessageFactory* DynamicMsgFactory{};  ///< creates the dyn. Messages
    set<const FileDescriptor*>
        FileDescriptors{};  ///< Descriptors for all files; need to outlive the DMF

    CErrorCollector* ErrorCollector{};  ///< records errors of SourceTreeDB
    CDynamicSourceTree* SourceTree{};  ///< will be filled with all dyn. proto files
    compiler::SourceTreeDescriptorDatabase* SourceTreeDescriptorDB{};
    DescriptorPool*
        DynamicPool{};  ///< Pool with generated Descriptors and all dyn. Descriptors to come

    bool DescriptorsNeedRebuild{true};
};

//============================================================================
CDynamicMessageFactory::PrivateImpl::PrivateImpl()
    : DynamicMsgFactory{new DynamicMessageFactory},
      ErrorCollector{new CErrorCollector},
      SourceTree{new CDynamicSourceTree},
      SourceTreeDescriptorDB{
          new compiler::SourceTreeDescriptorDatabase{SourceTree}},
      DynamicPool{new DescriptorPool{SourceTreeDescriptorDB}}
{
    SourceTreeDescriptorDB->RecordErrorsTo(ErrorCollector);
    // NOTE: `internal_set_underlay` should only be used by protobuf internally
    //  BUT we have to use it here since a DescriptorDatabase with Descriptors
    //  from the `generated_pool` didn't work. Using the DescriptorDatabase
    //  approach resulted in rebuilding of the SiLAFramework.proto file and thus
    //  in completely new Descriptors for String, Integer, etc. This on the other
    //  hand resulted in failures when reconstructing a List of a SiLA String, for
    //  example, from SiLA Any's payload. Because the Descriptor of the SiLA
    //  String used in the List was different from the Descriptor of a SiLA String
    //  in the `generated_pool`, libprotobuf threw an error.
    DynamicPool->internal_set_underlay(DescriptorPool::generated_pool());
}

//============================================================================
CDynamicMessageFactory::PrivateImpl::~PrivateImpl() noexcept
{
    delete DynamicPool;
    delete SourceTreeDescriptorDB;
    delete SourceTree;
    delete ErrorCollector;
    delete DynamicMsgFactory;
}

///===========================================================================
CDynamicMessageFactory::CDynamicMessageFactory(PrivateImplPtr priv)
    : d_ptr{priv ? priv : make_polymorphic_value<PrivateImpl>()}
{}

//============================================================================
void CDynamicMessageFactory::addProtoFile(const CProtobufFile& Protobuf)
{
    PIMPL_D(CDynamicMessageFactory);

    d->SourceTree->addFile(Protobuf);
    d->DescriptorsNeedRebuild = true;
}

//============================================================================
void CDynamicMessageFactory::buildDescriptors()
{
    PIMPL_D(CDynamicMessageFactory);

    const auto Files = d->SourceTree->files();
    for_each(begin(Files), end(Files), [&d](const auto& File) {
        const auto FileName = File.fileName().toStdString();
        const auto Desc = d->DynamicPool->FindFileByName(FileName);
        if (!Desc)
        {
            qCCritical(sila_cpp_codegen)
                << "Couldn't build FileDescriptor for" << FileName;
            return;
        }
        d->FileDescriptors.insert(Desc);
    });
    d->DescriptorsNeedRebuild = false;
}

//============================================================================
unique_ptr<Message> CDynamicMessageFactory::getPrototype(const string& MessageName)
{
    PIMPL_D(const CDynamicMessageFactory);

    if (d->DescriptorsNeedRebuild)
    {
        buildDescriptors();
    }

    const auto* const Desc = d->DynamicPool->FindMessageTypeByName(MessageName);
    if (!Desc)
    {
        throw runtime_error{"There is no Message called \"" + MessageName + "\""};
    }
    return unique_ptr<Message>{d->DynamicMsgFactory->GetPrototype(Desc)->New()};
}

//============================================================================
unique_ptr<Message> CDynamicMessageFactory::getParameterPrototype(
    const CFullyQualifiedCommandID& CommandID)
{
    return getPrototype(CommandID.parameterMessageName());
}

//============================================================================
unique_ptr<Message> CDynamicMessageFactory::getResponsePrototype(
    const CFullyQualifiedCommandID& CommandID,
    CFullyQualifiedCommandID::RPCType Type)
{
    return getPrototype(CommandID.responseMessageName(Type));
}
}  // namespace SiLA2::codegen::proto
