/**
 ** This file is part of the sila_cpp project.
 ** Copyright 2021 SiLA2
 **
 ** Permission is hereby granted, free of charge, to any person obtaining a copy
 ** of this software and associated documentation files (the "Software"), to deal
 ** in the Software without restriction, including without limitation the rights
 ** to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 ** copies of the Software, and to permit persons to whom the Software is
 ** furnished to do so, subject to the following conditions:
 **
 ** The above copyright notice and this permission notice shall be included in all
 ** copies or substantial portions of the Software.
 **
 ** THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 ** IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 ** FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 ** AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 ** LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 ** OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 ** SOFTWARE.
 **/

//============================================================================
/// \file   ProtobufGenerator.cpp
/// \author Florian Meinicke (florian.meinicke@cetoni.de)
/// \date   05.02.2021
/// \brief  Implementation of the CProtobufGenerator class
//============================================================================

//============================================================================
//                                  INCLUDES
//============================================================================
#include <sila_cpp/codegen/fdl/DataTypeBasic.h>
#include <sila_cpp/codegen/fdl/DataTypeConstrained.h>
#include <sila_cpp/codegen/fdl/DataTypeIdentifier.h>
#include <sila_cpp/codegen/fdl/DataTypeList.h>
#include <sila_cpp/codegen/fdl/DataTypeStructure.h>
#include <sila_cpp/codegen/proto/ProtobufGenerator.h>
#include <sila_cpp/common/logging.h>

#include <cmath>

using namespace std;

namespace SiLA2::codegen::proto
{
//============================================================================
//                              Feature template
//============================================================================
static const auto PROTOBUF_PACKAGE_NAME_TEMPLATE =
    QStringLiteral("sila2.%1.%2.%3.v%4");

static const auto FEATURE_TEMPLATE = QStringLiteral(R"(
syntax = "proto3";
import "SiLAFramework.proto";
package %1;

%2
service %3 {
%4
}
%5)");

//============================================================================
//                             Command templates
//============================================================================
static const auto COMMAND_TEMPLATE = QStringLiteral(R"(
%1
    rpc %2 (%2_Parameters) returns (%2_Responses) {})");
static const auto COMMAND_INITIATION_TEMPLATE = QStringLiteral(R"(
%1
    rpc %2 (%2_Parameters) returns (.sila2.org.silastandard.CommandConfirmation) {})");
static const auto COMMAND_INFO_TEMPLATE = QStringLiteral(R"(
    rpc %1_Info (.sila2.org.silastandard.CommandExecutionUUID) returns (stream .sila2.org.silastandard.ExecutionInfo) {})");
static const auto COMMAND_INTERMEDIATE_TEMPLATE = QStringLiteral(R"(
    rpc %1_Intermediate (.sila2.org.silastandard.CommandExecutionUUID) returns (stream %1_IntermediateResponses) {})");
static const auto COMMAND_RESULT_TEMPLATE = QStringLiteral(R"(
    rpc %1_Result (.sila2.org.silastandard.CommandExecutionUUID) returns (%1_Responses) {})");

//============================================================================
//                             Property templates
//============================================================================
static const auto PROPERTY_UNOBSERVABLE_TEMPLATE = QStringLiteral(R"(
%1
    rpc Get_%2 (Get_%2_Parameters) returns (Get_%2_Responses) {})");
static const auto PROPERTY_OBSERVABLE_TEMPLATE = QStringLiteral(R"(
%1
    rpc Subscribe_%2 (Subscribe_%2_Parameters) returns (stream Subscribe_%2_Responses) {})");

//============================================================================
//                             Metadata templates
//============================================================================
static const auto METADATA_RPC_TEMPLATE = QStringLiteral(R"(
%1
    rpc Get_FCPAffectedByMetadata_%2 (Get_FCPAffectedByMetadata_%2_Parameters) returns (Get_FCPAffectedByMetadata_%2_Responses) {})");

//============================================================================
//                             Message templates
//============================================================================
static const auto MESSAGE_TEMPLATE = QStringLiteral(R"(
message %1 {
%2
})");

static const auto MESSAGE_FIELD_TEMPLATE = QStringLiteral("%1 %2 = %3;");

static const auto FCP_AFFECTED_BY_PREFIX =
    QStringLiteral("Get_FCPAffectedByMetadata_");
static const auto FCP_AFFECTED_BY_FIELD = QStringLiteral(
    "    repeated .sila2.org.silastandard.String AffectedCalls = 1;");

//============================================================================
/**
 * @brief Generate the protobuf code for all elements in @a List
 *
 * @tparam T The type of the elements to generate the code from
 * @param List A list with elements to generate the code from
 * @return The concatenated protobuf code for all elements of @a List as a pair of
 * QString where @b first is the RPC definitions and @b second is the definition
 * of necessary Messages for the RPCs
 */
template<typename T>
std::pair<QString, QString> generateAll(const QList<T>& List);

/**
 * @brief Generate the protobuf code for a Message's fields based on the elements
 * in @a List
 *
 * @tparam T The type of the elements to generate the code from (e.g. Parameters,
 * Responses, ...)
 * @param List A list with elements to generate the code from
 * @param Indent (optional) The number of spaces to indent the fields
 * @return The concatenated protobuf code for all elements of @a List translated
 * into protobuf Message fields
 */
template<typename T>
QString generateFields(const QList<T>& List,
                       size_t Indent = CProtobufGenerator::INDENT);

/**
 * @brief Generate the protobuf code for a single field based on the given
 * @a Element
 *
 * @tparam T The type of the element to generate the code from (e.g. Parameters,
 * Responses, ...)
 * @param Element The Element to generate the code from
 * @param Indent (optional) The number of spaces to indent the field
 * @param i (optional) The number of the field to generate
 * @return The protobuf code for @a Element translated into a protobuf Message
 * field
 */
template<typename T>
QString generateField(const T& Element,
                      size_t Indent = CProtobufGenerator::INDENT, size_t i = 1);

/**
 * @brief Generate the protobuf code for a single field that should have the given
 * @a Identifier as its name and the given @a DataType as its type
 *
 * @param Identifier The name of the field
 * @param DataType The type of the field
 * @param Indent (optional) The number of spaces to indent the field
 * @param i (optional) The number of the field to generate
 * @return The protobuf code for a protobuf Message field with the given
 * @a Identifier and @a DataType
 */
QString generateField(const QString& Identifier, const fdl::CDataType& DataType,
                      size_t Indent = CProtobufGenerator::INDENT, size_t i = 1);

/**
 * @brief Convert the given Data Type @a DataType into its corresponding protobuf
 * representation
 *
 * @param DataType The Data Type to convert into protobuf code
 * @param Identifier An Identifier that is used to construct an embedded message
 * for a SiLA Structure Type
 * @param Indent (optional) The number of spaces to indent the field
 * @return The protobuf code for as a pair of QString where @b first is the
 * optional definition of embedded messages and @b second is the actual protobuf
 * string for the @a DataType
 */
std::pair<QString, QString> dataTypeToProto(
    const fdl::CDataType& DataType, const QString& Identifier,
    size_t Indent = CProtobufGenerator::INDENT);

/**
 * @brief Converts the given description string into a properly formatted protobuf
 * comment
 *
 * @param Description The raw description string
 * @param Indent (optional) The number of spaces to indent each line of the
 * description
 * @return A properly formatted protobuf comment for the given @a Description
 */
QString descriptionToComment(const QString& Description,
                             size_t Indent = CProtobufGenerator::INDENT);

//============================================================================
QString CProtobufGenerator::generate(const fdl::CFeature& Feature)
{
    const auto PackageName = PROTOBUF_PACKAGE_NAME_TEMPLATE.arg(
        Feature.originator(), Feature.category(), Feature.identifier().toLower(),
        Feature.featureVersion().split('.').front());

    const auto [CommandRPCs, CommandMessages] = generateAll(Feature.commands());
    const auto [PropertyRPCs, PropertyMessages] =
        generateAll(Feature.properties());
    const auto [MetadataRPCs, MetadataMessages] = generateAll(Feature.metadata());

    const auto RPCs = CommandRPCs + PropertyRPCs + MetadataRPCs;
    const auto Messages = CommandMessages + PropertyMessages + MetadataMessages
                          + generateAll(Feature.dataTypeDefinitions()).second;

    return FEATURE_TEMPLATE.arg(PackageName)
        .arg(descriptionToComment(Feature.description(), 0), Feature.identifier(),
             RPCs, Messages);
}

//============================================================================
std::pair<QString, QString> CProtobufGenerator::generate(
    const fdl::CCommand& Command)
{
    QString RPCs;
    const auto CommandID = Command.identifier();
    const auto CommandDescription = descriptionToComment(Command.description());

    auto Messages = MESSAGE_TEMPLATE.arg(CommandID + "_Parameters",
                                         generateFields(Command.parameters()));

    if (Command.isObservable())
    {
        RPCs = COMMAND_INITIATION_TEMPLATE.arg(CommandDescription, CommandID)
               + COMMAND_INFO_TEMPLATE.arg(CommandID);
        if (!Command.intermediateResponses().isEmpty())
        {
            RPCs += COMMAND_INTERMEDIATE_TEMPLATE.arg(CommandID);
            Messages += MESSAGE_TEMPLATE.arg(
                CommandID + "_IntermediateResponses",
                generateFields(Command.intermediateResponses()));
        }
        RPCs += COMMAND_RESULT_TEMPLATE.arg(CommandID);
    }
    else
    {
        RPCs = COMMAND_TEMPLATE.arg(CommandDescription, CommandID);
    }

    Messages += MESSAGE_TEMPLATE.arg(CommandID + "_Responses",
                                     generateFields(Command.responses()));

    RPCs += "\n";
    Messages += "\n";
    return {RPCs, Messages};
}

//============================================================================
std::pair<QString, QString> CProtobufGenerator::generate(
    const fdl::CProperty& Property)
{
    QString RPC;
    QString Prefix;
    const auto PropertyDescription = descriptionToComment(Property.description());

    if (Property.isObservable())
    {
        Prefix = "Subscribe_";
        RPC = PROPERTY_OBSERVABLE_TEMPLATE.arg(PropertyDescription,
                                               Property.identifier());
    }
    else
    {
        Prefix = "Get_";
        RPC = PROPERTY_UNOBSERVABLE_TEMPLATE.arg(PropertyDescription,
                                                 Property.identifier());
    }

    RPC += "\n";

    const auto Messages =
        MESSAGE_TEMPLATE.arg(
            Prefix + Property.identifier() + "_Parameters",
            QString{static_cast<int>(INDENT), ' '} + "// Empty message")
        + MESSAGE_TEMPLATE.arg(Prefix + Property.identifier() + "_Responses",
                               generateField(Property))
        + "\n";

    return {RPC, Messages};
}

//============================================================================
std::pair<QString, QString> CProtobufGenerator::generate(
    const fdl::CMetadata& Metadata)
{
    const auto MetadataDescription = descriptionToComment(Metadata.description());
    QString RPC =
        METADATA_RPC_TEMPLATE.arg(MetadataDescription, Metadata.identifier());
    QString Messages =
        MESSAGE_TEMPLATE.arg("Metadata_" + Metadata.identifier(),
                             generateField(Metadata))
        + MESSAGE_TEMPLATE.arg(
            FCP_AFFECTED_BY_PREFIX + Metadata.identifier() + "_Parameters",
            QString{static_cast<int>(INDENT), ' '} + "// Empty message")
        + MESSAGE_TEMPLATE.arg(
            FCP_AFFECTED_BY_PREFIX + Metadata.identifier() + "_Responses",
            FCP_AFFECTED_BY_FIELD);

    RPC += "\n";
    Messages += "\n";
    return {RPC, Messages};
}

//============================================================================
QString CProtobufGenerator::generate(const fdl::CDataType& DataType)
{
    static int i = 0;
    return MESSAGE_TEMPLATE.arg(QString{"DynamicMessage%1"}.arg(++i),
                                generateField("DynamicField", DataType));
}

//============================================================================
QString CProtobufGenerator::generate(
    const fdl::CDataTypeDefinition& DataTypeDefinition)
{
    return "\n" + descriptionToComment(DataTypeDefinition.description(), 0)
           + MESSAGE_TEMPLATE.arg("DataType_" + DataTypeDefinition.identifier(),
                                  generateField(DataTypeDefinition))
           + "\n";
}

//============================================================================
QString CProtobufGenerator::generate(const fdl::CDataTypeBasic& DataType)
{
    return QString{".sila2.org.silastandard.%1"}.arg(DataType.identifier());
}

//============================================================================
pair<QString, QString> CProtobufGenerator::generate(
    const fdl::CDataTypeConstrained& DataType, const QString& Identifier)
{
    const auto [EmbeddedMessages, Protobuf] =
        dataTypeToProto(DataType.dataType(), Identifier);
    return {EmbeddedMessages, "/*constrained*/ " + Protobuf};
}

//============================================================================
QString CProtobufGenerator::generate(const fdl::CDataTypeIdentifier& DataType)
{
    return "DataType_" + DataType.identifier();
}

//============================================================================
pair<QString, QString> CProtobufGenerator::generate(
    const fdl::CDataTypeList& DataType, const QString& Identifier)
{
    const auto [EmbeddedMessages, Protobuf] =
        dataTypeToProto(DataType.dataType(), Identifier);
    return {EmbeddedMessages, "repeated " + Protobuf};
}

//============================================================================
pair<QString, QString> CProtobufGenerator::generate(
    const fdl::CDataTypeStructure& DataType, const QString& Identifier,
    size_t Indent)
{
    auto Message = MESSAGE_TEMPLATE.arg(
        Identifier + "_Struct",
        generateFields(DataType.elements(), Indent + INDENT));
    Message.insert(1, QString{static_cast<int>(Indent), ' '});
    Message.insert(Message.size() - 1, QString{static_cast<int>(Indent), ' '});
    return {Message, Identifier + "_Struct"};
}

//============================================================================
template<typename T>
std::pair<QString, QString> generateAll(const QList<T>& List)
{
    if (List.isEmpty())
    {
        return {};
    }

    static auto banner = [](const QString& Message, const QChar& Padding = '-',
                            const int Length = 80) {
        const auto PaddingLength = (Length - Message.size() - 5) / 2;
        return "// " + QString{static_cast<int>(ceil(PaddingLength)), Padding}
               + " " + Message + " "
               + QString{static_cast<int>(floor(PaddingLength)), Padding};
    };

    QString RPCs;
    QString Messages = "\n";
    if constexpr (is_same_v<T, fdl::CCommand>)
    {
        Messages += banner("Command Parameters and Responses Definitions");
    }
    else if constexpr (is_same_v<T, fdl::CProperty>)
    {
        Messages += banner("Property Parameters and Responses Definitions");
    }
    else if constexpr (is_same_v<T, fdl::CMetadata>)
    {
        Messages += banner("Metadata Definitions");
    }
    else if constexpr (is_same_v<T, fdl::CDataTypeDefinition>)
    {
        Messages += banner("Data Type Definitions");
    }

    for_each(begin(List), end(List), [&RPCs, &Messages](const auto& Element) {
        if constexpr (is_same_v<T, fdl::CDataTypeDefinition>)
        {
            Messages += CProtobufGenerator::generate(Element);
        }
        else
        {
            const auto [RPC, Message] = CProtobufGenerator::generate(Element);
            RPCs += RPC;
            Messages += Message;
        }
    });
    return {RPCs, Messages};
}

//============================================================================
template<typename T>
QString generateFields(const QList<T>& List, size_t Indent)
{
    QString Fields;
    for_each(begin(List), end(List),
             [&Fields, &Indent, i = 0U](const auto& Element) mutable {
                 Fields += generateField(Element, Indent, ++i) + "\n";
             });
    return Fields.isEmpty() ?
               QString{static_cast<int>(Indent), ' '} + "// Empty message" :
               Fields.chopped(1);  // strip final newline
}

//============================================================================
template<typename T>
QString generateField(const T& Element, size_t Indent, size_t i)
{
    return generateField(Element.identifier(), Element.dataType(), Indent, i);
}

//============================================================================
QString generateField(const QString& Identifier, const fdl::CDataType& DataType,
                      size_t Indent, size_t i)
{
    auto [EmbeddedMessages, Protobuf] =
        dataTypeToProto(DataType, Identifier, Indent);
    EmbeddedMessages += EmbeddedMessages.isEmpty() ? "" : "\n";

    return EmbeddedMessages + QString{static_cast<int>(Indent), ' '}
           + MESSAGE_FIELD_TEMPLATE.arg(Protobuf, Identifier).arg(i);
}

//============================================================================
pair<QString, QString> dataTypeToProto(const fdl::CDataType& DataType,
                                       const QString& Identifier, size_t Indent)
{
    using Type = fdl::IDataType::Type;
    switch (DataType.type())
    {
    case Type::Invalid:
        qCCritical(sila_cpp_codegen) << "Invalid Data Type Type!";
        return {"", "Invalid"};
    case Type::Basic:
        return {"", CProtobufGenerator::generate(DataType.basic())};
    case Type::Constrained:
        return CProtobufGenerator::generate(DataType.constrained(), Identifier);
    case Type::Identifier:
        return {"", CProtobufGenerator::generate(DataType.identifier())};
    case Type::List:
        return CProtobufGenerator::generate(DataType.list(), Identifier);
    case Type::Structure:
        return CProtobufGenerator::generate(DataType.structure(), Identifier,
                                            Indent);
    }
    qCWarning(sila_cpp_codegen) << "Shouldn't get here!";
    return {"", "Unknown"};
}

//============================================================================
QString descriptionToComment(const QString& Description, size_t Indent)
{
    auto Lines = Description.split('\n');
    if (Lines.front().isEmpty())
    {
        Lines.pop_front();
    }
    if (Lines.back().isEmpty())
    {
        Lines.pop_back();
    }

    transform(begin(Lines), end(Lines), begin(Lines),
              [&Indent](QString Line) -> QString {
                  Line = Line.simplified();
                  if (Line.isEmpty())
                  {
                      return "";
                  }
                  return QString{static_cast<int>(Indent), ' '} + "// " + Line;
              });
    Lines.removeAll("");
    return Lines.join('\n');
}
}  // namespace SiLA2::codegen::proto
