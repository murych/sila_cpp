/**
 ** This file is part of the sila_cpp project.
 ** Copyright 2021 SiLA2
 **
 ** Permission is hereby granted, free of charge, to any person obtaining a copy
 ** of this software and associated documentation files (the "Software"), to deal
 ** in the Software without restriction, including without limitation the rights
 ** to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 ** copies of the Software, and to permit persons to whom the Software is
 ** furnished to do so, subject to the following conditions:
 **
 ** The above copyright notice and this permission notice shall be included in all
 ** copies or substantial portions of the Software.
 **
 ** THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 ** IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 ** FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 ** AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 ** LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 ** OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 ** SOFTWARE.
 **/

//============================================================================
/// \file   ProtobufFile.cpp
/// \author Florian Meinicke (florian.meinicke@cetoni.de)
/// \date   11.02.2021
/// \brief  Implementation of the CProtobufFile class
//============================================================================

//============================================================================
//                                  INCLUDES
//============================================================================
#include <sila_cpp/codegen/proto/ProtobufFile.h>

#include <utility>

static const auto PROTO_FILE_HEADER = QStringLiteral(R"(
syntax = "proto3";
import "SiLAFramework.proto";
package sila2.org.silastandard.dynamic.dynamic%1.v1;
)");

using namespace std;

namespace SiLA2::codegen::proto
{
//============================================================================
size_t CProtobufFile::m_DynamicFileCount = 0;

//============================================================================
CProtobufFile::CProtobufFile(QString FileName, QString Content)
    : m_FileName{std::move(FileName)}, m_Content{std::move(Content)}
{}

//============================================================================
CProtobufFile CProtobufFile::fromRawProtobuf(const QString& Protobuf)
{
    auto Result =
        CProtobufFile{QString{"Dynamic%1.proto"}.arg(m_DynamicFileCount),
                      PROTO_FILE_HEADER.arg(m_DynamicFileCount) + Protobuf};
    ++m_DynamicFileCount;
    return Result;
}
}  // namespace SiLA2::codegen::proto
