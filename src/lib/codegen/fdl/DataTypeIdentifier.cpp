/**
 ** This file is part of the sila_cpp project.
 ** Copyright 2021 SiLA2
 **
 ** Permission is hereby granted, free of charge, to any person obtaining a copy
 ** of this software and associated documentation files (the "Software"), to deal
 ** in the Software without restriction, including without limitation the rights
 ** to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 ** copies of the Software, and to permit persons to whom the Software is
 ** furnished to do so, subject to the following conditions:
 **
 ** The above copyright notice and this permission notice shall be included in all
 ** copies or substantial portions of the Software.
 **
 ** THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 ** IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 ** FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 ** AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 ** LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 ** OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 ** SOFTWARE.
 **/

//============================================================================
/// \file   DataTypeIdentifier.cpp
/// \author Florian Meinicke (florian.meinicke@cetoni.de)
/// \date   28.01.2021
/// \brief  Implementation of the CDataTypeIdentifier class
//============================================================================

//============================================================================
//                                  INCLUDES
//============================================================================
#include <sila_cpp/codegen/fdl/DataTypeIdentifier.h>
#include <sila_cpp/common/logging.h>

namespace SiLA2::codegen::fdl
{
//============================================================================
QVariant CDataTypeIdentifier::toVariant() const
{
    return m_Identifier;
}

//============================================================================
void CDataTypeIdentifier::fromVariant(const QVariant& from)
{
    const auto Identifier = from.toString();
    if (Q_UNLIKELY(Identifier.isEmpty()))
    {
        throw std::runtime_error{"Failed to parse Feature Description! "
                                 "<DataTypeIdentifier> data type is empty."};
    }
    m_Identifier = Identifier;
}

//============================================================================
bool CDataTypeIdentifier::isEqual(const ISerializable& r) const
{
    const auto rhs = dynamic_cast<const CDataTypeIdentifier&>(r);
    return m_Identifier == rhs.m_Identifier;
}

//============================================================================
QDebug operator<<(QDebug dbg, const CDataTypeIdentifier& rhs)
{
    return dbg << rhs.identifier();
}

//============================================================================
std::ostream& operator<<(std::ostream& os, const CDataTypeIdentifier& rhs)
{
    return os << rhs.identifier();
}
}  // namespace SiLA2::codegen::fdl
