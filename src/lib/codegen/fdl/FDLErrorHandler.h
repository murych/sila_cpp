/**
 ** This file is part of the sila_cpp project.
 ** Copyright (c) 2021 SiLA2
 **
 ** Permission is hereby granted, free of charge, to any person obtaining a copy
 ** of this software and associated documentation files (the "Software"), to deal
 ** in the Software without restriction, including without limitation the rights
 ** to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 ** copies of the Software, and to permit persons to whom the Software is
 ** furnished to do so, subject to the following conditions:
 **
 ** The above copyright notice and this permission notice shall be included in all
 ** copies or substantial portions of the Software.
 **
 ** THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 ** IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 ** FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 ** AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 ** LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 ** OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 ** SOFTWARE.
 **/

//============================================================================
/// \file   FDLErrorHandler.h
/// \author Florian Meinicke (florian.meinicke@cetoni.de)
/// \date   24.08.2021
/// \brief  Declaration of the CFDLErrorHandler class
//============================================================================
#ifndef CODEGEN_FDL_FDLERRORHANDLER_H
#define CODEGEN_FDL_FDLERRORHANDLER_H

//============================================================================
//                                  INCLUDES
//============================================================================
#include <sila_cpp/global.h>

#include <QString>
#include <QTextStream>

#include <xercesc/sax/ErrorHandler.hpp>

namespace SiLA2::codegen::fdl
{
/**
 * @brief The CFDLErrorHandler class implements a simple error handler for the
 * Feature Definition Lange XML format
 */
class CFDLErrorHandler : public xercesc::ErrorHandler
{
public:
    /**
     * @brief C'tor
     *
     * @param FDL The Feature Definition that is being parsed and that errors are
     * reported for (this is used for constructing error messages)
     */
    explicit CFDLErrorHandler(QString FDL);

    void warning(const xercesc::SAXParseException& exc) override;
    void error(const xercesc::SAXParseException& exc) override;
    void fatalError(const xercesc::SAXParseException& exc) override;
    void resetErrors() override {}

private:
    /**
     * @brief Constructs a printable message of type @a type from @a exc
     *
     * @param exc The exception that occurred during parsing
     * @param type The type of the exception (e.g. 'warning' or 'error')
     * @return The constructed message as a string
     */
    QString makeMessage(const xercesc::SAXParseException& exc, const char* type);

    QString m_FDL;
    QTextStream m_Stream{};
    size_t m_CurrentLine{1};
};
}  // namespace SiLA2::codegen::fdl
#endif  // CODEGEN_FDL_FDLERRORHANDLER_H
