/**
 ** This file is part of the sila_cpp project.
 ** Copyright (c) 2021 SiLA2
 **
 ** Permission is hereby granted, free of charge, to any person obtaining a copy
 ** of this software and associated documentation files (the "Software"), to deal
 ** in the Software without restriction, including without limitation the rights
 ** to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 ** copies of the Software, and to permit persons to whom the Software is
 ** furnished to do so, subject to the following conditions:
 **
 ** The above copyright notice and this permission notice shall be included in all
 ** copies or substantial portions of the Software.
 **
 ** THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 ** IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 ** FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 ** AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 ** LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 ** OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 ** SOFTWARE.
 **/

//============================================================================
/// \file   ConstraintUnit.cpp
/// \author Florian Meinicke (florian.meinicke@cetoni.de)
/// \date   02.02.2021
/// \brief  Implementation of the CConstraintUnit class
//============================================================================

//============================================================================
//                                  INCLUDES
//============================================================================
#include <sila_cpp/codegen/fdl/ConstraintUnit.h>
#include <sila_cpp/common/logging.h>

#include "utils.h"

using namespace std;

namespace SiLA2::codegen::fdl
{
//============================================================================
CUnitComponent::SIUnit CUnitComponent::stringToSIUnit(const QString& String)
{
    static const QMap<QString, SIUnit> Map{
        {"Dimensionless", SIUnit::Dimensionless},
        {"Meter", SIUnit::Meter},
        {"Kilogram", SIUnit::Kilogram},
        {"Second", SIUnit::Second},
        {"Ampere", SIUnit::Ampere},
        {"Kelvin", SIUnit::Kelvin},
        {"Mole", SIUnit::Mole}};
    return Map.value(String, SIUnit::Invalid);
}

//============================================================================
QString CUnitComponent::siUnitToString(SIUnit Unit)
{
    switch (Unit)
    {
    case SIUnit::Invalid:
        return "Invalid";
    case SIUnit::Dimensionless:
        return "Dimensionless";
    case SIUnit::Meter:
        return "Meter";
    case SIUnit::Kilogram:
        return "Kilogram";
    case SIUnit::Second:
        return "Second";
    case SIUnit::Ampere:
        return "Ampere";
    case SIUnit::Kelvin:
        return "Kelvin";
    case SIUnit::Mole:
        return "Mole";
    case SIUnit::Candela:
        return "Candela";
    }
    qCCritical(sila_cpp_codegen) << "Unknown Unit Name" << static_cast<int>(Unit);
    return "";
}

//============================================================================
QVariant CUnitComponent::toVariant() const
{
    return variantMultiMap({{"SIUnit", siUnitToString(m_SIUnit)},
                            {"Exponent", QVariant::fromValue(m_Exponent)}});
}

//============================================================================
void CUnitComponent::fromVariant(const QVariant& from)
{
    const auto Map = from.value<QVariantMultiMap>();
    //    qCDebug(sila_cpp_codegen) << Map;
    if (Q_UNLIKELY(Map.empty()))
    {
        throw runtime_error{"Failed to parse Feature Description! <Unit> "
                            "constraint is empty."};
    }
    m_SIUnit = stringToSIUnit(Map.value("SIUnit").toString());
    m_Exponent = Map.value("Exponent").toInt();
}

//============================================================================
bool CUnitComponent::isEqual(const ISerializable& r) const
{
    const auto rhs = dynamic_cast<const CUnitComponent&>(r);
    return m_SIUnit == rhs.m_SIUnit && m_Exponent == rhs.m_Exponent;
}

//============================================================================
QVariant CConstraintUnit::toVariant() const
{
    QVariantMultiMap Map{{"Label", m_Label},
                         {"Factor", m_ConversionFactor},
                         {"Offset", m_ConversionOffset}};
    Map.unite(toReverseVariantMaps("UnitComponent", m_SIBaseUnit));
    return QVariant::fromValue(Map);
}

//============================================================================
void CConstraintUnit::fromVariant(const QVariant& from)
{
    qCDebug(sila_cpp_codegen) << from;
    const auto Map = from.value<QVariantMultiMap>();
    qCDebug(sila_cpp_codegen) << Map;
    m_Label = Map.value("Label").toString();
    m_ConversionFactor = Map.value("Factor").toReal();
    m_ConversionOffset = Map.value("Offset").toReal();
    fromReverseVariantList(Map.values("UnitComponent"), m_SIBaseUnit);
}

//============================================================================
bool CConstraintUnit::isEqual(const ISerializable& r) const
{
    const auto rhs = dynamic_cast<const CConstraintUnit&>(r);
    return m_Label == rhs.m_Label && m_ConversionFactor == rhs.m_ConversionFactor
           && m_ConversionOffset == rhs.m_ConversionOffset
           && m_SIBaseUnit == rhs.m_SIBaseUnit;
}

//============================================================================
QDebug operator<<(QDebug dbg, const CUnitComponent& rhs)
{
    using SiLA2::codegen::fdl::CUnitComponent;
    QDebugStateSaver s{dbg};
    return dbg.nospace()
           << "SI Unit: " << CUnitComponent::siUnitToString(rhs.siUnit())
           << ", Exponent: " << rhs.exponent();
}

//============================================================================
std::ostream& operator<<(std::ostream& os, const CUnitComponent& rhs)
{
    using SiLA2::codegen::fdl::CUnitComponent;
    return os << "SI Unit: " << CUnitComponent::siUnitToString(rhs.siUnit())
              << ", Exponent: " << rhs.exponent();
}

//============================================================================
QDebug operator<<(QDebug dbg, const CConstraintUnit& rhs)
{
    QDebugStateSaver s{dbg};
    return dbg.nospace() << rhs.label() << ", Factor: " << rhs.conversionFactor()
                         << ", Offset: " << rhs.conversionOffset()
                         << ", SI Base Unit: " << rhs.siBaseUnit();
}

//============================================================================
std::ostream& operator<<(std::ostream& os, const CConstraintUnit& rhs)
{
    return os << rhs.label() << ", Factor: " << rhs.conversionFactor()
              << ", Offset: " << rhs.conversionOffset()
              << ", SI Base Unit: " << rhs.siBaseUnit();
}
}  // namespace SiLA2::codegen::fdl
