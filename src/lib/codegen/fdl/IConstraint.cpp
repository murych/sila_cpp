/**
 ** This file is part of the sila_cpp project.
 ** Copyright (c) 2021 SiLA2
 **
 ** Permission is hereby granted, free of charge, to any person obtaining a copy
 ** of this software and associated documentation files (the "Software"), to deal
 ** in the Software without restriction, including without limitation the rights
 ** to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 ** copies of the Software, and to permit persons to whom the Software is
 ** furnished to do so, subject to the following conditions:
 **
 ** The above copyright notice and this permission notice shall be included in all
 ** copies or substantial portions of the Software.
 **
 ** THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 ** IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 ** FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 ** AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 ** LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 ** OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 ** SOFTWARE.
 **/

//============================================================================
/// \file   IConstraint.cpp
/// \author Florian Meinicke (florian.meinicke@cetoni.de)
/// \date   29.01.2021
/// \brief  Implementation of the IConstraint class
//============================================================================

//============================================================================
//                                  INCLUDES
//============================================================================
#include <sila_cpp/codegen/fdl/IConstraint.h>
#include <sila_cpp/common/logging.h>

namespace SiLA2::codegen::fdl
{
//============================================================================
QString IConstraint::typeToString(Type t)
{
    switch (t)
    {
    case Type::Invalid:
        return "Invalid";
    case Type::Length:
        return "Length";
    case Type::MinimalLength:
        return "MinimalLength";
    case Type::MaximalLength:
        return "MaximalLength";
    case Type::Set:
        return "Set";
    case Type::Pattern:
        return "Pattern";
    case Type::MaximalExclusive:
        return "MaximalExclusive";
    case Type::MaximalInclusive:
        return "MaximalInclusive";
    case Type::MinimalExclusive:
        return "MinimalExclusive";
    case Type::MinimalInclusive:
        return "MinimalInclusive";
    case Type::Unit:
        return "Unit";
    case Type::ContentType:
        return "ContentType";
    case Type::ElementCount:
        return "ElementCount";
    case Type::MinimalElementCount:
        return "MinimalElementCount";
    case Type::MaximalElementCount:
        return "MaximalElementCount";
    case Type::FullyQualifiedIdentifier:
        return "FullyQualifiedIdentifier";
    case Type::Schema:
        return "Schema";
    case Type::AllowedTypes:
        return "AllowedTypes";
    }
    qCCritical(sila_cpp_codegen) << "Shouldn't get here!";
    return "Unknown";
}

//============================================================================
IConstraint::Type IConstraint::stringToType(const QString& String)
{
    static QHash<QString, Type> Map{
        {"Length", Type::Length},
        {"MinimalLength", Type::MinimalLength},
        {"MaximalLength", Type::MaximalLength},
        {"Set", Type::Set},
        {"Pattern", Type::Pattern},
        {"MaximalExclusive", Type::MaximalExclusive},
        {"MaximalInclusive", Type::MaximalInclusive},
        {"MinimalExclusive", Type::MinimalExclusive},
        {"MinimalInclusive", Type::MinimalInclusive},
        {"Unit", Type::Unit},
        {"ContentType", Type::ContentType},
        {"ElementCount", Type::ElementCount},
        {"MinimalElementCount", Type::MinimalElementCount},
        {"MaximalElementCount", Type::MaximalElementCount},
        {"FullyQualifiedIdentifier", Type::FullyQualifiedIdentifier},
        {"Schema", Type::Schema},
        {"AllowedTypes", Type::AllowedTypes}};
    if (!Map.contains(String))
    {
        qCCritical(sila_cpp_codegen) << "Unknown Constraint Type" << String;
    }
    return Map.value(String);
}

//============================================================================
QDebug operator<<(QDebug dbg, const IConstraint::Type& rhs)
{
    return dbg << SiLA2::codegen::fdl::IConstraint::typeToString(rhs);
}

//============================================================================
std::ostream& operator<<(std::ostream& os, const IConstraint::Type& rhs)
{
    return os << SiLA2::codegen::fdl::IConstraint::typeToString(rhs);
}
}  // namespace SiLA2::codegen::fdl
