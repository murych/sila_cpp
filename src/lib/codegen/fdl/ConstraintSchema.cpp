/**
 ** This file is part of the sila_cpp project.
 ** Copyright (c) 2021 SiLA2
 **
 ** Permission is hereby granted, free of charge, to any person obtaining a copy
 ** of this software and associated documentation files (the "Software"), to deal
 ** in the Software without restriction, including without limitation the rights
 ** to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 ** copies of the Software, and to permit persons to whom the Software is
 ** furnished to do so, subject to the following conditions:
 **
 ** The above copyright notice and this permission notice shall be included in all
 ** copies or substantial portions of the Software.
 **
 ** THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 ** IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 ** FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 ** AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 ** LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 ** OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 ** SOFTWARE.
 **/

//============================================================================
/// \file   ConstraintSchema.cpp
/// \author Florian Meinicke (florian.meinicke@cetoni.de)
/// \date   02.02.2021
/// \brief  Implementation of the CConstraintSchema class
//============================================================================

//============================================================================
//                                  INCLUDES
//============================================================================
#include <sila_cpp/codegen/fdl/ConstraintSchema.h>
#include <sila_cpp/common/logging.h>

#include "utils.h"

using namespace std;

namespace SiLA2::codegen::fdl
{
//============================================================================
QVariant CConstraintSchema::toVariant() const
{
    // TODO check again
    QVariantMultiMap Map{{"Type", schemaTypeToString(m_SchemaType)}};
    Map.insert(isURL() ? "Url" : "Inline", m_Schema);
    return QVariant::fromValue(Map);
}

//============================================================================
void CConstraintSchema::fromVariant(const QVariant& from)
{
    const auto Map = from.value<QVariantMultiMap>();
    //    qCDebug(sila_cpp_codegen) << Map;
    if (Q_UNLIKELY(Map.empty()))
    {
        throw std::runtime_error{"Failed to parse Feature Description! <Schema> "
                                 "constraint is empty."};
    }
    m_SchemaType = stringToSchemaType(Map.value("Type").toString());
    if (Map.contains("Url"))
    {
        m_SchemaLocation = SchemaLocation::Url;
        m_Schema = Map.value("Url").toString();
    }
    else if (Map.contains("Inline"))
    {
        m_SchemaLocation = SchemaLocation::Inline;
        m_Schema = Map.value("Inline").toString();
    }
}

//============================================================================
CConstraintSchema::SchemaType CConstraintSchema::stringToSchemaType(
    const QString& String)
{
    static const QMap<QString, SchemaType> Map{{"Xml", SchemaType::Xml},
                                               {"Json", SchemaType::Json}};
    return Map.value(String, SchemaType::Invalid);
}

//============================================================================
QString CConstraintSchema::schemaTypeToString(SchemaType Type)
{
    switch (Type)
    {
    case SchemaType::Invalid:
        return "Invalid";
    case SchemaType::Xml:
        return "Xml";
    case SchemaType ::Json:
        return "Json";
    }
    qCCritical(sila_cpp_codegen)
        << "Unknown Schema Type" << static_cast<int>(Type);
    return "";
}

//============================================================================
bool CConstraintSchema::isEqual(const ISerializable& r) const
{
    const auto rhs = dynamic_cast<const CConstraintSchema&>(r);
    return m_SchemaType == rhs.m_SchemaType
           && m_SchemaLocation == rhs.m_SchemaLocation
           && m_Schema == rhs.m_Schema;
}

//============================================================================
QDebug operator<<(QDebug dbg, const CConstraintSchema& rhs)
{
    using SiLA2::codegen::fdl::CConstraintSchema;
    QDebugStateSaver s{dbg};
    dbg.nospace()
        << "Type: " << CConstraintSchema::schemaTypeToString(rhs.schemaType())
        << ", ";
    if (rhs.isURL())
    {
        dbg << "URL: " << rhs.url();
    }
    else if (rhs.isInline())
    {
        dbg << "Inline: " << rhs.content();
    }
    return dbg;
}

//============================================================================
std::ostream& operator<<(std::ostream& os, const CConstraintSchema& rhs)
{
    using SiLA2::codegen::fdl::CConstraintSchema;
    os << "Type: " << CConstraintSchema::schemaTypeToString(rhs.schemaType())
       << ", ";
    if (rhs.isURL())
    {
        os << "URL: " << rhs.url();
    }
    else if (rhs.isInline())
    {
        os << "Inline: " << rhs.content();
    }
    return os;
}
}  // namespace SiLA2::codegen::fdl
