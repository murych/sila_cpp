/**
 ** This file is part of the sila_cpp project.
 ** Copyright 2021 SiLA2
 **
 ** Permission is hereby granted, free of charge, to any person obtaining a copy
 ** of this software and associated documentation files (the "Software"), to deal
 ** in the Software without restriction, including without limitation the rights
 ** to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 ** copies of the Software, and to permit persons to whom the Software is
 ** furnished to do so, subject to the following conditions:
 **
 ** The above copyright notice and this permission notice shall be included in all
 ** copies or substantial portions of the Software.
 **
 ** THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 ** IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 ** FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 ** AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 ** LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 ** OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 ** SOFTWARE.
 **/

//============================================================================
/// \file   utils.h
/// \author Florian Meinicke (florian.meinicke@cetoni.de)
/// \date   28.01.2021
/// \brief  Definition of internal utility functions for FDL serialization
//============================================================================
#ifndef CODEGEN_UTILS_INTERNAL_H
#define CODEGEN_UTILS_INTERNAL_H

//============================================================================
//                                  INCLUDES
//============================================================================
#include <sila_cpp/codegen/fdl/utils.h>
#include <sila_cpp/global.h>

//============================================================================
/**
 * @brief Converts the elements in the given list @a List from their actual type
 * indicated by @a T into their @c QVariant representation
 *
 * @tparam T The type of the input list element
 * @param List A @c QList of objects that should each be converted to a
 * @c QVariant
 * @return A @c QList of @c QVariant representations of the given input elements
 */
template<typename T>
QVariantList toVariantList(const QList<T>& List)
{
    // qCDebug(sila_cpp_codegen) << "toVariantList" << List;
    QVariantList Result;
    Result.reserve(List.size());
    std::transform(std::begin(List), std::end(List), std::back_inserter(Result),
                   [](const auto& Element) { return Element.toVariant(); });
    return Result;
}

/**
 * @brief Converts the elements in the given list @a List from their actual type
 * indicated by @a T into their @c QVariant representation and puts them into a
 * @c QVariantMap using the given @a Key for all elements
 *
 * @tparam T The type of the input list element
 *
 * @param Key The key used to insert each element of @a List into the resulting
 * map
 * @param List A @c QList of objects that should each be converted to a
 * @c QVariant
 * @return A @c QMap of @c QVariant representations of the given input elements
 */
template<typename T>
QVariantMultiMap toVariantMaps(const QString& Key, const QList<T>& List)
{
    // qCDebug(sila_cpp_codegen) << "toVariantMaps" << List;
    QVariantMultiMap Result;
    std::for_each(std::begin(List), std::end(List), [&](const auto& Element) {
        Result.insert(Key, Element.toVariant());
    });
    return Result;
}

/**
 * @brief Same as @c toVariantMaps() except that the given list @a ReverseList
 * is expected to be in reverse order. I.e. the element coming first in
 * @a ReverseList should end up as the last element
 *
 * @tparam T The type of the input list element
 *
 * @param Key The key used to insert each element of @a List into the resulting
 * map
 * @param ReverseList A @c QList of objects that is to be reversed first
 * @return A @c QMap of @c QVariant representations of the given input elements
 *
 * @note See @c fromReverseVariantList() for an explanation why we need to reverse
 *   the list
 */
template<typename T>
QVariantMultiMap toReverseVariantMaps(const QString& Key, QList<T> ReverseList)
{
    // qCDebug(sila_cpp_codegen) << "toReverseVariantMaps" << ReverseList;
    std::reverse(std::begin(ReverseList), std::end(ReverseList));
    return toVariantMaps(Key, ReverseList);
}

//============================================================================
/**
 * @brief Converts the elements in the given list @a Values from their @c QVariant
 * representation into their actual type indicated by @a T
 *
 * @tparam T The type of the target list element
 * @param Values A @c QList of @c QVariant elements
 * @param List A @c QList of the correct type to convert each input element to
 */
template<typename T>
void fromVariantList(const QVariantList& Values, QList<T>& List)
{
    // qCDebug(sila_cpp_codegen) << "fromVariantList" << Values;
    List.reserve(Values.size());
    std::transform(std::begin(Values), std::end(Values), std::back_inserter(List),
                   [](const auto& Variant) {
                       T Element;
                       Element.fromVariant(Variant);
                       return Element;
                   });
}

/**
 * @brief Same as @c fromVariantList() except that the given list @a ReverseValues
 * is expected to be in reverse order. I.e. the element coming first in
 * @a ReverseValues should end up as the last element in the target list @a List
 *
 * @tparam T The type of the target list element
 * @param ReverseValues A @c QList of @c QVariant elements that is to be reversed
 * first
 * @param List A @c QList of the correct type to convert each input element to
 *
 * @note This method is used for the lists that are returned by
 *   @c QMultiMap::values. Those lists order their items from the most recently
 *   inserted to the least recently inserted one. This means that an item that was
 *   read first from an XML input stream is the last item in the list. Because we
 *   want to retain the declaration order of the items from the XML we have to
 *   reverse the list we get from @c QMultiMap::values so that the first inserted
 *   element is also the first element to be inserted in the target list @a List.
 */
template<typename T>
void fromReverseVariantList(QVariantList ReverseValues, QList<T>& List)
{
    // qCDebug(sila_cpp_codegen) << "fromReverseVariantList" << ReverseValues;
    std::reverse(std::begin(ReverseValues), std::end(ReverseValues));
    fromVariantList(ReverseValues, List);
}
#endif  // CODEGEN_UTILS_INTERNAL_H
