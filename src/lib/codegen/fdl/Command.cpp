/**
 ** This file is part of the sila_cpp project.
 ** Copyright (c) 2021 SiLA2
 **
 ** Permission is hereby granted, free of charge, to any person obtaining a copy
 ** of this software and associated documentation files (the "Software"), to deal
 ** in the Software without restriction, including without limitation the rights
 ** to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 ** copies of the Software, and to permit persons to whom the Software is
 ** furnished to do so, subject to the following conditions:
 **
 ** The above copyright notice and this permission notice shall be included in all
 ** copies or substantial portions of the Software.
 **
 ** THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 ** IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 ** FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 ** AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 ** LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 ** OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 ** SOFTWARE.
 **/

//============================================================================
/// \file   Command.cpp
/// \author Florian Meinicke (florian.meinicke@cetoni.de)
/// \date   26.01.2021
/// \brief  Implementation of the CCommand class
//============================================================================

//============================================================================
//                                  INCLUDES
//============================================================================
#include <sila_cpp/codegen/fdl/Command.h>
#include <sila_cpp/common/logging.h>

#include "utils.h"

using namespace std;

namespace SiLA2::codegen::fdl
{
//============================================================================
QVariant CCommand::toVariant() const
{
    auto Map = CSiLAElementWithErrors::toVariant().value<QVariantMultiMap>();
    Map.insert("Observable", m_Observable ? "Yes" : "No");
    Map.unite(toReverseVariantMaps("Parameter", m_Parameters));
    Map.unite(toReverseVariantMaps("Response", m_Responses));
    if (isObservable())
    {
        Map.unite(toReverseVariantMaps("IntermediateResponse",
                                       m_IntermediateResponses));
    }
    else if (!Map.values("IntermediateResponse").isEmpty())
    {
        qCWarning(sila_cpp_codegen)
            << "Only Observable Commands can have Intermediate Responses. "
               "Dropping all Intermediate Responses!";
    }
    return QVariant::fromValue(Map);
}

//============================================================================
void CCommand::fromVariant(const QVariant& from)
{
    CSiLAElementWithErrors::fromVariant(from);
    const auto Map = from.value<QVariantMultiMap>();
    m_Observable = (Map.value("Observable").toString() == "Yes");
    fromReverseVariantList(Map.values("Parameter"), m_Parameters);
    fromReverseVariantList(Map.values("Response"), m_Responses);
    if (isObservable())
    {
        fromReverseVariantList(Map.values("IntermediateResponse"),
                               m_IntermediateResponses);
    }
    else if (!Map.values("IntermediateResponse").isEmpty())
    {
        qCWarning(sila_cpp_codegen)
            << "Only Observable Commands can have Intermediate "
               "Responses. Dropping all Intermediate Responses!";
    }
}

//============================================================================
bool CCommand::isEqual(const ISerializable& r) const
{
    const auto rhs = dynamic_cast<const CCommand&>(r);
    return CSiLAElementWithErrors::isEqual(rhs)
           && m_Observable == rhs.m_Observable && m_Parameters == rhs.m_Parameters
           && m_Responses == rhs.m_Responses
           && m_IntermediateResponses == rhs.m_IntermediateResponses;
}

//============================================================================
QDebug operator<<(QDebug dbg, const CCommand& rhs)
{
    QDebugStateSaver s{dbg};
    return dbg.nospace()
           << "\n\t\tCCommand(" << rhs.identifier() << ':'
           << "\n\t\t\tObservable: " << (rhs.isObservable() ? "Yes" : "No")
           << "\n\t\t\tParameters: " << rhs.parameters()
           << "\n\t\t\tResponses: " << rhs.responses()
           << "\n\t\t\tIntermediate Responses: " << rhs.intermediateResponses()
           << "\n\t\t\tErrors: " << rhs.errors() << ')';
}

//============================================================================
ostream& operator<<(ostream& os, const CCommand& rhs)
{
    return os << "\n\t\tCCommand(" << rhs.identifier() << ':'
              << "\n\t\t\tObservable: " << (rhs.isObservable() ? "Yes" : "No")
              << "\n\t\t\tParameters: " << rhs.parameters()
              << "\n\t\t\tResponses: " << rhs.responses()
              << "\n\t\t\tIntermediate Responses: " << rhs.intermediateResponses()
              << "\n\t\t\tErrors: " << rhs.errors() << ')';
}
}  // namespace SiLA2::codegen::fdl
