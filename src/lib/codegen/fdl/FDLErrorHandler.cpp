/**
 ** This file is part of the sila_cpp project.
 ** Copyright (c) 2021 SiLA2
 **
 ** Permission is hereby granted, free of charge, to any person obtaining a copy
 ** of this software and associated documentation files (the "Software"), to deal
 ** in the Software without restriction, including without limitation the rights
 ** to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 ** copies of the Software, and to permit persons to whom the Software is
 ** furnished to do so, subject to the following conditions:
 **
 ** The above copyright notice and this permission notice shall be included in all
 ** copies or substantial portions of the Software.
 **
 ** THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 ** IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 ** FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 ** AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 ** LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 ** OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 ** SOFTWARE.
 **/

//============================================================================
/// \file   FDLErrorHandler.cpp
/// \author Florian Meinicke (florian.meinicke@cetoni.de)
/// \date   24.08.2021
/// \brief  Implementation of the CFDLErrorHandler class
//============================================================================

//============================================================================
//                                  INCLUDES
//============================================================================
#include "FDLErrorHandler.h"

#include <sila_cpp/common/logging.h>

#include <xercesc/sax/SAXParseException.hpp>

using namespace std;

namespace SiLA2::codegen::fdl
{
//============================================================================
CFDLErrorHandler::CFDLErrorHandler(QString FDL)
    : m_FDL{std::move(FDL)}, m_Stream{&m_FDL}
{}

//============================================================================
void CFDLErrorHandler::warning(const xercesc::SAXParseException& exc)
{
    qCWarning(sila_cpp_codegen).noquote() << makeMessage(exc, "WARNING");
}

//============================================================================
void CFDLErrorHandler::error(const xercesc::SAXParseException& exc)
{
    qCCritical(sila_cpp_codegen).noquote() << makeMessage(exc, "ERROR");
}

//============================================================================
void CFDLErrorHandler::fatalError(const xercesc::SAXParseException& exc)
{
    qCCritical(sila_cpp_codegen).noquote() << makeMessage(exc, "FATAL ERROR");
}

//============================================================================
QString CFDLErrorHandler::makeMessage(const xercesc::SAXParseException& exc,
                                      const char* type)
{
    QString Result;
    QTextStream Stream{&Result};
    const auto LineNumber = exc.getLineNumber();
    Stream << xercesc::XMLString::transcode(exc.getSystemId()) << ':'
           << LineNumber << ':' << exc.getColumnNumber() << ':' << ' ' << type
           << ':' << ' ' << xercesc::XMLString::transcode(exc.getMessage())
           << '\n';

    for (auto i = m_CurrentLine; i < LineNumber; ++i, ++m_CurrentLine)
    {
        Q_UNUSED(m_Stream.readLine())
    }
    const auto Pos = m_Stream.pos();
    const auto Line = m_Stream.readLine();
    m_Stream.seek(Pos);  // reset to previous position because there might be
                         // multiple errors in one line
    const auto Padding = QString{Line.indexOf(Line.trimmed().at(0)), ' '};
    const auto Tildes = QString{Line.size() - 1 - Padding.size(), '~'};

    Stream.setFieldWidth(5);
    Stream.setFieldAlignment(QTextStream::AlignRight);
    Stream << LineNumber;
    Stream.setFieldWidth(0);
    Stream.setFieldAlignment(QTextStream::AlignLeft);
    Stream << ' ' << '|' << ' ' << Line << '\n'
           << ' ' << ' ' << ' ' << ' ' << ' ' << ' ' << '|' << ' ' << Padding
           << '^' << Tildes;

    return Result;
}
}  // namespace SiLA2::codegen::fdl
