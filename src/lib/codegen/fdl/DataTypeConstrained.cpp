/**
 ** This file is part of the sila_cpp project.
 ** Copyright 2021 SiLA2
 **
 ** Permission is hereby granted, free of charge, to any person obtaining a copy
 ** of this software and associated documentation files (the "Software"), to deal
 ** in the Software without restriction, including without limitation the rights
 ** to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 ** copies of the Software, and to permit persons to whom the Software is
 ** furnished to do so, subject to the following conditions:
 **
 ** The above copyright notice and this permission notice shall be included in all
 ** copies or substantial portions of the Software.
 **
 ** THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 ** IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 ** FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 ** AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 ** LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 ** OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 ** SOFTWARE.
 **/

//============================================================================
/// \file   DataTypeConstrained.cpp
/// \author Florian Meinicke (florian.meinicke@cetoni.de)
/// \date   28.01.2021
/// \brief  Implementation of the CDataTypeConstrained class
//============================================================================

//============================================================================
//                                  INCLUDES
//============================================================================
#include <sila_cpp/codegen/fdl/DataTypeBasic.h>
#include <sila_cpp/codegen/fdl/DataTypeConstrained.h>
#include <sila_cpp/common/logging.h>

#include "utils.h"

namespace SiLA2::codegen::fdl
{
//============================================================================
const QHash<QString, QList<CConstraint::Type>>
    CDataTypeConstrained::m_PossibleConstraints{
        {"String",
         {CConstraint::Type::Length, CConstraint::Type::MinimalLength,
          CConstraint::Type::MaximalLength, CConstraint::Type::Set,
          CConstraint::Type::Pattern, CConstraint::Type::ContentType,
          CConstraint::Type::FullyQualifiedIdentifier,
          CConstraint::Type::Schema}},
        {"Integer",
         {CConstraint::Type::Set, CConstraint::Type::MaximalExclusive,
          CConstraint::Type::MaximalInclusive,
          CConstraint::Type::MinimalExclusive,
          CConstraint::Type::MinimalInclusive, CConstraint::Type::Unit}},
        {"Real",
         {CConstraint::Type::Set, CConstraint::Type::MaximalExclusive,
          CConstraint::Type::MaximalInclusive,
          CConstraint::Type::MinimalExclusive,
          CConstraint::Type::MinimalInclusive, CConstraint::Type::Unit}},
        {"Binary",
         {CConstraint::Type::Length, CConstraint::Type::MinimalLength,
          CConstraint::Type::MaximalLength, CConstraint::Type::ContentType,
          CConstraint::Type::Schema}},
        {"Date",
         {CConstraint::Type::Set, CConstraint::Type::MaximalExclusive,
          CConstraint::Type::MaximalInclusive,
          CConstraint::Type::MinimalExclusive,
          CConstraint::Type::MinimalInclusive}},
        {"Time",
         {CConstraint::Type::Set, CConstraint::Type::MaximalExclusive,
          CConstraint::Type::MaximalInclusive,
          CConstraint::Type::MinimalExclusive,
          CConstraint::Type::MinimalInclusive}},
        {"Timestamp",
         {CConstraint::Type::Set, CConstraint::Type::MaximalExclusive,
          CConstraint::Type::MaximalInclusive,
          CConstraint::Type::MinimalExclusive,
          CConstraint::Type::MinimalInclusive}},
        {"Any", {CConstraint::Type::AllowedTypes}},
        {"List",
         {CConstraint::Type::ElementCount, CConstraint::Type::MinimalElementCount,
          CConstraint::Type::MaximalElementCount}}};

//============================================================================
QVariant CDataTypeConstrained::toVariant() const
{
    using std::for_each, std::cbegin, std::cend;
    QVariantMultiMap Constraints;
    for_each(
        cbegin(m_Constraints), cend(m_Constraints),
        [&Constraints](const CConstraint& Constraint) {
            Constraints.unite(Constraint.toVariant().value<QVariantMultiMap>());
        });
    return variantMultiMap({{"DataType", m_DataType.toVariant()},
                            {"Constraints", QVariant::fromValue(Constraints)}});
}

//============================================================================
void CDataTypeConstrained::fromVariant(const QVariant& from)
{
    const auto Map = from.value<QVariantMultiMap>();
    if (Q_UNLIKELY(Map.empty()))
    {
        throw std::runtime_error{"Failed to parse Feature Description! "
                                 "<Constrained> data type is empty."};
    }
    m_DataType.fromVariant(Map.value("DataType"));

    const auto ConstraintMap = Map.value("Constraints").value<QVariantMultiMap>();
    foreach (const auto& ConstraintKey, ConstraintMap.keys())
    {
        CConstraint Constraint;
        Constraint.fromVariant(variantMultiMap(
            {{ConstraintKey, ConstraintMap.value(ConstraintKey)}}));
        if (m_DataType.type() == CDataType::Type::Basic)
        {
            if (m_PossibleConstraints.value(m_DataType.basic().identifier())
                    .contains(Constraint.type()))
            {
                m_Constraints.push_front(Constraint);
            }
            else
            {
                qCWarning(sila_cpp_codegen).nospace()
                    << "Invalid Constraint " << Constraint.type()
                    << " for SiLA Basic Type " << m_DataType.basic().identifier()
                    << ". Skipping this Constraint!";
            }
        }
        else if (m_DataType.type() == CDataType::Type::List)
        {
            if (m_PossibleConstraints.value("List").contains(Constraint.type()))
            {
                m_Constraints.push_front(Constraint);
            }
            else
            {
                qCWarning(sila_cpp_codegen)
                    << "Invalid Constraint" << Constraint.type()
                    << "for SiLA List Type. Skipping this Constraint!";
            }
        }
        else
        {
            qCWarning(sila_cpp_codegen)
                << "Only SiLA Basic Types and SiLA List Types can be "
                   "constrained. Skipping all Constraints!";
            return;
        }
    }
}

//============================================================================
bool CDataTypeConstrained::isEqual(const ISerializable& r) const
{
    const auto rhs = dynamic_cast<const CDataTypeConstrained&>(r);
    return m_Constraints == rhs.m_Constraints && m_DataType == rhs.m_DataType;
}

//============================================================================
QDebug operator<<(QDebug dbg, const CDataTypeConstrained& rhs)
{
    QDebugStateSaver s{dbg};
    return dbg.nospace() << "Constraints: " << rhs.constraints() << ", "
                         << rhs.dataType();
}

//============================================================================
std::ostream& operator<<(std::ostream& os, const CDataTypeConstrained& rhs)
{
    return os << "Constraints: " << rhs.constraints() << ", " << rhs.dataType();
}
}  // namespace SiLA2::codegen::fdl
