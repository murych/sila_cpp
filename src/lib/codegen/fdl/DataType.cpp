/**
 ** This file is part of the sila_cpp project.
 ** Copyright (c) 2021 SiLA2
 **
 ** Permission is hereby granted, free of charge, to any person obtaining a copy
 ** of this software and associated documentation files (the "Software"), to deal
 ** in the Software without restriction, including without limitation the rights
 ** to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 ** copies of the Software, and to permit persons to whom the Software is
 ** furnished to do so, subject to the following conditions:
 **
 ** The above copyright notice and this permission notice shall be included in all
 ** copies or substantial portions of the Software.
 **
 ** THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 ** IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 ** FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 ** AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 ** LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 ** OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 ** SOFTWARE.
 **/

//============================================================================
/// \file   DataType.cpp
/// \author Florian Meinicke (florian.meinicke@cetoni.de)
/// \date   27.01.2021
/// \brief  Implementation of the CDataType class
//============================================================================

//============================================================================
//                                  INCLUDES
//============================================================================
#include <sila_cpp/codegen/fdl/DataType.h>
#include <sila_cpp/codegen/fdl/DataTypeBasic.h>
#include <sila_cpp/codegen/fdl/DataTypeConstrained.h>
#include <sila_cpp/codegen/fdl/DataTypeIdentifier.h>
#include <sila_cpp/codegen/fdl/DataTypeList.h>
#include <sila_cpp/codegen/fdl/DataTypeStructure.h>
#include <sila_cpp/common/logging.h>

#include "utils.h"

namespace SiLA2::codegen::fdl
{
//============================================================================
QVariant CDataType::toVariant() const
{
    switch (m_Type)
    {
    case Type::Basic:
        return variantMultiMap(
            {{typeToString(m_Type),
              dynamic_cast<CDataTypeBasic*>(dataTypeSafe())->toVariant()}});
    case Type::Constrained:
        return variantMultiMap(
            {{typeToString(m_Type),
              dynamic_cast<CDataTypeConstrained*>(dataTypeSafe())->toVariant()}});
    case Type::Identifier:
        return variantMultiMap(
            {{typeToString(m_Type),
              dynamic_cast<CDataTypeIdentifier*>(dataTypeSafe())->toVariant()}});
    case Type::List:
        return variantMultiMap(
            {{typeToString(m_Type),
              dynamic_cast<CDataTypeList*>(dataTypeSafe())->toVariant()}});
    case Type::Structure:
        return variantMultiMap(
            {{typeToString(m_Type),
              dynamic_cast<CDataTypeStructure*>(dataTypeSafe())->toVariant()}});
    case Type::Invalid:
        qCCritical(sila_cpp_codegen) << "Invalid Data Type Type!";
        return {};
    }
    qCCritical(sila_cpp_codegen) << "Shouldn't get here!";
    return {};
}

//============================================================================
void CDataType::fromVariant(const QVariant& from)
{
    // qCDebug(sila_cpp_codegen) << "from:\n" << from;
    const auto Map = from.value<QVariantMultiMap>();
    if (Q_UNLIKELY(Map.empty()))
    {
        throw std::runtime_error{"Failed to parse Feature Description! "
                                 "<DataType> is empty."};
    }
    const auto& Key = Map.firstKey();
    m_Type = stringToType(Key);

    switch (m_Type)
    {
    case Type::Invalid:
        qCCritical(sila_cpp_codegen) << "Invalid Data Type Type!";
        break;
    case Type::Basic:
        m_DataType = new CDataTypeBasic;
        dynamic_cast<CDataTypeBasic*>(m_DataType)->fromVariant(Map.value(Key));
        break;
    case Type::Constrained:
        m_DataType = new CDataTypeConstrained;
        dynamic_cast<CDataTypeConstrained*>(m_DataType)
            ->fromVariant(Map.value(Key));
        break;
    case Type::Identifier:
        m_DataType = new CDataTypeIdentifier;
        dynamic_cast<CDataTypeIdentifier*>(m_DataType)
            ->fromVariant(Map.value(Key));
        break;
    case Type::List:
        m_DataType = new CDataTypeList;
        dynamic_cast<CDataTypeList*>(m_DataType)->fromVariant(Map.value(Key));
        break;
    case Type::Structure:
        m_DataType = new CDataTypeStructure;
        dynamic_cast<CDataTypeStructure*>(m_DataType)->fromVariant(Map.value(Key));
        break;
    }
}

//============================================================================
bool CDataType::isEqual(const ISerializable& r) const
{
    const auto rhs = dynamic_cast<const CDataType&>(r);
    return IDataType::isEqual(rhs) && *dataTypeSafe() == *rhs.dataTypeSafe();
}

//============================================================================
CDataTypeBasic CDataType::basic() const
{
    switch (m_Type)
    {
    case Type::Basic:
        return *(dynamic_cast<CDataTypeBasic*>(dataTypeSafe()));
    case Type::Invalid:
        qCWarning(sila_cpp_codegen) << "Invalid Data Type Type!";
        [[fallthrough]];
    case Type::Constrained:
    case Type::Identifier:
    case Type::List:
    case Type::Structure:
        throw std::bad_cast{};
    }

    qCCritical(sila_cpp_client)
        << "Unhandled Data Type Type" << static_cast<int>(m_Type);
    throw std::bad_cast{};
}

//============================================================================
CDataTypeConstrained CDataType::constrained() const
{
    switch (m_Type)
    {
    case Type::Constrained:
        return *(dynamic_cast<CDataTypeConstrained*>(dataTypeSafe()));
    case Type::Invalid:
        qCWarning(sila_cpp_codegen) << "Invalid Data Type Type!";
        [[fallthrough]];
    case Type::Basic:
    case Type::Identifier:
    case Type::List:
    case Type::Structure:
        throw std::bad_cast{};
    }

    qCCritical(sila_cpp_client)
        << "Unhandled Data Type Type" << static_cast<int>(m_Type);
    throw std::bad_cast{};
}

//============================================================================
CDataTypeIdentifier CDataType::identifier() const
{
    switch (m_Type)
    {
    case Type::Identifier:
        return *(dynamic_cast<CDataTypeIdentifier*>(dataTypeSafe()));
    case Type::Invalid:
        qCWarning(sila_cpp_codegen) << "Invalid Data Type Type!";
        [[fallthrough]];
    case Type::Basic:
    case Type::Constrained:
    case Type::List:
    case Type::Structure:
        throw std::bad_cast{};
    }

    qCCritical(sila_cpp_client)
        << "Unhandled Data Type Type" << static_cast<int>(m_Type);
    throw std::bad_cast{};
}

//============================================================================
CDataTypeList CDataType::list() const
{
    switch (m_Type)
    {
    case Type::List:
        return *(dynamic_cast<CDataTypeList*>(dataTypeSafe()));
    case Type::Invalid:
        qCWarning(sila_cpp_codegen) << "Invalid Data Type Type!";
        [[fallthrough]];
    case Type::Basic:
    case Type::Constrained:
    case Type::Identifier:
    case Type::Structure:
        throw std::bad_cast{};
    }

    qCCritical(sila_cpp_client)
        << "Unhandled Data Type Type" << static_cast<int>(m_Type);
    throw std::bad_cast{};
}

//============================================================================
CDataTypeStructure CDataType::structure() const
{
    switch (m_Type)
    {
    case Type::Structure:
        return *(dynamic_cast<CDataTypeStructure*>(dataTypeSafe()));
    case Type::Invalid:
        qCWarning(sila_cpp_codegen) << "Invalid Data Type Type!";
        [[fallthrough]];
    case Type::Basic:
    case Type::Constrained:
    case Type::Identifier:
    case Type::List:
        throw std::bad_cast{};
    }

    qCCritical(sila_cpp_client)
        << "Unhandled Data Type Type" << static_cast<int>(m_Type);
    throw std::bad_cast{};
}

//============================================================================
IDataType* CDataType::dataTypeSafe() const
{
    if (Q_UNLIKELY(!m_DataType))
    {
        throw std::runtime_error{"m_DataType is nullptr!"};
    }
    return m_DataType;
}

//============================================================================
QDebug operator<<(QDebug dbg, const CDataType& rhs)
{
    using namespace SiLA2::codegen::fdl;
    QDebugStateSaver s{dbg};
    dbg.nospace() << "CDataType(" << CDataType::typeToString(rhs.type()) << ", ";

    if (auto* const DataType = rhs.dataType(); DataType)
    {
        switch (rhs.type())
        {
        case CDataType::Type::Invalid:
            dbg << "Invalid";
            break;
        case CDataType::Type::Basic:
            dbg << *(dynamic_cast<CDataTypeBasic*>(DataType));
            break;
        case CDataType::Type::Constrained:
            dbg << *(dynamic_cast<CDataTypeConstrained*>(DataType));
            break;
        case CDataType::Type::Identifier:
            dbg << *(dynamic_cast<CDataTypeIdentifier*>(DataType));
            break;
        case CDataType::Type::List:
            dbg << *(dynamic_cast<CDataTypeList*>(DataType));
            break;
        case CDataType::Type::Structure:
            dbg << *(dynamic_cast<CDataTypeStructure*>(DataType));
            break;
        default:
            qCCritical(sila_cpp_client)
                << "Unhandled Data Type Type" << static_cast<int>(rhs.type());
            break;
        }
    }
    else
    {
        dbg << "nullptr";
    }

    return dbg << ')';
}

//============================================================================
std::ostream& operator<<(std::ostream& os, const CDataType& rhs)
{
    using namespace SiLA2::codegen::fdl;
    os << "CDataType(" << CDataType::typeToString(rhs.type()) << ", ";

    if (auto* const DataType = rhs.dataType(); DataType)
    {
        switch (rhs.type())
        {
        case CDataType::Type::Invalid:
            os << "Invalid";
            break;
        case CDataType::Type::Basic:
            os << *(dynamic_cast<CDataTypeBasic*>(DataType));
            break;
        case CDataType::Type::Constrained:
            os << *(dynamic_cast<CDataTypeConstrained*>(DataType));
            break;
        case CDataType::Type::Identifier:
            os << *(dynamic_cast<CDataTypeIdentifier*>(DataType));
            break;
        case CDataType::Type::List:
            os << *(dynamic_cast<CDataTypeList*>(DataType));
            break;
        case CDataType::Type::Structure:
            os << *(dynamic_cast<CDataTypeStructure*>(DataType));
            break;
        default:
            qCCritical(sila_cpp_client)
                << "Unhandled Data Type Type" << static_cast<int>(rhs.type());
            break;
        }
    }
    else
    {
        os << "nullptr";
    }

    return os << ')';
}
}  // namespace SiLA2::codegen::fdl
