/**
 ** This file is part of the sila_cpp project.
 ** Copyright (c) 2021 SiLA2
 **
 ** Permission is hereby granted, free of charge, to any person obtaining a copy
 ** of this software and associated documentation files (the "Software"), to deal
 ** in the Software without restriction, including without limitation the rights
 ** to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 ** copies of the Software, and to permit persons to whom the Software is
 ** furnished to do so, subject to the following conditions:
 **
 ** The above copyright notice and this permission notice shall be included in all
 ** copies or substantial portions of the Software.
 **
 ** THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 ** IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 ** FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 ** AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 ** LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 ** OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 ** SOFTWARE.
 **/

//============================================================================
/// \file   ConstraintAllowedTypes.cpp
/// \author Florian Meinicke (florian.meinicke@cetoni.de)
/// \date   01.02.2021
/// \brief  Implementation of the CConstraintAllowedTypes class
//============================================================================

//============================================================================
//                                  INCLUDES
//============================================================================
#include <sila_cpp/codegen/fdl/ConstraintAllowedTypes.h>
#include <sila_cpp/common/logging.h>

#include "utils.h"

using namespace std;

namespace SiLA2::codegen::fdl
{
//============================================================================
QVariant CConstraintAllowedTypes::toVariant() const
{
    return QVariant::fromValue(toReverseVariantMaps("DataType", m_Types));
}

//============================================================================
void CConstraintAllowedTypes::fromVariant(const QVariant& from)
{
    const auto List = from.toList();
    if (Q_UNLIKELY(List.empty()))
    {
        throw std::runtime_error{"Failed to parse Feature Description! "
                                 "<AllowedTypes> constraint is empty."};
    }
    fromVariantList(List, m_Types);
}

//============================================================================
bool CConstraintAllowedTypes::isEqual(const ISerializable& r) const
{
    const auto rhs = dynamic_cast<const CConstraintAllowedTypes&>(r);
    return m_Types == rhs.m_Types;
}

//============================================================================
QDebug operator<<(QDebug dbg, const CConstraintAllowedTypes& rhs)
{
    return dbg << rhs.types();
}

//============================================================================
std::ostream& operator<<(std::ostream& os, const CConstraintAllowedTypes& rhs)
{
    return os << rhs.types();
}
}  // namespace SiLA2::codegen::fdl
