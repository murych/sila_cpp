/*******************************************************************************
 ** This file is part of the sila_cpp project.
 ** Copyright 2022 SiLA 2
 **
 ** Permission is hereby granted, free of charge, to any person obtaining a copy
 ** of this software and associated documentation files (the "Software"), to deal
 ** in the Software without restriction, including without limitation the rights
 ** to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 ** copies of the Software, and to permit persons to whom the Software is
 ** furnished to do so, subject to the following conditions:
 **
 ** The above copyright notice and this permission notice shall be included in all
 ** copies or substantial portions of the Software.
 **
 ** THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 ** IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 ** FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 ** AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 ** LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 ** OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 ** SOFTWARE.
 *****************************************************************************/

//============================================================================
/// \file   utils.cpp
/// \author Florian Meinicke (florian.meinicke@cetoni.de)
/// \date   20.10.2022
/// \brief  Implementation of constraints helpers
//============================================================================

//============================================================================
//                                  INCLUDES
//============================================================================
#include <sila_cpp/common/logging.h>
#include <sila_cpp/framework/constraints/utils.h>

#include <QStringList>

namespace SiLA2
{
static constexpr auto Originator = R"([a-z][a-z\.]*)";
static constexpr auto Category = Originator;
static constexpr auto Identifier = R"([A-Z][a-zA-Z0-9]*)";
static constexpr auto Version = R"(v\d+)";

static const auto FeatureIdentifier =
    QStringList{Originator, Category, Identifier, Version}.join('/');
static const auto CommandIdentifier =
    QStringList{FeatureIdentifier, "Command", Identifier}.join('/');
static const auto CommandParameterIdentifier =
    QStringList{CommandIdentifier, "Parameter", Identifier}.join('/');
static const auto CommandResponseIdentifier =
    QStringList{CommandIdentifier, "Response", Identifier}.join('/');
static const auto IntermediateCommandResponseIdentifier =
    QStringList{CommandIdentifier, "IntermediateResponse", Identifier}.join('/');
static const auto DefinedExecutionErrorIdentifier =
    QStringList{FeatureIdentifier, "DefinedExecutionError", Identifier}.join('/');
static const auto PropertyIdentifier =
    QStringList{FeatureIdentifier, "Property", Identifier}.join('/');
static const auto DataTypeIdentifier =
    QStringList{FeatureIdentifier, "DataType", Identifier}.join('/');
static const auto MetadataIdentifier =
    QStringList{FeatureIdentifier, "Metadata", Identifier}.join('/');

//============================================================================
QString CFullyQualifiedIdentifierRegex::featureIdentifier()
{
    return FeatureIdentifier;
}

//============================================================================
QString CFullyQualifiedIdentifierRegex::commandIdentifier()
{
    return CommandIdentifier;
}

//============================================================================
QString CFullyQualifiedIdentifierRegex::commandParameterIdentifier()
{
    return CommandParameterIdentifier;
}

//============================================================================
QString CFullyQualifiedIdentifierRegex::commandResponseIdentifier()
{
    return CommandResponseIdentifier;
}

//============================================================================
QString CFullyQualifiedIdentifierRegex::intermediateCommandResponseIdentifier()
{
    return IntermediateCommandResponseIdentifier;
}

//============================================================================
QString CFullyQualifiedIdentifierRegex::definedExecutionErrorIdentifier()
{
    return DefinedExecutionErrorIdentifier;
}

//============================================================================
QString CFullyQualifiedIdentifierRegex::propertyIdentifier()
{
    return PropertyIdentifier;
}

//============================================================================
QString CFullyQualifiedIdentifierRegex::dataTypeIdentifier()
{
    return DataTypeIdentifier;
}

//============================================================================
QString CFullyQualifiedIdentifierRegex::metadataIdentifier()
{
    return MetadataIdentifier;
}

//============================================================================
QString CFullyQualifiedIdentifierRegex::get(const QString& Id)
{
    if (Id == QStringLiteral("FeatureIdentifier"))
    {
        return FeatureIdentifier;
    }
    if (Id == QStringLiteral("CommandIdentifier"))
    {
        return CommandIdentifier;
    }
    if (Id == QStringLiteral("CommandParameterIdentifier"))
    {
        return CommandParameterIdentifier;
    }
    if (Id == QStringLiteral("CommandResponseIdentifier"))
    {
        return CommandResponseIdentifier;
    }
    if (Id == QStringLiteral("IntermediateCommandResponseIdentifier"))
    {
        return IntermediateCommandResponseIdentifier;
    }
    if (Id == QStringLiteral("DefinedExecutionErrorIdentifier"))
    {
        return DefinedExecutionErrorIdentifier;
    }
    if (Id == QStringLiteral("PropertyIdentifier"))
    {
        return PropertyIdentifier;
    }
    if (Id == QStringLiteral("DataTypeIdentifier"))
    {
        return DataTypeIdentifier;
    }
    if (Id == QStringLiteral("MetadataIdentifier"))
    {
        return MetadataIdentifier;
    }
    qCWarning(sila_cpp_constraints) << "Unknown Fully Qualified Identifier" << Id;
    return "";
}
}  // namespace SiLA2