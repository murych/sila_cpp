/**
 ** This file is part of the sila_cpp project.
 ** Copyright 2020 SiLA2
 **
 ** Permission is hereby granted, free of charge, to any person obtaining a copy
 ** of this software and associated documentation files (the "Software"), to deal
 ** in the Software without restriction, including without limitation the rights
 ** to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 ** copies of the Software, and to permit persons to whom the Software is
 ** furnished to do so, subject to the following conditions:
 **
 ** The above copyright notice and this permission notice shall be included in all
 ** copies or substantial portions of the Software.
 **
 ** THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 ** IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 ** FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 ** AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 ** LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 ** OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 ** SOFTWARE.
 **/

//============================================================================
/// \file   SiLAServiceImpl.cpp
/// \author Florian Meinicke (florian.meinicke@cetoni.de)
/// \date   22.01.2020
/// \brief  Implementation of the CSiLAService class
//============================================================================

//============================================================================
//                                  INCLUDES
//============================================================================
#include "SiLAServiceImpl.h"

#include <sila_cpp/common/ServerInformation.h>
#include <sila_cpp/framework/error_handling/ValidationError.h>

#include <QUuid>

#include <utility>

using namespace std;
using namespace sila2::org::silastandard::core::silaservice::v1;

namespace SiLA2
{
//=============================================================================
CSiLAService::CSiLAService(const CServerInformation& ServerInfo,
                           CSiLAServer* Server)
    : CSiLAFeature{Server},
      m_GetFeatureDefinitionCommand{this},
      m_SetServerNameCommand{this},
      m_ServerNameProperty{this, {ServerInfo.serverName()}},
      m_ServerTypeProperty{this, {ServerInfo.serverType()}},
      m_ServerUUIDProperty{
          this, {ServerInfo.serverUUID().toString(QUuid::WithoutBraces)}},
      m_ServerDescriptionProperty{this, {ServerInfo.description()}},
      m_ServerVersionProperty{this, {ServerInfo.version()}},
      m_ServerVendorURLProperty{this, {ServerInfo.vendorURL()}},
      m_ImplementedFeaturesProperty{this}
{
    // register the SiLAService feature to itself so that clients can request
    // its Feature Definition
    registerFeature(fullyQualifiedIdentifier(), featureDefinition());

    m_GetFeatureDefinitionCommand.setExecutor(
        this, &CSiLAService::GetFeatureDefinition);
    m_SetServerNameCommand.setExecutor(this, &CSiLAService::SetServerName);
}

//=========================================================================
GetFeatureDefinition_Responses CSiLAService::GetFeatureDefinition(
    GetFeatureDefinitionWrapper* Command) const
{
    const auto Request = Command->parameters();
    if (!Request.has_featureidentifier())
    {
        static const CFullyQualifiedCommandParameterID ParamID{
            {fullyQualifiedIdentifier(), "GetFeatureDefinition"},
            "FeatureIdentifier"};
        throw CValidationError{
            ParamID,
            "No FeatureIdentifier given! Specify a valid FeatureIdentifier to "
            "get its Feature Description. You can get a list of all implemented "
            "features by reading the property 'ImplementedFeatures'"};
    }
    const auto RequestedFeature = Request.featureidentifier().value();
    qCInfo(sila_cpp) << "Requested feature definition for feature"
                     << RequestedFeature;
    const auto FeatureID =
        CFullyQualifiedFeatureID::fromStdString(RequestedFeature);
    auto Response = GetFeatureDefinition_Responses{};
    try
    {
        Response.set_allocated_featuredefinition(
            CString{m_ImplementedFeatures.at(FeatureID).constData()}
                .toProtoMessagePtr());
        return Response;
    }
    catch (const std::out_of_range&)
    {
        static const CFullyQualifiedCommandParameterID ParamID{
            {fullyQualifiedIdentifier(), "GetFeatureDefinition"},
            "UnimplementedFeature"};
        throw CValidationError{ParamID,
                               "The Feature specified by the given Feature "
                               "Identifier is not implemented by the server."};
    }
}

//=========================================================================
SetServerName_Responses CSiLAService::SetServerName(SetServerNameWrapper* Command)
{
    static const CFullyQualifiedCommandParameterID ParamID{
        {fullyQualifiedIdentifier(), "SetServerName"}, "ServerName"};
    const auto NewServerName = Command->parameters().servername().value();
    if (NewServerName.empty())
    {
        throw CValidationError{ParamID, "No server name given! Provide a name "
                                        "with at least one character."};
    }
    if (NewServerName.length() > 255)  // Parameter Constraint
    {
        throw CValidationError{ParamID,
                               "The given server name is longer than the maximum "
                               "length of 255 characters! Specify a server name "
                               "with at least one and at most 255 characters."};
    }
    m_ServerNameProperty = NewServerName;
    emit serverNameChanged(QString::fromStdString(NewServerName));
    return {};
}

//=============================================================================
void CSiLAService::registerFeature(const CFullyQualifiedFeatureID& FeatureName,
                                   const QByteArray& FeatureDefinition)
{
    m_ImplementedFeatures[FeatureName] = FeatureDefinition;
    m_ImplementedFeaturesProperty.append(CString{FeatureName.toStdString()});
}

//============================================================================
CFullyQualifiedFeatureID CSiLAService::fullyQualifiedIdentifier() const
{
    static const CFullyQualifiedFeatureID FullyQualifiedFeatureID{
        "org.silastandard", "core", "SiLAService", "v1"};
    return FullyQualifiedFeatureID;
}

//============================================================================
CServerInformation CSiLAService::serverInformation() const
{
    return {m_ServerNameProperty.value().toQString(),
            m_ServerTypeProperty.value().toQString(),
            m_ServerDescriptionProperty.value().toQString(),
            m_ServerVersionProperty.value().toQString(),
            m_ServerVendorURLProperty.value().toQString(),
            QUuid{m_ServerUUIDProperty.value().toQString()}};
}
}  // namespace SiLA2
