/**
 ** This file is part of the sila_cpp project.
 ** Copyright 2020 SiLA2
 **
 ** Permission is hereby granted, free of charge, to any person obtaining a copy
 ** of this software and associated documentation files (the "Software"), to deal
 ** in the Software without restriction, including without limitation the rights
 ** to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 ** copies of the Software, and to permit persons to whom the Software is
 ** furnished to do so, subject to the following conditions:
 **
 ** The above copyright notice and this permission notice shall be included in all
 ** copies or substantial portions of the Software.
 **
 ** THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 ** IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 ** FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 ** AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 ** LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 ** OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 ** SOFTWARE.
 **/

//============================================================================
/// \file   SiLAAnyType.cpp
/// \author Florian Meinicke (florian.meinicke@cetoni.de)
/// \date   17.01.2020
/// \brief  Implementation of the CAnyType class
//============================================================================

//============================================================================
//                                  INCLUDES
//============================================================================
#include <sila_cpp/codegen/proto/ProtobufGenerator.h>
#include <sila_cpp/common/logging.h>
#include <sila_cpp/data_types.h>
#include <sila_cpp/framework/grpc/SiLAFramework.pb.h>

#include <QXmlStreamWriter>

#include <type_traits>

using namespace std;
using namespace SiLA2::codegen;
using sila2::org::silastandard::Any;
using TypePair = std::pair<std::string, std::string>;

namespace SiLA2
{
//============================================================================
proto::CDynamicMessageFactory CAnyType::m_DynamicMessageFactory;

//============================================================================
CAnyType::CAnyType(const Any& rhs)
    : CDataType<TypePair>{{rhs.type(), rhs.payload()}}
{}

//============================================================================
CAnyType::CAnyType(const CAnyType& Payload) : CDataType<TypePair>{}
{
    /// The @a DataTypeMap in @c CDataTypeSerializer does a copy somewhere which
    /// would unnecessarily wrap an empty AnyType into another AnyType
    if (Payload.type().empty() && Payload.payload().empty())
    {
        *this = CDataType<TypePair>{{"", ""}};
        return;
    }

    const auto PayloadAsProtoMsg = Payload.toProtoMessage();
    const auto XMLString = CDataTypeSerializer::serialize(PayloadAsProtoMsg);
    const auto SerializedPayload = PayloadAsProtoMsg.SerializeAsString();

    setValue({qPrintable(XMLString), SerializedPayload});
}

//============================================================================
CAnyType::CAnyType() : CDataType<TypePair>{{"", ""}}
{}

//============================================================================
CAnyType CAnyType::fromProtoMessage(const google::protobuf::Message& from)
{
    Any Temp;
    Temp.CopyFrom(from);
    return Temp;
}

//=============================================================================
Any CAnyType::toProtoMessage() const
{
    auto [Type, Payload] = value();
    auto Result = Any{};

    Result.set_type(Type);
    Result.set_payload(Payload);

    return Result;
}

//=============================================================================
Any* CAnyType::toProtoMessagePtr() const
{
    auto [Type, Payload] = value();
    auto* Result = new Any{};

    Result->set_type(Type);
    Result->set_payload(Payload);

    return Result;
}

//============================================================================
std::string CAnyType::type() const
{
    return value().first;
}

//============================================================================
std::string CAnyType::payload() const
{
    return value().second;
}

//============================================================================
CString CAnyType::toString()
{
    return convert<CString>();
}

//============================================================================
CInteger CAnyType::toInteger()
{
    return convert<CInteger>();
}

//============================================================================
CReal CAnyType::toReal()
{
    return convert<CReal>();
}

//============================================================================
CBoolean CAnyType::toBoolean()
{
    return convert<CBoolean>();
}

//============================================================================
CBinary CAnyType::toBinary()
{
    return convert<CBinary>();
}

//============================================================================
CDate CAnyType::toDate()
{
    return convert<CDate>();
}

//============================================================================
CTime CAnyType::toTime()
{
    return convert<CTime>();
}

//============================================================================
CTimestamp CAnyType::toTimestamp()
{
    return convert<CTimestamp>();
}

//============================================================================
CTimezone CAnyType::toTimezone()
{
    return convert<CTimezone>();
}

//============================================================================
CDuration CAnyType::toDuration()
{
    return convert<CDuration>();
}

//============================================================================
CAnyType CAnyType::toAnyType()
{
    return convert<CAnyType>();
}

//============================================================================
CString CAnyType::toString(const Any& from)
{
    return convert<CString>(from);
}

//============================================================================
CInteger CAnyType::toInteger(const Any& from)
{
    return convert<CInteger>(from);
}

//============================================================================
CReal CAnyType::toReal(const Any& from)
{
    return convert<CReal>(from);
}

//============================================================================
CBoolean CAnyType::toBoolean(const Any& from)
{
    return convert<CBoolean>(from);
}

//============================================================================
CBinary CAnyType::toBinary(const Any& from)
{
    return convert<CBinary>(from);
}

//============================================================================
CDate CAnyType::toDate(const Any& from)
{
    return convert<CDate>(from);
}

//============================================================================
CTime CAnyType::toTime(const Any& from)
{
    return convert<CTime>(from);
}

//============================================================================
CTimestamp CAnyType::toTimestamp(const Any& from)
{
    return convert<CTimestamp>(from);
}

//============================================================================
CTimezone CAnyType::toTimezone(const Any& from)
{
    return convert<CTimezone>(from);
}

//============================================================================
CDuration CAnyType::toDuration(const Any& from)
{
    return convert<CDuration>(from);
}

//============================================================================
CAnyType CAnyType::toAnyType(const Any& from)
{
    return convert<CAnyType>(from);
}

//============================================================================
unique_ptr<google::protobuf::Message> CAnyType::generateDynamicMessage(
    const QString& FDL)
{
    fdl::CDataType DataType;
    fdl::CFDLSerializer::deserialize(DataType, FDL);

    const auto DynamicMessageProto =
        proto::CProtobufGenerator::generate(DataType);

    const auto DynamicProtoFile =
        proto::CProtobufFile::fromRawProtobuf(DynamicMessageProto);
    const auto DynamicPackageName =
        DynamicProtoFile.content().section("package ", 1).section(";", 0, 0);
    const auto DynamicMessageName =
        DynamicProtoFile.content().section("message ", 1).section(" {", 0, 0);

    m_DynamicMessageFactory.addProtoFile(DynamicProtoFile);
    return m_DynamicMessageFactory.getPrototype(
        DynamicPackageName.toStdString() + "."
        + DynamicMessageName.toStdString());
}

//============================================================================
QDebug operator<<(QDebug dbg, const CAnyType& rhs)
{
    QDebugStateSaver s{dbg};
    return dbg.nospace() << "SiLA2::CAnyType(Type: " << rhs.type()
                         << ", Value: " << rhs.payload() << ')';
}

//============================================================================
std::ostream& operator<<(std::ostream& os, const CAnyType& rhs)
{
    return os << "SiLA2::CAnyType(Type: \"" << rhs.type() << "\", Value: \""
              << rhs.payload() << "\")";
}
}  // namespace SiLA2
