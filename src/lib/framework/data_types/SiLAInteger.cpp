/**
 ** This file is part of the sila_cpp project.
 ** Copyright 2020 SiLA2
 **
 ** Permission is hereby granted, free of charge, to any person obtaining a copy
 ** of this software and associated documentation files (the "Software"), to deal
 ** in the Software without restriction, including without limitation the rights
 ** to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 ** copies of the Software, and to permit persons to whom the Software is
 ** furnished to do so, subject to the following conditions:
 **
 ** The above copyright notice and this permission notice shall be included in all
 ** copies or substantial portions of the Software.
 **
 ** THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 ** IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 ** FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 ** AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 ** LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 ** OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 ** SOFTWARE.
 **/

//============================================================================
/// \file   SiLAInteger.cpp
/// \author Florian Meinicke (florian.meinicke@cetoni.de)
/// \date   17.01.2020
/// \brief  Implementation of the CInteger class
//============================================================================

//============================================================================
//                                  INCLUDES
//============================================================================
#include <sila_cpp/common/logging.h>
#include <sila_cpp/framework/data_types/SiLAInteger.h>
#include <sila_cpp/framework/grpc/SiLAFramework.pb.h>

using namespace std;
using sila2::org::silastandard::Integer;

namespace SiLA2
{
//============================================================================
CInteger::CInteger(int64_t Value) : CDataType<int64_t>{Value}
{}

//============================================================================
CInteger::CInteger(const Integer& rhs) : CDataType<int64_t>{rhs.value()}
{}

//============================================================================
CInteger CInteger::fromProtoMessage(const google::protobuf::Message& from)
{
    Integer Temp;
    Temp.CopyFrom(from);
    return Temp;
}

//============================================================================
CInteger& CInteger::operator++()
{
    setValue(value() + 1);
    return *this;
}

//============================================================================
const CInteger CInteger::operator++(int)
{
    auto res = *this;
    setValue(value() + 1);
    return res;
}

//============================================================================
CInteger& CInteger::operator--()
{
    setValue(value() - 1);
    return *this;
}

//============================================================================
const CInteger CInteger::operator--(int)
{
    auto res = *this;
    setValue(value() - 1);
    return res;
}

//============================================================================
Integer CInteger::toProtoMessage() const
{
    auto Result = Integer{};
    Result.set_value(value());
    return Result;
}

//============================================================================
Integer* CInteger::toProtoMessagePtr() const
{
    auto* Result = new Integer{};
    Result->set_value(value());
    return Result;
}

//============================================================================
QDebug operator<<(QDebug dbg, const CInteger& rhs)
{
    QDebugStateSaver s{dbg};
    return dbg.nospace() << "SiLA2::CInteger(" << rhs.value() << ')';
}

//============================================================================
std::ostream& operator<<(std::ostream& os, const CInteger& rhs)
{
    return os << "SiLA2::CInteger(" << rhs.value() << ')';
}
}  // namespace SiLA2
