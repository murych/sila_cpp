/**
 ** This file is part of the sila_cpp project.
 ** Copyright 2020 SiLA2
 **
 ** Permission is hereby granted, free of charge, to any person obtaining a copy
 ** of this software and associated documentation files (the "Software"), to deal
 ** in the Software without restriction, including without limitation the rights
 ** to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 ** copies of the Software, and to permit persons to whom the Software is
 ** furnished to do so, subject to the following conditions:
 **
 ** The above copyright notice and this permission notice shall be included in all
 ** copies or substantial portions of the Software.
 **
 ** THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 ** IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 ** FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 ** AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 ** LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 ** OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 ** SOFTWARE.
 **/

//============================================================================
/// \file   SiLABinary.cpp
/// \author Florian Meinicke (florian.meinicke@cetoni.de)
/// \date   17.01.2020
/// \brief  Implementation of the CBinary class
//============================================================================

//============================================================================
//                                  INCLUDES
//============================================================================
#include <sila_cpp/common/logging.h>
#include <sila_cpp/framework/data_types/SiLABinary.h>
#include <sila_cpp/framework/grpc/SiLAFramework.pb.h>

using sila2::org::silastandard::Binary;

namespace SiLA2
{
//============================================================================
CBinary::CBinary(const std::string& Value) : CDataType<std::string>{Value}
{
    // TBD
}

//============================================================================
CBinary::CBinary(const Binary& rhs) : CDataType<std::string>{rhs.value()}
{}

//============================================================================
CBinary CBinary::fromProtoMessage(const google::protobuf::Message& from)
{
    Binary Temp;
    Temp.CopyFrom(from);
    return Temp;
}

//============================================================================
Binary CBinary::toProtoMessage() const
{
    auto Result = Binary{};
    Result.set_value(value());
    return Result;
}

//============================================================================
Binary* CBinary::toProtoMessagePtr() const
{
    auto* Result = new Binary{};
    Result->set_value(value());
    return Result;
}

//============================================================================
QDebug operator<<(QDebug dbg, const CBinary& rhs)
{
    QDebugStateSaver s{dbg};
    return dbg.nospace() << "SiLA2::CBinary(" << rhs.value() << ')';
}

//============================================================================
std::ostream& operator<<(std::ostream& os, const CBinary& rhs)
{
    return os << "SiLA2::CBinary(" << rhs.value() << ')';
}
}  // namespace SiLA2
