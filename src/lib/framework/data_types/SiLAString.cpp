/**
 ** This file is part of the sila_cpp project.
 ** Copyright 2020 SiLA2
 **
 ** Permission is hereby granted, free of charge, to any person obtaining a copy
 ** of this software and associated documentation files (the "Software"), to deal
 ** in the Software without restriction, including without limitation the rights
 ** to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 ** copies of the Software, and to permit persons to whom the Software is
 ** furnished to do so, subject to the following conditions:
 **
 ** The above copyright notice and this permission notice shall be included in all
 ** copies or substantial portions of the Software.
 **
 ** THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 ** IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 ** FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 ** AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 ** LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 ** OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 ** SOFTWARE.
 **/

//============================================================================
/// \file   SiLAString.cpp
/// \author Florian Meinicke (florian.meinicke@cetoni.de)
/// \date   17.01.2020
/// \brief  Implementation of the CString class
//============================================================================

//============================================================================
//                                  INCLUDES
//============================================================================
#include <sila_cpp/common/logging.h>
#include <sila_cpp/framework/data_types/SiLAString.h>
#include <sila_cpp/framework/grpc/SiLAFramework.pb.h>

using namespace std;
using sila2::org::silastandard::String;

namespace SiLA2
{
//=============================================================================
CString::CString(const string& Value) : CDataType<std::string>{Value}
{}

//=============================================================================
CString::CString(QStringView Value)
    : CDataType<std::string>{Value.toString().toStdString()}
{}

//============================================================================
CString::CString(const String& rhs) : CDataType<std::string>{rhs.value()}
{}

//============================================================================
CString CString::fromProtoMessage(const google::protobuf::Message& from)
{
    String Temp;
    Temp.CopyFrom(from);
    return Temp;
}

//============================================================================
CString& CString::append(const string& s)
{
    *this += s;
    return *this;
}

//============================================================================
CString& CString::append(QStringView s)
{
    *this += s.toLocal8Bit().constData();
    return *this;
}

//============================================================================
CString& CString::append(const String& s)
{
    *this += s.value();
    return *this;
}

//============================================================================
String CString::toProtoMessage() const
{
    auto Result = String{};
    Result.set_value(value());
    return Result;
}

//============================================================================
String* CString::toProtoMessagePtr() const
{
    auto* Result = new String{};
    Result->set_value(value());
    return Result;
}

//============================================================================
QString CString::toQString() const
{
    return QString::fromStdString(value());
}

//============================================================================
QDebug operator<<(QDebug dbg, const CString& rhs)
{
    QDebugStateSaver s{dbg};
    return dbg.nospace() << "SiLA2::CString(\"" << rhs.value() << "\")";
}

//============================================================================
std::ostream& operator<<(std::ostream& os, const CString& rhs)
{
    return os << "SiLA2::CString(\"" << rhs.value() << "\")";
}
}  // namespace SiLA2
