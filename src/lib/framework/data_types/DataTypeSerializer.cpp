/**
 ** This file is part of the sila_cpp project.
 ** Copyright 2020 SiLA2
 **
 ** Permission is hereby granted, free of charge, to any person obtaining a copy
 ** of this software and associated documentation files (the "Software"), to deal
 ** in the Software without restriction, including without limitation the rights
 ** to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 ** copies of the Software, and to permit persons to whom the Software is
 ** furnished to do so, subject to the following conditions:
 **
 ** The above copyright notice and this permission notice shall be included in all
 ** copies or substantial portions of the Software.
 **
 ** THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 ** IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 ** FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 ** AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 ** LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 ** OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 ** SOFTWARE.
 **/

//============================================================================
/// \file   DataTypeSerializer.cpp
/// \author Florian Meinicke (florian.meinicke@cetoni.de)
/// \date   21.12.2020
/// \brief  Implementation of the CDataTypeSerializer class
//============================================================================

//============================================================================
//                                  INCLUDES
//============================================================================
#include <sila_cpp/codegen/fdl/DataTypeBasic.h>
#include <sila_cpp/codegen/fdl/DataTypeConstrained.h>
#include <sila_cpp/codegen/fdl/DataTypeIdentifier.h>
#include <sila_cpp/codegen/fdl/DataTypeList.h>
#include <sila_cpp/codegen/fdl/DataTypeStructure.h>
#include <sila_cpp/data_types.h>
#include <sila_cpp/framework/grpc/SiLAFramework.pb.h>

#include <include/sila_cpp/framework/data_types/DataTypeSerializer.h>

using namespace std;
using namespace sila2::org::silastandard;
using namespace SiLA2::codegen;

namespace SiLA2
{
/**
 * @brief Helper function to get the underlying Basic Data Type from the given
 * @a DataType
 *
 * @param DataType The Data Type to get the underlying Basic Type for
 * @return The Basic Data Type for the given @a DataType
 */
QString getBasicTypeIdentifier(const fdl::CDataType& DataType)
{
    switch (DataType.type())
    {
    case fdl::IDataType::Type::Invalid:
        qCCritical(sila_cpp_data_types) << "Invalid Data Type Type!";
        return "";
    case fdl::IDataType::Type::Basic:
        return DataType.basic().identifier();
    case fdl::IDataType::Type::Constrained:
        return getBasicTypeIdentifier(DataType.constrained().dataType());
    case fdl::IDataType::Type::Identifier:
        // Custom Data Types do not have an underlying Basic Data Type
        return "";
    case fdl::IDataType::Type::List:
        return getBasicTypeIdentifier(DataType.list().dataType());
    case fdl::IDataType::Type::Structure:
        qCWarning(sila_cpp_data_types)
            << "Using a Structure Data Type in an Any Type is not yet supported "
               "by sila_cpp!";
        return getBasicTypeIdentifier(
            DataType.structure().elements().first().dataType());
    }
    qCCritical(sila_cpp_data_types) << "Shouldn't get here!";
    return "";
}

//============================================================================
const CDataTypeSerializer::DataTypeMap CDataTypeSerializer::m_DataTypeMap{
    {"String", CString{}},     {"Integer", CInteger{}},
    {"Real", CReal{}},         {"Boolean", CBoolean{}},
    {"Binary", CBinary{}},     {"Date", CDate{}},
    {"Time", CTime{}},         {"Timestamp", CTimestamp{}},
    {"Timezone", CTimezone{}}, {"Duration", CDuration{}},
    {"Any", CAnyType{}}};

//=============================================================================
pair<fdl::IDataType::Type, CDataTypeSerializer::DataTypeVariant>
CDataTypeSerializer::deserialize(const QString& XMLString)
{
    // TODO validate FDL
    fdl::CDataType DataType;
    fdl::CFDLSerializer::deserialize(DataType, XMLString);
    return {DataType.type(), m_DataTypeMap.at(getBasicTypeIdentifier(DataType))};
}
}  // namespace SiLA2
