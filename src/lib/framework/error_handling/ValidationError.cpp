/**
 ** This file is part of the sila_cpp project.
 ** Copyright 2020 SiLA2
 **
 ** Permission is hereby granted, free of charge, to any person obtaining a copy
 ** of this software and associated documentation files (the "Software"), to deal
 ** in the Software without restriction, including without limitation the rights
 ** to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 ** copies of the Software, and to permit persons to whom the Software is
 ** furnished to do so, subject to the following conditions:
 **
 ** The above copyright notice and this permission notice shall be included in all
 ** copies or substantial portions of the Software.
 **
 ** THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 ** IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 ** FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 ** AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 ** LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 ** OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 ** SOFTWARE.
 **/

//============================================================================
/// \file   ValidationError.cpp
/// \author Florian Meinicke (florian.meinicke@cetoni.de)
/// \date   27.01.2020
/// \brief  Implementation of the CValidationError class
//============================================================================

//============================================================================
//                                  INCLUDES
//============================================================================
#include <sila_cpp/common/logging.h>
#include <sila_cpp/framework/error_handling/ValidationError.h>
#include <sila_cpp/framework/grpc/SiLAFramework.pb.h>

#include "SiLAError_p.h"

#include <string>
#include <utility>

using namespace std;
using namespace isocpp_p0201;
using namespace sila2::org::silastandard;

namespace SiLA2
{
/**
 * @brief Private data of the CValidationError class - pimpl
 */
class CValidationError::PrivateImpl : public CSiLAError::PrivateImpl
{
public:
    /**
     * @brief C'tor
     */
    PrivateImpl(string param, string msg)
        : CSiLAError::PrivateImpl{ErrorType::ValidationError, std::move(msg)},
          Parameter{std::move(param)}
    {}

    /**
     * @brief Construct a SiLA Error protobuf message from the internal fields
     * @a Message and @a Parameter. Note that the caller takes ownership
     * of the returned message.
     *
     * @return A pointer to the SiLA Error protobuf message
     */
    [[nodiscard]] unique_ptr<SiLAError> makeErrorMessage() const override;

    string Parameter{};  ///< The parameter for which the validation failed
};

//=============================================================================
unique_ptr<SiLAError> CValidationError::PrivateImpl::makeErrorMessage() const
{
    auto* VError = new ValidationError;
    VError->set_parameter(Parameter);
    VError->set_message(Message);
    auto Error = CSiLAError::PrivateImpl::makeErrorMessage();
    Error->set_allocated_validationerror(VError);
    return Error;
}

//=============================================================================
CValidationError::CValidationError(string ParameterID, string Message)
    : CSiLAError{make_polymorphic_value<CSiLAError::PrivateImpl, PrivateImpl>(
        std::move(ParameterID), std::move(Message))}
{}

//=============================================================================
CValidationError::CValidationError(
    const CFullyQualifiedCommandParameterID& ParameterID, string Message)
    : CSiLAError{make_polymorphic_value<CSiLAError::PrivateImpl, PrivateImpl>(
        ParameterID.toStdString(), std::move(Message))}
{}

//============================================================================
string CValidationError::parameter() const
{
    PIMPL_D(const CValidationError);
    return d->Parameter;
}

//============================================================================
CValidationError CValidationError::fromErrorMessage(const SiLAError& Error)
{
    if (Error.has_validationerror())
    {
        const auto& ValidationError = Error.validationerror();
        return CValidationError{CFullyQualifiedCommandParameterID::fromStdString(
                                    ValidationError.parameter()),
                                ValidationError.message()};
    }
    qCWarning(sila_cpp_errors)
        << "CValidationError::fromErrorMessage was called with an Error Message "
           "that does not contain a Validation Error!";
    return CValidationError{CFullyQualifiedCommandParameterID{}, ""};
}

//============================================================================
const char* CValidationError::what() const noexcept
{
    PIMPL_D(const CValidationError);
    static string ErrorString;
    ErrorString = "Validation Error:\n\tParameter: "s + d->Parameter
                  + "\n\tMessage: " + d->Message;
    return ErrorString.c_str();
}
}  // namespace SiLA2
