/**
 ** This file is part of the sila_cpp project.
 ** Copyright 2020 SiLA2
 **
 ** Permission is hereby granted, free of charge, to any person obtaining a copy
 ** of this software and associated documentation files (the "Software"), to deal
 ** in the Software without restriction, including without limitation the rights
 ** to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 ** copies of the Software, and to permit persons to whom the Software is
 ** furnished to do so, subject to the following conditions:
 **
 ** The above copyright notice and this permission notice shall be included in all
 ** copies or substantial portions of the Software.
 **
 ** THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 ** IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 ** FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 ** AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 ** LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 ** OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 ** SOFTWARE.
 **/

//============================================================================
/// \file   SiLAError.cpp
/// \author Florian Meinicke (florian.meinicke@cetoni.de)
/// \date   27.01.2020
/// \brief  Implementation of the CSiLAError class
//============================================================================

//============================================================================
//                                  INCLUDES
//============================================================================
#include <sila_cpp/common/logging.h>
#include <sila_cpp/framework/error_handling/SiLAError.h>
#include <sila_cpp/framework/grpc/SiLAFramework.pb.h>
#include <sila_cpp/internal/Base64.h>

#include "SiLAError_p.h"

#include <grpcpp/impl/codegen/status.h>

#include <string>
#include <utility>

using namespace std;
using namespace isocpp_p0201;
using namespace sila2::org::silastandard;

namespace SiLA2
{
//=============================================================================
CSiLAError::PrivateImpl::PrivateImpl(ErrorType t, string msg)
    : Type{t}, Message{std::move(msg)}
{
    // at least have a most generic error message
    if (Message.empty())
    {
        qCInfo(sila_cpp_errors)
            << "Constructing a" << errorTypeToString(Type)
            << "with a generic message. Note that the SiLA 2 standard requires a "
               "description about the error and should make proposals on how to "
               "resolve it!";

        Message =
            "A " + errorTypeToString(Type)
            + " occurred while executing a SiLA 2 Command or reading a Property!";
    }
}

//=============================================================================
unique_ptr<SiLAError> CSiLAError::PrivateImpl::makeErrorMessage() const
{
    return make_unique<SiLAError>();
}

///============================================================================
string CSiLAError::message() const
{
    PIMPL_D(const CSiLAError);
    return d->Message;
}

//============================================================================
CSiLAError::ErrorType CSiLAError::errorType() const
{
    PIMPL_D(const CSiLAError);
    return d->Type;
}

//============================================================================
string CSiLAError::errorTypeName() const
{
    PIMPL_D(const CSiLAError);
    return errorTypeToString(d->Type);
}

//============================================================================
string CSiLAError::errorTypeToString(ErrorType Type)
{
    switch (Type)
    {
    case ErrorType::DefinedExecutionError:
        return "Defined Execution Error";
    case ErrorType::UndefinedExecutionError:
        return "Undefined Execution Error";
    case ErrorType::FrameworkError:
        return "Framework Error";
    case ErrorType::ValidationError:
        return "Validation Error";
    case ErrorType::ConnectionError:
        return "Connection Error";
    }
    qCCritical(sila_cpp_errors) << "Unknown ErrorType" << static_cast<int>(Type);
    return "";
}

//============================================================================
grpc::Status CSiLAError::toStatus() const
{
    PIMPL_D(const CSiLAError);
    const auto Error = d->makeErrorMessage();
    qCCritical(sila_cpp_errors)
        << "An" << errorTypeName() << "occurred:" << *Error;
    return {grpc::StatusCode::ABORTED,
            internal::base64Encode(Error->SerializeAsString())};
}

//============================================================================
const char* CSiLAError::what() const noexcept
{
    PIMPL_D(const CSiLAError);
    static string ErrorString;
    ErrorString = d->makeErrorMessage()->DebugString();
    return ErrorString.c_str();
}

//=============================================================================
CSiLAError::CSiLAError(PrivateImplPtr priv) : d_ptr{std::move(priv)}
{}
}  // namespace SiLA2
