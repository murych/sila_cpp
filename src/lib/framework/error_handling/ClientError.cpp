/**
 ** This file is part of the sila_cpp project.
 ** Copyright 2020 SiLA2
 **
 ** Permission is hereby granted, free of charge, to any person obtaining a copy
 ** of this software and associated documentation files (the "Software"), to deal
 ** in the Software without restriction, including without limitation the rights
 ** to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 ** copies of the Software, and to permit persons to whom the Software is
 ** furnished to do so, subject to the following conditions:
 **
 ** The above copyright notice and this permission notice shall be included in all
 ** copies or substantial portions of the Software.
 **
 ** THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 ** IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 ** FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 ** AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 ** LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 ** OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 ** SOFTWARE.
 **/

//============================================================================
/// \file   ClientError.cpp
/// \author Florian Meinicke (florian.meinicke@cetoni.de)
/// \date   24.01.2020
/// \brief  Implementation of client related error handling methods
//============================================================================

//============================================================================
//                                  INCLUDES
//============================================================================
#include <sila_cpp/common/logging.h>
#include <sila_cpp/framework/error_handling/ClientError.h>
#include <sila_cpp/framework/error_handling/ConnectionError.h>
#include <sila_cpp/framework/error_handling/ExecutionError.h>
#include <sila_cpp/framework/error_handling/FrameworkError.h>
#include <sila_cpp/framework/error_handling/ValidationError.h>
#include <sila_cpp/framework/grpc/SiLAFramework.pb.h>
#include <sila_cpp/internal/Base64.h>

#include <grpcpp/impl/codegen/status.h>

using namespace std;
using namespace sila2::org::silastandard;

namespace SiLA2
{
/**
 * @brief Helper function for @b hasError and @b throwOnError
 *
 * @param Status The gRPC Status object returned from every gRPC call
 * @param Throw Whether to throw if @a Status indicates an error or not
 * @returns true if the @a Status indicates no error, false otherwise
 */
bool maybeThrowOnError(const grpc::Status& Status, bool Throw);

//============================================================================
bool hasError(const grpc::Status& Status)
{
    return maybeThrowOnError(Status, false);
}

//============================================================================
void throwOnError(const grpc::Status& Status)
{
    maybeThrowOnError(Status, true);
}

//=============================================================================
bool maybeThrowOnError(const grpc::Status& Status, bool Throw)
{
    if (Status.ok())
    {
        return false;
    }

    auto Error = SiLAError{};
    Error.ParseFromString(internal::base64Decode(Status.error_message()));
    switch (Error.error_case())
    {
    case SiLAError::kValidationError:
        qCCritical(sila_cpp_errors) << "A Validation Error occurred:" << Error;
        if (Throw)
        {
            throw CValidationError::fromErrorMessage(Error);
        }
        return true;
    case SiLAError::kDefinedExecutionError:
        qCCritical(sila_cpp_errors)
            << "A Defined Execution Error occurred:" << Error;
        if (Throw)
        {
            throw CDefinedExecutionError::fromErrorMessage(Error);
        }
        return true;
    case SiLAError::kUndefinedExecutionError:
        qCCritical(sila_cpp_errors)
            << "An Undefined Execution Error occurred:" << Error;
        if (Throw)
        {
            throw CUndefinedExecutionError::fromErrorMessage(Error);
        }
        return true;
    case SiLAError::kFrameworkError:
        qCCritical(sila_cpp_errors) << "A Framework Error occurred:" << Error;
        if (Throw)
        {
            throw CFrameworkError::fromErrorMessage(Error);
        }
        return true;
    case SiLAError::ERROR_NOT_SET:
        // every cancellation of an RPC is initiated by the client side, so
        // it's not really an error in our case
        if (Status.error_code() != grpc::StatusCode::CANCELLED)
        {
            qCCritical(sila_cpp_errors)
                << "Error during gRPC communication." << Status;
            if (Throw)
            {
                throw CConnectionError{Status};
            }
        }
        return true;
    }
    qCCritical(sila_cpp_errors)
        << "Unhandled SiLA Error error case" << Error.error_case();
    return true;
}
}  // namespace SiLA2
