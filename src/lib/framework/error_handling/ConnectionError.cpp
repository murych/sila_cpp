/**
 ** This file is part of the sila_cpp project.
 ** Copyright 2021 SiLA2
 **
 ** Permission is hereby granted, free of charge, to any person obtaining a copy
 ** of this software and associated documentation files (the "Software"), to deal
 ** in the Software without restriction, including without limitation the rights
 ** to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 ** copies of the Software, and to permit persons to whom the Software is
 ** furnished to do so, subject to the following conditions:
 **
 ** The above copyright notice and this permission notice shall be included in all
 ** copies or substantial portions of the Software.
 **
 ** THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 ** IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 ** FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 ** AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 ** LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 ** OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 ** SOFTWARE.
 **/

//============================================================================
/// \file   ConnectionError.cpp
/// \author Florian Meinicke (florian.meinicke@cetoni.de)
/// \date   27.01.2020
/// \brief  Implementation of the CConnectionError class
//============================================================================

//============================================================================
//                                  INCLUDES
//============================================================================
#include <sila_cpp/common/logging.h>
#include <sila_cpp/framework/error_handling/ConnectionError.h>
#include <sila_cpp/framework/grpc/SiLAFramework.pb.h>

#include "SiLAError_p.h"

#include <utility>

using namespace std;
using namespace isocpp_p0201;
using namespace sila2::org::silastandard;

namespace SiLA2
{
/**
 * @brief Private data of the CConnectionError class - pimpl
 */
class CConnectionError::PrivateImpl : public CSiLAError::PrivateImpl
{
public:
    /**
     * @brief C'tor
     */
    explicit PrivateImpl(grpc::Status s)
        : CSiLAError::PrivateImpl{ErrorType::ConnectionError, s.error_message()},
          Status{std::move(s)}
    {}

    grpc::Status Status{};  ///< The gRPC::Status for the error
};

//=============================================================================
CConnectionError::CConnectionError(grpc::Status Status)
    : CSiLAError{make_polymorphic_value<CSiLAError::PrivateImpl, PrivateImpl>(
        std::move(Status))}
{}

//============================================================================
grpc::StatusCode CConnectionError::statusCode() const
{
    PIMPL_D(const CConnectionError);
    return d->Status.error_code();
}

//============================================================================
string CConnectionError::statusCodeName() const
{
    PIMPL_D(const CConnectionError);
    return statusCodeToString(d->Status.error_code());
}

//============================================================================
string CConnectionError::statusCodeToString(grpc::StatusCode Code)
{
    switch (Code)
    {
    case grpc::StatusCode::OK:
        return "OK";
    case grpc::StatusCode::CANCELLED:
        return "CANCELLED";
    case grpc::StatusCode::UNKNOWN:
        return "UNKNOWN";
    case grpc::StatusCode::INVALID_ARGUMENT:
        return "INVALID_ARGUMENT";
    case grpc::StatusCode::DEADLINE_EXCEEDED:
        return "DEADLINE_EXCEEDED";
    case grpc::StatusCode::NOT_FOUND:
        return "NOT_FOUND";
    case grpc::StatusCode::ALREADY_EXISTS:
        return "ALREADY_EXISTS";
    case grpc::StatusCode::PERMISSION_DENIED:
        return "PERMISSION_DENIED";
    case grpc::StatusCode::UNAUTHENTICATED:
        return "UNAUTHENTICATED";
    case grpc::StatusCode::RESOURCE_EXHAUSTED:
        return "RESOURCE_EXHAUSTED";
    case grpc::StatusCode::FAILED_PRECONDITION:
        return "FAILED_PRECONDITION";
    case grpc::StatusCode::ABORTED:
        return "ABORTED";
    case grpc::StatusCode::OUT_OF_RANGE:
        return "OUT_OF_RANGE";
    case grpc::StatusCode::UNIMPLEMENTED:
        return "UNIMPLEMENTED";
    case grpc::StatusCode::INTERNAL:
        return "INTERNAL";
    case grpc::StatusCode::UNAVAILABLE:
        return "UNAVAILABLE";
    case grpc::StatusCode::DATA_LOSS:
        return "DATA_LOSS";
    case grpc::StatusCode::DO_NOT_USE:
        return "DO_NOT_USE";
    }
    return "";
}

//============================================================================
grpc::Status CConnectionError::toStatus() const
{
    PIMPL_D(const CConnectionError);
    qCWarning(sila_cpp_errors)
        << "Connection Errors are only issued by gRPC and not the SiLA "
           "Server/Client! Converting them to a `grpc::Status` is not necessary "
           "and calling this function indicates that you might be doing "
           "something wrong!";
    return d->Status;
}

//============================================================================
const char* CConnectionError::what() const noexcept
{
    static string ErrorString;
    ErrorString = "Connection Error:\n\tStatus Code: "s + statusCodeName()
                  + "\n\tError Message: " + message();
    return ErrorString.c_str();
}
}  // namespace SiLA2
