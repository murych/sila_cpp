/**
 ** This file is part of the sila_cpp project.
 ** Copyright 2020 SiLA2
 **
 ** Permission is hereby granted, free of charge, to any person obtaining a copy
 ** of this software and associated documentation files (the "Software"), to deal
 ** in the Software without restriction, including without limitation the rights
 ** to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 ** copies of the Software, and to permit persons to whom the Software is
 ** furnished to do so, subject to the following conditions:
 **
 ** The above copyright notice and this permission notice shall be included in all
 ** copies or substantial portions of the Software.
 **
 ** THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 ** IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 ** FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 ** AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 ** LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 ** OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 ** SOFTWARE.
 **/

//============================================================================
/// \file   SiLAServer.cpp
/// \author Florian Meinicke (florian.meinicke@cetoni.de)
/// \date   06.01.2020
/// \brief  Implementation of the CSiLAServer class
//============================================================================

//============================================================================
//                                  INCLUDES
//============================================================================
#include <sila_cpp/common/FullyQualifiedFeatureID.h>
#include <sila_cpp/common/ServerInformation.h>
#include <sila_cpp/common/logging.h>
#include <sila_cpp/common/utils.h>
#include <sila_cpp/data_types.h>
#include <sila_cpp/discovery/SiLAServerDiscovery.h>
#include <sila_cpp/server/SiLAServer.h>

#include "../framework/features/org/silastandard/core/SiLAService/SiLAServiceImpl.h"
#include "InterruptSignalHandler.h"

#include <QCoreApplication>
#include <QSettings>

#include <grpcpp/grpcpp.h>

#include <numeric>
#include <utility>
#include <vector>

static const auto SERVER_NAME_SETTINGS_KEY = QStringLiteral("ServerName");

using namespace std;
using namespace isocpp_p0201;

namespace SiLA2
{
/**
 * @brief Private data of the CSiLAServer class - pimpl
 */
class CSiLAServer::PrivateImpl
{
public:
    /**
     * @brief C'tor
     */
    PrivateImpl(CSiLAServer* parent, CServerAddress Address);

    /**
     * @brief Set the @b ServerUUID and @b ServerName fields of @a ServerInfo to
     * the UUID/Name stored in the @a ServerSettings
     *
     * @param ServerInfo The ServerInfo to update with the @b ServerUUID and
     * @b ServerName fields read from the settings
     */
    void updateServerInfoFromSettings(CServerInformation& ServerInfo) const;

    /**
     * @brief Create and register the SiLA Service Feature
     *
     * @param ServerInfo The information about the Server (name, type, ...)
     */
    void registerSiLAService(const CServerInformation& ServerInfo);

    /**
     * @brief Publishes the Server using SiLA Server Discovery (calling this
     * function multiple times will result in a reload of the Discovery entry with
     * every call)
     *
     * @param ServerInfo Additional information of the Server to add to the
     * Discovery entry
     */
    void publishServer(const CServerInformation& ServerInfo) const;

    /**
     * @brief Run the server
     *
     * @param Block Whether to block execution in the main thread until the server
     * is shutdown again
     * @param ForceInsecure Whether to force running the server without encryption
     */
    void run(bool Block, bool ForceInsecure);

    CSiLAServer* q_ptr;

    // clang-format off
    const CServerAddress ServerAddress{};
    CSSLCredentials Credentials{};                    ///< for secure communication
    shared_ptr<QSettings> ServerSettings{};           ///< (not copyable)
    shared_ptr<grpc::ServerBuilder> ServerBuilder{};  ///< gRPC server builder (not copyable)
    shared_ptr<grpc::Server> Server;                  ///< The actual gRPC server
    // clang-format on
    vector<shared_ptr<grpc::ServerCompletionQueue>>
        CompletionQueues{};  ///< For async communication (shared with CAsyncRPCHandlers)

    shared_ptr<CSiLAService> SiLAService{};  ///< Mandatory SiLAService feature
    shared_ptr<CSiLAServerDiscovery> Discovery{};  ///< to publish server via mDNS

    bool IsBlocking{false};  ///< whether `run` was called with block=true

    PIMPL_DECLARE_PUBLIC(CSiLAServer)
};

//============================================================================
CSiLAServer::PrivateImpl::PrivateImpl(CSiLAServer* parent, CServerAddress Address)
    : q_ptr{parent},
      ServerAddress{std::move(Address)},
      ServerSettings{make_shared<QSettings>("SiLA2", "sila_cpp", parent)},
      ServerBuilder{make_shared<grpc::ServerBuilder>()},
      Discovery{make_shared<CSiLAServerDiscovery>(parent)}
{
    Q_UNUSED(SiLALogManager)
}

//============================================================================
void CSiLAServer::PrivateImpl::updateServerInfoFromSettings(
    CServerInformation& ServerInfo) const
{
    // group to store all Server UUIDs ever created on this machine
    ServerSettings->beginGroup("UUIDs");
    // create a hash of Server Name, Type, Description, Version and Vendor URL...
    const auto v = vector{ServerInfo.serverName(), ServerInfo.serverType(),
                          ServerInfo.description(), ServerInfo.version(),
                          ServerInfo.vendorURL()};
    const auto Hash = std::accumulate(
        begin(v), end(v), 0,
        [](uint lhs, const auto& rhs) { return lhs ^ qHash(rhs); });
    const auto HashString = QString::number(Hash);
    // ... and use that hash to get the UUID for our Server (if already present)
    const auto UUID =
        ServerSettings->value(HashString, ServerInfo.serverUUID().toString())
            .toUuid();
    if (UUID == ServerInfo.serverUUID())
    {
        // this Server is started for the first time ever, so we save this UUID
        ServerSettings->setValue(HashString, UUID);
    }
    else
    {
        // this Server has been run before and had the UUID that we just read from
        // the Settings
        ServerInfo.setServerUUID(UUID);
    }
    qCDebug(sila_cpp_server) << "Server UUID:" << UUID;
    ServerSettings->endGroup();

    // now with the correct UUID let's get the Server Name
    ServerSettings->beginGroup(UUID.toString());
    const auto ServerName =
        ServerSettings->value(SERVER_NAME_SETTINGS_KEY, ServerInfo.serverName())
            .toString();
    if (ServerName != ServerInfo.serverName())
    {
        ServerInfo.setServerName(ServerName);
    }
    ServerSettings->setValue(SERVER_NAME_SETTINGS_KEY, ServerName);
    qCDebug(sila_cpp_server) << "Server Name:" << ServerName;
    ServerSettings->endGroup();
}

//============================================================================
void CSiLAServer::PrivateImpl::registerSiLAService(
    const CServerInformation& ServerInfo)
{
    PIMPL_Q(CSiLAServer);
    qCDebug(sila_cpp_server) << "Registering SiLAService feature";
    SiLAService = make_shared<CSiLAService>(ServerInfo, q);
    ServerBuilder->RegisterService(this->SiLAService.get());
}

//============================================================================
void CSiLAServer::PrivateImpl::publishServer(
    const CServerInformation& ServerInfo) const
{
    // setting SILA_CPP_DISABLE_DISCOVERY non-zero disables zeroconf registration
    static const auto DisableDiscovery =
        qEnvironmentVariable("SILA_CPP_DISABLE_DISCOVERY", "0");
    if (DisableDiscovery != "0")
    {
        qCWarning(sila_cpp_server)
            << "Not publishing zeroconf service for this SiLA server";
        return;
    }
    Discovery->unpublishServer();
    if (Credentials.isSelfSigned())
    {
        Discovery->publishServer(ServerInfo, ServerAddress.port().toInt(),
                                 QString::fromStdString(Credentials.rootCA()));
    }
    else
    {
        Discovery->publishServer(ServerInfo, ServerAddress.port().toInt());
    }
}

//============================================================================
void CSiLAServer::PrivateImpl::run(bool Block, bool ForceInsecure)
{
    PIMPL_Q(CSiLAServer);

    try
    {
        if (ForceInsecure)
        {
            Credentials = CSSLCredentials{};
        }
        // couldn't get credentials from c'tor argument
        else if (Credentials.isEmpty())
        {
            Credentials = CSSLCredentials::fromSelfSignedCertificate(
                SiLAService->serverInformation().serverUUID(),
                ServerAddress.ip());
            qCDebug(sila_cpp)
                << Credentials.certificate() << Credentials.rootCA();
        }
    }
    catch (const runtime_error& err)
    {
        qCCritical(sila_cpp_server) << "OpenSSL error" << err.what();
        throw runtime_error{
            "Won't start the server because no SSL credentials have been "
            "provided and generating a self-signed certificate failed!"};
    }

    ServerBuilder->AddListeningPort(ServerAddress.toStdString(),
                                    Credentials.toServerCredentials());
    const auto ServerInformation = SiLAService->serverInformation();
    qCInfo(sila_cpp_server) << "Starting SiLA2 server with server name"
                            << ServerInformation.serverName();
    publishServer(ServerInformation);

    Server = ServerBuilder->BuildAndStart();
    if (!Server)
    {
        throw runtime_error{"SiLA Server could not be started! Maybe the "s
                            + qPrintable(ServerAddress.prettyString())
                            + " is already in use by another process?"};
    }
    emit q->started();
    qCInfo(sila_cpp_server) << "Server listening on" << ServerAddress;

    IsBlocking = Block;
    if (Block)
    {
        const auto SignalHandler = CInterruptSignalHandler{q};
        connect(&SignalHandler,
                &CInterruptSignalHandler::interruptSignalTriggered, q,
                &CSiLAServer::shutdown);

        qCInfo(sila_cpp_server)
            << "Blocking this thread until server is shutdown with Ctrl-C...";
        QCoreApplication::exec();
    }
}

///===========================================================================
CSiLAServer::CSiLAServer(CServerInformation ServerInformation,
                         const CServerAddress& Address, QObject* parent,
                         PrivateImplPtr priv)
    : QObject{parent},
      d_ptr{priv ? std::move(priv) :
                   make_polymorphic_value<PrivateImpl>(this, Address)}
{
    PIMPL_D(CSiLAServer);

    d->updateServerInfoFromSettings(ServerInformation);

    d->registerSiLAService(ServerInformation);

    connect(d->SiLAService.get(), &CSiLAService::serverNameChanged, this,
            [d](const auto& ServerName) {
                const auto ServerInfo = d->SiLAService->serverInformation();
                d->ServerSettings->beginGroup(ServerInfo.serverUUID().toString());
                d->ServerSettings->setValue(SERVER_NAME_SETTINGS_KEY, ServerName);
                d->ServerSettings->endGroup();
                d->publishServer(ServerInfo);
            });
}

//============================================================================
CSiLAServer::~CSiLAServer()
{
    PIMPL_D(CSiLAServer);
    if (d->Server)
    {
        shutdown();
    }
}

//=============================================================================
void CSiLAServer::registerFeature(grpc::Service* Feature,
                                  const CFullyQualifiedFeatureID& Identifier,
                                  const QByteArray& FeatureDefinition)
{
    PIMPL_D(CSiLAServer);
    qCInfo(sila_cpp_server) << "Registering feature" << Identifier;
    d->ServerBuilder->RegisterService(Feature);
    d->SiLAService->registerFeature(Identifier, FeatureDefinition);
}

//=============================================================================
shared_ptr<grpc::ServerCompletionQueue> CSiLAServer::addCompletionQueue()
{
    PIMPL_D(CSiLAServer);
    d->CompletionQueues.push_back(d->ServerBuilder->AddCompletionQueue());
    return d->CompletionQueues.back();
}

//============================================================================
CSSLCredentials CSiLAServer::credentials() const
{
    PIMPL_D(const CSiLAServer);
    return d->Credentials;
}

//=============================================================================
void CSiLAServer::run(bool block)
{
    PIMPL_D(CSiLAServer);
    d->run(block, false);
}

//=============================================================================
void CSiLAServer::run(const CSSLCredentials& Credentials, bool block)
{
    PIMPL_D(CSiLAServer);
    d->Credentials = Credentials;
    d->run(block, false);
}

//============================================================================
void CSiLAServer::runInsecure(bool block)
{
    PIMPL_D(CSiLAServer);
    qCWarning(sila_cpp_server)
        << "Running SiLA 2 Server without encryption. This violates the SiLA 2 "
           "specification! Only use this in a safe environment.";
    d->run(block, true);
}

//============================================================================
void CSiLAServer::shutdown()
{
    PIMPL_D(CSiLAServer);
    qCInfo(sila_cpp_server) << "Shutting down server";

    d->Discovery->unpublishServer();

    d->Server->Shutdown(gpr_time_from_seconds(1, GPR_TIMESPAN));
    d->Server->Wait();
    for_each(begin(d->CompletionQueues), end(d->CompletionQueues),
             [](auto CQ) { shutdownAndDrainCompletionQueue(*CQ); });
    QThread::msleep(500);  // give the RPCs some time to deallocate themselves
    d->Server.reset();
    if (d->IsBlocking)
    {
        QCoreApplication::quit();
    }
}
}  // namespace SiLA2
