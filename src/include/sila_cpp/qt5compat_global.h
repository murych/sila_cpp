/*******************************************************************************
 ** This file is part of the sila_cpp project.
 ** Copyright 2023 SiLA 2
 **
 ** Permission is hereby granted, free of charge, to any person obtaining a copy
 ** of this software and associated documentation files (the "Software"), to deal
 ** in the Software without restriction, including without limitation the rights
 ** to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 ** copies of the Software, and to permit persons to whom the Software is
 ** furnished to do so, subject to the following conditions:
 **
 ** The above copyright notice and this permission notice shall be included in all
 ** copies or substantial portions of the Software.
 **
 ** THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 ** IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 ** FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 ** AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 ** LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 ** OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 ** SOFTWARE.
 *****************************************************************************/

//============================================================================
/// \file   qt5compat_global.h
/// \author Florian Meinicke (florian.meinicke@cetoni.de)
/// \date   22.11.2023
/// \brief  Declaration of Qt5 compatibility macros
//============================================================================
#ifndef QT5COMPAT_GLOBAL_H
#define QT5COMPAT_GLOBAL_H

//============================================================================
//                                  INCLUDES
//============================================================================

// In Qt6 the order of arguments for QtConcurrent::run changed - use this
// to avoid many #ifdeds in code
#if QT_VERSION <= QT_VERSION_CHECK(6, 0, 0)
#    define Qt5Compat_ConcurrentMemberFunc(_member_, _object_) _object_, _member_
#else
#    define Qt5Compat_ConcurrentMemberFunc(_member_, _object_) _member_, _object_
#endif

#endif  // QT5COMPAT_GLOBAL_H
