/**
 ** This file is part of the sila_cpp project.
 ** Copyright 2021 SiLA2
 **
 ** Permission is hereby granted, free of charge, to any person obtaining a copy
 ** of this software and associated documentation files (the "Software"), to deal
 ** in the Software without restriction, including without limitation the rights
 ** to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 ** copies of the Software, and to permit persons to whom the Software is
 ** furnished to do so, subject to the following conditions:
 **
 ** The above copyright notice and this permission notice shall be included in all
 ** copies or substantial portions of the Software.
 **
 ** THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 ** IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 ** FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 ** AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 ** LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 ** OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 ** SOFTWARE.
 **/

//============================================================================
/// \file   DynamicValue.h
/// \author Florian Meinicke (florian.meinicke@cetoni.de)
/// \date   24.02.2021
/// \brief  Definition of the CDynamicValue class
//============================================================================
#ifndef DYNAMICVALUE_H
#define DYNAMICVALUE_H

//============================================================================
//                                  INCLUDES
//============================================================================
#include <sila_cpp/codegen/fdl/Constraint.h>
#include <sila_cpp/common/FullyQualifiedDataTypeID.h>
#include <sila_cpp/common/logging.h>
#include <sila_cpp/data_types.h>
#include <sila_cpp/global.h>
#include <sila_cpp/internal/type_traits.h>

#include <polymorphic_value.h>

//============================================================================
//                            FORWARD DECLARATIONS
//============================================================================
namespace SiLA2
{
class CDynamicStructure;
class CCustomDataType;
class CDynamicFeatureStub;
}  // namespace SiLA2

namespace SiLA2
{
/**
 * @brief The CDynamicValue class encompasses a value that has a type that is
 * not known at compile time
 *
 * A dynamic value may be of any SiLA Data Type, i.e. either
 * @li a plain SiLA Basic Type like a String or Integer,
 * @li a List of a SiLA Basic Type,
 * @li a constrained version of the above (which is handled exactly the same as
 * the unconstrained Type except that the @c dataTypeType() is different and
 * @c constraints() will return the list of constraints on the value)
 * @li a SiLA Structure Type,
 * @li a Custom SiLA Type defined by a Data Type Definition in the FDL.
 * Use @c dataTypeType() to find out which Data Type the value has and in case
 * it's one of the first three types described above use @c basicTypeType() to
 * find out what type the underlying Basic Type has (i.e. String, Integer, Real,
 * ...).
 */
class SILA_CPP_EXPORT CDynamicValue
{
protected:
    friend class CDynamicProperty;
    class PrivateImpl;
    using PrivateImplPtr = isocpp_p0201::polymorphic_value<PrivateImpl>;

public:
    enum class DataTypeType
    {
        Unknown,
        Basic,
        Constrained,
        DataTypeDefinition,
        List,
        Structure,

        UNKNOWN [[deprecated("Use the CamelCase version instead")]] = Unknown,
        BASIC [[deprecated("Use the CamelCase version instead")]] = Basic,
        CONSTRAINED
        [[deprecated("Use the CamelCase version instead")]] = Constrained,
        DATA_TYPE_DEFINITION
        [[deprecated("Use the CamelCase version instead")]] = DataTypeDefinition,
        LIST [[deprecated("Use the CamelCase version instead")]] = List,
        STRUCTURE [[deprecated("Use the CamelCase version instead")]] = Structure,
    };
    using eDataTypeType
        [[deprecated("Use DataTypeType (without the 'e' prefix) instead")]] =
            DataTypeType;

    enum class BasicTypeType
    {
        Invalid,  ///< not a actually a Basic Type
        Any,
        Binary,
        Boolean,
        Date,
        Duration,
        Integer,
        Real,
        String,
        Time,
        Timestamp,
        Timezone,

        INVALID [[deprecated("Use the CamelCase version instead")]] = Invalid,
        ANY [[deprecated("Use the CamelCase version instead")]] = Any,
        BINARY [[deprecated("Use the CamelCase version instead")]] = Binary,
        BOOLEAN [[deprecated("Use the CamelCase version instead")]] = Boolean,
        DATE [[deprecated("Use the CamelCase version instead")]] = Date,
        DURATION [[deprecated("Use the CamelCase version instead")]] = Duration,
        INTEGER [[deprecated("Use the CamelCase version instead")]] = Integer,
        REAL [[deprecated("Use the CamelCase version instead")]] = Real,
        STRING [[deprecated("Use the CamelCase version instead")]] = String,
        TIME [[deprecated("Use the CamelCase version instead")]] = Time,
        TIMESTAMP [[deprecated("Use the CamelCase version instead")]] = Timestamp,
        TIMEZONE [[deprecated("Use the CamelCase version instead")]] = Timezone,
    };
    using eBasicTypeType
        [[deprecated("Use BasicTypeType (without the 'e' prefix) instead")]] =
            BasicTypeType;

    /**
     * @brief Default c'tor (constructs a Dynamic Value with no value)
     */
    CDynamicValue();

    /**
     * @brief C'tor for non-list values
     *
     * @param Value The initial value (already having the correct type)
     * @param DataType A list of the parsed FDL description of the Value's Data
     * Type
     * @param DMF A shared_ptr to the Dynamic Message Factory that should be
     * used to convert this Value to dynamic protobuf @c Messages
     *
     * @note The @c CDynamicValue will @b not take ownership of the @a Value
     */
    CDynamicValue(
        google::protobuf::Message* Value, codegen::fdl::CDataType DataType,
        std::shared_ptr<codegen::proto::CDynamicMessageFactory> DMF = nullptr);

    /**
     * @brief C'tor for list values (i.e. this Dynamic Value is to represent a
     * List)
     *
     * @param Values The initial values (already having the correct type)
     * @param DataType A list of the parsed FDL description of the Value's Data
     * Type
     * @param DMF A shared_ptr to the Dynamic Message Factory that should be
     * used to convert this Value to dynamic protobuf @c Messages
     *
     * @note The @c CDynamicValue will @b not take ownership of the @a Values
     */
    CDynamicValue(
        std::vector<google::protobuf::Message*> Values,
        codegen::fdl::CDataType DataType,
        std::shared_ptr<codegen::proto::CDynamicMessageFactory> DMF = nullptr);

    /**
     * @brief Copy c'tor
     */
    CDynamicValue(const CDynamicValue& rhs);

    /**
     * @brief Move c'tor
     */
    CDynamicValue(CDynamicValue&& rhs) noexcept;

    /**
     * @brief Copy assignment operator
     */
    CDynamicValue& operator=(const CDynamicValue& rhs);

    /**
     * @brief Move assignment operator
     */
    CDynamicValue& operator=(CDynamicValue&& rhs) noexcept;

    /**
     * @brief D'tor
     */
    virtual ~CDynamicValue() = default;

    /// ---------------------------- Single value -----------------------------
    /**
     * @brief Get the value
     *
     * @note No ownership is transferred here! Use @c getCopy() instead if you
     * need ownership of the underlying @c Message pointer
     *
     * @return A pointer to the value (already having the correct type)
     */
    [[nodiscard]] google::protobuf::Message* value() const;

    /**
     * @brief Get a copy of the value passing ownership of the copy to the caller
     *
     * @note If the Value is empty then the returned pointer is of course
     * @c nullptr, so be sure to check for that
     *
     * @return A copy of the value (already having the correct type)
     */
    [[nodiscard]] std::unique_ptr<google::protobuf::Message> getCopy() const;

    /**
     * @brief Set the value to the given @a Value
     *
     * @tparam T The type of the value to set (must be a wrapped SiLA Type,
     * e.g. @c SiLA2::CString)
     * @param Value The value to set
     */
    template<typename T, typename = std::enable_if_t<internal::is_sila_type_v<T>>>
    void setValue(const T& Value);

    /**
     * @brief Set the value to the given @a Value
     *
     * @tparam T The type of the value to set (must be a SiLA Framework Type,
     * e.g. @c sila2::org::silastandard::String)
     * @param Value The value to set
     */
    template<typename T,
             typename = std::enable_if_t<internal::is_sila_framework_type_v<T>>,
             typename = void>
    void setValue(const T& Value);

    /**
     * @brief Set the value to the given @a Value
     *
     * @tparam T The type of the value to set (must be a SiLA Framework Type,
     * e.g. @c sila2::org::silastandard::String)
     * @param Value The value to set
     *
     * @note The @c CDynamicValue will @b not take ownership of the @a Value
     */
    template<typename T,
             typename = std::enable_if_t<internal::is_sila_framework_type_v<T>>>
    void setValue(T* Value);

    /**
     * @brief Set the value to the given @a Value
     *
     * @tparam T The type of the value to set (must be a type that is
     * convertible to a wrapped SiLA Type, e.g. a @c std::string for a @c
     * SiLA2::CString)
     * @param Value The value to set
     *
     * @note You'll need to provide the direct C++ equivalent for the desired
     * SiLA Type, i.e. if you want to set the value to a @c SiLA2::CBoolean
     * you have to pass a value of type @c bool, for a @c SiLA2::CInteger a
     * @c int, @c uint or @c long, for a @c SiLA2::CReal a @c double or a @c float
     * and for a @c SiLA2::CString a <code>const char*</code>, a @c std::string or
     * a @c QString.
     */
    template<typename T,
             typename = std::enable_if_t<internal::is_sila_type_convertible_v<T>>,
             typename = void, typename = void>
    void setValue(const T& Value);

    /**
     * @brief Set the value to the given Structure @a Struct
     *
     * @param Struct The Structure to set as the value
     */
    void setValue(const CDynamicStructure& Struct);

    /**
     * @brief Set the value to the given Data Type @a Data Type
     *
     * @param DataType The Data Type to set as the value
     */
    void setValue(const CCustomDataType& DataType);

    /**
     * @brief Assignment operator
     *
     * @tparam T The type of the value (must be either a wrapped SiLA Type,
     * e.g. @c SiLA2::CString, or a SiLA Framework Type, e.g.
     * @c sila2::org::silastandard::String or a C++ type that is convertible
     * to a wrapped SiLA Type, e.g. a @c std::string for a @c SiLA2::CString)
     * @param Value The value to set
     */
    template<typename T,
             typename = std::enable_if_t<internal::is_sila_type_v<T>
                                         || internal::is_sila_framework_type_v<T>
                                         || internal::is_sila_type_convertible_v<T>
                                         || std::is_same_v<T, CDynamicStructure>
                                         || std::is_same_v<T, CCustomDataType>>>
    CDynamicValue& operator=(const T& Value);

    /// ----------------------------- List value ------------------------------
    /**
     * @brief Get the values
     *
     * @note No ownership is transferred here! Use @c getCopy() instead if you
     * need ownership of the underlying @c Message pointers
     *
     * @return A vector of pointers to the values (already having the correct
     * type)
     */
    [[nodiscard]] std::vector<google::protobuf::Message*> values() const;

    /**
     * @brief Get a copy of the values passing ownership of the copies to the
     * caller
     *
     * @note If the list of Values is empty then the returned vector is of course
     * empty as well, so be sure to check for that
     *
     * @return A copy of the values (already having the correct type)
     */
    [[nodiscard]] std::vector<std::unique_ptr<google::protobuf::Message>>
    getCopies() const;

    /**
     * @brief Set the values to the given @a Values. @a Values must be a <i>fixed
     * sized</i> list
     *
     * @tparam ContainerT The List type (e.g. @c std::array)
     * @tparam T The type of the value to set (must be a wrapped SiLA Type, a SiLA
     * Framework Type, or a type that is convertible to a wrapped SiLA Type, e.g.
     * a @c std::string for a @c SiLA2::CString)
     * @tparam N The size of the List
     *
     * @param Values The values to set
     */
    template<template<typename, auto> class ContainerT, typename T, auto N,
             typename =
                 std::enable_if_t<internal::is_container_v<ContainerT<T, N>>
                                  && (internal::is_sila_type_v<T>
                                      || internal::is_sila_framework_type_v<T>
                                      || internal::is_sila_type_convertible_v<T>)>>
    void setValues(const ContainerT<T, N>& Values);

    /**
     * @brief Set the values to the given @a Values
     *
     * @tparam ContainerT The List type (e.g. @c std::vector, @c std::list, ...)
     * @tparam T The type of the value to set (must be a wrapped SiLA Type, a SiLA
     * Framework Type, or a type that is convertible to a wrapped SiLA Type, e.g.
     * a @c std::string for a @c SiLA2::CString)
     *
     * @param Values The values to set
     */
    template<
        template<typename> class ContainerT, typename T,
        typename = std::enable_if_t<(internal::is_container_v<ContainerT<T>>)&&(
            (internal::is_sila_type_v<T>) || (internal::is_sila_framework_type_v<T>)
            || (internal::is_sila_type_convertible_v<T>))>>
    void setValues(const ContainerT<T>& Values);

    /**
     * @brief Add the given @a Value to this Dynamic Value if it represents a List
     * (if not this function does nothing)
     *
     * @tparam T The type of the value to set (must be a wrapped SiLA Type, a SiLA
     * Framework Type, or a type that is convertible to a wrapped SiLA Type, e.g.
     * a @c std::string for a @c SiLA2::CString)
     *
     * @param Value The value to add
     *
     * @note If the value to add is not of the same (or a convertible) type as the
     * values already in the list then this function will log a warning but it
     * will not add the value to the list
     */
    template<typename T, typename = std::enable_if_t<
                             (internal::is_sila_type_v<T>)
                             || (internal::is_sila_framework_type_v<T>)
                             || (internal::is_sila_type_convertible_v<T>)>>
    void addValue(const T& Value);

    /**
     * @brief Add the Structure  @a Struct to this Dynamic Value if it represents
     * a List (if not this function does nothing)
     *
     * @param Struct The Structure to add
     */
    void addValue(const CDynamicStructure& Struct);

    /**
     * @brief Add the given Data Type @a DataType to this Dynamic Value if it
     * represents a List (if not this function does nothing)
     *
     * @param DataType The Data Type to add
     */
    void addValue(const CCustomDataType& DataType);

    /**
     * @brief Set the value at the given @a Index to the given @a Value
     *
     * @tparam T The type of the value to set (must be a wrapped SiLA Type,
     * e.g. @c SiLA2::CString)
     * @param Value The value to set
     * @param Index The index of the value in the list to set
     *
     * @note If the value to set is not of the same (or a convertible) type as the
     * values already in the list then this function will log a warning but it
     * will not set the value in the list
     */
    template<typename T, typename = std::enable_if_t<internal::is_sila_type_v<T>>>
    void setValue(const T& Value, size_t Index);

    /**
     * @brief Set the value at the given @a Index to the given @a Value
     *
     * @tparam T The type of the value to set (must be a SiLA Framework Type,
     * e.g. @c sila2::org::silastandard::String)
     * @param Value The value to set
     * @param Index The index of the value in the list to set
     *
     * @note If the value to set is not of the same (or a convertible) type as the
     * values already in the list then this function will log a warning but it
     * will not set the value in the list
     */
    template<typename T,
             typename = std::enable_if_t<internal::is_sila_framework_type_v<T>>,
             typename = void>
    void setValue(const T& Value, size_t Index);

    /**
     * @brief Set the value at the given @a Index to the given @a Value
     *
     * @tparam T The type of the value to set (must be a SiLA Framework Type,
     * e.g. @c sila2::org::silastandard::String)
     * @param Value The value to set
     * @param Index The index of the value in the list to set
     *
     * @note If the value to set is not of the same (or a convertible) type as the
     * values already in the list then this function will log a warning but it
     * will not set the value in the list
     *
     * @note The @c CDynamicValue will @b not take ownership of the @a Value
     */
    template<typename T,
             typename = std::enable_if_t<internal::is_sila_framework_type_v<T>>>
    void setValue(T* Value, size_t Index);

    /**
     * @brief Set the value at the given @a Index to the given @a Value
     *
     * @tparam T The type of the value to set (must be a type that is
     * convertible to a wrapped SiLA Type, e.g. a @c std::string for a @c
     * SiLA2::CString)
     * @param Value The value to set
     * @param Index The index of the value in the list to set
     *
     * @note If the value to set is not of the same (or a convertible) type as the
     * values already in the list then this function will log a warning but it
     * will not set the value in the list
     *
     * @note You'll need to provide the direct C++ equivalent for the desired
     * SiLA Type, i.e. if you want to set the value to a @c SiLA2::CBoolean
     * you have to pass a value of type @c bool, for a @c SiLA2::CInteger a
     * @c int, @c uint or @c long, for a @c SiLA2::CReal a @c double or a @c float
     * and for a @c SiLA2::CString a <code>const char*</code>, a @c std::string or
     * a @c QString.
     */
    template<typename T,
             typename = std::enable_if_t<internal::is_sila_type_convertible_v<T>>,
             typename = void, typename = void>
    void setValue(const T& Value, size_t Index);

    /**
     * @brief Set the value at the given @a Index to the given Structure
     * @a Struct
     *
     * @param Struct The Structure to set
     * @param Index The index of the value in the list to set
     */
    void setValue(const CDynamicStructure& Struct, size_t Index);

    /**
     * @brief Set the value at the given @a Index to the given Data Type
     * @a DataType
     *
     * @param DataType The Data Type to set
     * @param Index The index of the value in the list to set
     */
    void setValue(const CCustomDataType& DataType, size_t Index);

    /**
     * @brief Assignment operator (fixed-size list version)
     *
     * @tparam T The type of the values (must be either a wrapped SiLA Type,
     * e.g. @c SiLA2::CString, or a SiLA Framework Type, e.g.
     * @c sila2::org::silastandard::String or a C++ type that is convertible
     * to a wrapped SiLA Type, e.g. a @c std::string for a @c SiLA2::CString)
     * @param Values The values to set
     */
    template<
        template<typename, auto> class ContainerT, typename T, auto N,
        typename = std::enable_if_t<(internal::is_container_v<ContainerT<T, N>>)&&(
            (internal::is_sila_type_v<T>) || (internal::is_sila_framework_type_v<T>)
            || (internal::is_sila_type_convertible_v<T>))>>
    CDynamicValue& operator=(const ContainerT<T, N>& Values);

    /**
     * @brief Assignment operator (list version)
     *
     * @tparam T The type of the values (must be either a wrapped SiLA Type,
     * e.g. @c SiLA2::CString, or a SiLA Framework Type, e.g.
     * @c sila2::org::silastandard::String or a C++ type that is convertible
     * to a wrapped SiLA Type, e.g. a @c std::string for a @c SiLA2::CString)
     * @param Values The values to set
     */
    template<
        template<typename> class ContainerT, typename T,
        typename = std::enable_if_t<(internal::is_container_v<ContainerT<T>>)&&(
            (internal::is_sila_type_v<T>) || (internal::is_sila_framework_type_v<T>)
            || (internal::is_sila_type_convertible_v<T>))>>
    CDynamicValue& operator=(const ContainerT<T>& Values);

    /// ------------------------------- Common --------------------------------
    /**
     * @brief Clears the value (essentially deleting the underlying protobuf
     * @c Message(s))
     */
    void clear();

    /**
     * @brief Returns the string representation of this Value that can be
     * displayed in a UI
     *
     * @return The Value's string representation
     */
    [[nodiscard]] virtual QString prettyString() const;

    /**
     * @brief Convert this Dynamic Value into its corresponding protobuf
     * @c Message
     *
     * @note This is reimplemented by each subclass. The base class implementation
     *   always returns @c nullptr
     *
     * @return A pointer to the protobuf @c Message for this Value
     */
    [[nodiscard]] virtual google::protobuf::Message* toProtoMessagePtr() const;

    /// ------------------ Type information about the value ------------------
    /**
     * @brief Get the FDL description of the Data Type of this value
     *
     * @return The value's Data Type's FDL description
     */
    [[nodiscard]] codegen::fdl::CDataType dataType() const;

    /**
     * @brief Get the type of the Value's Data Type
     *
     * @return The Value's Data Type Type
     */
    [[nodiscard]] DataTypeType dataTypeType() const;

    /**
     * @brief Get a human-readable representation of the Data Type type of this
     * Dynamic Value
     *
     * This is a convenience method and essentially the same as
     * @code
     * CDynamicValue::dataTypeTypeToString(SomeValue.dataTypeType());
     * @endcode
     *
     * @return This value's Data Type type's human-readable string representation
     */
    [[nodiscard]] std::string dataTypeTypeName() const;

    /**
     * @brief Get the type of the Value's underlying Data Type
     *
     * @note This function will only return the underlying Data Type for
     * Constrained and List Types. In all other cases this function returns the
     * same value as @c dataTypeType()
     *
     * @return The Value's underlying Data Type Type
     */
    [[nodiscard]] DataTypeType underlyingDataTypeType() const;

    /**
     * @brief Get a human-readable representation of the underlying Data Type type
     * of this Dynamic Value
     *
     * This is a convenience method and essentially the same as
     * @code
     * CDynamicValue::dataTypeTypeToString(SomeValue.underlyingDataTypeType());
     * @endcode
     *
     * @note This function will only return the underlying Data Type for
     * Constrained and List Types. In all other cases this function returns the
     * same value as @c dataTypeTypeName()
     *
     * @return This value's underlying Data Type type's human-readable string
     * representation
     */
    [[nodiscard]] std::string underlyingDataTypeTypeName() const;

    /**
     * @brief Convert the given @c DataTypeType @a Type to its human-readable
     * string representation
     *
     * @param Type The @c DataTypeType to convert
     * @return The @a Type's human-readable string representation
     */
    [[nodiscard]] static std::string dataTypeTypeToString(DataTypeType Type);

    /**
     * @brief Get the type of Basic Data Type when this Value has a Basic,
     * Constrained of Basic, List of Basic, Constrained List of Basic or List of
     * Constrained Basic Data Type
     *
     * @return The Value's Basic Data Type Type
     */
    [[nodiscard]] BasicTypeType basicTypeType() const;

    /**
     * @brief Get a human-readable representation of the Basic Data Type type of
     * this Dynamic Value
     *
     * This is a convenience method and essentially the same as
     * @code
     * CDynamicValue::basicDataTypeTypeToString(SomeValue.basicDataTypeType());
     * @endcode
     *
     * @return This value's Basic Data Type type's human-readable string
     * representation
     */
    [[nodiscard]] std::string basicTypeTypeName() const;

    /**
     * @brief Convert the given @c BasicTypeType @a BasicType to its
     * human-readable string representation
     *
     * @param BasicType The @c BasicTypeType to convert
     * @return The @a BasicType's human-readable string representation
     */
    [[nodiscard]] static std::string basicTypeTypeToString(
        BasicTypeType BasicType);

    /**
     * @brief Get whether the type of this value is a SiLA Basic Type
     *
     * @returns true, if this value's type is a SiLA Basic Type, false otherwise
     */
    [[nodiscard]] bool isBasic() const
    {
        return dataTypeType() == DataTypeType::Basic;
    }

    /**
     * @brief Get whether the type of this value is a SiLA Constrained Type
     *
     * @returns true, if this value's type is a SiLA Constrained Type, false
     * otherwise
     */
    [[nodiscard]] bool isConstrained() const
    {
        return dataTypeType() == DataTypeType::Constrained;
    }

    /**
     * @brief Get whether the type of this value is a SiLA List Type
     *
     * @returns true, if this value's type is a SiLA List Type, false otherwise
     */
    [[nodiscard]] bool isList() const
    {
        return dataTypeType() == DataTypeType::List;
    }

    /**
     * @brief Get whether the type of this value is a SiLA Structure Type
     *
     * @returns true, if this value's type is a SiLA Structure Type, false
     * otherwise
     */
    [[nodiscard]] bool isStructure() const
    {
        return dataTypeType() == DataTypeType::Structure;
    }

    /**
     * @brief Get whether the type of this value is a Custom Data Type
     *
     * @returns true, if this value's type is a Custom Data Type, false otherwise
     */
    [[nodiscard]] bool isCustomDataType() const
    {
        return dataTypeType() == DataTypeType::DataTypeDefinition;
    }

    /**
     * @brief Checks if this Dynamic Value is a (possibly Constrained or a List
     * of) SiLA 2 Any
     *
     * @returns true, if the Dynamic Value is an Any, false otherwise
     */
    [[nodiscard]] bool isAnyType() const
    {
        return basicTypeType() == BasicTypeType::Any;
    }

    /**
     * @brief Checks if this Dynamic Value is a (possibly Constrained or a List
     * of) SiLA 2 Binary
     *
     * @returns true, if the Dynamic Value is a Binary, false otherwise
     */
    [[nodiscard]] bool isBinary() const
    {
        return basicTypeType() == BasicTypeType::Binary;
    }

    /**
     * @brief Checks if this Dynamic Value is a (possibly Constrained or a List
     * of) SiLA 2 Boolean
     *
     * @returns true, if the Dynamic Value is a Boolean, false otherwise
     */
    [[nodiscard]] bool isBoolean() const
    {
        return basicTypeType() == BasicTypeType::Boolean;
    }

    /**
     * @brief Checks if this Dynamic Value is a (possibly Constrained or a List
     * of) SiLA 2 Date
     *
     * @returns true, if the Dynamic Value is a Date, false otherwise
     */
    [[nodiscard]] bool isDate() const
    {
        return basicTypeType() == BasicTypeType::Date;
    }

    /**
     * @brief Checks if this Dynamic Value is a (possibly Constrained or a List
     * of) SiLA 2 Duration
     *
     * @returns true, if the Dynamic Value is a Duration, false otherwise
     */
    [[nodiscard]] bool isDuration() const
    {
        return basicTypeType() == BasicTypeType::Duration;
    }

    /**
     * @brief Checks if this Dynamic Value is a (possibly Constrained or a List
     * of) SiLA 2 Integer
     *
     * @returns true, if the Dynamic Value is a Integer, false otherwise
     */
    [[nodiscard]] bool isInteger() const
    {
        return basicTypeType() == BasicTypeType::Integer;
    }

    /**
     * @brief Checks if this Dynamic Value is a (possibly Constrained or a List
     * of) SiLA 2 Real
     *
     * @returns true, if the Dynamic Value is a Real, false otherwise
     */
    [[nodiscard]] bool isReal() const
    {
        return basicTypeType() == BasicTypeType::Real;
    }

    /**
     * @brief Checks if this Dynamic Value is a (possibly Constrained or a List
     * of) SiLA 2 String
     *
     * @returns true, if the Dynamic Value is a String, false otherwise
     */
    [[nodiscard]] bool isString() const
    {
        return basicTypeType() == BasicTypeType::String;
    }

    /**
     * @brief Checks if this Dynamic Value is a (possibly Constrained or a List
     * of) SiLA 2 Time
     *
     * @returns true, if the Dynamic Value is a Time, false otherwise
     */
    [[nodiscard]] bool isTime() const
    {
        return basicTypeType() == BasicTypeType::Time;
    }

    /**
     * @brief Checks if this Dynamic Value is a (possibly Constrained or a List
     * of) SiLA 2 Timestamp
     *
     * @returns true, if the Dynamic Value is a Timestamp, false otherwise
     */
    [[nodiscard]] bool isTimestamp() const
    {
        return basicTypeType() == BasicTypeType::Timestamp;
    }

    /**
     * @brief Checks if this Dynamic Value is a (possibly Constrained or a List
     * of) SiLA 2 Timezone
     *
     * @returns true, if the Dynamic Value is a Timezone, false otherwise
     */
    [[nodiscard]] bool isTimezone() const
    {
        return basicTypeType() == BasicTypeType::Timezone;
    }

    /**
     * @brief Get the list of Constraints that this Value might have
     *
     * @return The list of this Value's Constraints as they have been parsed from
     * an FDL description or an empty list if this Value is not Constrained
     */
    [[nodiscard]] QList<codegen::fdl::CConstraint> constraints() const;

    /// ------------------ Convenient access to the value --------------------
    /**
     * @brief Convert this Dynamic Value to a Custom Data Type
     *
     * @param Feature The SiLA Feature that contains the Data Type Definition of
     * the Custom Data Type that this Value shall be converted to
     *
     * @return The value as a Custom Data Type
     *
     * @throws std::bad_cast if the value does not hold a Custom Data Type
     */
    [[nodiscard]] CCustomDataType toCustomDataType(
        const CDynamicFeatureStub* Feature) const;

    /**
     * @brief Convert this Dynamic Value to a Structure
     *
     * @param Feature The SiLA Feature that is used to resolve any nested Custom
     * Data Types
     *
     * @return The value as a Dynamic Structure or an empty Structure if the value
     * does not hold a Structure
     */
    [[nodiscard]] CDynamicStructure toStructure(
        CDynamicFeatureStub* Feature) const;

    /**
     * @brief Convert this Dynamic Value to an unconstrained Value
     *
     * @return The value as an unconstrained Dynamic Value or the same value if it
     * is not constrained
     */
    [[nodiscard]] CDynamicValue toUnconstrained() const;

    /**
     * @brief Convert this Dynamic Value to a List of Dynamic Values
     *
     * This is useful if the List's Element Type is not a SiLA Basic Type but a
     * Structure for example
     *
     * @return The value as a List of Dynamic Values or an empty List if the value
     * does not hold a List
     */
    [[nodiscard]] std::vector<CDynamicValue> toList() const;

    /**
     * @brief Convert this Dynamic Value to an AnyType
     *
     * @return The value as a AnyType or an empty (default constructed) AnyType if
     * the value does not hold an AnyType
     */
    [[nodiscard]] CAnyType toAnyType() const;

    /**
     * @brief Convert this Dynamic Value to a List of AnyTypes
     *
     * @return The value as a List of AnyTypes or an empty List if the value does
     * not hold a List of AnyTypes
     */
    [[nodiscard]] std::vector<CAnyType> toAnyTypeList() const;

    /**
     * @brief Convert this Dynamic Value to a Binary
     *
     * @return The value as a Binary or an empty (default constructed) Binary if
     * the value does not hold a Binary
     */
    [[nodiscard]] CBinary toBinary() const;

    /**
     * @brief Convert this Dynamic Value to a List of Binaries
     *
     * @return The value as a List of Binaries or an empty List if the value does
     * not hold a List of Binaries
     */
    [[nodiscard]] std::vector<CBinary> toBinaryList() const;

    /**
     * @brief Convert this Dynamic Value to a Boolean
     *
     * @return The value as a Boolean or an empty (default constructed) Boolean if
     * the value does not hold a Boolean
     */
    [[nodiscard]] CBoolean toBoolean() const;

    /**
     * @brief Convert this Dynamic Value to a List of Booleans
     *
     * @return The value as a List of Booleans or an empty List if the value does
     * not hold a List of Booleans
     */
    [[nodiscard]] std::vector<CBoolean> toBooleanList() const;

    /**
     * @brief Convert this Dynamic Value to a Date
     *
     * @return The value as a Date or an empty (default constructed) Date if
     * the value does not hold a Date
     */
    [[nodiscard]] CDate toDate() const;

    /**
     * @brief Convert this Dynamic Value to a List of Dates
     *
     * @return The value as a List of Dates or an empty List if the value does
     * not hold a List of Dates
     */
    [[nodiscard]] std::vector<CDate> toDateList() const;

    /**
     * @brief Convert this Dynamic Value to a Duration
     *
     * @return The value as a Duration or an empty (default constructed) Duration
     * if the value does not hold a Duration
     */
    [[nodiscard]] CDuration toDuration() const;

    /**
     * @brief Convert this Dynamic Value to a List of Durations
     *
     * @return The value as a List of Durations or an empty List if the value does
     * not hold a List of Durations
     */
    [[nodiscard]] std::vector<CDuration> toDurationList() const;

    /**
     * @brief Convert this Dynamic Value to a Integer
     *
     * @return The value as an Integer or an empty (default constructed) Integer
     * if the value does not hold an Integer
     */
    [[nodiscard]] CInteger toInteger() const;

    /**
     * @brief Convert this Dynamic Value to a List of Integer
     *
     * @return The value as a List of Integers or an empty List if the value does
     * not hold a List of Integers
     */
    [[nodiscard]] std::vector<CInteger> toIntegerList() const;

    /**
     * @brief Convert this Dynamic Value to a Real
     *
     * @return The value as a Real or an empty (default constructed) Real if
     * the value does not hold a Real
     */
    [[nodiscard]] CReal toReal() const;

    /**
     * @brief Convert this Dynamic Value to a List of Reals
     *
     * @return The value as a List of Reals or an empty List if the value does
     * not hold a List of Reals
     */
    [[nodiscard]] std::vector<CReal> toRealList() const;

    /**
     * @brief Convert this Dynamic Value to a String
     *
     * @return The value as a String or an empty (default constructed) String if
     * the value does not hold a String
     */
    [[nodiscard]] CString toString() const;

    /**
     * @brief Convert this Dynamic Value to a List of Strings
     *
     * @return The value as a List of Strings or an empty List if the value does
     * not hold a List of Strings
     */
    [[nodiscard]] std::vector<CString> toStringList() const;

    /**
     * @brief Convert this Dynamic Value to a Time
     *
     * @return The value as a Time or an empty (default constructed) Time if
     * the value does not hold a Time
     */
    [[nodiscard]] CTime toTime() const;

    /**
     * @brief Convert this Dynamic Value to a List of Times
     *
     * @return The value as a List of Times or an empty List if the value does
     * not hold a List of Times
     */
    [[nodiscard]] std::vector<CTime> toTimeList() const;

    /**
     * @brief Convert this Dynamic Value to a Timestamp
     *
     * @return The value as a Timestamp or an empty (default constructed)
     * Timestamp if the value does not hold a Timestamp
     */
    [[nodiscard]] CTimestamp toTimestamp() const;

    /**
     * @brief Convert this Dynamic Value to a List of Timestamps
     *
     * @return The value as a List of Timestamps or an empty List if the value
     * does not hold a List of Timestamps
     */
    [[nodiscard]] std::vector<CTimestamp> toTimestampList() const;

    /**
     * @brief Convert this Dynamic Value to a Timezone
     *
     * @return The value as a Timezone or an empty (default constructed) Timezone
     * if the value does not hold a Timezone
     */
    [[nodiscard]] CTimezone toTimezone() const;

    /**
     * @brief Convert this Dynamic Value to a List of Timezones
     *
     * @return The value as a List of Timezones or an empty List if the value does
     * not hold a List of Timezones
     */
    [[nodiscard]] std::vector<CTimezone> toTimezoneList() const;

    /**
     * @brief Get the Fully Qualified Data Type Identifier for the (underlying)
     * Custom Data Type Definition of this Value
     *
     * This Identifier can be used to get the corresponding @c CCustomDataType
     * object from the @a Feature. That object can then be filled with the desired
     * value(s) and then set as this Value by using @a setValue() or added to a
     * List by using @a addValue()
     *
     * @note This function should only be called when
     * <code>dataTypeType() == DataTypeType::DATA_TYPE_DEFINITION</code> or
     * <code>underlyingDataTypeType()==DataTypeType::DATA_TYPE_DEFINITION</code>.
     * Otherwise the returned @c CFullyQualifiedDataTypeID will be invalid (since
     * this Value has a different Data Type Type)
     *
     * @param Feature The SiLA Feature that contains the Data Type Definition of
     * the Custom Data Type of this Value
     *
     * @return The @c CFullyQualifiedDataTypeID of this Value's (underlying)
     * Custom Data Type Definition
     */
    [[nodiscard]] CFullyQualifiedDataTypeID customDataTypeIdentifier(
        const CDynamicFeatureStub* Feature) const;

    /**
     * @brief Get a default constructed (i.e. empty) @c CDynamicStructure
     * object for this Value
     *
     * This object can be filled with the necessary Structure Element values
     * and then set as this Value by using @a setValue() or added to a List by
     * using @a addValue()
     *
     * Has to be implemented differently be each subclass since Structures are
     * always messages nested into other messages and the Structs need their
     * fully qualified message name.
     *
     * @note This function should only be called when
     * <code>dataTypeType() == DataTypeType::STRUCTURE</code> or
     * <code>underlyingDataTypeType() == DataTypeType::STRUCTURE</code>.
     * Otherwise the returned @c CDynamicStructure will have no Elements (since
     * this Value has a different Data Type Type)
     *
     * @return An empty @c CDynamicStructure
     */
    [[nodiscard]] virtual CDynamicStructure structure() const;

protected:
    /**
     * @brief C'tor for derived classes
     *
     * @param priv Private data of the class
     */
    explicit CDynamicValue(PrivateImplPtr priv);

    /**
     * @internal
     * @brief Set the value to @a Value
     *
     * @param Value The value to set
     * @param DataType The FDL description of the value's Data Type
     */
    void setValue(google::protobuf::Message* Value,
                  const codegen::fdl::CDataType& DataType);

    /**
     * @internal
     * @brief Set the values to @a Values
     *
     * @param Values The values to set
     * @param DataType The FDL description of the value's Data Type
     */
    void setValues(const std::vector<google::protobuf::Message*>& Values,
                   const codegen::fdl::CDataType& DataType);

    /**
     * @internal
     * @brief Add the given @a Value to the list of values
     *
     * @param Value The value to add
     * @param DataType The FDL description of the value's Data Type
     */
    void addValue(google::protobuf::Message* Value,
                  const codegen::fdl::CDataType& DataType);

    /**
     * @internal
     * @brief Set the value at the given @a Index to @a Value
     *
     * @param Value The value to set
     * @param DataType The FDL description of the value's Data Type
     * @param Index The index of the value in the list to set
     */
    void setValue(google::protobuf::Message* Value,
                  const codegen::fdl::CDataType& DataType, size_t Index);

    /**
     * @brief Convert the given @a Value from its type @a T to a pointer to a
     * protobuf @c Message
     *
     * @tparam T The type of the @a Value
     *
     * @param Value The value to convert to a protobuf @c Message
     *
     * @return A pointer to a protobuf @c Message with the converted @a Value
     */
    template<typename T>
    [[nodiscard]] static google::protobuf::Message* toProtoMessagePtr(
        const T& Value);

    template<typename T>
    [[nodiscard]] codegen::fdl::CDataType basicTypeDataType() const;

    PrivateImplPtr d_ptr;

private:
    PIMPL_DECLARE_PRIVATE(CDynamicValue)
};

/**
 * @brief Overload for debugging @c CDynamicValues
 */
SILA_CPP_EXPORT QDebug operator<<(QDebug dbg, const CDynamicValue& rhs);

/**
 * @brief Overload for printing @c CDynamicValues
 */
SILA_CPP_EXPORT std::ostream& operator<<(std::ostream& os,
                                         const CDynamicValue& rhs);
}  // namespace SiLA2

#include "DynamicValue.ipp"

#endif  // DYNAMICVALUE_H
