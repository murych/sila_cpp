/**
 ** This file is part of the sila_cpp project.
 ** Copyright (c) 2021 SiLA2
 **
 ** Permission is hereby granted, free of charge, to any person obtaining a copy
 ** of this software and associated documentation files (the "Software"), to deal
 ** in the Software without restriction, including without limitation the rights
 ** to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 ** copies of the Software, and to permit persons to whom the Software is
 ** furnished to do so, subject to the following conditions:
 **
 ** The above copyright notice and this permission notice shall be included in all
 ** copies or substantial portions of the Software.
 **
 ** THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 ** IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 ** FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 ** AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 ** LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 ** OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 ** SOFTWARE.
 **/

//============================================================================
/// \file   DynamicSiLAClient.h
/// \author Florian Meinicke (florian.meinicke@cetoni.de)
/// \date   22.01.2021
/// \brief  Declaration of the CDynamicSiLAClient class
//============================================================================
#ifndef DYNAMICSILACLIENT_H
#define DYNAMICSILACLIENT_H

//============================================================================
//                                  INCLUDES
//============================================================================
#include <sila_cpp/client/DynamicFeatureStub.h>
#include <sila_cpp/client/SiLAClient.h>

namespace SiLA2
{
//============================================================================
//                            FORWARD DECLARATIONS
//============================================================================
namespace codegen::fdl
{
    class CFeature;
}  // namespace codegen::fdl

/**
 * @brief The CDynamicSiLAClient class is a SiLA Client that enables communication
 * with any SiLA Server without requiring to know the Features implemented by the
 * Server at compile time.
 *
 * @note The Dynamic Client also provides a Dynamic Feature Stub for the
 * SiLAService feature. It is strongly recommended to use the API that is provided
 * by @c CSiLAClient to work with the SiLAService Feature as it is easier to use.
 * The Dynamic Feature Stub is just here as it makes building a GUI a lot easier
 * since all Features/Commands/Properties can be accessed through a common
 * interface. Nevertheless, both the API provided by @c CSiLAClient as well as the
 * Dynamic Feature Stub for the SiLAService Feature are functionally the same.
 */
class SILA_CPP_EXPORT CDynamicSiLAClient : public CSiLAClient
{
    class PrivateImpl;
    friend class CDynamicFeatureStub;

public:
    /**
     * @brief C'tor
     *
     * Builds the dynamic structures for all of the Server's Features making them
     * available for subsequent Command calls / Property requests
     *
     * @param Address The IP address and port of the server to connect to
     *
     * See @c CSiLAClient::CSiLAClient for detailed information on all parameters
     *
     * @sa CSiLAClient::CSiLAClient
     */
    explicit CDynamicSiLAClient(const CServerAddress& Address);

    /**
     * @brief Connects to the Server, retrieves all implemented Features'
     * Descriptions and builds the dynamic structures for all of the Server's
     * Features making them available for subsequent Command calls / Property
     * requests
     *
     * If any of the Features could not be parsed it won't be available as a
     * @c CDynamicFeatureStub. Use @c errors() to get a list of the errors that
     * occurred during parsing of the Feature Description.
     *
     * @throws std::runtime_error if the connection to the server cannot be
     * established
     * @throws CSiLAError if there is an error during creation of the Dynamic
     * Feature Stubs (because this requires calling Commands of the SiLAService
     * Feature, for example)
     *
     * @sa CSiLAClient::connect()
     */
    void connect() override;

    using CSiLAClient::connect;

    /**
     * @brief Get a list with descriptions of all errors that occurred while
     * connecting to the server.
     *
     * These errors were not fatal, i.e. the connection could still be established
     * but some Features might be missing because their Feature Description could
     * not be parsed
     * @return A list with a description of all occurred errors
     */
    [[nodiscard]] QStringList errors() const;

    /**
     * @brief Returns a list of all Features' FDL as XML strings
     *
     * @return All Features' FDL as XML strings
     */
    [[nodiscard]] QStringList fdlStrings() const;

    /**
     * @brief Returns a list of the internal FDL representations of all Features
     *
     * @return All Features' FDL representations
     */
    [[nodiscard]] QList<codegen::fdl::CFeature> fdl() const;

    /**
     * @brief Get a list of the Fully Qualified Feature Identifiers for all
     * Features implemented by the server
     *
     * @return A list of all implemented Features' Identifiers
     */
    [[nodiscard]] QList<CFullyQualifiedFeatureID> featureIdentifiers() const;

    /**
     * @brief Get a list with shared_ptrs to the @c CDynamicFeatureStub for all
     * Features that the server implements
     *
     * @return A list of shared_ptrs for all @c CDynamicFeatureStubs
     */
    [[nodiscard]] QList<std::shared_ptr<CDynamicFeatureStub>> featureStubs() const;

    /**
     * @brief Get a shared_ptr to the @c CDynamicFeatureStub for the Feature that
     * is identified by the given Fully Qualified Feature Identifier @a FeatureID
     *
     * @param FeatureID The Fully Qualified Feature Identifier of the Feature for
     * which to get the Stub
     * @return A shared_ptr to the @c CDynamicFeatureStub for the given
     * @a FeatureID or a shared_ptr to nullptr if there is no such Stub
     *
     * @sa featureIdentifiers()
     */
    [[nodiscard]] std::shared_ptr<CDynamicFeatureStub> featureStub(
        const CFullyQualifiedFeatureID& FeatureID) const;

    /**
     * @brief Whether the Client should accept the untrusted @a Certificate.
     * This uses the handler function that was set through
     * @c setAcceptUntrustedServerCertificateHandler or if no handler was set
     * falls back to the default implementation of @c CSiLAClient (i.e. rejects
     * all untrusted certificates)
     *
     * @param Certificate The certificate that could not be verified to be trusted
     *
     * @returns true, if the @a Certificate should be accepted, false otherwise
     *
     * @sa setAcceptUntrustedServerCertificateHandler()
     * @sa CSiLAClient::acceptUntrustedServerCertificate()
     */
    [[nodiscard]] bool acceptUntrustedServerCertificate(
        const QSslCertificate& Certificate) const override;

    using AcceptUntrustedServerCertificateHandler = std::function<bool(
        const QSslCertificate&, const CDynamicSiLAClient* Client)>;

    /**
     * @brief Set a handler function for the @c CDynamicSiLAClient's
     * @c acceptUntrustedServerCertificate member function. Since a
     * @c CDynamicSiLAClient instance gets created for you by the
     * @c CServerManager you can't override @c acceptUntrustedServerCertificate
     * yourself. However, it is recommended to implement custom behaviour to allow
     * connecting to a Server that has an untrusted or self-signed certificate
     * that only you (or users of your application) know you can trust. Therefore
     * you can provide a handler function here that will be called when such a
     * certificate is encountered. In order for your handler function to be used
     * you have to call this function @em before you connect to a Server through
     * @c CSiLAServerManager.
     *
     * The handler function has to take one argument which is the certificate in
     * question and it has to return either @c true (if you want to accept the
     * certificate) or @c false (if you don't).
     *
     * @param Handler A handler function that returns if a given certificate shall
     * be trusted or not.
     *
     * @return The previously installed handler or @c nullptr if no handler was
     * installed
     *
     * @sa acceptUntrustedServerCertificate()
     */
    static AcceptUntrustedServerCertificateHandler
    setAcceptUntrustedServerCertificateHandler(
        AcceptUntrustedServerCertificateHandler Handler);

private:
    /**
     * @brief Constructs a new @c CDynamicFeatureStub
     *
     * @param FeatureID The Fully Qualified Feature Identifier of the Feature that
     * the Stub represents
     * @param Feature The parsed FDL description of the Feature
     * @return A shared_ptr to the @c CDynamicFeatureStub
     *
     * @sa CDynamicFeatureStub::CDynamicFeatureStub
     */
    [[nodiscard]] std::shared_ptr<CDynamicFeatureStub> makeFeatureStub(
        const CFullyQualifiedFeatureID& FeatureID,
        const codegen::fdl::CFeature& Feature) const;

    static AcceptUntrustedServerCertificateHandler
        m_AcceptUntrustedServerCertificateHandler;

    PIMPL_DECLARE_PRIVATE(CDynamicSiLAClient)
};

}  // namespace SiLA2

SILA_CPP_EXPORT QDebug operator<<(QDebug dbg,
                                  const SiLA2::CDynamicSiLAClient& rhs);

#endif  // DYNAMICSILACLIENT_H
