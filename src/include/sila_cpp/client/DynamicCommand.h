/**
 ** This file is part of the sila_cpp project.
 ** Copyright 2021 SiLA2
 **
 ** Permission is hereby granted, free of charge, to any person obtaining a copy
 ** of this software and associated documentation files (the "Software"), to deal
 ** in the Software without restriction, including without limitation the rights
 ** to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 ** copies of the Software, and to permit persons to whom the Software is
 ** furnished to do so, subject to the following conditions:
 **
 ** The above copyright notice and this permission notice shall be included in all
 ** copies or substantial portions of the Software.
 **
 ** THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 ** IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 ** FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 ** AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 ** LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 ** OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 ** SOFTWARE.
 **/

//============================================================================
/// \file   DynamicCommand.h
/// \author Florian Meinicke (florian.meinicke@cetoni.de)
/// \date   17.02.2021
/// \brief  Declaration of the CDynamicCommand class
//============================================================================
#ifndef DYNAMICCOMMAND_H
#define DYNAMICCOMMAND_H

//============================================================================
//                                  INCLUDES
//============================================================================
#include <sila_cpp/client/ClientMetadata.h>
#include <sila_cpp/client/DynamicParameter.h>
#include <sila_cpp/client/DynamicResponse.h>
#include <sila_cpp/common/FullyQualifiedCommandID.h>
#include <sila_cpp/common/logging.h>
#include <sila_cpp/config.h>
#include <sila_cpp/framework/data_types/CommandExecutionUUID.h>
#include <sila_cpp/framework/data_types/ExecutionInfo.h>
#include <sila_cpp/global.h>

#include <QFuture>
#include <QObject>

#include <google/protobuf/message.h>

#include <polymorphic_value.h>

//============================================================================
//                            FORWARD DECLARATIONS
//============================================================================
#if GRPC_VERSION >= GRPC_VERSION_CHECK(1, 32, 0)
namespace grpc
{
class Channel;
}  // namespace grpc
#else
namespace grpc_impl
{
class Channel;
}  // namespace grpc_impl
namespace grpc
{
typedef grpc_impl::Channel Channel;
}  // namespace grpc
#endif

namespace SiLA2::codegen
{
namespace fdl
{
    class CCommand;
}  // namespace fdl
namespace proto
{
    class CDynamicMessageFactory;
}  // namespace proto
}  // namespace SiLA2::codegen

namespace SiLA2
{
/**
 * @brief The CDynamicCommand class represents a stub-like object that
 * provides means to call a specific Command that is not known at compile time
 */
class SILA_CPP_EXPORT CDynamicCommand : public QObject
{
    Q_OBJECT

    class PrivateImpl;
    using PrivateImplPtr = isocpp_p0201::polymorphic_value<PrivateImpl>;

public:
    /**
     * @brief C'tor
     *
     * @param Identifier The Fully Qualified Command Identifier of this Command
     * @param CommandFDL The parsed FDL description of the Command
     * @param Channel The gRPC channel to the Server
     * @param DMF A shared_ptr to the Dynamic Message Factory that should be
     * used to generate the dynamic Parameter/Response messages
     * @param Feature The SiLA Feature Stub that this Command belongs to
     */
    CDynamicCommand(const CFullyQualifiedCommandID& Identifier,
                    const codegen::fdl::CCommand& CommandFDL,
                    std::shared_ptr<grpc::Channel> Channel,
                    std::shared_ptr<codegen::proto::CDynamicMessageFactory> DMF,
                    CDynamicFeatureStub* Feature);

    /// --------------------------- Introspection ----------------------------
    /**
     * @brief Get the Fully Qualified Identifier of this Command
     *
     * @return The Command's Fully Qualified Identifier
     */
    [[nodiscard]] CFullyQualifiedCommandID identifier() const;

    /**
     * @brief Get the Display Name of this Command
     *
     * @return The Command's Display Name
     */
    [[nodiscard]] QString displayName() const;

    /**
     * @brief Get the Description of this Command
     *
     * @return The Command's Description
     */
    [[nodiscard]] QString description() const;

    /**
     * @brief Get whether this Command is Observable or not
     *
     * @returns true, if the Command is Observable, otherwise false
     */
    [[nodiscard]] bool isObservable() const;

    /**
     * @brief Get a list of all the Parameters that this Command requires.
     *
     * This list can be directly used to set Parameter values and then call the
     * Command by passing in the modified list.
     *
     * @return A list of all Parameters required to call this Command
     */
    [[nodiscard]] CDynamicParameterList parameters() const;

    /**
     * @brief Get a list of all the Responses that this Command returns.
     *
     * This list will always be empty. This function is just here for completeness
     * and shall help develop generic applications that need to know the structure
     * of Command Response before an actual Command has been executed (since you
     * only get a @c CDynamicResponseList that contains the actual Responses
     * returned by that Command Execution from @c call() or @c result()).
     *
     * @return A list of all Responses returned by this Command
     */
    [[nodiscard]] CDynamicResponseList responses() const;

    /**
     * @brief Get a list of all the display names of Responses that this  Command
     * returns
     *
     * @return A list of all Response names returned by this Command
     */
    [[nodiscard]] QStringList responseNames() const;

    /**
     * @brief Get the description for the Response with the given @a ResponseName
     *
     * @param ResponseName The display name of the Response (as returned by
     * @c responseNames) for which to get the description
     * @return The description of the given Response of this Command
     */
    [[nodiscard]] QString responseDescription(const QString& ResponseName) const;

    /**
     * @brief Get a list of all the Metadata that affects this Command.
     *
     * This list can be directly used to set Metadata values and then call the
     * Command by passing in the modified list.
     *
     * @return A list of all Metadata affecting this Command
     */
    [[nodiscard]] CClientMetadataList metadata() const;

    /**
     * @brief Add the given @a Metadata to this Command
     *
     * @internal This method should only be used by @c CDynamicSiLAClient since
     * it's the only one knowing about all Metadata provided by the server. The
     * affecting metadata is automatically added by the Dynamic Client after
     * successfully connecting to a server and retrieving all Feature Definitions.
     *
     * @param Metadata The metadata that affects this Command
     */
    void addAffectingMetadata(const CClientMetadata& Metadata);

    /**
     * @brief Get a pointer to the Feature that this Command belongs to
     *
     * @note No ownership is transferred here!
     *
     * @return A pointer to the Feature of this Command
     */
    [[nodiscard]] CDynamicFeatureStub* feature() const;

    /**
     * @brief Get the FDL of this Command as an XML string
     *
     * @return The Command's FDL as an XML string (i.e. beginning with
     * <code>&lt;Command&gt;</code>)
     */
    [[nodiscard]] QString commandFDLString() const;

    /**
     * @brief Returns the internal FDL representation of this Command
     *
     * @return The Command's FDL representation
     */
    [[nodiscard]] codegen::fdl::CCommand commandFDL() const;

    /// ----------------------------- Execution ------------------------------
    /**
     * @brief Call this Command with the given @a Parameters on the server
     *
     * For Observable Commands this method will block execution of this thread
     * until the Command has finished on the server and the final result could be
     * retrieved. To do a non-blocking execution use @b start().
     *
     * @param Parameters The parameters of the Command already of the correct
     * protobuf Message type
     * @param Metadata The optional SiLA Client Metadata to add to the call
     * @return The Command Responses
     *
     * @throws SiLA2::CSiLAError if the Command execution throws an error
     * @throws std::runtime_error for Observable Commands if the Subscription to
     * the Execution Info could not be started
     *
     * @sa parameters()
     * @sa start()
     */
    CDynamicResponseList call(
        std::unique_ptr<google::protobuf::Message> Parameters,
        const CClientMetadataList& Metadata = {});

    /**
     * @brief Call this Command with the given @a Parameters and @a Metadata on
     * the server
     *
     * For Observable Commands this method will block execution of this thread
     * until the Command has finished on the server and the final result could be
     * retrieved. To do a non-blocking execution use @b start().
     *
     * @param Parameters The parameters of the Command
     * @param Metadata The optional SiLA Client Metadata to add to the call
     * @return The Command Responses
     *
     * @throws SiLA2::CSiLAError if the Command execution throws an error
     * @throws std::runtime_error for Observable Commands if the Subscription to
     * the Execution Info could not be started
     *
     * @sa parameters()
     * @sa metadata()
     * @sa start()
     */
    CDynamicResponseList call(const CDynamicParameterList& Parameters,
                              const CClientMetadataList& Metadata = {});

    /**
     * @brief Call this Command with the given @a Metadata on the server
     *
     * For Observable Commands this method will block execution of this thread
     * until the Command has finished on the server and the final result could be
     * retrieved. To do a non-blocking execution use @b start().
     *
     * @param Metadata The optional SiLA Client Metadata to add to the call
     * @return The Command Responses
     *
     * @throws SiLA2::CSiLAError if the Command execution throws an error
     * @throws std::runtime_error for Observable Commands if the Subscription to
     * the Execution Info could not be started
     *
     * @sa metadata()
     * @sa start()
     */
    CDynamicResponseList call(const CClientMetadataList& Metadata = {});

    /**
     * @brief Start execution of this Command with the given @a Parameters on the
     * server
     *
     * @param Parameters The parameters of the Command already of the correct
     * protobuf Message type
     * @param Metadata The optional SiLA Client Metadata to add to the call
     * @return The Command Execution UUID for this Command Execution
     *
     * @throws SiLA2::CSiLAError if the Command initiation throws an error (most
     * likely a Validation Error if the Parameters are wrong or a Framework Error
     * if the server does not accept Command Execution)
     *
     * @sa parameters()
     */
    [[nodiscard]] CCommandExecutionUUID start(
        std::unique_ptr<google::protobuf::Message> Parameters,
        const CClientMetadataList& Metadata = {});

    /**
     * @brief Start execution of this Command with the given @a Parameters and
     * @a Metadata on the server
     *
     * @param Parameters The parameters of the Command
     * @param Metadata The optional SiLA Client Metadata to add to the call
     * @return The Command Execution UUID for this Command Execution
     *
     * @throws std::logic_error if this Command is not Observable
     * @throws SiLA2::CSiLAError if the Command initiation throws an error (most
     * likely a Validation Error if the Parameters are wrong or a Framework Error
     * if the server does not accept Command Execution)
     *
     * @sa parameters()
     * @sa metadata()
     */
    [[nodiscard]] CCommandExecutionUUID start(
        const CDynamicParameterList& Parameters,
        const CClientMetadataList& Metadata = {});

    /**
     * @brief Start execution of this Command with the given @a Metadata on the
     * server
     *
     * @param Metadata The optional SiLA Client Metadata to add to the call
     * @return The Command Execution UUID for this Command Execution
     *
     * @throws std::logic_error if this Command is not Observable
     * @throws SiLA2::CSiLAError if the Command initiation throws an error (most
     * likely a Validation Error if the Parameters are wrong or a Framework Error
     * if the server does not accept Command Execution)
     *
     * @sa metadata()
     */
    [[nodiscard]] CCommandExecutionUUID start(
        const CClientMetadataList& Metadata = {});

    /**
     * @brief Given a Command Execution UUID this returns a single Execution Info
     * value
     *
     * This function will block until another Execution Info value is available.
     *
     * @param UUID The UUID of the Command Execution to get the Execution Info for
     * @return The most recent @c CExecutionInfo
     *
     * @throws std::logic_error if this Command is not Observable
     * @throws SiLA2::CSiLAError if the Execution Info cannot be obtained (most
     * likely a Framework Error because there is no Command Execution with the
     * given @a UUID)
     * @throws std::runtime_error if the Subscription could not be started
     */
    [[nodiscard]] CExecutionInfo getExecutionInfo(
        const CCommandExecutionUUID& UUID);

    /**
     * @brief Given a Command Execution UUID subscribe to the Execution Info for
     * this Command Execution
     *
     * @param UUID The UUID of the Command Execution for which to subscribe to the
     * Execution Info
     * @return A @c QFuture for the Execution Info value(s). Use @c result() or
     * @c resultAt(int) to get individual values. Note that this might throw a
     * @c SiLA2::CSiLAError if reading this value resulted in an error. Use a
     * @c QFutureWatcher to be notified via a signal when new values are
     * available.
     *
     * @throws std::logic_error if this Command is not Observable
     * @throws std::runtime_error if the Subscription could not be started
     */
    [[nodiscard]] QFuture<CExecutionInfo> subscribeExecutionInfo(
        const CCommandExecutionUUID& UUID);

    /**
     * @brief Given a Command Execution UUID this returns a single Intermediate
     * Response value
     *
     * This function will block until another Intermediate Result value is
     * available.
     *
     * @param UUID The UUID of the Command Execution to get the Intermediate
     * Response for
     * @return The (current) Intermediate Response value
     *
     * @throws std::logic_error if this Command is not Observable or has no
     * Intermediate Responses
     * @throws SiLA2::CSiLAError if the Intermediate Response cannot be obtained
     * (most likely a Framework Error because there is no Command Execution with
     * the given @a UUID)
     * @throws std::runtime_error if the Subscription could not be started
     */
    [[nodiscard]] CDynamicResponseList getIntermediateResponses(
        const CCommandExecutionUUID& UUID);

    /**
     * @brief Given a Command Execution UUID subscribe to the Intermediate
     * Responses for this Command Execution
     *
     * @param UUID The UUID of the Command Execution for which to subscribe to the
     * Intermediate Responses
     * @return A @c QFuture for the Intermediate Response value(s). Use
     * @c result() or @c resultAt(int) to get individual values. Note that this
     * might throw a @c SiLA2::CSiLAError if reading this value resulted in an
     * error. Use a @c QFutureWatcher to be notified via a signal when new values
     * are available.
     *
     * @throws std::logic_error if this Command is not Observable or has no
     * Intermediate Responses
     * @throws std::runtime_error if the Subscription could not be started
     */
    [[nodiscard]] QFuture<CDynamicResponseList> subscribeIntermediateResponses(
        const CCommandExecutionUUID& UUID);

    /**
     * @brief Given a Command Execution UUID this returns the final Command
     * Execution Result
     *
     * @param UUID The UUID of the Command Execution to get the Result for
     * @return The Command Execution Result value
     *
     * @throws std::logic_error if this Command is not Observable
     * @throws SiLA2::CSiLAError if the Result cannot be obtained (most likely a
     * Framework Error because there is no Command Execution with the given
     * @a UUID or the Execution has not finished yet)
     */
    CDynamicResponseList result(const CCommandExecutionUUID& UUID);

signals:
    /**
     * @brief This signal is emitted whenever this particular Command gets called.
     * More precisely it will be emitted @em after executing the Command, i.e.
     * right before the Command Result is returned by @c call().
     */
    void called() const;

protected:
    PrivateImplPtr d_ptr;

private:
    PIMPL_DECLARE_PRIVATE(CDynamicCommand)
};

/**
 * @brief Overload for debugging @c CDynamicCommand
 */
SILA_CPP_EXPORT QDebug operator<<(QDebug dbg, const CDynamicCommand& rhs);

/**
 * @brief Overload for printing @c CDynamicCommand
 */
SILA_CPP_EXPORT std::ostream& operator<<(std::ostream& os,
                                         const CDynamicCommand& rhs);
}  // namespace SiLA2
#endif  // DYNAMICCOMMAND_H
