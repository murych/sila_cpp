/**
 ** This file is part of the sila_cpp project.
 ** Copyright 2021 SiLA2
 **
 ** Permission is hereby granted, free of charge, to any person obtaining a copy
 ** of this software and associated documentation files (the "Software"), to deal
 ** in the Software without restriction, including without limitation the rights
 ** to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 ** copies of the Software, and to permit persons to whom the Software is
 ** furnished to do so, subject to the following conditions:
 **
 ** The above copyright notice and this permission notice shall be included in all
 ** copies or substantial portions of the Software.
 **
 ** THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 ** IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 ** FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 ** AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 ** LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 ** OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 ** SOFTWARE.
 **/

//============================================================================
/// \file   DynamicFeatureStub.h
/// \author Florian Meinicke (florian.meinicke@cetoni.de)
/// \date   11.02.2021
/// \brief  Declaration of the CDynamicFeatureStub class
//============================================================================
#ifndef DYNAMICFEATURESTUB_H
#define DYNAMICFEATURESTUB_H

//============================================================================
//                                  INCLUDES
//============================================================================
#include <sila_cpp/client/ClientMetadata.h>
#include <sila_cpp/client/CustomDataType.h>
#include <sila_cpp/client/DynamicCommand.h>
#include <sila_cpp/client/DynamicProperty.h>
#include <sila_cpp/common/logging.h>
#include <sila_cpp/config.h>
#include <sila_cpp/global.h>

#include <google/protobuf/message.h>

#include <polymorphic_value.h>

#include <memory>

//============================================================================
//                            FORWARD DECLARATIONS
//============================================================================
#if GRPC_VERSION >= GRPC_VERSION_CHECK(1, 32, 0)
namespace grpc
{
class Channel;
}  // namespace grpc
#else
namespace grpc_impl
{
class Channel;
}  // namespace grpc_impl
namespace grpc
{
typedef grpc_impl::Channel Channel;
}  // namespace grpc
#endif

namespace SiLA2
{
class CFullyQualifiedFeatureID;
class CDynamicSiLAClient;

namespace codegen
{
    namespace fdl
    {
        class CFeature;
    }  // namespace fdl
    namespace proto
    {
        class CDynamicMessageFactory;
    }  // namespace proto
}  // namespace codegen
}  // namespace SiLA2

namespace SiLA2
{
/**
 * @brief The CDynamicFeatureStub class represents a Stub of a SiLA 2 Feature that
 * is not known at compile time. It provides generic methods to call the Feature's
 * Commands and request or subscribe to its Properties.
 *
 * @todo[FM] Find a friendlier return type for all dynamic calls (maybe something
 *   like QVariant)
 */
class SILA_CPP_EXPORT CDynamicFeatureStub
{
public:
    /// --------------------------- Introspection ----------------------------
    /**
     * @brief Get the Fully Qualified Identifier of this Feature
     *
     * @return The Feature's Fully Qualified Identifier
     */
    [[nodiscard]] CFullyQualifiedFeatureID identifier() const;

    /**
     * @brief Get the Display Name of this Feature
     *
     * @return The Feature's Display Name
     */
    [[nodiscard]] QString displayName() const;

    /**
     * @brief Get the Description of this Feature
     *
     * @return The Feature's Description
     */
    [[nodiscard]] QString description() const;

    /**
     * @brief Get the Features FDL as an XML string
     *
     * @return The Feature's FDL as an XML string
     */
    [[nodiscard]] QString fdlString() const;

    /**
     * @brief Get the internal FDL representation of this Feature
     *
     * @return This Feature's FDL representation
     */
    [[nodiscard]] codegen::fdl::CFeature fdl() const;

    /// ------------------------------ Commands ------------------------------
    /**
     * @brief Get a list of the Fully Qualified Commands Identifiers for all
     * Commands implemented by the Feature
     *
     * @return A list of all implemented Commands' Identifiers
     */
    [[nodiscard]] QList<CFullyQualifiedCommandID> commandIdentifiers() const;

    /**
     * @brief Get a list with shared_ptrs to the @c CDynamicCommand for all of the
     * Commands implemented by the Feature that is represented by this Stub
     *
     * @return A list of shared_ptr for all @c CDynamicCommands
     */
    [[nodiscard]] QList<std::shared_ptr<CDynamicCommand>> commands() const;

    /**
     * @brief Get a shared_ptr to the @c CDynamicCommand for the Command that is
     * identified by the given Fully Qualified Command Identifier @a CommandID
     *
     * @param CommandID The Fully Qualified Command Identifier for which to get
     * the Command
     * @return A shared_ptr to the @c CDynamicCommand for the given @a CommandID
     * or @c nullptr if there is no such Command
     *
     * @sa commandIdentifiers()
     */
    [[nodiscard]] std::shared_ptr<CDynamicCommand> command(
        const CFullyQualifiedCommandID& CommandID) const;

    /**
     * @brief Get a shared_ptr to the @c CDynamicCommand for the Command that is
     * identified by the given Fully Qualified Command Identifier @a CommandID
     *
     * These functions exist for convenience since it's not strictly necessary to
     * provide the Fully Qualified Command Identifier as the Feature can deduce
     * it from its own Fully Qualified Feature Identifier and the non-qualified
     * Identifier of the Command.
     *
     * @param CommandID The Non-Fully Qualified Command Identifier for which to
     * get the Command
     * @return A shared_ptr to the @c CDynamicCommand for the given @a CommandID
     * or @c nullptr if there is no such Command
     *
     * @sa commandIdentifiers()
     */
    [[nodiscard]] std::shared_ptr<CDynamicCommand> command(
        QStringView CommandID) const;
    [[nodiscard]] std::shared_ptr<CDynamicCommand> command(
        std::string_view CommandID) const;

    /// ----------------------------- Properties ------------------------------
    /**
     * @brief Get a list of the Fully Qualified Property Identifiers for all
     * Properties implemented by the Feature
     *
     * @return A list of all implemented Properties' Identifiers
     */
    [[nodiscard]] QList<CFullyQualifiedPropertyID> propertyIdentifiers() const;

    /**
     * @brief Get a list with shared_ptrs to the @c CDynamicProperty for all of
     * the Properties implemented by the Feature that is represented by this Stub
     *
     * @return A list with shared_ptr for all @c CDynamicProperty
     */
    [[nodiscard]] QList<std::shared_ptr<CDynamicProperty>> properties() const;

    /**
     * @brief Get a shared_ptr to the @c CDynamicProperty for the Property that is
     * identified by the given Fully Qualified Property Identifier @a PropertyID
     *
     * @param PropertyID The Fully Qualified Property Identifier for which to get
     * the Property
     * @return A shared_ptr to the @c CDynamicProperty for the given @a PropertyID
     * or @c nullptr if there is no such Property
     *
     * @sa propertyIdentifiers()
     */
    [[nodiscard]] std::shared_ptr<CDynamicProperty> property(
        const CFullyQualifiedPropertyID& PropertyID) const;

    /**
     * @brief Get a shared_ptr to the @c CDynamicProperty for the Property that is
     * identified by the given Fully Qualified Property Identifier @a PropertyID
     *
     * These functions exist for convenience since it's not strictly necessary to
     * provide the Fully Qualified Property Identifier as the Feature can deduce
     * it from its own Fully Qualified Feature Identifier and the non-qualified
     * Identifier of the Property.
     *
     * @param PropertyID The Non-Fully Qualified Property Identifier for which to
     * get the Property
     * @return A shared_ptr to the @c CDynamicProperty for the given @a PropertyID
     * or @c nullptr if there is no such Property
     *
     * @sa propertyIdentifiers()
     */
    [[nodiscard]] std::shared_ptr<CDynamicProperty> property(
        QStringView PropertyID) const;
    [[nodiscard]] std::shared_ptr<CDynamicProperty> property(
        std::string_view PropertyID) const;

    /// ------------------------------ Metadata ------------------------------
    /**
     * @brief Get a list of the Fully Qualified Metadata Identifiers for all
     * Metadata implemented by the Feature
     *
     * @return A list of all implemented Metadata's Identifiers
     */
    [[nodiscard]] QList<CFullyQualifiedMetadataID> metadataIdentifiers() const;

    /**
     * @brief Get a list with the @c CClientMetadata for all Metadata used by the
     * Feature that is represented by this Stub
     *
     * @return A list of all @c CClientMetadata
     */
    [[nodiscard]] QList<CClientMetadata> metadata() const;

    /**
     * @brief Get the @c CClientMetadata that is identified by the given Fully
     * Qualified Metadata Identifier @a MetadataID
     *
     * @param MetadataID The Fully Qualified Metadata Identifier for which to get
     * the Metadata
     * @return The @c CClientMetadata for the given @a MetadataID
     *
     * @throws std::invalid_argument if there is no @c CClientMetadata for the
     * given @a MetadataID
     *
     * @sa metadataIdentifiers()
     */
    [[nodiscard]] CClientMetadata metadata(
        const CFullyQualifiedMetadataID& MetadataID) const;

    /**
     * @brief Get a shared_ptr to the @c CDynamicClientMetadata for the Metadata
     * that is identified by the given Fully Qualified Metadata Identifier @a
     * MetadataID
     *
     * These functions exist for convenience since it's not strictly necessary to
     * provide the Fully Qualified Metadata Identifier as the Feature can deduce
     * it from its own Fully Qualified Feature Identifier and the non-qualified
     * Identifier of the Metadata.
     *
     * @param MetadataID The Non-Fully Qualified Metadata Identifier for which to
     * get the Metadata
     * @return A shared_ptr to the @c CDynamicClientMetadata for the given
     * @a MetadataID or @c nullptr if there is no such Metadata
     *
     * @sa metadataIdentifiers()
     */
    [[nodiscard]] CClientMetadata metadata(QStringView MetadataID) const;
    [[nodiscard]] CClientMetadata metadata(std::string_view MetadataID) const;

    /// ----------------------- Data Type Definitions ------------------------
    /**
     * @brief Get a list of the Fully Qualified Data Type Identifiers for all
     * Custom Data Type Definitions implemented by the Feature
     *
     * @return A list of all implemented Data Type Definition's Identifiers
     */
    [[nodiscard]] QList<CFullyQualifiedDataTypeID> dataTypeIdentifiers() const;

    /**
     * @brief Get a list with the @c CCustomDataType for all Custom Data Type
     * Definitions implemented by the Feature that is represented by this Stub
     *
     * @return A list of all @c CCustomDataType
     */
    [[nodiscard]] QList<CCustomDataType> dataTypes() const;

    /**
     * @brief Get the @c CCustomDataType that is identified by the given Fully
     * Qualified Data Type Identifier @a DataTypeID
     *
     * @param DataTypeID The Fully Qualified Data Type Identifier of the Custom
     * Data Type to get
     * @return The @c CCustomDataType for the given @a DataTypeID
     *
     * @throws std::invalid_argument if there is no @c CCustomDataType for the
     * given @a DataTypeID
     *
     * @sa dataTypeIdentifiers()
     */
    [[nodiscard]] CCustomDataType dataType(
        const CFullyQualifiedDataTypeID& DataTypeID) const;

    /**
     * @brief Get the @c CCustomDataType that is identified by the given non-Fully
     * Qualified Data Type Identifier
     * @a DataTypeID
     *
     * These functions exist for convenience since it's not strictly necessary to
     * provide the Fully Qualified Data Type Identifier as the Feature can deduce
     * it from its own Fully Qualified Feature Identifier and the non-qualified
     * Identifier of the Data Type.
     *
     * @param DataTypeID The Non-Fully Qualified Data Type Identifier of the
     * Custom Data Type to get (not Fully Qualified since the Feature can deduce
     * this itself)
     * @return The @c CCustomDataType for the given @a DataTypeID
     *
     * @throws std::invalid_argument if there is no @c CCustomDataType for the
     * given @a DataTypeID
     *
     * @sa CDynamicValue::dataTypeIdentifier()
     */
    [[nodiscard]] CCustomDataType dataType(QStringView DataTypeID) const;
    [[nodiscard]] CCustomDataType dataType(std::string_view DataTypeID) const;

protected:
    class PrivateImpl;
    using PrivateImplPtr = isocpp_p0201::polymorphic_value<PrivateImpl>;
    PrivateImplPtr d_ptr;

private:
    friend class CDynamicSiLAClient;
    /**
     * @internal Only for use by @c CDynamicSiLAClient
     * @brief Construct a new @c CDynamicFeatureStub with the given Dynamic
     * Message Factory @a DMF
     *
     * @param FeatureID The Fully Qualified Feature Identifier of the Feature that
     * this Stub represents
     * @param Feature The parsed FDL description of the Feature
     * @param Channel The gRPC channel to the Server
     * @param DMF A shared_ptr to the Dynamic Message Factory that should be used
     * to generate the dynamic messages for Command calls / Property requests
     */
    CDynamicFeatureStub(
        const CFullyQualifiedFeatureID& FeatureID,
        const codegen::fdl::CFeature& Feature,
        std::shared_ptr<grpc::Channel> Channel,
        std::shared_ptr<codegen::proto::CDynamicMessageFactory> DMF);

    PIMPL_DECLARE_PRIVATE(CDynamicFeatureStub)
};
}  // namespace SiLA2

#endif  // DYNAMICFEATURESTUB_H
