/**
 ** This file is part of the sila_cpp project.
 ** Copyright 2021 SiLA2
 **
 ** Permission is hereby granted, free of charge, to any person obtaining a copy
 ** of this software and associated documentation files (the "Software"), to deal
 ** in the Software without restriction, including without limitation the rights
 ** to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 ** copies of the Software, and to permit persons to whom the Software is
 ** furnished to do so, subject to the following conditions:
 **
 ** The above copyright notice and this permission notice shall be included in all
 ** copies or substantial portions of the Software.
 **
 ** THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 ** IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 ** FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 ** AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 ** LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 ** OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 ** SOFTWARE.
 **/

//============================================================================
/// \file   DynamicValue.ipp
/// \author Florian Meinicke (florian.meinicke@cetoni.de)
/// \date   09.06.2021
/// \brief  Template implementation of the CDynamicValue class
//============================================================================

//============================================================================
//                                  INCLUDES
//============================================================================
#include <sila_cpp/codegen/fdl/Constraint.h>
#include <sila_cpp/codegen/fdl/utils.h>
#include <sila_cpp/common/logging.h>
#include <sila_cpp/data_types.h>
#include <sila_cpp/global.h>
#include <sila_cpp/internal/type_traits.h>

#include <polymorphic_value.h>

#include <typeindex>

namespace SiLA2
{
//===========================================================================
//                                Single value
//===========================================================================
template<typename T, typename>
void CDynamicValue::setValue(const T& Value)
{
    setValue(Value.toProtoMessagePtr(), basicTypeDataType<T>());
}

//============================================================================
template<typename T, typename, typename>
void CDynamicValue::setValue(const T& Value)
{
    auto Temp = Value.New();
    Temp->CopyFrom(Value);
    setValue(Temp, basicTypeDataType<T>());
}

//============================================================================
template<typename T, typename>
void CDynamicValue::setValue(T* Value)
{
    setValue(*Value);
}

//============================================================================
template<typename T, typename, typename, typename>
void CDynamicValue::setValue(const T& Value)
{
    using namespace sila2::org::silastandard;
    if constexpr (std::is_same_v<T, bool>)
    {
        setValue(CBoolean{Value}.toProtoMessagePtr(),
                 basicTypeDataType<Boolean>());
    }
    else if constexpr (std::is_same_v<T, float> || std::is_same_v<T, double>)
    {
        setValue(CReal{Value}.toProtoMessagePtr(), basicTypeDataType<Real>());
    }
    else if constexpr (std::is_constructible_v<CInteger, T>)
    {
        setValue(CInteger{Value}.toProtoMessagePtr(),
                 basicTypeDataType<Integer>());
    }
    else if constexpr (std::is_constructible_v<CString, T>)
    {
        setValue(CString{Value}.toProtoMessagePtr(), basicTypeDataType<String>());
    }
    else
    {
        qCDebug(sila_cpp_client)
            << "Cannot convert the given value of type"
            << std::type_index(typeid(T)).name()
            << "to a SiLA Type. You need to provide the desired "
               "SiLA Type explicitly.";
    }
}

//===========================================================================
template<typename T, typename>
CDynamicValue& CDynamicValue::operator=(const T& Value)
{
    setValue(Value);
    return *this;
}

//===========================================================================
//                                List value
//===========================================================================
template<template<typename, auto> class ContainerT, typename T, auto N, typename>
void CDynamicValue::setValues(const ContainerT<T, N>& Values)
{
    setValue(std::vector<T>(Values.begin(), Values.end()));
}

//============================================================================
template<template<typename> class ContainerT, typename T, typename>
void CDynamicValue::setValues(const ContainerT<T>& Values)
{
    std::vector<google::protobuf::Message*> RawValues;
    std::transform(std::begin(Values), std::end(Values),
                   std::back_inserter(RawValues), [this](const auto& Value) {
                       return CDynamicValue::toProtoMessagePtr(Value);
                   });
    codegen::fdl::CDataType DataType;
    DataType.fromVariant(variantMultiMap(
        {{"List",
          variantMultiMap({{"DataType", basicTypeDataType<T>().toVariant()}})}}));
    setValues(RawValues, DataType);
}

//===========================================================================
template<typename T, typename>
void CDynamicValue::addValue(const T& Value)
{
    if (!isList())
    {
        qCDebug(sila_cpp_client)
            << "Adding a value to a CDynamicValue is only possible "
               "if this Dynamic Value represents a List!";
        return;
    }
    addValue(toProtoMessagePtr(Value), basicTypeDataType<T>());
}

//===========================================================================
template<typename T, typename>
void CDynamicValue::setValue(const T& Value, size_t Index)
{
    setValue(Value.toProtoMessagePtr(), basicTypeDataType<T>(), Index);
}

//============================================================================
template<typename T, typename, typename>
void CDynamicValue::setValue(const T& Value, size_t Index)
{
    auto Temp = Value.New();
    Temp->CopyFrom(Value);
    setValue(Temp, basicTypeDataType<T>(), Index);
}

//============================================================================
template<typename T, typename>
void CDynamicValue::setValue(T* Value, size_t Index)
{
    setValue(*Value, Index);
}

//============================================================================
template<typename T, typename, typename, typename>
void CDynamicValue::setValue(const T& Value, size_t Index)
{
    using namespace sila2::org::silastandard;
    if constexpr (std::is_same_v<T, bool>)
    {
        setValue(CBoolean{Value}.toProtoMessagePtr(),
                 basicTypeDataType<Boolean>(), Index);
    }
    else if constexpr (std::is_same_v<T, float> || std::is_same_v<T, double>)
    {
        setValue(CReal{Value}.toProtoMessagePtr(), basicTypeDataType<Real>(),
                 Index);
    }
    else if constexpr (std::is_constructible_v<CInteger, T>)
    {
        setValue(CInteger{Value}.toProtoMessagePtr(),
                 basicTypeDataType<Integer>(), Index);
    }
    else if constexpr (std::is_constructible_v<CString, T>)
    {
        setValue(CString{Value}.toProtoMessagePtr(), basicTypeDataType<String>(),
                 Index);
    }
    else
    {
        qCDebug(sila_cpp_client)
            << "Cannot convert the given value of type"
            << std::type_index(typeid(T)).name()
            << "to a SiLA Type. You need to provide the desired "
               "SiLA Type explicitly.";
    }
}

//===========================================================================
template<template<typename, auto> class ContainerT, typename T, auto N, typename>
CDynamicValue& CDynamicValue::operator=(const ContainerT<T, N>& Values)
{
    setValues(Values);
    return *this;
}

template<template<typename> class ContainerT, typename T, typename>
CDynamicValue& CDynamicValue::operator=(const ContainerT<T>& Values)
{
    setValues(Values);
    return *this;
}

//===========================================================================
//                                  Common
//===========================================================================
template<typename T>
google::protobuf::Message* CDynamicValue::toProtoMessagePtr(const T& Value)
{
    if constexpr (internal::is_sila_type_v<T>)
    {
        return Value.toProtoMessagePtr();
    }
    else if constexpr (internal::is_sila_framework_type_v<T>)
    {
        auto Temp = Value.New();
        Temp->CopyFrom(Value);
        return Temp;
    }
    else if constexpr (std::is_same_v<T, bool>)
    {
        return CBoolean{Value}.toProtoMessagePtr();
    }
    else if constexpr (std::is_same_v<T, float> || std::is_same_v<T, double>)
    {
        return CReal{Value}.toProtoMessagePtr();
    }
    else if constexpr (std::is_constructible_v<CInteger, T>)
    {
        return CInteger{Value}.toProtoMessagePtr();
    }
    else if constexpr (std::is_constructible_v<CString, T>)
    {
        return CString{Value}.toProtoMessagePtr();
    }
    else
    {
        return Value;
    }
}

//============================================================================
template<typename T>
codegen::fdl::CDataType CDynamicValue::basicTypeDataType() const
{
    codegen::fdl::CDataType DataType;
    if constexpr (internal::is_sila_type_v<T>)
    {
        return basicTypeDataType<decltype(T{}.toProtoMessage())>();
    }
    else if constexpr (std::is_same_v<T, sila2::org::silastandard::Any>)
    {
        DataType.fromVariant(variantMultiMap({{"Basic", "Any"}}));
    }
    else if constexpr (std::is_same_v<T, sila2::org::silastandard::Binary>)
    {
        DataType.fromVariant(variantMultiMap({{"Basic", "Binary"}}));
    }
    else if constexpr (std::is_same_v<T, sila2::org::silastandard::Boolean>
                       || std::is_same_v<T, bool>)
    {
        DataType.fromVariant(variantMultiMap({{"Basic", "Boolean"}}));
    }
    else if constexpr (std::is_same_v<T, sila2::org::silastandard::Date>)
    {
        DataType.fromVariant(variantMultiMap({{"Basic", "Date"}}));
    }
    else if constexpr (std::is_same_v<T, sila2::org::silastandard::Duration>)
    {
        DataType.fromVariant(variantMultiMap({{"Basic", "Duration"}}));
    }
    else if constexpr (std::is_same_v<T, sila2::org::silastandard::Real>
                       || std::is_same_v<T, float> || std::is_same_v<T, double>)
    {
        DataType.fromVariant(variantMultiMap({{"Basic", "Real"}}));
    }
    else if constexpr (std::is_same_v<T, sila2::org::silastandard::Integer>
                       || std::is_constructible_v<CInteger, T>)
    {
        DataType.fromVariant(variantMultiMap({{"Basic", "Integer"}}));
    }
    else if constexpr (std::is_same_v<T, sila2::org::silastandard::String>
                       || std::is_constructible_v<CString, T>)
    {
        DataType.fromVariant(variantMultiMap({{"Basic", "String"}}));
    }
    else if constexpr (std::is_same_v<T, sila2::org::silastandard::Time>)
    {
        DataType.fromVariant(variantMultiMap({{"Basic", "Time"}}));
    }
    else if constexpr (std::is_same_v<T, sila2::org::silastandard::Timestamp>)
    {
        DataType.fromVariant(variantMultiMap({{"Basic", "Timestamp"}}));
    }
    else if constexpr (std::is_same_v<T, sila2::org::silastandard::Timezone>)
    {
        DataType.fromVariant(variantMultiMap({{"Basic", "Timezone"}}));
    }
    else
    {
        qCDebug(sila_cpp_client)
            << "Unknown SiLA Data Type" << std::type_index(typeid(T)).name();
    }
    return DataType;
}
}  // namespace SiLA2
