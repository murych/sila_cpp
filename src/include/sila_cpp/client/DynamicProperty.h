/**
 ** This file is part of the sila_cpp project.
 ** Copyright 2021 SiLA2
 **
 ** Permission is hereby granted, free of charge, to any person obtaining a copy
 ** of this software and associated documentation files (the "Software"), to deal
 ** in the Software without restriction, including without limitation the rights
 ** to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 ** copies of the Software, and to permit persons to whom the Software is
 ** furnished to do so, subject to the following conditions:
 **
 ** The above copyright notice and this permission notice shall be included in all
 ** copies or substantial portions of the Software.
 **
 ** THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 ** IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 ** FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 ** AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 ** LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 ** OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 ** SOFTWARE.
 **/

//============================================================================
/// \file   DynamicProperty.h
/// \author Florian Meinicke (florian.meinicke@cetoni.de)
/// \date   18.02.2021
/// \brief  Declaration of the CDynamicProperty class
//============================================================================
#ifndef DYNAMICPROPERTY_H
#define DYNAMICPROPERTY_H

//============================================================================
//                                  INCLUDES
//============================================================================
#include <sila_cpp/client/ClientMetadata.h>
#include <sila_cpp/common/FullyQualifiedPropertyID.h>
#include <sila_cpp/common/logging.h>
#include <sila_cpp/config.h>
#include <sila_cpp/global.h>

#include <QFuture>
#include <QObject>

#include <google/protobuf/message.h>

#include <polymorphic_value.h>

//============================================================================
//                            FORWARD DECLARATIONS
//============================================================================
#if GRPC_VERSION >= GRPC_VERSION_CHECK(1, 32, 0)
namespace grpc
{
class Channel;
}  // namespace grpc
#else
namespace grpc_impl
{
class Channel;
}  // namespace grpc_impl
namespace grpc
{
typedef grpc_impl::Channel Channel;
}  // namespace grpc
#endif

namespace SiLA2::codegen
{
namespace fdl
{
    class CProperty;
}  // namespace fdl
namespace proto
{
    class CDynamicMessageFactory;
}  // namespace proto
}  // namespace SiLA2::codegen

namespace SiLA2
{
/**
 * @brief The CDynamicProperty class represents a stub-like object that
 * provides means to read a specific Property that is not known at compile time
 */
class SILA_CPP_EXPORT CDynamicProperty : public QObject
{
    Q_OBJECT

    class PrivateImpl;
    using PrivateImplPtr = isocpp_p0201::polymorphic_value<PrivateImpl>;

public:
    /**
     * @brief C'tor
     *
     * @param Identifier The Fully Qualified Property Identifier of this Property
     * @param PropertyFDL The parsed FDL description of the Property
     * @param Channel The gRPC channel to the Server
     * @param DMF A shared_ptr to the Dynamic Message Factory that should be
     * used to generate the dynamic Parameter/Response messages
     * @param Feature The SiLA Feature Stub that this Command belongs to
     */
    CDynamicProperty(const CFullyQualifiedPropertyID& Identifier,
                     const codegen::fdl::CProperty& PropertyFDL,
                     std::shared_ptr<grpc::Channel> Channel,
                     std::shared_ptr<codegen::proto::CDynamicMessageFactory> DMF,
                     CDynamicFeatureStub* Feature);

    /// --------------------------- Introspection ----------------------------
    /**
     * @brief Get the Fully Qualified Identifier of this Property
     *
     * @return The Property's Fully Qualified Identifier
     */
    [[nodiscard]] CFullyQualifiedPropertyID identifier() const;

    /**
     * @brief Get the Display Name of this Property
     *
     * @return The Property's Display Name
     */
    [[nodiscard]] QString displayName() const;

    /**
     * @brief Get the Description of this Property
     *
     * @return The Property's Description
     */
    [[nodiscard]] QString description() const;

    /**
     * @brief Get whether this Property is Observable or not
     *
     * @returns true, if the Property is Observable, otherwise false
     */
    [[nodiscard]] bool isObservable() const;

    /**
     * @brief Get a default (empty) value that has the correct type of this
     * Property as if it were returned by the server.
     *
     * This function is just here for completeness and shall help develop generic
     * applications that need to know the structure of the Property value before
     * an actual Property has been read (since you only get a @c CDynamicValue
     * that contains the actual Responses returned by that Property Read from
     * @c get() or through @c subscribe()).
     *
     * @return An empty value for this Property
     */
    [[nodiscard]] CDynamicValue defaultValue() const;

    /**
     * @brief Get a list of all the Metadata that affects this Property.
     *
     * This list can be directly used to set Metadata values and then call the
     * Property by passing in the modified list.
     *
     * @return A list of all Metadata affecting this Property
     */
    [[nodiscard]] CClientMetadataList metadata() const;

    /**
     * @brief Add the given @a Metadata to this Property
     *
     * @internal This method should only be used by @c CDynamicSiLAClient since
     * it's the only one knowing about all Metadata provided by the server. The
     * affecting metadata is automatically added by the Dynamic Client after
     * successfully connecting to a server and retrieving all Feature Definitions.
     *
     * @param Metadata The metadata that affects this Property
     */
    void addAffectingMetadata(const CClientMetadata& Metadata);

    /**
     * @brief Get a pointer to the Feature that this Property belongs to
     *
     * @note No ownership is transferred here!
     *
     * @return A pointer to the Feature of this Property
     */
    [[nodiscard]] CDynamicFeatureStub* feature() const;

    /**
     * @brief Get the FDL of this Property as an XML string
     *
     * @return The Property's FDL as an XML string (i.e. beginning with
     * <code>&lt;Property&gt;</code>)
     */
    [[nodiscard]] QString propertyFDLString() const;

    /**
     * @brief Get the internal FDL representation of this Property
     *
     * @return The Property's FDL representation
     */
    [[nodiscard]] codegen::fdl::CProperty propertyFDL() const;

    /// ----------------------------- Execution ------------------------------
    /**
     * @brief Get the value of this Property from the server
     *
     * This function will always block the calling thread until a value is
     * available. This shouldn't have a significant impact though since
     * Unobservable Property values are always available and for Observable
     * Properties this is the same as staring a Subscription, receiving the first
     * value (which should be sent by the Server right away), and cancelling the
     * Subscription again.
     *
     * @param Metadata The optional SiLA Client Metadata to add to the call
     * @return The (current) value of the Property
     *
     * @throws SiLA2::CSiLAError if the Property reading throws an error
     */
    [[nodiscard]] CDynamicValue get(const CClientMetadataList& Metadata = {});

    /**
     * @brief Subscribe to this (Observable!) Property on the server
     *
     * @param Metadata The optional SiLA Client Metadata to add to the call
     * @return A @c QFuture for the value(s) of the Property. Use @c result() or
     * @c resultAt(int) to get individual values. Note that this might throw a
     * @c SiLA2::CSiLAError if reading this value resulted in an error. Use a
     * @c QFutureWatcher to be notified via a signal when new values are
     * available.
     *
     * @throws std::logic_error if the Property is not Observable
     * @throws std::runtime_error if the Subscription could not be started
     *
     * @sa isObservable()
     */
    [[nodiscard]] QFuture<CDynamicValue> subscribe(
        const CClientMetadataList& Metadata = {});

protected:
    PrivateImplPtr d_ptr;

private:
    PIMPL_DECLARE_PRIVATE(CDynamicProperty)
};

/**
 * @brief Overload for debugging @c CDynamicProperties
 */
QDebug operator<<(QDebug dbg, const CDynamicProperty& rhs);

/**
 * @brief Overload for printing @c CDynamicProperties
 */
SILA_CPP_EXPORT std::ostream& operator<<(std::ostream& os,
                                         const CDynamicProperty& rhs);
}  // namespace SiLA2
#endif  // DYNAMICPROPERTY_H
