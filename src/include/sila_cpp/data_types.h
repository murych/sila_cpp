/**
 ** This file is part of the sila_cpp project.
 ** Copyright 2020 SiLA2
 **
 ** Permission is hereby granted, free of charge, to any person obtaining a copy
 ** of this software and associated documentation files (the "Software"), to deal
 ** in the Software without restriction, including without limitation the rights
 ** to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 ** copies of the Software, and to permit persons to whom the Software is
 ** furnished to do so, subject to the following conditions:
 **
 ** The above copyright notice and this permission notice shall be included in all
 ** copies or substantial portions of the Software.
 **
 ** THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 ** IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 ** FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 ** AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 ** LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 ** OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 ** SOFTWARE.
 **/

//============================================================================
/// \file   data_types.h
/// \author Florian Meinicke (florian.meinicke@cetoni.de)
/// \date   17.01.2020
/// \brief  Declaration of the SiLA data type convenience classes
//============================================================================
#ifndef DATA_TYPES_H
#define DATA_TYPES_H

//============================================================================
//                                  INCLUDES
//============================================================================
#include <sila_cpp/framework/data_types/CommandExecutionUUID.h>
#include <sila_cpp/framework/data_types/ExecutionInfo.h>
#include <sila_cpp/framework/data_types/SiLAAnyType.h>
#include <sila_cpp/framework/data_types/SiLABinary.h>
#include <sila_cpp/framework/data_types/SiLABoolean.h>
#include <sila_cpp/framework/data_types/SiLADate.h>
#include <sila_cpp/framework/data_types/SiLADuration.h>
#include <sila_cpp/framework/data_types/SiLAInteger.h>
#include <sila_cpp/framework/data_types/SiLAReal.h>
#include <sila_cpp/framework/data_types/SiLAString.h>
#include <sila_cpp/framework/data_types/SiLATime.h>
#include <sila_cpp/framework/data_types/SiLATimestamp.h>
#include <sila_cpp/framework/data_types/SiLATimezone.h>

#endif  // DATA_TYPES_H
