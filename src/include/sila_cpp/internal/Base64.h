/**
 ** This file is part of the sila_cpp project.
 ** Copyright 2020 SiLA2
 **
 ** Permission is hereby granted, free of charge, to any person obtaining a copy
 ** of this software and associated documentation files (the "Software"), to deal
 ** in the Software without restriction, including without limitation the rights
 ** to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 ** copies of the Software, and to permit persons to whom the Software is
 ** furnished to do so, subject to the following conditions:
 **
 ** The above copyright notice and this permission notice shall be included in all
 ** copies or substantial portions of the Software.
 **
 ** THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 ** IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 ** FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 ** AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 ** LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 ** OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 ** SOFTWARE.
 **/

//============================================================================
/// \file   Base64.h
/// \author Florian Meinicke (florian.meinicke@cetoni.de)
/// \date   13.07.2020
/// \brief  Declaration of helper functions for working with Base64 De-/Encoding
//============================================================================
#ifndef SILA_CPP_BASE64_H
#define SILA_CPP_BASE64_H

//============================================================================
//                                  INCLUDES
//============================================================================
#include <sila_cpp/global.h>

#include <string>

namespace SiLA2::internal
{
/**
 * @brief Decode the given string @a Encoded using Base64.
 *
 * @param Encoded The string to decode given in Base64 encoding
 *
 * @return The decoded string
 */
std::string SILA_CPP_EXPORT base64Decode(const std::string& Encoded);

/**
 * @brief Encode the given string @a Raw using Base64.
 *
 * @param Raw The raw string to encode
 *
 * @return The encoded string
 */
std::string SILA_CPP_EXPORT base64Encode(const std::string& Raw);

}  // namespace SiLA2::internal

#endif  // SILA_CPP_BASE64_H
