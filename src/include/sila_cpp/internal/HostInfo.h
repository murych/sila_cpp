/**
 ** This file is part of the sila_cpp project.
 ** Copyright 2020 SiLA2
 **
 ** Permission is hereby granted, free of charge, to any person obtaining a copy
 ** of this software and associated documentation files (the "Software"), to deal
 ** in the Software without restriction, including without limitation the rights
 ** to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 ** copies of the Software, and to permit persons to whom the Software is
 ** furnished to do so, subject to the following conditions:
 **
 ** The above copyright notice and this permission notice shall be included in all
 ** copies or substantial portions of the Software.
 **
 ** THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 ** IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 ** FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 ** AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 ** LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 ** OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 ** SOFTWARE.
 **/

//============================================================================
/// \file   HostInfo.h
/// \author Florian Meinicke (florian.meinicke@cetoni.de)
/// \date   29.09.2020
/// \brief  Declaration of the CHostInfo class
//============================================================================
#ifndef HOSTINFO_H
#define HOSTINFO_H

//============================================================================
//                                  INCLUDES
//============================================================================
#include <sila_cpp/global.h>

#include <QString>

namespace SiLA2::internal
{
/**
 * @brief The CHostInfo class is a utility class to construct the FQDN of the
 * localhost and lookup hosts by their IP address
 */
class SILA_CPP_EXPORT CHostInfo
{
public:
    /**
     * @brief Look up the host name associated with IP address @a IP
     *
     * @param IP The IP address to look up
     *
     * @returns The host name that corresponds to the given IP address @a IP or an
     * empty string on error
     */
    static QString lookupHostname(const QString& IP);

    /**
     * @brief Get the FQDN of the local machine's host name
     *
     * @return QString The FQDN of the local machine's host name
     */
    static QString localHostName();

    /**
     * @brief Checks if the given @a IP is a private IP address as per RFC 1918
     * for IPv4 networks respectively a Unique Local Address (ULA) as per RFC 4193
     * for IPv6 networks
     *
     * @param IP The IP address to check. If this is a hostname then the
     * corresponding IP address of this name is looked up first
     * @returns true, if @a IP is a private IPv4 or a ULA IPv6 address
     */
    static bool isPrivateIP(const QString& IP);
};
}  // namespace SiLA2::internal

#endif  // HOSTINFO_H
