/**
 ** This file is part of the sila_cpp project.
 ** Copyright 2020 SiLA2
 **
 ** Permission is hereby granted, free of charge, to any person obtaining a copy
 ** of this software and associated documentation files (the "Software"), to deal
 ** in the Software without restriction, including without limitation the rights
 ** to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 ** copies of the Software, and to permit persons to whom the Software is
 ** furnished to do so, subject to the following conditions:
 **
 ** The above copyright notice and this permission notice shall be included in all
 ** copies or substantial portions of the Software.
 **
 ** THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 ** IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 ** FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 ** AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 ** LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 ** OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 ** SOFTWARE.
 **/

//============================================================================
/// \file   IAsyncRPC.h
/// \author Florian Meinicke (florian.meinicke@cetoni.de)
/// \date   28.10.2020
/// \brief  Declaration of the IAsyncRPC class
//============================================================================
#ifndef IASYNCRPC_H
#define IASYNCRPC_H

//============================================================================
//                                  INCLUDES
//============================================================================
#include <sila_cpp/global.h>

#include <QObject>

namespace SiLA2
{
/**
 * @brief The IAsyncRPC class is the non-templated base class of all async RPCs.
 * It derives from QObject and thus provides useful Qt features (like
 * signals/slots) to work with the templated derived RPC classes.
 */
class SILA_CPP_EXPORT IAsyncRPC : public QObject
{
    Q_OBJECT

signals:
    /**
     * @brief This signal is emitted by streaming RPCs to indicate that they are
     * waiting for a new value to send to the client.
     */
    void waitingForValue() const;
};
}  // namespace SiLA2
#endif  // IASYNCRPC_H
