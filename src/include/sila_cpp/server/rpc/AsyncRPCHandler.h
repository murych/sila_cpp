/**
 ** This file is part of the sila_cpp project.
 ** Copyright 2020 SiLA2
 **
 ** Permission is hereby granted, free of charge, to any person obtaining a copy
 ** of this software and associated documentation files (the "Software"), to deal
 ** in the Software without restriction, including without limitation the rights
 ** to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 ** copies of the Software, and to permit persons to whom the Software is
 ** furnished to do so, subject to the following conditions:
 **
 ** The above copyright notice and this permission notice shall be included in all
 ** copies or substantial portions of the Software.
 **
 ** THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 ** IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 ** FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 ** AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 ** LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 ** OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 ** SOFTWARE.
 **/

//============================================================================
/// \file   AsyncRPCHandler.h
/// \author Florian Meinicke (florian.meinicke@cetoni.de)
/// \date   25.05.2020
/// \brief  Declaration of the CAsyncRPCHandler class
//============================================================================
#ifndef ASYNCRPCHANDLER_H
#define ASYNCRPCHANDLER_H

//============================================================================
//                                  INCLUDES
//============================================================================
#include <sila_cpp/global.h>
#include <sila_cpp/server/SiLAFeature.h>
#include <sila_cpp/server/rpc/AsyncRPC.h>

#include <QEventLoop>
#include <QHash>
#include <QThread>

#include <grpcpp/completion_queue.h>

#include <memory>
#include <utility>

namespace SiLA2
{
/**
 * @brief The CAsyncRPCHandler class handles one type of AsyncRPCs in a separate
 * thread
 *
 * @tparam ManagerT The Command or Property that contains this handler
 * @tparam ServiceT The gRPC Service that the handled RPCs belong to
 * @tparam AsyncRPCT The AsyncRPC class that shall be managed
 */
template<typename ManagerT, typename ServiceT, typename AsyncRPCT>
class CAsyncRPCHandler : public QThread
{
public:
    /**
     * @brief C'tor
     *
     * @param Parent The Observable (Command|Property) Manager that contains this
     * RPC Handler (resp. the RPC)
     * @param Service The gRPC Service that the handled RPC  belongs to
     * @param CompletionQueue The gRPC server completion queue used for server
     * notifications
     */
    CAsyncRPCHandler(ManagerT* Parent, ServiceT* Service,
                     std::shared_ptr<grpc::ServerCompletionQueue> CompletionQueue);

    /**
     * @brief Get a reference to the managed AsyncRPC object. Ownership is not
     * transferred.
     *
     * @return The managed RPC instance
     */
    [[nodiscard]] AsyncRPCT* rpc() const;

protected:
    void run() override;

private:
    // clang-format off
    ManagerT* m_Manager;    ///< The Command that contains this Handler (resp. the RPC)
    ServiceT* m_Service{};  ///< The gRPC service that the handled RPCs belong to
    std::shared_ptr<grpc::ServerCompletionQueue>
        m_CompletionQueue{};    ///< Queue used for server notifications
                                ///< (shared with CSiLAServer)
    AsyncRPCT* m_RPC{nullptr};  ///< The RPC that's currently processed
    QHash<AsyncRPCT*, QThread*> m_RPCThreadMap;  ///< Maps an RPC to the Thread that executes it
    // clang-format on
};

//============================================================================
template<typename ManagerT, typename ServiceT, typename AsyncRPCT>
CAsyncRPCHandler<ManagerT, ServiceT, AsyncRPCT>::CAsyncRPCHandler(
    ManagerT* Parent, ServiceT* Service,
    std::shared_ptr<grpc::ServerCompletionQueue> CompletionQueue)
    : m_Manager{Parent},
      m_Service{Service},
      m_CompletionQueue{std::move(CompletionQueue)}
{}

//============================================================================
template<typename ManagerT, typename ServiceT, typename AsyncRPCT>
AsyncRPCT* CAsyncRPCHandler<ManagerT, ServiceT, AsyncRPCT>::rpc() const
{
    return m_RPC;
}

//============================================================================
template<typename ManagerT, typename ServiceT, typename AsyncRPCT>
void CAsyncRPCHandler<ManagerT, ServiceT, AsyncRPCT>::run()
{
    m_RPC = new AsyncRPCT{m_Service, m_CompletionQueue, m_Manager};
    void* Tag;  ///< uniquely identifies a request
    bool ok;
    while (m_CompletionQueue->Next(&Tag, &ok))
    {
        const auto AsyncRPCTag = static_cast<CAsyncRPCTag<AsyncRPCT>*>(Tag);
        m_RPC = AsyncRPCTag->rpc();
        const auto RPCTagType = AsyncRPCTag->type();

        if (!ok)
        {
            qCDebug(sila_cpp_server) << m_RPC << "cannot be handled";
            delete m_RPC;
            delete AsyncRPCTag;
            continue;
        }

        if (RPCTagType == TagType::Done)
        {
            qCDebug(sila_cpp_server) << m_RPC << "is done";
            m_RPC->finish();
            auto Thread = m_RPCThreadMap.value(m_RPC, nullptr);
            if (Thread)
            {
                // If there is another Thread where this RPC is currently
                // executing (probably just waiting on a new value) we need to
                // wait for this Thread to finish first, before we can further
                // process this RPC.
                Thread->wait();
                m_RPCThreadMap.remove(m_RPC);
                qCDebug(sila_cpp_server) << Thread << "is done";
            }
        }

        if (m_RPC->isRunning() && m_RPC->isStreamingRPC())
        {
            // We need to execute `serve()` in another thread if the RPC is
            // running because this function blocks for streaming RPCs (if e.g.
            // the RPC has to wait for new Execution Info or a new Property
            // value). We don't want this to block our handler thread as well,
            // since this would result in other clients not being processed until
            // the blocking RPC returns. Thus, to be able to serve new clients
            // while some RPCs might wait on other resources to become available,
            // we need yet another thread.
            auto Thread = QThread::create([this]() { m_RPC->serve(); });
            QObject::connect(Thread, &QThread::finished, Thread,
                             &QThread::deleteLater);
            // We need to make sure that the thread has actually been started and
            // that the RPC is indeed waiting for the next value by waiting for
            // its signal. Otherwise, the thread might be started too late or the
            // RPC might not be waiting yet (because of the system's scheduling
            // policies) and the DONE tag for this RPC might have already been
            // returned from the CQ in the meantime. This scenario could then lead
            // to a double-free or use-after-free when the thread finally starts
            // executing. The CallStatus would be set to CLEANUP for the RPC in
            // the new thread, hence it would delete itself. When the DONE tag is
            // then returned from the CQ and scheduled for execution it would try
            // the same. By waiting for the RPC signal we can be sure that the RPC
            // in that thread is now waiting on a new value and that the clean-up
            // mechanism works as expected.
            QEventLoop EventLoop;
            QObject::connect(m_RPC, &AsyncRPCT::waitingForValue, &EventLoop,
                             &QEventLoop::quit);
            Thread->start();
            EventLoop.exec();
            m_RPCThreadMap[m_RPC] = Thread;
            qCDebug(sila_cpp_server) << "Started" << Thread << "for" << m_RPC;
        }
        else
        {
            m_RPC->serve();
        }
        delete AsyncRPCTag;
    }
}
}  // namespace SiLA2

#endif  // ASYNCRPCHANDLER_H
