/**
 ** This file is part of the sila_cpp project.
 ** Copyright 2020 SiLA2
 **
 ** Permission is hereby granted, free of charge, to any person obtaining a copy
 ** of this software and associated documentation files (the "Software"), to deal
 ** in the Software without restriction, including without limitation the rights
 ** to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 ** copies of the Software, and to permit persons to whom the Software is
 ** furnished to do so, subject to the following conditions:
 **
 ** The above copyright notice and this permission notice shall be included in all
 ** copies or substantial portions of the Software.
 **
 ** THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 ** IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 ** FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 ** AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 ** LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 ** OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 ** SOFTWARE.
 **/

//============================================================================
/// \file   AsyncRPC.h
/// \author Florian Meinicke (florian.meinicke@cetoni.de)
/// \date   20.05.2020
/// \brief  Declaration of the CAsyncRPC class
//============================================================================
#ifndef ASYNCRPC_H
#define ASYNCRPC_H

//============================================================================
//                                  INCLUDES
//============================================================================
#include <sila_cpp/common/logging.h>
#include <sila_cpp/global.h>
#include <sila_cpp/server/rpc/IAsyncRPC.h>

#include <grpcpp/grpcpp.h>
#include <grpcpp/impl/codegen/async_unary_call.h>

#include <atomic>
#include <memory>
#include <utility>

namespace SiLA2
{
/**
 * @brief The TagType enum contains the different types of Tags used in gRPC
 * Completion Queue operations.
 */
enum class TagType : uint8_t
{
    Request,  ///< used in the `Request<RPC>` function calls
    Write,    ///< used in `Write`, `Finish` and `WriteAndFinish` function calls
    Done,     ///< used in `AsyncNotifyWhenDone` function call to identify
              ///< when this RPC is done
};

/**
 * @brief The CAsyncRPCTag class represents the Tag that is used for gRPC
 * Completion Queue operations.
 */
template<typename AsyncRPC>
class CAsyncRPCTag
{
public:
    /**
     * @brief C'tor
     *
     * @param RPC Pointer to the Async RPC associated with this Tag
     * @param Type The Type of this Tag
     */
    CAsyncRPCTag(AsyncRPC* RPC, TagType Type);

    [[nodiscard]] AsyncRPC* rpc() const;

    [[nodiscard]] TagType type() const;

private:
    AsyncRPC* m_RPC;
    TagType m_Type{};
};

//============================================================================
template<typename AsyncRPC>
CAsyncRPCTag<AsyncRPC>::CAsyncRPCTag(AsyncRPC* RPC, TagType Type)
    : m_RPC{RPC}, m_Type{Type}
{}

//============================================================================
template<typename AsyncRPC>
AsyncRPC* CAsyncRPCTag<AsyncRPC>::rpc() const
{
    return m_RPC;
}

//============================================================================
template<typename AsyncRPC>
TagType CAsyncRPCTag<AsyncRPC>::type() const
{
    return m_Type;
}

/**
 * @brief The CAsyncRPC class
 *
 * @tparam ServiceT The gRPC Service that this RPC belongs to
 * @tparam WriterT The type of the gRPC Response Writer (either for unary or
 * streaming RPCs)
 * @tparam ResponsesT The type of the RPC Responses
 */
template<typename ServiceT, template<typename> class WriterT, typename ResponsesT>
class CAsyncRPC : public IAsyncRPC
{
public:
    /**
     * @brief The CallStatus enum implements a tiny state machine for the
     * CAsyncRPC class
     */
    enum class CallStatus : uint8_t
    {
        Create,
        FirstProcess,
        Process,
        Finish,
        Cleanup,
    };

    /**
     * @brief C'tor
     *
     * @param Parent The Parent SiLA Feature (resp. gRPC Service) that this RPC
     * belongs to
     * @param CompletionQueue The gRPC server completion queue that should be
     * used for this RPC
     */
    CAsyncRPC(ServiceT* Parent,
              std::shared_ptr<grpc::ServerCompletionQueue> CompletionQueue);

    ~CAsyncRPC() override = default;

    /**
     * @brief The serving logic
     */
    void serve();

    /**
     * @brief Whether this RPC is a streaming RPC or not
     *
     * @returns true, if this RPC streams, false otherwise
     */
    [[nodiscard]] bool isStreamingRPC() const;

    /**
     * @brief Whether the RPC is in the @a FIRST_PROCESS or @a PROCESS state
     *
     * @returns true, if @a m_CallStatus is @a FIRST_PROCESS or @a PROCESS
     * @returns false, otherwise
     */
    [[nodiscard]] bool isRunning() const;

    /**
     * @brief Change the call status of this RPC to @a FINISH.
     * Streaming RPCs may call this function to indicate that streaming has
     * finished and want to report that to the gRPC framework by calling
     * the writer's @a Finish method.
     * As a side effect, this function will check if the RCP has been
     * cancelled. If this is the case, the @a onCancelled() handler will be
     * invoked.
     */
    void finish();

    /**
     * @brief Get the name of this RPC. This is only for nicer debugging.
     *
     * @return The RPC name as a string
     */
    [[nodiscard]] virtual const char* name() const = 0;

protected:
    /**
     * @brief Safely set the @a m_CallStatus to the given @a Status.
     *
     * @param Status The new CallStatus to set
     */
    void setStatus(CallStatus Status);

    /**
     * @brief Handler function that is invoked when the CAsyncRPC instance
     * advances to the @a CREATE state. This should invoke the function that
     * 'requests' processing of this RPC.
     */
    virtual void onCreate() = 0;

    /**
     * @brief Handler function that is invoked when the CAsyncRPC instance
     * advances to the @a PROCESS state. This should invoke the function
     * containing the actual RPC logic.
     *
     * @return A pair of the Status of the RPC execution and its result
     */
    virtual std::pair<const grpc::Status, const ResponsesT> onProcess() = 0;

    /**
     * @brief Create a new CAsyncRPC instance. This is used to create new
     * instances when the current instance is in the @a FIRST_PROCESS state. The
     * newly created instance can then take care of new (concurrent) requests,
     * e.g. from other clients.
     */
    virtual void create() const = 0;

    /**
     * @brief Handler function that is invoked when this RPC has been cancelled by
     * the client.
     * The default implementation just sets the @a m_CallStatus to @a FINISH. This
     * can be overridden in subclasses to e.g. stop observing Property value
     * changes or wait for Command Execution Info.
     */
    virtual void onCancelled();

    ServiceT*
        m_Service;  ///< The gRPC Service containing the member functions for this RPC
    std::shared_ptr<grpc::ServerCompletionQueue>
        m_CompletionQueue;  ///< For server notifications (used in derived classes)
    grpc::ServerContext m_Context{};  ///< gRPC server context for e.g. metadata
    WriterT<ResponsesT> m_Responder;  ///< The means to get back to the client

private:
    // clang-format off
    std::atomic<CallStatus>
        m_CallStatus{CallStatus::Create};  ///< Current state of this RPC
    // clang-format on
};

//=============================================================================
template<typename Service, template<typename> class Writer, typename Responses>
CAsyncRPC<Service, Writer, Responses>::CAsyncRPC(
    Service* Parent, std::shared_ptr<grpc::ServerCompletionQueue> CompletionQueue)
    : m_Service{Parent},
      m_CompletionQueue{std::move(CompletionQueue)},
      m_Responder{&m_Context}
{}

//=============================================================================
template<typename Service, template<typename> class Writer, typename Responses>
void CAsyncRPC<Service, Writer, Responses>::serve()
{
    switch (m_CallStatus)
    {
    case CallStatus::Create:
        m_CallStatus = CallStatus::FirstProcess;
        qCDebug(sila_cpp_server) << "Creating new" << this;
        m_Context.AsyncNotifyWhenDone(new CAsyncRPCTag{this, TagType::Done});

        // As part of the initial CREATE state, we *request* that the system
        // starts processing requests. In this request, "this" acts as the tag
        // uniquely identifying the request (so that different CAsyncRPC
        // instances can serve different requests concurrently), in this case
        // the memory address of this CAsyncRPC instance.
        onCreate();
        return;
    case CallStatus::FirstProcess:
        // Spawn a new RPC instance to serve new clients while processing this
        // request. The instance will deallocate itself as part of its CLEANUP
        // state.
        // Only do this once so that there won't be a million new instances for
        // streaming RPCs (as they will be in the PROCESS state for multiple
        // cycles)
        m_CallStatus = CallStatus::Process;
        create();
        [[fallthrough]];
    case CallStatus::Process:
    {
        if (m_Context.IsCancelled())
        {
            qCDebug(sila_cpp_server)
                << this << "has been cancelled by the client";
        }

        qCDebug(sila_cpp_server) << "Processing" << this;
        // The actual processing
        const auto [Status, Response] = onProcess();

        if constexpr (std::is_same_v<grpc::ServerAsyncResponseWriter<Responses>,
                                     decltype(m_Responder)>)
        {
            finish();
            // m_Responder is grpc::ServerAsyncResponseWriter that returns the
            // response to the client through the `Finish` call
            m_Responder.Finish(Response, Status,
                               new CAsyncRPCTag{this, TagType::Write});
        }
        else
        {
            // m_Responder is grpc::ServerAsyncWriter that returns a message as
            // part of the stream through `Write` and the final Message and Status
            // through `WriteAndFinish`

            // state might change during `onProcess` when `finish` is called
            if (m_CallStatus == CallStatus::Process)
            {
                m_Responder.Write(Response,
                                  new CAsyncRPCTag{this, TagType::Write});
            }
            else if (m_CallStatus == CallStatus::Finish)
            {
                if (Status.ok())
                {
                    m_Responder.WriteAndFinish(
                        Response, grpc::WriteOptions{}.set_last_message(), Status,
                        new CAsyncRPCTag{this, TagType::Write});
                }
                else
                {
                    m_Responder.Finish(Status,
                                       new CAsyncRPCTag{this, TagType::Write});
                }
            }
        }
        return;
    }
    case CallStatus::Finish:
        qCDebug(sila_cpp_server) << this << "is finished";
        m_CallStatus = CallStatus::Cleanup;
        return;
    case CallStatus::Cleanup:
        qCDebug(sila_cpp_server) << "Cleaning up" << this;
        delete this;
        return;
    }
    qCWarning(sila_cpp_server) << "Unhandled RPC Call State"
                               << static_cast<uint8_t>(m_CallStatus.load());
}

//============================================================================
template<typename Service, template<typename> class Writer, typename Responses>
bool CAsyncRPC<Service, Writer, Responses>::isStreamingRPC() const
{
    return !std::is_same_v<grpc::ServerAsyncResponseWriter<Responses>,
                           decltype(m_Responder)>;
}

//============================================================================
template<typename Service, template<typename> class Writer, typename Responses>
bool CAsyncRPC<Service, Writer, Responses>::isRunning() const
{
    return m_CallStatus == CallStatus::Process;
}

//============================================================================
template<typename Service, template<typename> class Writer, typename Responses>
void CAsyncRPC<Service, Writer, Responses>::finish()
{
    if (m_CallStatus == CallStatus::Finish)
    {
        // In newer gRPC versions it might happen that the RPC `finish()`ed itself
        // already but the AsyncRPCTag with TagType::DONE that will be delivered
        // next in the CQ indicates client cancellation. This might result in
        // immediate deletion of the RPC eventually leading to a use-after-free
        // since there is still the normal AsyncRPCTag with TagType::WRITE for the
        // same RPC in the CQ that was put in by the call to `Finish()`.
        return;
    }

    qCDebug(sila_cpp_server) << "Finishing" << this;
    if (m_Context.IsCancelled())
    {
        qCDebug(sila_cpp_server) << this << "has been cancelled by the client";
        this->onCancelled();
        return;
    }
    m_CallStatus = CallStatus::Finish;
}

//============================================================================
template<typename Service, template<typename> class Writer, typename Responses>
void CAsyncRPC<Service, Writer, Responses>::setStatus(CallStatus Status)
{
    m_CallStatus = Status;
}

//============================================================================
template<typename Service, template<typename> class Writer, typename Responses>
void CAsyncRPC<Service, Writer, Responses>::onCancelled()
{
    qCDebug(sila_cpp_server) << "Cancelling" << this;
    if (m_CallStatus == CallStatus::Cleanup)
    {
        // Even though this shouldn't happen, be sure to not reset the status
        // to FINISH if we're already in CLEANUP
        qCDebug(sila_cpp_server) << this << "is already Cleanup state";
        return;
    }
    m_CallStatus = CallStatus::Finish;
}

//============================================================================
template<typename Service, template<typename> class Writer, typename Responses>
QDebug& operator<<(QDebug dbg, CAsyncRPC<Service, Writer, Responses>* rhs)
{
    return dbg << rhs->name() << static_cast<void*>(rhs);
}
}  // namespace SiLA2

#endif  // ASYNCRPC_H
