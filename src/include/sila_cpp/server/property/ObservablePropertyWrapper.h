/**
 ** This file is part of the sila_cpp project.
 ** Copyright 2020 SiLA2
 **
 ** Permission is hereby granted, free of charge, to any person obtaining a copy
 ** of this software and associated documentation files (the "Software"), to deal
 ** in the Software without restriction, including without limitation the rights
 ** to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 ** copies of the Software, and to permit persons to whom the Software is
 ** furnished to do so, subject to the following conditions:
 **
 ** The above copyright notice and this permission notice shall be included in all
 ** copies or substantial portions of the Software.
 **
 ** THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 ** IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 ** FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 ** AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 ** LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 ** OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 ** SOFTWARE.
 **/

//============================================================================
/// \file   ObservableProperty.h
/// \author Florian Meinicke (florian.meinicke@cetoni.de)
/// \date   03.06.2020
/// \brief  Definition of the CObservablePropertyWrapper class
//============================================================================
#ifndef OBSERVABLEPROPERTYMANAGER_H
#define OBSERVABLEPROPERTYMANAGER_H

//============================================================================
//                                  INCLUDES
//============================================================================
#include <sila_cpp/common/logging.h>
#include <sila_cpp/server/SiLAServer.h>
#include <sila_cpp/server/rpc/AsyncRPCHandler.h>
#include <sila_cpp/server/rpc/SubscribeRPC.h>
#include <sila_cpp/server/rpc/definitions.h>

#include "IPropertyWrapper.h"

#include <QObject>

namespace SiLA2
{
/**
 * @brief The CObservablePropertyWrapper class is a class template that allows to
 * implement SiLA 2 Observable Properties.
 *
 * @tparam T Underlying type of the actual Property
 * @tparam RequestSubscribeF Member function pointer of the
 * `RequestSubscribe_<Property>` function
 *
 * @note The underlying type must be convertible to the Data Type used in the
 * Feature Description. I.e. if in the FDL file the Property was declared as
 * @code
 *     <Property>
 *         <Identifier>MyObservableProperty</Identifier>
 *         <DisplayName>My Observable Property</DisplayName>
 *         <Description>...</Description>
 *         <Observable>Yes</Observable>
 *         <DataType>
 *             <Basic>Real</Basic>
 *         </DataType>
 *     </Property>
 * @endcode
 *
 * then the underlying type should either be @a SiLA2::CReal or @a double.
 *
 * @details As a user you just need to create a CObservablePropertyWrapper
 * instance providing the necessary function for the Observable Property and the
 * underlying type of the actual Property as template parameters. In your
 * Feature's constructor you need to pass a reference to your Feature and
 * optionally set an initial value. The rest is handled by this class. For example
 * @code
 * class MySiLAFeatureImpl : public SiLA2::CSiLAFeature<...>
 * {
 * public:
 *     MySiLAFeatureImpl()
 *         : m_MyObservableProperty{this, SiLA2::CReal{1.0}}
 *     {}
 *
 * private:
 *     SiLA2::CObservablePropertyWrapper<
 *         &MySiLAFeatureImpl::RequestSubscribe_MyObservableProperty,
 *         SiLA2::CReal>
 *         m_MyObservableProperty;
 * };
 * @endcode
 *
 * You can access your property via the getter and setter functions (@a value()
 * and @a setValue()). If the underlying type of the Property supports the
 * assignment operator you can also assign values directly by using this class's
 * @a operator=() overload. E.g. considering the code above you could assign new
 * values to @c m_MyObservableProperty like this:
 * @code
 * m_MyObservableProperty.setValue(123.45); // using the setter function
 * m_MyObservableProperty = 123.45;         // using the `operator=()` overload
 * @endcode
 *
 * See the CTemperatureControllerImpl class in the HelloSiLA2 example for an
 * example on how to implement Observable Properties.
 */
template<typename T, auto RequestSubscribeF>
class CObservablePropertyWrapper;

/**
 * @brief Template specialisation of the CObservablePropertyWrapper class
 *
 * @tparam T Underlying type of the actual Property
 * @tparam SubscribeServiceT The gRPC Service that contains the
 * `RequestSubscribe_<Property>` member function
 * @tparam ParametersT The type of the `Subscribe_<Property>` parameters
 * @tparam ResponsesT The type of the `Subscribe_<Property>` responses
 * @tparam RS Signature of the `RequestSubscribe_<Property>` member function
 * pointer
 */
template<typename T, typename SubscribeServiceT, typename ParametersT,
         typename ResponsesT, SubscribeServiceTemplate>
class CObservablePropertyWrapper<T, RS> : public IPropertyWrapper<T, ResponsesT>
{
    using Super = IPropertyWrapper<T, ResponsesT>;

public:
    using typename Super::ValueCallbackF;
    template<typename FeatureT>
    using ValueCallbackMemF =
        typename Super::template ValueCallbackMemF<FeatureT>;
    template<typename FeatureT>
    using ValueCallbackConstMemF =
        typename Super::template ValueCallbackConstMemF<FeatureT>;

    /**
     * @brief C'tor
     *
     * @tparam SiLAFeatureT User defined SiLA Feature Implementation
     * @param Feature The SiLA Feature that contains this Observable Property
     * @param Value The initial value of the Property
     */
    template<typename SiLAFeatureT,
             typename =
                 std::enable_if_t<std::is_base_of_v<ISiLAFeature, SiLAFeatureT>>>
    explicit CObservablePropertyWrapper(SiLAFeatureT* Feature,
                                        const T& Value = {});

    /**
     * @brief C'tor
     *
     * @tparam SiLAFeatureT User defined SiLA Feature Implementation
     * @param Feature A pointer to the SiLA Feature that this Property belongs to
     * @param Callback A callback function (can be a free function or a lambda, or
     * a const/non-const member function of @a Feature) that should be used to get
     * the current value of the Property
     */
    template<typename SiLAFeatureT,
             typename =
                 std::enable_if_t<std::is_base_of_v<ISiLAFeature, SiLAFeatureT>>>
    CObservablePropertyWrapper(SiLAFeatureT* Feature, ValueCallbackF Callback);

    template<typename SiLAFeatureT,
             typename =
                 std::enable_if_t<std::is_base_of_v<ISiLAFeature, SiLAFeatureT>>>
    CObservablePropertyWrapper(SiLAFeatureT* Feature,
                               ValueCallbackMemF<SiLAFeatureT> Callback);

    template<typename SiLAFeatureT,
             typename =
                 std::enable_if_t<std::is_base_of_v<ISiLAFeature, SiLAFeatureT>>>
    CObservablePropertyWrapper(SiLAFeatureT* Feature,
                               ValueCallbackConstMemF<SiLAFeatureT> Callback);

    /**
     * @brief C'tor
     *
     * @tparam SiLAFeatureT User defined SiLA Feature Implementation
     * @param Feature A pointer to the SiLA Feature that this Property belongs to
     * @param Value The initial value of the Property
     * @param Callback A callback function (can be a free function or a lambda, or
     * a const/non-const member function of @a Feature) that should be used to get
     * the current value of the Property
     */
    template<typename SiLAFeatureT,
             typename =
                 std::enable_if_t<std::is_base_of_v<ISiLAFeature, SiLAFeatureT>>>
    CObservablePropertyWrapper(SiLAFeatureT* Feature, const T& Value,
                               ValueCallbackF Callback);

    template<typename SiLAFeatureT,
             typename =
                 std::enable_if_t<std::is_base_of_v<ISiLAFeature, SiLAFeatureT>>>
    CObservablePropertyWrapper(SiLAFeatureT* Feature, const T& Value,
                               ValueCallbackMemF<SiLAFeatureT> Callback);

    template<typename SiLAFeatureT,
             typename =
                 std::enable_if_t<std::is_base_of_v<ISiLAFeature, SiLAFeatureT>>>
    CObservablePropertyWrapper(SiLAFeatureT* Feature, const T& Value,
                               ValueCallbackConstMemF<SiLAFeatureT> Callback);

    /**
     * @brief Assignment operator for a more convenient usage with underlying
     * types that have an assignment operator themselves.
     * @note This function is only available for underlying types that are either
     * POD or implement @c operator=()
     *
     * @tparam U The type of the value to assign
     *
     * @param val The new value to set
     */
    template<typename U>
    typename std::enable_if_t<(std::is_pod_v<T> && std::is_pod_v<U>)
                                  || internal::has_assignment_operator_v<T, U>,
                              CObservablePropertyWrapper&>
    operator=(const U& val);

private:
    /**
     * @brief Connects to the SiLA Server's @c started() signal to start the Async
     * RPC handler after the Server has been started
     *
     * @param Feature The SiLA Feature whose server to connect to
     */
    void connectToServerStarted(ISiLAFeature* Feature);

    using SubscribeRPC =
        CSubscribeRPC<SubscribeServiceT, ParametersT, ResponsesT, T, RS>;
    CAsyncRPCHandler<CObservablePropertyWrapper, SubscribeServiceT, SubscribeRPC>
        m_Handler;  ///< RPC handler for the Subscribe RPC
};

//=============================================================================
template<typename T, typename SubscribeServiceT, typename ParametersT,
         typename ResponsesT, SubscribeServiceTemplate>
template<typename SiLAFeatureT, typename>
CObservablePropertyWrapper<T, RS>::CObservablePropertyWrapper(
    SiLAFeatureT* Feature, const T& Value)
    : Super{Feature, Value},
      m_Handler{this, Feature, Feature->server()->addCompletionQueue()}
{
    connectToServerStarted(Feature);
}

//=============================================================================
template<typename T, typename SubscribeServiceT, typename ParametersT,
         typename ResponsesT, SubscribeServiceTemplate>
template<typename SiLAFeatureT, typename>
CObservablePropertyWrapper<T, RS>::CObservablePropertyWrapper(
    SiLAFeatureT* Feature, ValueCallbackF Callback)
    : Super{Feature, std::move(Callback)},
      m_Handler{this, Feature, Feature->server()->addCompletionQueue()}
{
    connectToServerStarted(Feature);
}

//=============================================================================
template<typename T, typename SubscribeServiceT, typename ParametersT,
         typename ResponsesT, SubscribeServiceTemplate>
template<typename SiLAFeatureT, typename>
CObservablePropertyWrapper<T, RS>::CObservablePropertyWrapper(
    SiLAFeatureT* Feature, ValueCallbackMemF<SiLAFeatureT> Callback)
    : Super{Feature, std::move(Callback)},
      m_Handler{this, Feature, Feature->server()->addCompletionQueue()}
{
    connectToServerStarted(Feature);
}

//=============================================================================
template<typename T, typename SubscribeServiceT, typename ParametersT,
         typename ResponsesT, SubscribeServiceTemplate>
template<typename SiLAFeatureT, typename>
CObservablePropertyWrapper<T, RS>::CObservablePropertyWrapper(
    SiLAFeatureT* Feature, ValueCallbackConstMemF<SiLAFeatureT> Callback)
    : Super{Feature, std::move(Callback)},
      m_Handler{this, Feature, Feature->server()->addCompletionQueue()}
{
    connectToServerStarted(Feature);
}

//=============================================================================
template<typename T, typename SubscribeServiceT, typename ParametersT,
         typename ResponsesT, SubscribeServiceTemplate>
template<typename SiLAFeatureT, typename>
CObservablePropertyWrapper<T, RS>::CObservablePropertyWrapper(
    SiLAFeatureT* Feature, const T& Value, ValueCallbackF Callback)
    : Super{Feature, Value, std::move(Callback)},
      m_Handler{this, Feature, Feature->server()->addCompletionQueue()}
{
    connectToServerStarted(Feature);
}

//=============================================================================
template<typename T, typename SubscribeServiceT, typename ParametersT,
         typename ResponsesT, SubscribeServiceTemplate>
template<typename SiLAFeatureT, typename>
CObservablePropertyWrapper<T, RS>::CObservablePropertyWrapper(
    SiLAFeatureT* Feature, const T& Value,
    ValueCallbackMemF<SiLAFeatureT> Callback)
    : Super{Feature, Value, std::move(Callback)},
      m_Handler{this, Feature, Feature->server()->addCompletionQueue()}
{
    connectToServerStarted(Feature);
}

//=============================================================================
template<typename T, typename SubscribeServiceT, typename ParametersT,
         typename ResponsesT, SubscribeServiceTemplate>
template<typename SiLAFeatureT, typename>
CObservablePropertyWrapper<T, RS>::CObservablePropertyWrapper(
    SiLAFeatureT* Feature, const T& Value,
    ValueCallbackConstMemF<SiLAFeatureT> Callback)
    : Super{Feature, Value, std::move(Callback)},
      m_Handler{this, Feature, Feature->server()->addCompletionQueue()}
{
    connectToServerStarted(Feature);
}

//=============================================================================
template<typename T, typename SubscribeServiceT, typename ParametersT,
         typename ResponsesT, SubscribeServiceTemplate>
template<typename U>
typename std::enable_if_t<(std::is_pod_v<T> && std::is_pod_v<U>)
                              || internal::has_assignment_operator_v<T, U>,
                          CObservablePropertyWrapper<T, RS>&>
CObservablePropertyWrapper<T, RS>::operator=(const U& val)
{
    this->setValue(static_cast<T>(val));
    return *this;
}

//=============================================================================
template<typename T, typename SubscribeServiceT, typename ParametersT,
         typename ResponsesT, SubscribeServiceTemplate>
void CObservablePropertyWrapper<T, RS>::connectToServerStarted(
    ISiLAFeature* Feature)
{
    QObject::connect(Feature->server(), &CSiLAServer::started,
                     [this]() { m_Handler.start(); });
}
}  // namespace SiLA2

#endif  // OBSERVABLEPROPERTYMANAGER_H
