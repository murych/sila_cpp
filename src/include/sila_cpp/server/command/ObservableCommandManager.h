/**
 ** This file is part of the sila_cpp project.
 ** Copyright 2020 SiLA2
 **
 ** Permission is hereby granted, free of charge, to any person obtaining a copy
 ** of this software and associated documentation files (the "Software"), to deal
 ** in the Software without restriction, including without limitation the rights
 ** to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 ** copies of the Software, and to permit persons to whom the Software is
 ** furnished to do so, subject to the following conditions:
 **
 ** The above copyright notice and this permission notice shall be included in all
 ** copies or substantial portions of the Software.
 **
 ** THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 ** IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 ** FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 ** AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 ** LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 ** OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 ** SOFTWARE.
 **/

//============================================================================
/// \file   ObservableCommand.h
/// \author Florian Meinicke (florian.meinicke@cetoni.de)
/// \date   03.06.2020
/// \brief  Definition of the CObservableCommandManager class
//============================================================================
#ifndef OBSERVABLECOMMANDMANAGER_H
#define OBSERVABLECOMMANDMANAGER_H

//============================================================================
//                                  INCLUDES
//============================================================================
#include <sila_cpp/common/FullyQualifiedFeatureID.h>
#include <sila_cpp/common/logging.h>
#include <sila_cpp/framework/data_types/CommandExecutionUUID.h>
#include <sila_cpp/server/SiLAServer.h>
#include <sila_cpp/server/rpc/AsyncRPCHandler.h>
#include <sila_cpp/server/rpc/CommandInfoRPC.h>
#include <sila_cpp/server/rpc/CommandInitRPC.h>
#include <sila_cpp/server/rpc/CommandResultRPC.h>
#include <sila_cpp/server/rpc/IntermediateResultRPC.h>
#include <sila_cpp/server/rpc/definitions.h>

#include "IObservableCommandManager.h"
#include "ObservableCommandWrapper.h"

#include <QHash>
#include <QObject>

#include <memory>

namespace SiLA2
{
/**
 * @brief The CObservableCommandManager class is a class template that allows to
 * implement SiLA 2 Observable Commands.
 *
 * @tparam RequestCommandF Member function pointer of the `Request<Command>`
 * function
 * @tparam RequestCommandInfoF Member function pointer of the
 * `Request<Command>_Info` function
 * @tparam RequestCommandResultF Member function pointer of the
 * `Request<Command>_Result` function
 * @tparam RequestIntermediateResultF Member function pointer of the
 * `Request<Command>_Intermediate` function
 *
 * @details As a user you just need to create a CObservableCommandManager instance
 * providing the necessary functions for the Observable Command as template
 * parameters and give it a pointer to your Feature implementation in the c'tor.
 * Additionally, you need to provide a function that contains the actual logic of
 * the Command (the Executor). The rest is handled by this class. For example
 * @code
 * class MySiLAFeatureImpl : public SiLA2::CSiLAFeature<...>
 * {
 *  using MyObservableCommand = SiLA2::CObservableCommandManager<
 *      &MySiLAFeatureImpl::RequestMyObservableCommand,
 *      &MySiLAFeatureImpl::RequestMyObservableCommand_Info,
 *      &MySiLAFeatureImpl::RequestMyObservableCommand_Result,
 *      // optional; only if Intermediate Result was specified in FDL
 *      &MySiLAFeatureImpl::RequestMyObservableCommand_Intermediate>;
 *  using MyObservableCommandWrapper =
 *      SiLA2::CObservableCommandWrapper<MyObservableCommand_Parameters,
 *                                       MyObservableCommand_Responses,
 *      // optional; only if Intermediate Result was specified in FDL
 *                                       MyObservableCommand_IntermediateResponsesT>;
 * public:
 *     MySiLAFeatureImpl()
 *         : m_MyObservableCommand{this}
 *     {
 *         m_MyObservableCommand.setExecutor(
 *             this, &MySiLAFeatureImpl::MyObservableCommandExecutor);
 *     }
 *
 *     MyObservableCommand_Responses MyObservableCommandExecutor(
 *         MyObservableCommandWrapper* Command)
 *     {
 *     }
 * private:
 *     MyObservableCommand m_MyObservableCommand;
 * };
 * @endcode
 *
 * If your Executor contains relatively little code and you don't want to use a
 * dedicated member function, you can also pass it as anonymous lambda to the
 * Command Manager.
 *
 * @note The SiLA 2 standard requires validation of the Command Parameters before
 * starting your actual implementation of the Command. If any of the Parameters
 * don't meet your requirements, you need to throw a corresponding Validation
 * Error. Also, if your Command Execution fails, you can throw a corresponding
 * Execution Error to notify the client that something went wrong.
 *
 * See the CTemperatureControllerImpl class in the HelloSiLA2 example for an
 * example on how to implement Observable Commands.
 */
template<auto RequestCommandF, auto RequestCommandInfoF,
         auto RequestCommandResultF, auto RequestIntermediateResultF = nullptr>
class CObservableCommandManager;

/**
 * @brief Template specialisation of the CObservableCommandManager class for a
 * basic Command without Intermediate Responses
 *
 * @internal Because of how gRPC's async API works we need to have 3 different
 * Service classes for the 3 Request functions. Every Request function is
 * therefore a pointer to a member function of a different class. And since we
 * need to use the exact class when calling the member function pointer, we need
 * to have the 3 different Service classes.
 */
template<typename InitServiceT, typename InfoServiceT, typename ResultServiceT,
         typename ParametersT, typename ResponsesT, InitServiceTemplate,
         InfoServiceTemplate, ResultServiceTemplate>
class CObservableCommandManager<RC, RCI, RCR> :
    public IObservableCommandManager<ParametersT, ResponsesT>
{
public:
    /**
     * @brief C'tor
     *
     * @tparam SiLAFeatureT User defined SiLA Feature Implementation class
     *
     * @param Feature A pointer to the SiLA Feature that contains this Observable
     * Command
     * @param Lifetime The Lifetime of this Command (specifies the Duration for
     * which the UUID of a Command Execution is valid)
     * The Lifetime can be updated when calling
     * @a CObservableCommandWrapper::setExecutionInfo() function.
     *
     * @sa CObservableCommandWrapper::setExecutionInfo()
     */
    template<typename SiLAFeatureT>
    explicit CObservableCommandManager(SiLAFeatureT* Feature,
                                       const CDuration& Lifetime = {});

private:
    /**
     * @brief Start accepting requests for this Observable Command
     */
    void serve();

    CAsyncRPCHandler<CObservableCommandManager, InitServiceT,
                     CCommandInitRPC<InitServiceT, ParametersT, ResponsesT, RC>>
        m_CommandInitHandler;  ///< RPC handler for the Command Initialisation RPC
    CAsyncRPCHandler<CObservableCommandManager, InfoServiceT,
                     CCommandInfoRPC<InfoServiceT, ParametersT, ResponsesT, RCI>>
        m_CommandInfoHandler;  ///< RPC handler for the Command Info RPC
    CAsyncRPCHandler<
        CObservableCommandManager, ResultServiceT,
        CCommandResultRPC<ResultServiceT, ParametersT, ResponsesT, RCR>>
        m_CommandResultHandler;  ///< RPC handler for the Command Result RPC
};

//=============================================================================
template<typename InitServiceT, typename InfoServiceT, typename ResultServiceT,
         typename ParametersT, typename ResponsesT, InitServiceTemplate,
         InfoServiceTemplate, ResultServiceTemplate>
template<typename SiLAFeatureT>
CObservableCommandManager<RC, RCI, RCR>::CObservableCommandManager(
    SiLAFeatureT* Feature, const CDuration& Lifetime)
    : IObservableCommandManager<ParametersT, ResponsesT>{Feature, Lifetime},
      m_CommandInitHandler{this, Feature,
                           Feature->server()->addCompletionQueue()},
      m_CommandInfoHandler{this, Feature,
                           Feature->server()->addCompletionQueue()},
      m_CommandResultHandler{this, Feature,
                             Feature->server()->addCompletionQueue()}
{
    QObject::connect(Feature->server(), &CSiLAServer::started,
                     [this]() { serve(); });
}

//============================================================================
template<typename InitServiceT, typename InfoServiceT, typename ResultServiceT,
         typename ParametersT, typename ResponsesT, InitServiceTemplate,
         InfoServiceTemplate, ResultServiceTemplate>
void CObservableCommandManager<RC, RCI, RCR>::serve()
{
    if (!this->executor())
    {
        qCCritical(sila_cpp_server)
            << "No executor function set for Observable Command"
            << this->identifier()
            << "\nThis Command will not be available for clients to request!";
        return;
    }

    m_CommandInitHandler.start();
    m_CommandInfoHandler.start();
    m_CommandResultHandler.start();
}

/**
 * @brief Template specialization of the CObservableCommandManager class for
 * Commands with Intermediate Responses
 *
 * @internal Because we cannot use defaults for the template parameters in
 * template specializations, we have to have two specializations with most of the
 * code duplicated from above.
 */
template<typename InitServiceT, typename InfoServiceT, typename ResultServiceT,
         typename ParametersT, typename ResponsesT, InitServiceTemplate,
         InfoServiceTemplate, ResultServiceTemplate,
         typename IntermediateResultServiceT, typename IntermediateResponsesT,
         IntermediateResultServiceTemplate>
class CObservableCommandManager<RC, RCI, RCR, RIR> :
    public IObservableCommandManager<ParametersT, ResponsesT,
                                     IntermediateResponsesT>
{
public:
    /**
     * @brief C'tor
     *
     * @tparam SiLAFeatureT User defined SiLA Feature Implementation class
     *
     * @param Feature A pointer to the SiLA Feature that contains this Observable
     * Command
     * @param Lifetime The Lifetime of this Command (specifies the Duration for
     * which the UUID of a Command Execution is valid)
     * The Lifetime can be updated when calling
     * @a CObservableCommandWrapper::setExecutionInfo() function.
     *
     * @note It is strongly recommended to specify a @a Lifetime. If no
     * @a Lifetime is given the Command will be valid (and thus occupy resources)
     * for the whole lifetime of the SiLA Server. This may result in extensive
     * memory usage depending on how many Observable Commands there are.
     *
     * @sa CObservableCommandWrapper::setExecutionInfo()
     */
    template<typename SiLAFeatureT>
    explicit CObservableCommandManager(SiLAFeatureT* Feature,
                                       const CDuration& Lifetime = {});

private:
    /**
     * @brief Start accepting requests for this Observable Command
     */
    void serve();

    CAsyncRPCHandler<CObservableCommandManager, InitServiceT,
                     CCommandInitRPC<InitServiceT, ParametersT, ResponsesT, RC,
                                     IntermediateResponsesT>>
        m_CommandInitHandler;  ///< RPC handler for the Command Initialisation RPC
    CAsyncRPCHandler<CObservableCommandManager, InfoServiceT,
                     CCommandInfoRPC<InfoServiceT, ParametersT, ResponsesT, RCI,
                                     IntermediateResponsesT>>
        m_CommandInfoHandler;  ///< RPC handler for the Command Info RPC
    CAsyncRPCHandler<CObservableCommandManager, ResultServiceT,
                     CCommandResultRPC<ResultServiceT, ParametersT, ResponsesT, RCR,
                                       IntermediateResponsesT>>
        m_CommandResultHandler;  ///< RPC handler for the Command Result RPC
    CAsyncRPCHandler<
        CObservableCommandManager, IntermediateResultServiceT,
        CIntermediateResultRPC<IntermediateResultServiceT, ParametersT,
                               ResponsesT, IntermediateResponsesT, RIR>>
        m_IntermediateResultHandler;  ///< RPC handler for the Intermediate Result RPC
};

//=============================================================================
template<typename InitServiceT, typename InfoServiceT, typename ResultServiceT,
         typename ParametersT, typename ResponsesT, InitServiceTemplate,
         InfoServiceTemplate, ResultServiceTemplate,
         typename IntermediateResultServiceT, typename IntermediateResponsesT,
         IntermediateResultServiceTemplate>
template<typename SiLAFeatureT>
CObservableCommandManager<RC, RCI, RCR, RIR>::CObservableCommandManager(
    SiLAFeatureT* Feature, const CDuration& Lifetime)
    : IObservableCommandManager<ParametersT, ResponsesT,
                                IntermediateResponsesT>{Feature, Lifetime},
      m_CommandInitHandler{this, Feature,
                           Feature->server()->addCompletionQueue()},
      m_CommandInfoHandler{this, Feature,
                           Feature->server()->addCompletionQueue()},
      m_CommandResultHandler{this, Feature,
                             Feature->server()->addCompletionQueue()},
      m_IntermediateResultHandler{this, Feature,
                                  Feature->server()->addCompletionQueue()}
{
    QObject::connect(Feature->server(), &CSiLAServer::started,
                     [this]() { serve(); });
}

//============================================================================
template<typename InitServiceT, typename InfoServiceT, typename ResultServiceT,
         typename ParametersT, typename ResponsesT, InitServiceTemplate,
         InfoServiceTemplate, ResultServiceTemplate,
         typename IntermediateResultServiceT, typename IntermediateResponsesT,
         IntermediateResultServiceTemplate>
void CObservableCommandManager<RC, RCI, RCR, RIR>::serve()
{
    if (!this->executor())
    {
        qCCritical(sila_cpp_server)
            << "No executor function set for Observable Command"
            << this->identifier()
            << "\nThis Command will not be available for clients to request!";
        return;
    }

    m_CommandInitHandler.start();
    m_CommandInfoHandler.start();
    m_CommandResultHandler.start();
    m_IntermediateResultHandler.start();
}
}  // namespace SiLA2

#endif  // OBSERVABLECOMMANDMANAGER_H
