/**
 ** This file is part of the sila_cpp project.
 ** Copyright 2020 SiLA2
 **
 ** Permission is hereby granted, free of charge, to any person obtaining a copy
 ** of this software and associated documentation files (the "Software"), to deal
 ** in the Software without restriction, including without limitation the rights
 ** to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 ** copies of the Software, and to permit persons to whom the Software is
 ** furnished to do so, subject to the following conditions:
 **
 ** The above copyright notice and this permission notice shall be included in all
 ** copies or substantial portions of the Software.
 **
 ** THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 ** IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 ** FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 ** AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 ** LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 ** OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 ** SOFTWARE.
 **/

//============================================================================
/// \file   SiLAServer.h
/// \author Florian Meinicke (florian.meinicke@cetoni.de)
/// \date   06.01.2020
/// \brief  Declaration of the CSiLAServer class
//============================================================================
#ifndef SILASERVER_H
#define SILASERVER_H

//============================================================================
//                                  INCLUDES
//============================================================================
#include <sila_cpp/common/SSLCredentials.h>
#include <sila_cpp/common/ServerAddress.h>
#include <sila_cpp/config.h>
#include <sila_cpp/global.h>

#include "SiLAFeature.h"

#include <QObject>

#include <polymorphic_value.h>

//=============================================================================
//                            FORWARD DECLARATIONS
//=============================================================================
namespace grpc
{
class Service;
}  // namespace grpc

#if GRPC_VERSION >= GRPC_VERSION_CHECK(1, 32, 0)
namespace grpc
{
class ServerCompletionQueue;
}  // namespace grpc
#else
namespace grpc_impl
{
class ServerCompletionQueue;
}  // namespace grpc_impl

namespace grpc
{
typedef grpc_impl::ServerCompletionQueue ServerCompletionQueue;
}  // namespace grpc
#endif

class QCommandLineParser;

namespace SiLA2
{
class CServerInformation;
class CFullyQualifiedFeatureID;
template<typename T>
class CSiLAFeature;

/**
 * @brief The CSiLAServer class provides a base for implementing custom SiLA 2
 * servers
 */
class SILA_CPP_EXPORT CSiLAServer : public QObject
{
    Q_OBJECT

    class PrivateImpl;
    using PrivateImplPtr = isocpp_p0201::polymorphic_value<PrivateImpl>;

public:
    /**
     * @brief C'tor
     *
     * @param ServerInformation The server's information such as name, type, ...
     * @param Address The server's address (IP and port number), defaults to
     * 127.0.0.1:50051
     * @param parent The parent object
     */
    explicit CSiLAServer(CServerInformation ServerInformation,
                         const CServerAddress& Address = {},
                         QObject* parent = nullptr, PrivateImplPtr priv = {});

    /**
     * @brief D'tor
     * Shuts down the server if it is still running
     */
    ~CSiLAServer() override;

    /**
     * @brief Register the given @a Feature to the server.
     * Required information such as the Fully Qualified Feature Identifier or the
     * Feature Definition is automatically detected from the Feature.
     * This call does not take ownership of the Feature. The Feature must exist
     * for the lifetime of the SiLA server.
     *
     * @param Feature The Feature to register
     *
     * @sa registerFeature(grpc::Service* Feature, const
     * CFullyQualifiedFeatureID& Identifier, const QByteArray& FeatureDefinition)
     */
    template<typename T>
    void registerFeature(CSiLAFeature<T>* Feature);

    /**
     * @brief Register the given @a Feature to the server.
     * This call does not take ownership of the Feature. The Feature must exist
     * for the lifetime of the SiLA server.
     *
     * @param Feature The Feature to register
     * @param Identifier The Fully Qualified Feature Identifier
     * @param FeatureDefinition The Feature Definition XML document
     */
    void registerFeature(grpc::Service* Feature,
                         const CFullyQualifiedFeatureID& Identifier,
                         const QByteArray& FeatureDefinition);

    /**
     * @brief Add a completion queue to the server that can be used in
     * asynchronous Features. Note that the server shares ownership of the queue
     * with the caller. However the server is responsible for shutting down and
     * draining the queue after the server itself was shut down via @a shutdown.
     *
     * @return std::shared_ptr<grpc::ServerCompletionQueue> The shared completion
     * queue
     */
    std::shared_ptr<grpc::ServerCompletionQueue> addCompletionQueue();

    /**
     * @brief Get the SSL credentials used for encrypting communication to clients
     *
     * @return The SSL credentials used by the server
     */
    [[nodiscard]] CSSLCredentials credentials() const;

    /**
     * @brief Run the SiLA2 server with encryption.
     *
     * @param Credentials Optional SSL certificate and key for secure
     * communication; if the credentials are empty the server tries to create a
     * self-signed certificate on the fly. If no credentials are provided and the
     * generation of a self-signed certificate fails this function throws a
     * @c std::runtime_error).
     * @param block Indicates whether the server should block execution until
     * Ctrl-C is pressed on the command line or if this function should
     * immediately return after it started the server. In case of the latter the
     * server has to be shut down by calling the @c shutdown() method.
     * Because this function internally uses @c QCoreApplication::exec it is
     * required that you call @c QCoreApplication::exec yourself when @a block is
     * set to @c false. Otherwise some internal signals can not be processed.
     *
     * @note You can also send @c SIGINT on Unix systems or @c CTRL_C_EVENT on
     * Windows to shutdown the server when @a block was set to true.
     *
     * @throws std::invalid_argument if CLI parsing fails
     * @throws std::runtime_error if credentials aren't provided by the user and
     * generating a self-signed certificate fails, as well, or if the server
     * cannot be started
     */
    void run(const CSSLCredentials& Credentials, bool block = true);
    void run(bool block = true);

    /**
     * @brief Run the SiLA2 server without encryption.
     *
     * @param block Indicates whether the server should block execution until
     * Ctrl-C is pressed on the command line or if this function should
     * immediately return after it started the server. In case of the latter the
     * server has to be shut down by calling the @c shutdown() method.
     * Because this function internally uses @c QCoreApplication::exec it is
     * required that you call @c QCoreApplication::exec yourself when @a block is
     * set to @c false. Otherwise some internal signals can not be processed.
     *
     * @note You can also send @c SIGINT on Unix systems or @c CTRL_C_EVENT on
     * Windows to shutdown the server when @a block was set to true.
     *
     * @throws std::invalid_argument if CLI parsing fails
     * @throws std::runtime_error if the server cannot be started
     */
    void runInsecure(bool block = true);

public slots:
    /**
     * @brief Shutdown the server
     */
    void shutdown();

signals:
    /**
     * @brief This signal is emitted when the server has been started. All
     * Features that have been registered before calling @a run() can now safely
     * start accepting Command Requests and Property reads.
     */
    void started() const;

protected:
    PrivateImplPtr d_ptr;

private:
    PIMPL_DECLARE_PRIVATE(CSiLAServer)
};

//============================================================================
template<typename T>
void CSiLAServer::registerFeature(CSiLAFeature<T>* Feature)
{
    registerFeature(Feature, Feature->fullyQualifiedIdentifier(),
                    Feature->featureDefinition());
}
}  // namespace SiLA2

#endif  // SILASERVER_H
