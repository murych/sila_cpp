/**
 ** This file is part of the sila_cpp project.
 ** Copyright 2020 SiLA2
 **
 ** Permission is hereby granted, free of charge, to any person obtaining a copy
 ** of this software and associated documentation files (the "Software"), to deal
 ** in the Software without restriction, including without limitation the rights
 ** to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 ** copies of the Software, and to permit persons to whom the Software is
 ** furnished to do so, subject to the following conditions:
 **
 ** The above copyright notice and this permission notice shall be included in all
 ** copies or substantial portions of the Software.
 **
 ** THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 ** IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 ** FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 ** AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 ** LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 ** OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 ** SOFTWARE.
 **/

//============================================================================
/// \file   ClientError.h
/// \author Florian Meinicke (florian.meinicke@cetoni.de)
/// \date   24.01.2020
/// \brief  Declaration of client related error handling methods
//============================================================================
#ifndef CLIENTERROR_H
#define CLIENTERROR_H

//============================================================================
//                                  INCLUDES
//============================================================================
#include <sila_cpp/global.h>

//=============================================================================
//                            FORWARD DECLARATIONS
//=============================================================================
namespace grpc
{
class Status;
}  // namespace grpc

namespace SiLA2
{
/**
 * @brief Check if the given @a Status indicates an error. If so, this function
 * will log the corresponding error message.
 *
 * @param Status The gRPC Status object returned from every gRPC call
 * @returns true if the @a Status indicates no error, false otherwise
 */
bool SILA_CPP_EXPORT hasError(const grpc::Status& Status);

/**
 * @brief Similarly to @a hasError(), this checks if the given @a Status indicates
 * an error. But if this is the case AND the error is a SiLA Error (i.e. not an
 * error from gRPC) not only will this error be logged the corresponding
 * @c CSiLAError will be thrown. Note that Connection Errors (every gRPC error
 * that does not have the status code @c CANCELLED) is considered a SiLA Error as
 * well.
 *
 * @param Status The gRPC Status object returned from every gRPC call
 *
 * @throws SiLA2::CSiLAError if @a Status indicates an error
 */
void SILA_CPP_EXPORT throwOnError(const grpc::Status& Status);

}  // namespace SiLA2

#endif  // CLIENTERROR_H
