/**
 ** This file is part of the sila_cpp project.
 ** Copyright 2020 SiLA2
 **
 ** Permission is hereby granted, free of charge, to any person obtaining a copy
 ** of this software and associated documentation files (the "Software"), to deal
 ** in the Software without restriction, including without limitation the rights
 ** to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 ** copies of the Software, and to permit persons to whom the Software is
 ** furnished to do so, subject to the following conditions:
 **
 ** The above copyright notice and this permission notice shall be included in all
 ** copies or substantial portions of the Software.
 **
 ** THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 ** IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 ** FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 ** AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 ** LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 ** OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 ** SOFTWARE.
 **/

//============================================================================
/// \file   SiLAError.h
/// \author Florian Meinicke (florian.meinicke@cetoni.de)
/// \date   27.01.2020
/// \brief  Declaration of the CSiLAError class
//============================================================================
#ifndef SILAERROR_H
#define SILAERROR_H

//============================================================================
//                                  INCLUDES
//============================================================================
#include <sila_cpp/global.h>

#include <QException>

#include <polymorphic_value.h>

#include <string>

//=============================================================================
//                            FORWARD DECLARATIONS
//=============================================================================
namespace grpc
{
class Status;
}  // namespace grpc

namespace SiLA2
{
/**
 * @brief The CSiLAError class is an abstract base class for all SiLA error types
 */
class SILA_CPP_EXPORT CSiLAError : public QException
{
protected:
    class PrivateImpl;
    using PrivateImplPtr = isocpp_p0201::polymorphic_value<PrivateImpl>;

public:
    /**
     * @brief The ErrorType enum defines all the different error types of SiLA 2
     */
    enum class ErrorType : uint8_t
    {
        DefinedExecutionError,
        UndefinedExecutionError,
        FrameworkError,
        ValidationError,
        ConnectionError,

        DEFINED_EXECUTION_ERROR
        [[deprecated("Use the CamelCase version instead")]] =
            DefinedExecutionError,
        UNDEFINED_EXECUTION_ERROR
        [[deprecated("Use the CamelCase version instead")]] =
            UndefinedExecutionError,
        FRAMEWORK_ERROR
        [[deprecated("Use the CamelCase version instead")]] = FrameworkError,
        VALIDATION_ERROR
        [[deprecated("Use the CamelCase version instead")]] = ValidationError,
        CONNECTION_ERROR
        [[deprecated("Use the CamelCase version instead")]] = ConnectionError,
    };
    using eErrorType
        [[deprecated("Use ErrorType (without the 'e' prefix) instead")]] =
            ErrorType;

    /**
     * @brief Get the error message of this error
     *
     * @return This error's message with details about the occurred error
     */
    [[nodiscard]] std::string message() const;

    /**
     * @brief Get the error type of this error
     *
     * @return This error's type
     */
    [[nodiscard]] ErrorType errorType() const;

    /**
     * @brief Get a human-readable representation of the error type of this error
     *
     * This is a convenience method and essentially the same as
     * @code
     * CSiLAError::errorTypeToString(SomeError.errorType());
     * @endcode
     *
     * @return This error's type's human-readable string representation
     */
    [[nodiscard]] std::string errorTypeName() const;

    /**
     * @brief Convert the given @c ErrorType @a Type to its human-readable string
     * representation
     *
     * @param Type The @c ErrorType to convert
     * @return The @a Type's human-readable string representation
     */
    [[nodiscard]] static std::string errorTypeToString(ErrorType Type);

    /**
     * @brief Converts the SiLA Error to a @c grpc::Status that can be sent along
     * with an RPC.
     *
     * @return grpc::Status The gRPC status that corresponds to the particular
     * SiLA error.
     */
    [[nodiscard]] virtual grpc::Status toStatus() const;

    /**
     * @override
     * @brief Get a string with explanatory information about the error.
     */
    [[nodiscard]] const char* what() const noexcept override;

protected:
    /**
     * @brief C'tor for derived classes
     *
     * @param priv Pointer to the derived private class
     */
    explicit CSiLAError(PrivateImplPtr priv);

    PrivateImplPtr d_ptr;

private:
    PIMPL_DECLARE_PRIVATE(CSiLAError)
};
}  // namespace SiLA2

#endif  // SILAERROR_H
