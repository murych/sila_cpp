/**
 ** This file is part of the sila_cpp project.
 ** Copyright 2020 SiLA2
 **
 ** Permission is hereby granted, free of charge, to any person obtaining a copy
 ** of this software and associated documentation files (the "Software"), to deal
 ** in the Software without restriction, including without limitation the rights
 ** to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 ** copies of the Software, and to permit persons to whom the Software is
 ** furnished to do so, subject to the following conditions:
 **
 ** The above copyright notice and this permission notice shall be included in all
 ** copies or substantial portions of the Software.
 **
 ** THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 ** IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 ** FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 ** AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 ** LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 ** OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 ** SOFTWARE.
 **/

//============================================================================
/// \file   ValidationError.h
/// \author Florian Meinicke (florian.meinicke@cetoni.de)
/// \date   27.01.2020
/// \brief  Declaration of the CValidationError class
//============================================================================
#ifndef VALIDATIONERROR_H
#define VALIDATIONERROR_H

//============================================================================
//                                  INCLUDES
//============================================================================
#include <sila_cpp/common/FullyQualifiedCommandID.h>
#include <sila_cpp/global.h>

#include "SiLAError.h"

#include <grpcpp/impl/codegen/status.h>

//============================================================================
//                            FORWARD DECLARATIONS
//============================================================================
namespace sila2::org::silastandard
{
class SiLAError;
}  // namespace sila2::org::silastandard

namespace SiLA2
{
/**
 * @brief The CValidationError class represents a SiLA 2 Validation Error.
 *
 * A Validation Error is an error that occurs during the validation of Parameters
 * before executing a Command.
 */
class SILA_CPP_EXPORT CValidationError : public CSiLAError
{
    class PrivateImpl;

public:
    /**
     * @brief C'tor
     *
     * @param ParameterID The parameter for which validation failed
     * @param Message The error message providing details about the occurred
     * error. If left empty, an extremely generic error message will be used.
     */
    explicit QT_DEPRECATED_X("Use CValidationError(const "
                             "CFullyQualifiedCommandParameterID&, std::string) "
                             "instead!")
        CValidationError(std::string ParameterID, std::string Message = "");
    explicit CValidationError(const CFullyQualifiedCommandParameterID& ParameterID,
                              std::string Message);

    /**
     * @brief Get the parameter for which validation failed
     *
     * @return The parameter that failed validation
     */
    [[nodiscard]] std::string parameter() const;

    /**
     * @brief Create a Validation Error from the given gRPC error message @a Error
     *
     * @param Error The gRPC error message that is serialized into a
     * @c grpc::Status object in case an error occurred during a RPC
     * @return A Validation Error where its fields have been deserialized from
     * @a Error
     */
    [[nodiscard]] static CValidationError fromErrorMessage(
        const sila2::org::silastandard::SiLAError& Error);

    /**
     * @override
     * @brief Get a string with explanatory information about the error.
     */
    [[nodiscard]] const char* what() const noexcept override;

    /**
     * @override
     *
     * @brief Throws this error
     */
    void raise() const override { throw *this; }

    /**
     * @override
     *
     * @brief Clones this error
     *
     * @return A copy of this error
     */
    [[nodiscard]] CValidationError* clone() const override
    {
        return new CValidationError{*this};
    }

private:
    PIMPL_DECLARE_PRIVATE(CValidationError)
};
}  // namespace SiLA2

#endif  // VALIDATIONERROR_H
