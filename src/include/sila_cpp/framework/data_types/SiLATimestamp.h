/**
 ** This file is part of the sila_cpp project.
 ** Copyright 2020 SiLA2
 **
 ** Permission is hereby granted, free of charge, to any person obtaining a copy
 ** of this software and associated documentation files (the "Software"), to deal
 ** in the Software without restriction, including without limitation the rights
 ** to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 ** copies of the Software, and to permit persons to whom the Software is
 ** furnished to do so, subject to the following conditions:
 **
 ** The above copyright notice and this permission notice shall be included in all
 ** copies or substantial portions of the Software.
 **
 ** THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 ** IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 ** FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 ** AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 ** LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 ** OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 ** SOFTWARE.
 **/

//============================================================================
/// \file   SiLATimestamp.h
/// \author Florian Meinicke (florian.meinicke@cetoni.de)
/// \date   17.01.2020
/// \brief  Declaration of the CTimestamp class
//============================================================================
#ifndef SILATIMESTAMP_H
#define SILATIMESTAMP_H

//============================================================================
//                                  INCLUDES
//============================================================================
#include <sila_cpp/framework/data_types/DataType.h>
#include <sila_cpp/framework/data_types/SiLATimezone.h>
#include <sila_cpp/framework/data_types/utils.h>
#include <sila_cpp/global.h>

#include <QDateTime>

#include <tuple>

//============================================================================
//                            FORWARD DECLARATIONS
//============================================================================
namespace sila2::org::silastandard
{
class Timestamp;
}

namespace SiLA2
{
/**
 * @brief The CInvalidTime class is used to indicate that a particular SiLA
 * Timestamp is invalid.
 *
 * @sa SiLA2::CTimestamp
 */
class SILA_CPP_EXPORT CInvalidTimestamp : public std::exception
{
public:
    /**
     * @brief The TimestampPart enum defines the different parts of a timestamp
     * that can be invalid
     */
    enum class TimestampPart : uint8_t
    {
        Hour,
        Minute,
        Second,
        Day,
        Month,
        Year,

        HOUR [[deprecated("Use the CamelCase version instead")]] = Hour,
        MINUTE [[deprecated("Use the CamelCase version instead")]] = Minute,
        SECOND [[deprecated("Use the CamelCase version instead")]] = Second,
        DAY [[deprecated("Use the CamelCase version instead")]] = Day,
        MONTH [[deprecated("Use the CamelCase version instead")]] = Month,
        YEAR [[deprecated("Use the CamelCase version instead")]] = Year,
    };

    /**
     * @brief Construct a @a CInvalidTimestamp for the time specified by @a Hour,
     * @a Minute, @a Second, qa Day, @a Month and @a Year
     */
    CInvalidTimestamp(uint32_t Hour, uint32_t Minute, uint32_t Second,
                      uint32_t Day, uint32_t Month, uint32_t Year);

    /**
     * @brief Construct a @a CInvalidTime for the part @a Part that has the
     * invalid value @a Value
     *
     * @param Value The invalid value
     * @param Part The part of the timestamp that is invalid
     */
    CInvalidTimestamp(uint32_t Value, TimestampPart Part);

    /**
     * @override
     * @brief Get a string with explanatory information about the error.
     */
    [[nodiscard]] const char* what() const noexcept override;

private:
    QString m_Message{};
};

/**
 * @brief The CTimestamp class provides a convenience wrapper around
 * SiLAFramework's Timestamp class.
 *
 * @details The <b>SiLA Timestamp Type</b> represents both a ISO 8601 time and
 * date (hours [0-23}, minutes [0-59], seconds [0-59], year [1-9999], month
 * [1-12], day [1-31], timezone [as an offset from UTC]).
 */
class SILA_CPP_EXPORT CTimestamp :
    public CDataType<std::tuple<uint32_t, uint32_t, uint32_t, uint32_t, uint32_t,
                                uint32_t, CTimezone>>
{
    using Base = CDataType<std::tuple<uint32_t, uint32_t, uint32_t, uint32_t,
                                      uint32_t, uint32_t, CTimezone>>;

public:
    /**
     * @brief C'tor
     *
     * @throws SiLA2::CInvalidTimestamp if the timestamp specified by @a Hour,
     * @a Minute, @a Second, @a Date, @a Month and @a Year is invalid.
     */
    // NOLINTNEXTLINE(google-explicit-constructor)
    CTimestamp(uint32_t Hour = 0, uint32_t Minute = 0, uint32_t Second = 0,
               uint32_t Day = 1, uint32_t Month = 1, uint32_t Year = 1970,
               const CTimezone& Timezone = {});

    SILA_CPP_CREATE_SPECIAL_MEMBER_FUNCTIONS(CTimestamp, Base)

    /**
     * @brief Converting copy c'tor from sila2::org::silastandard::Timestamp
     */
    // NOLINTNEXTLINE(google-explicit-constructor)
    CTimestamp(const sila2::org::silastandard::Timestamp& rhs);

    /**
     * @brief Construct a new @c CTimestamp from the given protobuf @c Message
     * @a from
     *
     * This is just for convenience if you happen to have a pointer or reference
     * to @c google::protobuf::Message and want to convert that to a @c CTimestamp
     * without having to cast it to a @c sila2::org::silastandard::Timestamp
     * before. The conversion will be performed for you here but might fail if you
     * pass a @c Message that is not a @c Timestamp.
     */
    [[nodiscard]] static CTimestamp fromProtoMessage(
        const google::protobuf::Message& from);

    /**
     * @brief Construct a new @c CTimestamp from the given @c QDateTime @a from
     *
     * @return A @c CTimestamp constructed from @a from or a default constructed
     * @c CTimestamp if @a from is invalid
     */
    [[nodiscard]] static CTimestamp fromQDateTime(const QDateTime& from);

    /**
     * @brief Convert this convenience type to a SiLAFramework type, i.e. the
     * Protobuf Message
     *
     * @return The SiLAFramework equivalent of this type as a Protobuf Message
     */
    [[nodiscard]] sila2::org::silastandard::Timestamp toProtoMessage() const;

    /**
     * @brief Convert this convenience type to a SiLAFramework type, i.e. the
     * Protobuf Message
     *
     * @return The SiLAFramework equivalent of this type as a Protobuf Message
     * pointer
     */
    [[nodiscard]] sila2::org::silastandard::Timestamp* toProtoMessagePtr() const;

    /**
     * @brief Get the hour
     *
     * @return The hour
     */
    [[nodiscard]] uint32_t hour() const;

    /**
     * @brief Set the hour
     *
     * @param Hour The new hour
     *
     * @throws SiAL2::CInvalidTimestamp if the resulting timestamp would be
     * invalid
     */
    void setHour(uint32_t Hour);

    /**
     * @brief Get the minute
     *
     * @return The minute
     */
    [[nodiscard]] uint32_t minute() const;

    /**
     * @brief Set the minute
     *
     * @param Minute The new minute
     *
     * @throws SiAL2::CInvalidTimestamp if the resulting timestamp would be
     * invalid
     */
    void setMinute(uint32_t Minute);

    /**
     * @brief Get the second
     *
     * @return The second
     */
    [[nodiscard]] uint32_t second() const;

    /**
     * @brief Set the second
     *
     * @param Second The new second
     *
     * @throws SiAL2::CInvalidTimestamp if the resulting timestamp would be
     * invalid
     */
    void setSecond(uint32_t Second);

    /**
     * @brief Get the day
     *
     * @return The day
     */
    [[nodiscard]] uint32_t day() const;

    /**
     * @brief Set the day
     *
     * @param Day The new day
     *
     * @throws SiAL2::CInvalidTimestamp if the resulting timestamp would be
     * invalid
     */
    void setDay(uint32_t Day);

    /**
     * @brief Get the month
     *
     * @return The month
     */
    [[nodiscard]] uint32_t month() const;

    /**
     * @brief Set the month
     *
     * @param Month The new month
     *
     * @throws SiAL2::CInvalidTimestamp if the resulting timestamp would be
     * invalid
     */
    void setMonth(uint32_t Month);

    /**
     * @brief Get the year
     *
     * @return The year
     */
    [[nodiscard]] uint32_t year() const;

    /**
     * @brief Set the year
     *
     * @param Year The new year
     *
     * @throws SiAL2::CInvalidTimestamp if the resulting timestamp would be
     * invalid
     */
    void setYear(uint32_t Year);

    /**
     * @brief Get the timezone
     *
     * @return The timezone
     */
    [[nodiscard]] CTimezone timezone() const;

    /**
     * @brief Set the timezone
     *
     * @param Timezone The new timezone
     */
    void setTimezone(const CTimezone& Timezone);

    /**
     * @brief Check if the timestamp given by @a Hour, @a Minute, @a Second, @a
     * Day, @a Month and @a Year is valid
     *
     * @return true, if the date is valid, false otherwise
     */
    static bool isValid(uint32_t Hour, uint32_t Minute, uint32_t Second,
                        uint32_t Day, uint32_t Month, uint32_t Year);

    /**
     * @brief Convert this time into a QDateTime
     *
     * @return The QDateTime representation of this time
     */
    [[nodiscard]] QDateTime toQDateTime() const;
};

/**
 * @brief Overload for debugging CTimestamps
 */
SILA_CPP_EXPORT QDebug operator<<(QDebug dbg, const CTimestamp& rhs);
SILA_CPP_EXPORT std::ostream& operator<<(std::ostream& os, const CTimestamp& rhs);
}  // namespace SiLA2

#endif  // SILATIMESTAMP_H
