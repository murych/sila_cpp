/**
 ** This file is part of the sila_cpp project.
 ** Copyright 2020 SiLA2
 **
 ** Permission is hereby granted, free of charge, to any person obtaining a copy
 ** of this software and associated documentation files (the "Software"), to deal
 ** in the Software without restriction, including without limitation the rights
 ** to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 ** copies of the Software, and to permit persons to whom the Software is
 ** furnished to do so, subject to the following conditions:
 **
 ** The above copyright notice and this permission notice shall be included in all
 ** copies or substantial portions of the Software.
 **
 ** THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 ** IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 ** FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 ** AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 ** LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 ** OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 ** SOFTWARE.
 **/

//============================================================================
/// \file   utils.h
/// \author Florian Meinicke (florian.meinicke@cetoni.de)
/// \date   10.07.2020
/// \brief  Declaration of helper macros for the SiLA Data Type classes
//============================================================================
#ifndef SILA_DATA_TYPES_UTILS_H
#define SILA_DATA_TYPES_UTILS_H

/**
 * @brief Helper macro to create all special member functions for @a Class that is
 * derived from @a Base
 */
#define SILA_CPP_CREATE_SPECIAL_MEMBER_FUNCTIONS(Class, Base)                    \
    /* NOLINTNEXTLINE(google-explicit-constructor) */                            \
    Class(const Base& rhs) : Base{rhs}                                           \
    {}                                                                           \
    /* NOLINTNEXTLINE(google-explicit-constructor) */                            \
    Class(Base&& rhs) : Base{rhs}                                                \
    {}                                                                           \
    using Base::operator=;                                                       \
    Class& operator=(const Base& rhs)                                            \
    {                                                                            \
        static_cast<Base>(*this) = rhs;                                          \
        return *this;                                                            \
    }                                                                            \
    Class& operator=(Base&& rhs)                                                 \
    {                                                                            \
        static_cast<Base>(*this) = rhs;                                          \
        return *this;                                                            \
    }

#endif  // SILA_DATA_TYPES_UTILS_H
