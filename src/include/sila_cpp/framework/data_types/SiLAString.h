/**
 ** This file is part of the sila_cpp project.
 ** Copyright 2020 SiLA2
 **
 ** Permission is hereby granted, free of charge, to any person obtaining a copy
 ** of this software and associated documentation files (the "Software"), to deal
 ** in the Software without restriction, including without limitation the rights
 ** to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 ** copies of the Software, and to permit persons to whom the Software is
 ** furnished to do so, subject to the following conditions:
 **
 ** The above copyright notice and this permission notice shall be included in all
 ** copies or substantial portions of the Software.
 **
 ** THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 ** IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 ** FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 ** AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 ** LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 ** OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 ** SOFTWARE.
 **/

//============================================================================
/// \file   SiLAString.h
/// \author Florian Meinicke (florian.meinicke@cetoni.de)
/// \date   17.01.2020
/// \brief  Declaration of the CString class
//============================================================================
#ifndef SILASTRING_H
#define SILASTRING_H

//============================================================================
//                                  INCLUDES
//============================================================================
#include <sila_cpp/framework/data_types/DataType.h>
#include <sila_cpp/framework/data_types/utils.h>
#include <sila_cpp/global.h>

#include <QStringView>

#include <string>

//============================================================================
//                            FORWARD DECLARATIONS
//============================================================================
namespace sila2::org::silastandard
{
class String;
}

namespace SiLA2
{
/**
 * @brief The CString class provides a convenience wrapper around SiLAFramework's
 * String class.
 *
 * @details The <b>SiLA String Type</b> represents plain text composed of maximum
 * 2 * 2^20 Unicode characters. Use the <i>SiLA Binary Type</i> for larger data.
 */
class SILA_CPP_EXPORT CString : public CDataType<std::string>
{
    using Base = CDataType<std::string>;

public:
    /**
     * @brief C'tor
     */
    // NOLINTNEXTLINE(google-explicit-constructor)
    CString(const std::string& Value = "");
    // NOLINTNEXTLINE(google-explicit-constructor)
    CString(QStringView Value);

    SILA_CPP_CREATE_SPECIAL_MEMBER_FUNCTIONS(CString, Base)

    /**
     * @brief Converting copy c'tor from sila2::org::silastandard::String
     */
    // NOLINTNEXTLINE(google-explicit-constructor)
    CString(const sila2::org::silastandard::String& rhs);

    /**
     * @brief Construct a new @c CString from the given protobuf @c Message
     * @a from
     *
     * This is just for convenience if you happen to have a pointer or reference
     * to @c google::protobuf::Message and want to convert that to a @c CString
     * without having to cast it to a @c sila2::org::silastandard::String before.
     * The conversion will be performed for you here but might fail if you pass a
     * @c Message that is not a @c String.
     */
    [[nodiscard]] static CString fromProtoMessage(
        const google::protobuf::Message& from);

    /**
     * @brief Append the string @a s to this string
     *
     * @param s The string to append
     *
     * @return This string with @a s appended to it
     */
    CString& append(const std::string& s);
    CString& append(QStringView s);
    CString& append(const sila2::org::silastandard::String& s);

    /**
     * @brief Convert this convenience type to a SiLAFramework type, i.e. the
     * Protobuf Message
     *
     * @return The SiLAFramework equivalent of this type as a Protobuf Message
     */
    [[nodiscard]] sila2::org::silastandard::String toProtoMessage() const;

    /**
     * @brief Convert this convenience type to a SiLAFramework type, i.e. the
     * Protobuf Message
     *
     * @return The SiLAFramework equivalent of this type as a Protobuf Message
     * pointer
     */
    [[nodiscard]] sila2::org::silastandard::String* toProtoMessagePtr() const;

    /**
     * @brief Convert this string into a QString
     *
     * @return The QString representation of this string
     */
    [[nodiscard]] QString toQString() const;
};

/**
 * @brief Overload for debugging CStrings
 */
SILA_CPP_EXPORT QDebug operator<<(QDebug dbg, const CString& rhs);
SILA_CPP_EXPORT std::ostream& operator<<(std::ostream& os, const CString& rhs);
}  // namespace SiLA2

#endif  // SILASTRING_H
