/**
 ** This file is part of the sila_cpp project.
 ** Copyright 2020 SiLA2
 **
 ** Permission is hereby granted, free of charge, to any person obtaining a copy
 ** of this software and associated documentation files (the "Software"), to deal
 ** in the Software without restriction, including without limitation the rights
 ** to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 ** copies of the Software, and to permit persons to whom the Software is
 ** furnished to do so, subject to the following conditions:
 **
 ** The above copyright notice and this permission notice shall be included in all
 ** copies or substantial portions of the Software.
 **
 ** THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 ** IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 ** FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 ** AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 ** LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 ** OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 ** SOFTWARE.
 **/

//============================================================================
/// \file   SiLAAnyType.h
/// \author Florian Meinicke (florian.meinicke@cetoni.de)
/// \date   17.01.2020
/// \brief  Declaration of the CAnyType class
//============================================================================
#ifndef SILAANYTYPE_H
#define SILAANYTYPE_H

//============================================================================
//                                  INCLUDES
//============================================================================
#include <sila_cpp/codegen/proto/DynamicMessageFactory.h>
#include <sila_cpp/common/logging.h>
#include <sila_cpp/framework/data_types/CommandExecutionUUID.h>
#include <sila_cpp/framework/data_types/DataType.h>
#include <sila_cpp/framework/data_types/DataTypeSerializer.h>
#include <sila_cpp/framework/data_types/ExecutionInfo.h>
#include <sila_cpp/framework/data_types/SiLABinary.h>
#include <sila_cpp/framework/data_types/SiLABoolean.h>
#include <sila_cpp/framework/data_types/SiLADate.h>
#include <sila_cpp/framework/data_types/SiLADuration.h>
#include <sila_cpp/framework/data_types/SiLAInteger.h>
#include <sila_cpp/framework/data_types/SiLAReal.h>
#include <sila_cpp/framework/data_types/SiLAString.h>
#include <sila_cpp/framework/data_types/SiLATime.h>
#include <sila_cpp/framework/data_types/SiLATimestamp.h>
#include <sila_cpp/framework/data_types/SiLATimezone.h>
#include <sila_cpp/framework/data_types/utils.h>
#include <sila_cpp/global.h>
#include <sila_cpp/internal/type_traits.h>

#include <google/protobuf/message.h>
#include <google/protobuf/reflection.h>

#include <algorithm>
#include <string>
#include <utility>

//============================================================================
//                            FORWARD DECLARATIONS
//============================================================================
namespace sila2::org::silastandard
{
class Any;
}

namespace SiLA2
{
/**
 * @brief The CAnyType class provides a convenience wrapper around SiLAFramework's
 * AnyType class.
 *
 * @details The <b>SiLA Any Type</b> represents information that can be of any
 * <i>SiLA Data Type</i>, except for a <i>Custom Data Type</i>. The value of a
 * <b>SiLA Any Type</b> contains both the information itself and the <i>SiLA
 * Data Type</i>.
 */
class SILA_CPP_EXPORT CAnyType :
    public CDataType<std::pair<std::string, std::string>>
{
    using Base = CDataType<std::pair<std::string, std::string>>;

public:
    /**
     * @brief Construct a new CAnyType object from the given @a Payload. The
     * payload must be the value of another SiLA Type.
     *
     * @tparam DataTypeT A SiLA Data Type
     * This type has to either be a wrapped SiLA Data Type (e.g. a
     * @c SiLA2::CString) or a SiLAFramework Type (e.g. a
     * @c sila2::org::silastandard::String as it can be obtained by
     * @c SiLA2::CString's @c toProtoMessage() method)
     * @param Payload The SiLA Data Type object containing the value to serialize
     * into the Any Type
     */
    template<typename DataTypeT,
             typename = std::enable_if_t<
                 internal::is_sila_type_v<DataTypeT>
                 || internal::is_sila_framework_type_v<DataTypeT>>>
    explicit CAnyType(const DataTypeT& Payload);

    /**
     * @brief Construct a new CAnyType object from the given @a Payload. The
     * payload must be a <b>fixed size</b> List of another SiLA Type.
     *
     * @tparam ContainerT The List type (e.g. @c std::array)
     * @tparam DataTypeT A SiLA Data Type
     * This type has to either be a wrapped SiLA Data Type (e.g. a
     * @c SiLA2::CString) or a SiLAFramework Type (e.g. a
     * @c sila2::org::silastandard::String as it can be obtained by
     * @c SiLA2::CString's @c toProtoMessage() method)
     * @tparam N The size of the List
     * @param Payload A List of the SiLA Data Type containing the values to
     * serialize into the Any Type
     *
     * @sa CAnyType(const ContainerT<DataTypeT>& Payload);
     */
    template<template<typename, auto> class ContainerT, typename DataTypeT, auto N,
             typename = std::enable_if_t<
                 internal::is_container_v<ContainerT<DataTypeT, N>>
                 && (internal::is_sila_type_v<DataTypeT>
                     || internal::is_sila_framework_type_v<DataTypeT>)>>
    explicit CAnyType(const ContainerT<DataTypeT, N>& Payload);

    /**
     * @brief Construct a new CAnyType object from the given @a Payload. The
     * payload must be a List of another SiLA Type.
     *
     * @tparam ContainerT The List type (e.g. @c std::vector, @c std::list, ...)
     * @tparam DataTypeT A SiLA Data Type
     * This type has to either be a wrapped SiLA Data Type (e.g. a
     * @c SiLA2::CString) or a SiLAFramework Type (e.g. a
     * @c sila2::org::silastandard::String as it can be obtained by
     * @c SiLA2::CString's @c toProtoMessage() method)
     * @param Payload A List of the SiLA Data Type containing the values to
     * serialize into the Any Type
     */
    template<template<typename> class ContainerT, typename DataTypeT,
             typename = std::enable_if_t<
                 internal::is_container_v<ContainerT<DataTypeT>>
                 && (internal::is_sila_type_v<DataTypeT>
                     || internal::is_sila_framework_type_v<DataTypeT>)>>
    explicit CAnyType(const ContainerT<DataTypeT>& Payload);

    /**
     * @brief Construct a new CAnyType object from the given CAnyType @a Payload.
     * This does not copy the @a Payload but instead wraps it into a new CAnyType.
     * Hence, with this c'tor you can create nested Any Types. If you want to
     * create a nested Any Type from a SiLA Framework Any Type (i.e.
     * @c sila2::org::silastandard::Any) you need to convert it into a
     * @c SiLA2::CAnyType first.
     *
     * @param Payload The Any Type object containing the value to serialize
     * into the Any Type
     */
    CAnyType(const CAnyType& Payload);

    /**
     * @brief Converting copy c'tor from sila2::org::silastandard::Any
     */
    // NOLINTNEXTLINE(google-explicit-constructor)
    CAnyType(const sila2::org::silastandard::Any& rhs);

    /**
     * @brief Construct a new @c CAnyType from the given protobuf @c Message
     * @a from
     *
     * This is just for convenience if you happen to have a pointer or reference
     * to @c google::protobuf::Message and want to convert that to a @c CAnyType
     * without having to cast it to a @c sila2::org::silastandard::Any before.
     * The conversion will be performed for you here but might fail if you pass a
     * @c Message that is not an @c Any.
     */
    [[nodiscard]] static CAnyType fromProtoMessage(
        const google::protobuf::Message& from);

    /**
     * @brief Convert this convenience type to a SiLAFramework type, i.e. the
     * Protobuf Message
     *
     * @return The SiLAFramework equivalent of this type as a Protobuf Message
     */
    [[nodiscard]] sila2::org::silastandard::Any toProtoMessage() const;

    /**
     * @brief Convert this convenience type to a SiLAFramework type, i.e. the
     * Protobuf Message
     *
     * @return The SiLAFramework equivalent of this type as a Protobuf Message
     * pointer
     */
    [[nodiscard]] sila2::org::silastandard::Any* toProtoMessagePtr() const;

    /**
     * @brief Get the @a type description of the actual Data Type contained in
     * this AnyType as an XML string
     * @return The XML string representing the type contained in this AnyType
     */
    [[nodiscard]] std::string type() const;

    /**
     * @brief Get the @a payload of this AnyType as a Protobuf message serialized
     * into a string
     * @return The payload as a serialized Protobuf message
     */
    [[nodiscard]] std::string payload() const;

    //=========================================================================
    //                  Convenient access to the contained type
    //=========================================================================
    /**
     * @brief Check if the type contained in this Any Type can be converted to the
     * @a DataTypeT type given as the template parameter
     *
     * @tparam DataTypeT The SiLA Type for which the conversion should be checked
     * @returns true if the DataType contained in @a this can be converted to the
     * @a DataTypeT type
     */
    template<typename DataTypeT,
             typename = std::enable_if_t<internal::is_sila_type_v<DataTypeT>>>
    [[nodiscard]] bool canConvert();

    /**
     * @brief Check if the type contained in this Any Type can be converted to the
     * List @a ContainerT type given as template parameters
     *
     * @note For fixed size arrays it is only checked if the types match and not
     * if @a this AnyType actually contains the number of elements of the given
     * fixed size array.
     *
     * @tparam ContainerT The List type (e.g. std::vector, std::list, ...) for
     * which conversion should be checked
     * @returns true if the DataType contained in @a this can be converted to the
     * List @a ContainerT type
     */
    template<typename ContainerT, typename = void,
             typename = std::enable_if_t<
                 internal::is_container_v<ContainerT>
                 && internal::is_sila_type_v<typename ContainerT::value_type>>>
    [[nodiscard]] bool canConvert();

    /**
     * @brief Convert this AnyType to the actual SiLA Type it contains. If this
     * Any Type does not contain the requested
     * @a DataTypeT this function returns a default constructed object of the
     * requested type.
     * You can check if your desired Data Type is contained in this Any Type by
     * calling e.g. @c CAnyType::canConvert<DataType>().
     *
     * @tparam DataTypeT The SiLA Type to convert to
     * @return DataTypeT The desired SiLA Type containing the value from the
     * @b payload field or a default constructed @a DataTypeT object
     *
     * @sa CAnyType::canConvert
     */
    template<typename DataTypeT,
             typename = std::enable_if_t<internal::is_sila_type_v<DataTypeT>>>
    [[nodiscard]] DataTypeT convert();

    /**
     * @brief Convert this AnyType to a List of the actual SiLA Type it contains.
     * If this Any Type does not contain a List of the requested
     * SiLA Data Type this function returns a default constructed List of the
     * requested type.
     * You can check if a List of your desired Data Type is contained in this Any
     * Type by calling @c CAnyType::canConvert<std::vector<DataType>>().
     *
     * @note If you're using a fixed size array you have to make sure that the
     * array is big enough to hold all elements of the List @a from contains. If
     * the array is too small this function will throw @c std::overflow_error. If
     * the array is too big the last elements will be default constructed.
     *
     * @tparam ContainerT The List type to convert to
     * @return ContainerT A List of the desired SiLA Type containing
     * the values from the @b payload field or a default constructed List
     *
     * @throws std::overflow_error if a given fixed sized array is too small
     *
     * @sa CAnyType::canConvert
     */
    template<typename ContainerT, typename = void,
             typename = std::enable_if_t<
                 internal::is_container_v<ContainerT>
                 && internal::is_sila_type_v<typename ContainerT::value_type>>>
    [[nodiscard]] ContainerT convert();

    /**
     * @brief Convenience functions to convert this AnyType into the actual SiLA
     * Data Type it contains. This is analogous to calling
     * @c CAnyType::convert<DataType>(), hence the same applies if the
     * corresponding Data Type is not contained in this Any Type.
     *
     * @return A SiLA Data Type containing the value from the @b payload field or
     * a default constructed object of the corresponding Data Type
     *
     * @sa CAnyType::canConvert
     * @sa CAnyType::convert
     */
    [[nodiscard]] CString toString();
    [[nodiscard]] CInteger toInteger();
    [[nodiscard]] CReal toReal();
    [[nodiscard]] CBoolean toBoolean();
    [[nodiscard]] CBinary toBinary();
    [[nodiscard]] CDate toDate();
    [[nodiscard]] CTime toTime();
    [[nodiscard]] CTimestamp toTimestamp();
    [[nodiscard]] CTimezone toTimezone();
    [[nodiscard]] CDuration toDuration();
    [[nodiscard]] CAnyType toAnyType();
    template<typename T>
    [[nodiscard]] std::vector<T> toVector();
    template<typename T>
    [[nodiscard]] std::list<T> toList();
    template<typename T, size_t N>
    [[nodiscard]] std::array<T, N> toArray();

    //=========================================================================
    //             Static convenient access to the contained type
    //=========================================================================
    /**
     * @brief Check if the type contained in the @a from parameter can be
     * converted to the @a DataTypeT type given as the template parameter
     *
     * @tparam DataTypeT The SiLA Type for which the conversion should be checked
     * @param from A SiLA Framework Any Type containing another SiLA Data Type
     * @returns true if the DataType contained in @a from can be converted to the
     * @a DataTypeT type
     */
    template<typename DataTypeT,
             typename = std::enable_if_t<internal::is_sila_type_v<DataTypeT>>>
    [[nodiscard]] static bool canConvert(
        const sila2::org::silastandard::Any& from);

    /**
     * @brief Check if the type contained in the @a from parameter can be
     * converted to the List @a ContainerT type given as the template parameter
     *
     * @note For fixed size arrays it is only checked if the types match and not
     * if the AnyType @a from actually contains the number of elements of the
     * given fixed size array.
     *
     * @tparam ContainerT The List type (e.g. std::vector, std::list, ...) for
     * which conversion should be checked
     * @param from A SiLA Framework Any Type containing a List of  another SiLA
     * Data Type
     * @returns true if the DataType contained in @a from can be converted to the
     * List @a ContainerT type
     */
    template<typename ContainerT, typename = void,
             typename = std::enable_if_t<
                 internal::is_container_v<ContainerT>
                 && internal::is_sila_type_v<typename ContainerT::value_type>>>
    [[nodiscard]] static bool canConvert(
        const sila2::org::silastandard::Any& from);

    /**
     * @brief Convert the AnyType given in the @a from parameter to the actual
     * SiLA Type that @a from contains. If @a from does not contain the requested
     * @a DataTypeT this function returns a default constructed object of the
     * requested type.
     * You can check if your desired Data Type is contained in @a from by calling
     * @c CAnyType::canConvert<DataType>(from).
     *
     * @tparam DataTypeT The SiLA Type to convert to
     * @param from A SiLA Framework Any Type containing the @b type and @b payload
     * information needed to construct the desired SiLA Type.
     * @return DataTypeT The desired SiLA Type containing the value from @a from's
     * @b payload field or a default constructed @a DataTypeT object
     *
     * @sa CAnyType::canConvert
     */
    template<typename DataTypeT,
             typename = std::enable_if_t<internal::is_sila_type_v<DataTypeT>>>
    [[nodiscard]] static DataTypeT convert(
        const sila2::org::silastandard::Any& from);

    /**
     * @brief Convert the AnyType given in the @a from parameter to a List of the
     * actual SiLA Type @a from contains. If @a from does not contain a List of
     * the requested Data Type this function returns a default constructed List of
     * the requested type.
     * You can check if a List of your desired Data Type is contained in
     * this Any Type by calling e.g. @c CAnyType::canConvert<ContainerT>(from).
     *
     * @note If you're using a fixed size array you have to make sure that the
     * array is big enough to hold all elements of the List @a from contains. If
     * the array is too small this function will throw @c std::bad_overflow_error.
     * If the array is too big the last elements will be default constructed.
     *
     * @tparam ContainerT The List type to convert to
     * @param from A SiLA Framework Any Type containing the @b type and @b payload
     * information needed to construct a List of the desired SiLA Type.
     * @return ContainerT A List of the desired SiLA Type containing the values
     * from the @b payload field or a default constructed List
     *
     * @throws std::overflow_error if a given fixed sized array is too small
     *
     * @sa CAnyType::canConvert
     */
    template<typename ContainerT, typename = void,
             typename = std::enable_if_t<
                 internal::is_container_v<ContainerT>
                 && internal::is_sila_type_v<typename ContainerT::value_type>>>
    [[nodiscard]] static ContainerT convert(
        const sila2::org::silastandard::Any& from);

    /**
     * @brief Convenience functions to convert the AnyType given in the @a from
     * parameter into the actual SiLA Data Type it contains. This is analogous to
     * calling @c CAnyType::convert<DataType>(from), hence the same applies if
     * @a from does not contain the corresponding Data Type.
     *
     * @param from A SiLA Framework Any Type containing the @b type and @b payload
     * information needed to construct the corresponding Data Type.
     * @return A SiLA Data Type containing the value from @a from's @b payload
     * field or a default constructed object of the corresponding Data Type
     *
     * @sa CAnyType::canConvert
     * @sa CAnyType::convert
     */
    [[nodiscard]] static CString toString(
        const sila2::org::silastandard::Any& from);
    [[nodiscard]] static CInteger toInteger(
        const sila2::org::silastandard::Any& from);
    [[nodiscard]] static CReal toReal(const sila2::org::silastandard::Any& from);
    [[nodiscard]] static CBoolean toBoolean(
        const sila2::org::silastandard::Any& from);
    [[nodiscard]] static CBinary toBinary(
        const sila2::org::silastandard::Any& from);
    [[nodiscard]] static CDate toDate(const sila2::org::silastandard::Any& from);
    [[nodiscard]] static CTime toTime(const sila2::org::silastandard::Any& from);
    [[nodiscard]] static CTimestamp toTimestamp(
        const sila2::org::silastandard::Any& from);
    [[nodiscard]] static CTimezone toTimezone(
        const sila2::org::silastandard::Any& from);
    [[nodiscard]] static CDuration toDuration(
        const sila2::org::silastandard::Any& from);
    [[nodiscard]] static CAnyType toAnyType(
        const sila2::org::silastandard::Any& from);
    template<typename T>
    [[nodiscard]] static std::vector<T> toVector(
        const sila2::org::silastandard::Any& from);
    template<typename T>
    [[nodiscard]] static std::list<T> toList(
        const sila2::org::silastandard::Any& from);
    template<typename T, size_t N>
    [[nodiscard]] static std::array<T, N> toArray(
        const sila2::org::silastandard::Any& from);

    SILA_CPP_CREATE_SPECIAL_MEMBER_FUNCTIONS(CAnyType, Base)

private:
    friend class CDataTypeSerializer;
    friend class CDynamicValue;

    /**
     * @internal
     * @brief Default c'tor for internal use (by @c CDataTypeSerializer and
     * @c CDynamicValue) only
     *
     * Initializes both fields with empty strings
     */
    CAnyType();

    /**
     * @brief Generate the dynamic protobuf @c Message for the Data Type that is
     * described in the given FDL description @a FDL
     *
     * @param FDL The FDL description of the SiLA Data Type for which to get the
     * dynamic protobuf @c Message
     * @return A unique_ptr to the dynamic @c Message generated from @a FDL
     */
    static std::unique_ptr<google::protobuf::Message> generateDynamicMessage(
        const QString& FDL);

    static codegen::proto::CDynamicMessageFactory m_DynamicMessageFactory;
};

//=============================================================================
template<typename DataTypeT, typename>
CAnyType::CAnyType(const DataTypeT& Payload)
{
    const auto XMLString = CDataTypeSerializer::serialize(Payload);
    std::string SerializedPayload;
    if constexpr (internal::is_sila_type_v<DataTypeT>)
    {
        SerializedPayload = Payload.toProtoMessage().SerializeAsString();
    }
    else
    {
        SerializedPayload = Payload.SerializeAsString();
    }

    setValue({qPrintable(XMLString), SerializedPayload});
}

//=============================================================================
template<template<typename, auto> class ContainerT, typename DataTypeT, auto N,
         typename>
CAnyType::CAnyType(const ContainerT<DataTypeT, N>& Payload)
    : CAnyType(std::vector<DataTypeT>(Payload.begin(), Payload.end()))
{}

//=============================================================================
template<template<typename> class ContainerT, typename DataTypeT, typename>
CAnyType::CAnyType(const ContainerT<DataTypeT>& Payload)
{
    const auto toProtoMessagePtr = [](const auto& Type) {
        if constexpr (internal::is_sila_type_v<DataTypeT>)
        {
            return Type.toProtoMessagePtr();
        }
        else
        {
            return new DataTypeT{Type};
        }
    };

    const auto XMLString = CDataTypeSerializer::serialize(Payload);
    auto DynamicMessage = generateDynamicMessage(XMLString);
    qCDebug(sila_cpp_data_types)
        << "DynamicMessage protobuf:\n"
        << DynamicMessage->GetDescriptor()->DebugString();
    auto Reflection = DynamicMessage->GetReflection();
    auto Field = DynamicMessage->GetDescriptor()->field(0);

    std::for_each(std::begin(Payload), std::end(Payload), [&](const auto& el) {
        Reflection->AddAllocatedMessage(DynamicMessage.get(), Field,
                                        toProtoMessagePtr(el));
    });
    qCDebug(sila_cpp_data_types)
        << "Constructed DynamicMessage" << *DynamicMessage;

    setValue({qPrintable(XMLString), DynamicMessage->SerializeAsString()});
}

//=============================================================================
template<typename DataTypeT, typename>
bool CAnyType::canConvert()
{
    return CAnyType::canConvert<DataTypeT>(toProtoMessage());
}

//=============================================================================
template<typename ContainerT, typename, typename>
bool CAnyType::canConvert()
{
    return CAnyType::canConvert<ContainerT>(toProtoMessage());
}

//=============================================================================
template<typename DataTypeT, typename>
DataTypeT CAnyType::convert()
{
    return CAnyType::convert<DataTypeT>(toProtoMessage());
}

//=============================================================================
template<typename ContainerT, typename, typename>
ContainerT CAnyType::convert()
{
    return CAnyType::convert<ContainerT>(toProtoMessage());
}

//=============================================================================
template<typename DataTypeT, typename>
bool CAnyType::canConvert(const sila2::org::silastandard::Any& from)
{
    const auto [DataTypeType, DataType] =
        CDataTypeSerializer::deserialize(QString::fromStdString(from.type()));
    return DataTypeType == codegen::fdl::IDataType::Type::Basic
           && std::holds_alternative<DataTypeT>(DataType);
}

//=============================================================================
template<typename ContainerT, typename, typename>
bool CAnyType::canConvert(const sila2::org::silastandard::Any& from)
{
    const auto [DataTypeType, DataType] =
        CDataTypeSerializer::deserialize(QString::fromStdString(from.type()));
    return DataTypeType == codegen::fdl::IDataType::Type::List
           && std::holds_alternative<typename ContainerT::value_type>(DataType);
}

//=============================================================================
template<typename DataTypeT, typename>
DataTypeT CAnyType::convert(const sila2::org::silastandard::Any& from)
{
    try
    {
        auto Type =
            std::get<DataTypeT>(std::get<1>(CDataTypeSerializer::deserialize(
                QString::fromStdString(from.type()))));

        auto ProtoMsg = Type.toProtoMessage();
        ProtoMsg.ParseFromString(from.payload());
        return DataTypeT{ProtoMsg};
    }
    catch (const std::bad_variant_access&)
    {
        return DataTypeT{};
    }
}

//=============================================================================
template<typename ContainerT, typename, typename>
ContainerT CAnyType::convert(const sila2::org::silastandard::Any& from)
{
    using DataTypeT = typename ContainerT::value_type;
    using FrameworkDataTypeT = decltype(DataTypeT{}.toProtoMessage());
    const auto XMLString = QString::fromStdString(from.type());
    try
    {
        // we need an empty Message of the correct type first, ...
        const auto DynamicMessage = generateDynamicMessage(XMLString);
        if (!DynamicMessage)
        {
            return ContainerT{};
        }
        qCDebug(sila_cpp_data_types)
            << "DynamicMessage protobuf:\n"
            << DynamicMessage->GetDescriptor()->DebugString();
        // ... which we can then fill with the serialized protobuf
        DynamicMessage->ParseFromString(from.payload());
        qCDebug(sila_cpp_data_types)
            << "Parsed DynamicMessage" << *DynamicMessage;

        // now access the List field ...
        const auto Reflection = DynamicMessage->GetReflection();
        const auto Field = DynamicMessage->GetDescriptor()->field(0);
        // ... to get the size of the List we're working with ...
        // NOTE: You might think we could use the `RepeatedFieldRef` directly and
        //  iterate over it since it has STL-like iterator support and all. But as
        //  it turns out for whatever reason this `RepeatedFieldRef` does not have
        //  the correct values of the List elements. Manually iterating over the
        //  List and accessing each Message separately with `GetRepeatedMessage`
        //  gives all Messages with their correct value(s). At least the size
        //  reported by `RepeatedFieldRef` is correct...
        const auto ProtoListSize =
            Reflection
                ->GetRepeatedFieldRef<FrameworkDataTypeT>(*DynamicMessage, Field)
                .size();
        // ... and the wrapped SiLA Type for all List values ...
        const auto Type = std::get<DataTypeT>(
            std::get<1>(CDataTypeSerializer::deserialize(XMLString)));
        // ... so that we can iterate over the List and get the values
        std::vector<DataTypeT> List;
        for (int i = 0; i < ProtoListSize; ++i)
        {
            auto ProtoMsg = Type.toProtoMessage();
            ProtoMsg.CopyFrom(
                Reflection->GetRepeatedMessage(*DynamicMessage, Field, i));
            List.emplace_back(ProtoMsg);
        }

        if constexpr (std::is_same_v<ContainerT, decltype(List)>)
        {
            // don't need to make another copy if we already have the correct type
            return List;
        }
        else if constexpr (std::is_aggregate_v<ContainerT>)
        {
            // std::array doesn't have any explicitly defined c'tors (i.e. it only
            // supports aggregate initialization)
            ContainerT Result;
            if (Result.size() < List.size())
            {
                throw std::overflow_error{
                    "The size of the destination container is smaller than the "
                    "List that is contained in the given SiLA Any Type!"};
            }
            std::move(std::begin(List), std::end(List), std::begin(Result));
            return Result;
        }
        else
        {
            return ContainerT(std::begin(List), std::end(List));
        }
    }
    catch (const std::bad_variant_access& e)
    {
        qCCritical(sila_cpp_data_types) << e.what();
        return ContainerT{};
    }
}

//============================================================================
template<typename T>
std::vector<T> CAnyType::toVector()
{
    return convert<std::vector<T>>();
}

//============================================================================
template<typename T>
std::list<T> CAnyType::toList()
{
    return convert<std::list<T>>();
}

//============================================================================
template<typename T, size_t N>
std::array<T, N> CAnyType::toArray()
{
    return convert<std::array<T, N>>();
}

//============================================================================
template<typename T>
std::vector<T> CAnyType::toVector(const sila2::org::silastandard::Any& from)
{
    return convert<std::vector<T>>(from);
}

//============================================================================
template<typename T>
std::list<T> CAnyType::toList(const sila2::org::silastandard::Any& from)
{
    return convert<std::list<T>>(from);
}

//============================================================================
template<typename T, size_t N>
std::array<T, N> CAnyType::toArray(const sila2::org::silastandard::Any& from)
{
    return convert<std::array<T, N>>(from);
}

/**
 * @brief Overload for debugging CAnyTypes
 */
SILA_CPP_EXPORT QDebug operator<<(QDebug dbg, const CAnyType& rhs);
SILA_CPP_EXPORT std::ostream& operator<<(std::ostream& os, const CAnyType& rhs);
}  // namespace SiLA2

#endif  // SILAANYTYPE_H
