/**
 ** This file is part of the sila_cpp project.
 ** Copyright (c) 2021 SiLA2
 **
 ** Permission is hereby granted, free of charge, to any person obtaining a copy
 ** of this software and associated documentation files (the "Software"), to deal
 ** in the Software without restriction, including without limitation the rights
 ** to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 ** copies of the Software, and to permit persons to whom the Software is
 ** furnished to do so, subject to the following conditions:
 **
 ** The above copyright notice and this permission notice shall be included in all
 ** copies or substantial portions of the Software.
 **
 ** THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 ** IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 ** FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 ** AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 ** LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 ** OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 ** SOFTWARE.
 **/

//============================================================================
/// \file   SingleValueConstraint.h
/// \author Florian Meinicke (florian.meinicke@cetoni.de)
/// \date   29.01.2021
/// \brief  Declaration of the CSingleValueConstraint,
/// CConstraintSingleValueMinimal and CConstraintSingleValueMaximal classes
//============================================================================
#ifndef CODEGEN_SINGLEVALULECONSTRAINT_H
#define CODEGEN_SINGLEVALULECONSTRAINT_H

//============================================================================
//                                  INCLUDES
//============================================================================
#include <sila_cpp/codegen/fdl/IConstraint.h>
#include <sila_cpp/common/logging.h>
#include <sila_cpp/global.h>

namespace SiLA2::codegen::fdl
{
/**
 * @brief The CSingleValueConstraint class represents a SiLA 2 Constraint that
 * only has a single value
 *
 * @tparam T The type of the Constraint value
 */
template<typename T>
class SILA_CPP_EXPORT CSingleValueConstraint : public IConstraint
{
public:
    /**
     * @brief Convert this Constraint to its @c QVariant representation
     *
     * @return A QVariant instance for this Constraint
     */
    [[nodiscard]] QVariant toVariant() const override { return m_Value; }

    /**
     * @brief Initializes this Constraint with the values given in the @c QVariant
     * @a from
     *
     * @param from The QVariant instance that represents a Constraint
     */
    void fromVariant(const QVariant& from) override { m_Value = from.value<T>(); }

    /**
     * @brief Get the value of this Constraint
     *
     * @return The Constraint's value
     */
    [[nodiscard]] T value() const { return m_Value; }

protected:
    /**
     * @brief Helper function for the equality comparison operators
     *
     * @param r The Constraint to compare this Constraint to
     *
     * @return @c true, if this Constraint is semantically equal to @a rhs, i.e
     * if @b Value matches, @c false otherwise
     */
    [[nodiscard]] bool isEqual(const ISerializable& r) const override
    {
        auto rhs = dynamic_cast<const CSingleValueConstraint<T>*>(&r);
        return m_Value == rhs->m_Value;
    }

    T m_Value{};
};

/**
 * @brief Overload for debugging CSingleValueConstraints
 */
template<typename T>
QDebug operator<<(QDebug dbg, const CSingleValueConstraint<T>& rhs)
{
    return dbg << rhs.value();
}

template<typename T>
std::ostream& operator<<(std::ostream& os, const CSingleValueConstraint<T>& rhs)
{
    return os << rhs.value();
}
}  // namespace SiLA2::codegen::fdl

#endif  // CODEGEN_SINGLEVALULECONSTRAINT_H
