/**
 ** This file is part of the sila_cpp project.
 ** Copyright 2023 SiLA2
 **
 ** Permission is hereby granted, free of charge, to any person obtaining a copy
 ** of this software and associated documentation files (the "Software"), to deal
 ** in the Software without restriction, including without limitation the rights
 ** to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 ** copies of the Software, and to permit persons to whom the Software is
 ** furnished to do so, subject to the following conditions:
 **
 ** The above copyright notice and this permission notice shall be included in all
 ** copies or substantial portions of the Software.
 **
 ** THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 ** IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 ** FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 ** AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 ** LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 ** OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 ** SOFTWARE.
 **/

//============================================================================
/// \file   utils.h
/// \author Florian Meinicke (florian.meinicke@cetoni.de)
/// \date   27.11.2023
/// \brief  Definition of utility types and functions for FDL serialization
//============================================================================
#ifndef CODEGEN_UTILS_H
#define CODEGEN_UTILS_H

//============================================================================
//                                  INCLUDES
//============================================================================
#include <sila_cpp/global.h>

#include <QMultiMap>
#include <QVariant>

//============================================================================
//                            USING DECLARATIONS
//============================================================================
using QVariantMultiMap = QMultiMap<QString, QVariant>;
using QVariantMultiMapElementType =
    std::pair<QVariantMultiMap::key_type, QVariantMultiMap::mapped_type>;

// Make `QVariantMultiMap` known to Qt's meta-object system to be able to use it
// in `QVariant`.
Q_DECLARE_METATYPE(QVariantMultiMap);

namespace SiLA2::codegen::fdl
{
/**
 * @brief Returns a @c QVariant which contains a @c QVariantMultiMap constructed
 * from the given @a List
 *
 * @param List Initializer list to pass to the @c QVariantMultiMap constructor
 * @return A @c QVariant containing the constructed @c QVariantMultiMap
 */
SILA_CPP_EXPORT QVariant
variantMultiMap(std::initializer_list<QVariantMultiMapElementType>&& List);
}  // namespace SiLA2::codegen::fdl

#endif  // CODEGEN_UTILS_H
