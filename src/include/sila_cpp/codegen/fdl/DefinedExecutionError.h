/**
 ** This file is part of the sila_cpp project.
 ** Copyright (c) 2021 SiLA2
 **
 ** Permission is hereby granted, free of charge, to any person obtaining a copy
 ** of this software and associated documentation files (the "Software"), to deal
 ** in the Software without restriction, including without limitation the rights
 ** to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 ** copies of the Software, and to permit persons to whom the Software is
 ** furnished to do so, subject to the following conditions:
 **
 ** The above copyright notice and this permission notice shall be included in all
 ** copies or substantial portions of the Software.
 **
 ** THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 ** IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 ** FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 ** AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 ** LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 ** OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 ** SOFTWARE.
 **/

//============================================================================
/// \file   DefinedExecutionError.h
/// \author Florian Meinicke (florian.meinicke@cetoni.de)
/// \date   02.02.2021
/// \brief  Declaration of the CDefinedExecutionError class
//============================================================================
#ifndef CODEGEN_DEFINEDEXECUTIONERROR_H
#define CODEGEN_DEFINEDEXECUTIONERROR_H

//============================================================================
//                                  INCLUDES
//============================================================================
#include <sila_cpp/codegen/fdl/SiLAElement.h>
#include <sila_cpp/global.h>

namespace SiLA2::codegen::fdl
{
/**
 * @brief The CDefinedExecutionError class represents a SiLA 2
 * DefinedExecutionError Definition that was defined in a FDL file
 */
class SILA_CPP_EXPORT CDefinedExecutionError final : public CSiLAElement
{
public:
    /**
     * @brief Get the name of this object
     *
     * @return The object's name
     */
    [[nodiscard]] QString name() const override
    {
        return "DefinedExecutionError";
    }
};

/**
 * @brief Overload for debugging CProperties
 */
SILA_CPP_EXPORT QDebug operator<<(QDebug dbg, const CDefinedExecutionError& rhs);
SILA_CPP_EXPORT std::ostream& operator<<(std::ostream& os,
                                         const CDefinedExecutionError& rhs);
}  // namespace SiLA2::codegen::fdl

#endif  // CODEGEN_DEFINEDEXECUTIONERROR_H
