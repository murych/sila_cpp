/**
 ** This file is part of the sila_cpp project.
 ** Copyright (c) 2021 SiLA2
 **
 ** Permission is hereby granted, free of charge, to any person obtaining a copy
 ** of this software and associated documentation files (the "Software"), to deal
 ** in the Software without restriction, including without limitation the rights
 ** to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 ** copies of the Software, and to permit persons to whom the Software is
 ** furnished to do so, subject to the following conditions:
 **
 ** The above copyright notice and this permission notice shall be included in all
 ** copies or substantial portions of the Software.
 **
 ** THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 ** IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 ** FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 ** AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 ** LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 ** OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 ** SOFTWARE.
 **/

//============================================================================
/// \file   ConstraintSchema.h
/// \author Florian Meinicke (florian.meinicke@cetoni.de)
/// \date   02.02.2021
/// \brief  Declaration of the CConstraintSchema class
//============================================================================
#ifndef CODEGEN_CONSTRAINTSCHEMA_H
#define CODEGEN_CONSTRAINTSCHEMA_H

//============================================================================
//                                  INCLUDES
//============================================================================
#include <sila_cpp/codegen/fdl/DataType.h>
#include <sila_cpp/codegen/fdl/IConstraint.h>
#include <sila_cpp/global.h>

namespace SiLA2::codegen::fdl
{
/**
 * @brief The CConstraintSchema class represents a SiLA 2 Schema Constraint
 */
class SILA_CPP_EXPORT CConstraintSchema final : public IConstraint
{
public:
    /**
     * @brief The SchemaType enum defines all possible types of data that can be
     * constrained with a schema
     */
    enum class SchemaType
    {
        Invalid,
        Xml,
        Json,
    };

    /**
     * @brief The SchemaLocation enum defines the possible locations of a schema
     * definition
     */
    enum class SchemaLocation
    {
        Invalid,
        Url,
        Inline
    };

    /**
     * @brief Get the name of this object
     *
     * @return The object's name
     */
    [[nodiscard]] QString name() const override { return "Schema"; }

    /**
     * @brief Convert this Schema Constraint to its @c QVariant representation
     *
     * @return A QVariant instance for this Schema Constraint
     */
    [[nodiscard]] QVariant toVariant() const override;

    /**
     * @brief Initializes this Schema Constraint with the values given in the
     * @c QVariant @a from
     *
     * @param from The QVariant instance that represents a Schema Constraint
     *
     * @throws std::runtime_error if the initialization fails
     */
    void fromVariant(const QVariant& from) override;

    /**
     * @brief Convert the given string @a String into the corresponding SchemaType
     *
     * @return The SchemaType corresponding to the given string
     */
    static SchemaType stringToSchemaType(const QString& String);

    /**
     * @brief Convert the given SchemaType @a Type into its String representation
     *
     * @return The string representation of the given SchemaType
     */
    static QString schemaTypeToString(SchemaType Type);

    /**
     * @brief Get the Schema Type of this Schema Constraint
     *
     * @return The Schema Constraint's Schema Type
     */
    [[nodiscard]] SchemaType schemaType() const { return m_SchemaType; }

    /**
     * @brief Check if this Schema Constraint holds a URL
     *
     * @returns true, if the Schema Constraint holds a URL, false otherwise
     */
    [[nodiscard]] bool isURL() const
    {
        return m_SchemaLocation == SchemaLocation::Url;
    }

    /**
     * @brief Check if this Schema Constraint holds an inline Schema
     *
     * @returns true, if the Schema Constraint holds an inline Schema, false
     * otherwise
     */
    [[nodiscard]] bool isInline() const
    {
        return m_SchemaLocation == SchemaLocation::Inline;
    }

    /**
     * @brief Get the URL to the Schema of this Schema Constraint but only if the
     * Constraint holds a URL
     *
     * @return The Schema Constraint's URL to the Schema
     */
    [[nodiscard]] QString url() const { return isURL() ? m_Schema : ""; }

    /**
     * @brief Get the inline Schema of this Schema Constraint but only if the
     * Constraint holds an inline Schema
     *
     * @return The Schema Constraint's inline Schema
     */
    [[nodiscard]] QString content() const { return isInline() ? m_Schema : ""; }

protected:
    /**
     * @brief Helper function for the equality comparison operators
     *
     * @param rhs The Constraint to compare this Constraint to
     *
     * @return @c true, if this Constraint is semantically equal to @a rhs, i.e.
     * if @b SchemaType, @b SchemaLocation, and @b Schema match, @c false
     * otherwise
     */
    [[nodiscard]] bool isEqual(const ISerializable& rhs) const override;

private:
    SchemaType m_SchemaType{SchemaType::Invalid};
    SchemaLocation m_SchemaLocation{SchemaLocation::Invalid};
    // TODO[FM]: maybe have a URL schema fetched automatically?
    QString m_Schema;  ///< depending on @a m_SchemaLocation either a URL to the
                       ///< schema or the actual schema
};

/**
 * @brief Overload for debugging CConstraintSchema
 */
SILA_CPP_EXPORT QDebug operator<<(QDebug dbg, const CConstraintSchema& rhs);

SILA_CPP_EXPORT std::ostream& operator<<(std::ostream& os,
                                         const CConstraintSchema& rhs);
}  // namespace SiLA2::codegen::fdl
#endif  // CODEGEN_CONSTRAINTSCHEMA_H
