/**
 ** This file is part of the sila_cpp project.
 ** Copyright (c) 2021 SiLA2
 **
 ** Permission is hereby granted, free of charge, to any person obtaining a copy
 ** of this software and associated documentation files (the "Software"), to deal
 ** in the Software without restriction, including without limitation the rights
 ** to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 ** copies of the Software, and to permit persons to whom the Software is
 ** furnished to do so, subject to the following conditions:
 **
 ** The above copyright notice and this permission notice shall be included in all
 ** copies or substantial portions of the Software.
 **
 ** THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 ** IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 ** FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 ** AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 ** LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 ** OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 ** SOFTWARE.
 **/

//============================================================================
/// \file   FDLSerializer.h
/// \author Florian Meinicke (florian.meinicke@cetoni.de)
/// \date   26.01.2021
/// \brief  Declaration of the CFDLSerializer class
//============================================================================
#ifndef CODEGEN_FDLSERIALIZER_H
#define CODEGEN_FDLSERIALIZER_H

//============================================================================
//                                  INCLUDES
//============================================================================
#include <sila_cpp/codegen/fdl/ISerializable.h>
#include <sila_cpp/global.h>

#include <QString>

namespace SiLA2
{
class CFullyQualifiedFeatureID;
}  // namespace SiLA2

namespace SiLA2::codegen::fdl
{
/**
 * @brief The CFDLSerializer class is responsible for de-/serializing SiLA Feature
 * Definitions from/to the FDL (Feature Definition Language) format
 */
class SILA_CPP_EXPORT CFDLSerializer
{
public:
    /**
     * @brief Serialize the given serializable @a Element into the FDL format
     *
     * @param Element The serializable element to serialize
     * @return The FDL representation of the @a Element as a string
     */
    static QString serialize(const ISerializable& Element);

    /**
     * @brief Serialize the given @a ElementVariant named @a ElementName into the
     * FDL format
     *
     * @param ElementName The name of the element to serialize (will be the top
     * level XML tag)
     * @param ElementVariant The element to serialize already as a @c QVariant
     * @return The FDL representation of the @a ElementVariant as a string
     */
    static QString serialize(const QString& ElementName,
                             const QVariant& ElementVariant);

    /**
     * @brief Deserialize the given FDL string @a FDL into the given serializable
     * @a Element
     *
     * @param Element The serializable element to deserialize into
     * @param FDL The FDL string to deserialize
     *
     * @returns @c true, if deserialization was successful, @c false otherwise
     */
    static bool deserialize(ISerializable& Element, const QString& FDL);

    /**
     * @brief Validate the given FDL string @a FDL for the Feature identified by
     * @a FeatureID
     *
     * @param FeatureID The Feature Identifier of the FDL to validate
     * @param FDL The FDL string to validate
     *
     * @returns @c true, if validation was successful, @c false otherwise
     */
    static bool validate(const CFullyQualifiedFeatureID& FeatureID,
                         const QString& FDL);
};
}  // namespace SiLA2::codegen::fdl
#endif  // CODEGEN_FDLSERIALIZER_H
