/**
 ** This file is part of the sila_cpp project.
 ** Copyright (c) 2021 SiLA2
 **
 ** Permission is hereby granted, free of charge, to any person obtaining a copy
 ** of this software and associated documentation files (the "Software"), to deal
 ** in the Software without restriction, including without limitation the rights
 ** to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 ** copies of the Software, and to permit persons to whom the Software is
 ** furnished to do so, subject to the following conditions:
 **
 ** The above copyright notice and this permission notice shall be included in all
 ** copies or substantial portions of the Software.
 **
 ** THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 ** IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 ** FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 ** AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 ** LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 ** OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 ** SOFTWARE.
 **/

//============================================================================
/// \file   DataType.h
/// \author Florian Meinicke (florian.meinicke@cetoni.de)
/// \date   27.01.2021
/// \brief  Declaration of the CDataType class
//============================================================================
#ifndef CODEGEN_DATATYPE_H
#define CODEGEN_DATATYPE_H

//============================================================================
//                                  INCLUDES
//============================================================================
#include <sila_cpp/codegen/fdl/IDataType.h>
#include <sila_cpp/global.h>

namespace SiLA2::codegen::fdl
{
//============================================================================
//                            FORWARD DECLARATIONS
//============================================================================
class CDataTypeBasic;
class CDataTypeConstrained;
class CDataTypeIdentifier;
class CDataTypeList;
class CDataTypeStructure;

/**
 * @brief The CDataType class represents a compound of another SiLA 2 Data Type to
 * provide a uniform interface to classes derived from @c CSiLAElement
 */
class SILA_CPP_EXPORT CDataType : public IDataType
{
public:
    /**
     * @brief Get the name of this object
     *
     * @return The object's name
     */
    [[nodiscard]] QString name() const override { return "DataType"; }

    /**
     * @brief Convert this Data Type to its @c QVariant representation
     *
     * @return A QVariant instance for this Data Type
     */
    [[nodiscard]] QVariant toVariant() const override;

    /**
     * @brief Initializes this Data Type with the values given in the @c QVariant
     * @a from
     *
     * @param from The QVariant instance that represents a Data Type
     *
     * @throws std::runtime_error if the initialization fails
     */
    void fromVariant(const QVariant& from) override;

    /**
     * @brief Get the actual Data Type
     *
     * @return The actual Data Type
     */
    [[nodiscard]] IDataType* dataType() const { return m_DataType; }

    /**
     * @brief Get the Basic Data Type that this Data Type contains
     *
     * @throws std::bad_cast if the actual Data Type is not a Basic Data Type
     *
     * @return The actual Basic Data Type
     */
    [[nodiscard]] CDataTypeBasic basic() const;

    /**
     * @brief Get the Constrained Data Type that this Data Type contains
     *
     * @throws std::bad_cast if the actual Data Type is not a Constrained Data
     * Type
     *
     * @return The actual Constrained Data Type
     */
    [[nodiscard]] CDataTypeConstrained constrained() const;

    /**
     * @brief Get the Data Type Identifier that this Data Type contains
     *
     * @throws std::bad_cast if the actual Data Type is not a Data Type Identifier
     *
     * @return The actual Data Type Identifier
     */
    [[nodiscard]] CDataTypeIdentifier identifier() const;

    /**
     * @brief Get the Data Type List that this Data Type contains
     *
     * @throws std::bad_cast if the actual Data Type is not a Data Type List
     *
     * @return The actual List Data Type
     */
    [[nodiscard]] CDataTypeList list() const;

    /**
     * @brief Get the Data Type Structure that this Data Type contains
     *
     * @throws std::bad_cast if the actual Data Type is not a Data Type Structure
     *
     * @return The actual Structure Data Type
     */
    [[nodiscard]] CDataTypeStructure structure() const;

protected:
    /**
     * @brief Helper function for the equality comparison operators
     *
     * @param rhs The Data Type to compare this Data Type to
     *
     * @return @c true, if this Data Type is semantically equal to @a rhs,
     * @c false otherwise
     *
     * @sa IDataType::isEqual(const ISerializable&)
     */
    [[nodiscard]] bool isEqual(const ISerializable& rhs) const override;

private:
    /**
     * @brief Returns @b DataType if it's not @c nullptr, throws otherwise
     *
     * @note @b DataType should actually never be @c nullptr, this function is
     *   just here to get rid of "nullptr dereference" warnings
     *
     * @return @b DataType, if not @c nullptr
     *
     * @throws std::runtime_error if @b DataType is @c nullptr
     */
    [[nodiscard]] IDataType* dataTypeSafe() const;

    IDataType* m_DataType{nullptr};
};

/**
 * @brief Overload for debugging CDataTypes
 */
SILA_CPP_EXPORT QDebug operator<<(QDebug dbg, const CDataType& rhs);
SILA_CPP_EXPORT std::ostream& operator<<(std::ostream& os, const CDataType& rhs);
}  // namespace SiLA2::codegen::fdl

#endif  // CODEGEN_DATATYPE_H
