/**
 ** This file is part of the sila_cpp project.
 ** Copyright (c) 2021 SiLA2
 **
 ** Permission is hereby granted, free of charge, to any person obtaining a copy
 ** of this software and associated documentation files (the "Software"), to deal
 ** in the Software without restriction, including without limitation the rights
 ** to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 ** copies of the Software, and to permit persons to whom the Software is
 ** furnished to do so, subject to the following conditions:
 **
 ** The above copyright notice and this permission notice shall be included in all
 ** copies or substantial portions of the Software.
 **
 ** THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 ** IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 ** FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 ** AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 ** LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 ** OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 ** SOFTWARE.
 **/

//============================================================================
/// \file   ConstraintAllowedTypes.h
/// \author Florian Meinicke (florian.meinicke@cetoni.de)
/// \date   01.02.2021
/// \brief  Declaration of the CConstraintAllowedTypes class
//============================================================================
#ifndef CODEGEN_CONSTRAINTALLOWEDTYPES_H
#define CODEGEN_CONSTRAINTALLOWEDTYPES_H

//============================================================================
//                                  INCLUDES
//============================================================================
#include <sila_cpp/codegen/fdl/DataType.h>
#include <sila_cpp/codegen/fdl/IConstraint.h>
#include <sila_cpp/global.h>

namespace SiLA2::codegen::fdl
{
/**
 * @brief The CConstraintAllowedTypes class represents a SiLA 2 AllowedTypes
 * Constraint
 */
class SILA_CPP_EXPORT CConstraintAllowedTypes final : public IConstraint
{
public:
    /**
     * @brief Get the name of this object
     *
     * @return The object's name
     */
    [[nodiscard]] QString name() const override { return "AllowedTypes"; }

    /**
     * @brief Convert this AllowedTypes Constraint to its @c QVariant
     * representation
     *
     * @return A QVariant instance for this AllowedTypes Constraint
     */
    [[nodiscard]] QVariant toVariant() const override;

    /**
     * @brief Initializes this AllowedTypes Constraint with the values given in
     * the @c QVariant @a from
     *
     * @param from The QVariant instance that represents a AllowedTypes Constraint
     *
     * @throws std::runtime_error if the initialization fails
     */
    void fromVariant(const QVariant& from) override;

    /**
     * @brief Get the Data Types of this AllowedTypes Constraint
     *
     * @return The AllowedTypes Constraint's Data Types
     */
    [[nodiscard]] QList<CDataType> types() const { return m_Types; }

    /**
     * @brief Get the <i>i</i>th Data Type of this AllowedTypes Constraint
     *
     * @param i The index of the value to return
     * @return The AllowedTypes Constraint's <i>i</i>th Data Type
     */
    [[nodiscard]] const CDataType& type(int i) const { return m_Types.at(i); }

protected:
    /**
     * @brief Helper function for the equality comparison operators
     *
     * @param rhs The Constraint to compare this Constraint to
     *
     * @return @c true, if this Constraint is semantically equal to @a rhs, i.e.
     * if @b Types match, @c false otherwise
     */
    [[nodiscard]] bool isEqual(const ISerializable& rhs) const override;

private:
    QList<CDataType> m_Types;
};

/**
 * @brief Overload for debugging CConstraintAllowedTypes
 */
SILA_CPP_EXPORT QDebug operator<<(QDebug dbg, const CConstraintAllowedTypes& rhs);

SILA_CPP_EXPORT std::ostream& operator<<(std::ostream& os,
                                         const CConstraintAllowedTypes& rhs);
}  // namespace SiLA2::codegen::fdl
#endif  // CODEGEN_CONSTRAINTALLOWEDTYPES_H
