/**
 ** This file is part of the sila_cpp project.
 ** Copyright 2021 SiLA2
 **
 ** Permission is hereby granted, free of charge, to any person obtaining a copy
 ** of this software and associated documentation files (the "Software"), to deal
 ** in the Software without restriction, including without limitation the rights
 ** to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 ** copies of the Software, and to permit persons to whom the Software is
 ** furnished to do so, subject to the following conditions:
 **
 ** The above copyright notice and this permission notice shall be included in all
 ** copies or substantial portions of the Software.
 **
 ** THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 ** IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 ** FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 ** AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 ** LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 ** OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 ** SOFTWARE.
 **/

//============================================================================
/// \file   DefinedExecutionErrorIdentifier.h
/// \author Florian Meinicke (florian.meinicke@cetoni.de)
/// \date   04.02.2021
/// \brief  Declaration of the CDefinedExecutionErrorIdentifier class
//============================================================================
#ifndef CODEGEN_DEFINEDEXECUTIONERRORIDENTIFIER_H
#define CODEGEN_DEFINEDEXECUTIONERRORIDENTIFIER_H

//============================================================================
//                                  INCLUDES
//============================================================================
#include <sila_cpp/codegen/fdl/ISerializable.h>
#include <sila_cpp/global.h>

namespace SiLA2::codegen::fdl
{
/**
 * @brief The CDefinedExecutionErrorIdentifier class represents a Defined
 * Execution Error of a SiLA 2 Feature that is identified by its @a Identifier
 */
class SILA_CPP_EXPORT CDefinedExecutionErrorIdentifier final :
    public ISerializable
{
public:
    /**
     * @brief Get the name of this object
     *
     * @return The object's name
     */
    [[nodiscard]] QString name() const override { return "Identifier"; }

    /**
     * @brief Convert this Data Type to its @c QVariant representation
     *
     * @return A QVariant instance for this Data Type
     */
    [[nodiscard]] QVariant toVariant() const override;

    /**
     * @brief Initializes this Data Type with the values given in the @c QVariant
     * @a from
     *
     * @param from The QVariant instance that represents a Data Type
     *
     * @throws std::runtime_error if the initialization fails
     */
    void fromVariant(const QVariant& from) override;

    /**
     * @brief Get the Identifier of this Data Type
     *
     * @return The Data Type's Identifier
     */
    [[nodiscard]] QString identifier() const { return m_Identifier; }

protected:
    /**
     * @brief Helper function for the equality comparison operators
     *
     * @param rhs The Identifier to compare this Identifier to
     *
     * @return @c true, if this Identifier is semantically equal to @a rhs, i.e.
     * if @b Identifier matches, @c false otherwise
     */
    [[nodiscard]] bool isEqual(const ISerializable& rhs) const override;

private:
    QString m_Identifier;
};

/**
 * @brief Overload for debugging CDefinedExecutionErrorIdentifiers
 */
SILA_CPP_EXPORT QDebug operator<<(QDebug dbg,
                                  const CDefinedExecutionErrorIdentifier& rhs);
SILA_CPP_EXPORT std::ostream& operator<<(
    std::ostream& os, const CDefinedExecutionErrorIdentifier& rhs);
}  // namespace SiLA2::codegen::fdl

#endif  // CODEGEN_DEFINEDEXECUTIONERRORIDENTIFIER_H
