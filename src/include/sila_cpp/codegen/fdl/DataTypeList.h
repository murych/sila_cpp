/**
 ** This file is part of the sila_cpp project.
 ** Copyright (c) 2021 SiLA2
 **
 ** Permission is hereby granted, free of charge, to any person obtaining a copy
 ** of this software and associated documentation files (the "Software"), to deal
 ** in the Software without restriction, including without limitation the rights
 ** to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 ** copies of the Software, and to permit persons to whom the Software is
 ** furnished to do so, subject to the following conditions:
 **
 ** The above copyright notice and this permission notice shall be included in all
 ** copies or substantial portions of the Software.
 **
 ** THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 ** IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 ** FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 ** AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 ** LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 ** OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 ** SOFTWARE.
 **/

//============================================================================
/// \file   DataTypeList.h
/// \author Florian Meinicke (florian.meinicke@cetoni.de)
/// \date   28.01.2021
/// \brief  Declaration of the CDataTypeList class
//============================================================================
#ifndef CODEGEN_DATATYPELIST_H
#define CODEGEN_DATATYPELIST_H

//============================================================================
//                                  INCLUDES
//============================================================================
#include <sila_cpp/codegen/fdl/DataType.h>
#include <sila_cpp/codegen/fdl/IDataType.h>
#include <sila_cpp/global.h>

namespace SiLA2::codegen::fdl
{
/**
 * @brief The CDataTypeList class represents a List SiLA 2 Data Type
 */
class SILA_CPP_EXPORT CDataTypeList final : public IDataType
{
public:
    /**
     * @brief Get the name of this object
     *
     * @return The object's name
     */
    [[nodiscard]] QString name() const override { return "List"; }

    /**
     * @brief Convert this Data Type to its @c QVariant representation
     *
     * @return A QVariant instance for this Data Type
     */
    [[nodiscard]] QVariant toVariant() const override;

    /**
     * @brief Initializes this Data Type with the values given in the @c QVariant
     * @a from
     *
     * @param from The QVariant instance that represents a Data Type
     *
     * @throws std::runtime_error if the initialization fails
     */
    void fromVariant(const QVariant& from) override;

    /**
     * @brief Get the Data Type of this List Data Type
     *
     * @return The List Data Type's Data Type
     */
    [[nodiscard]] CDataType dataType() const { return m_DataType; }

protected:
    /**
     * @brief Helper function for the equality comparison operators
     *
     * @param rhs The Data Type to compare this Data Type to
     *
     * @return @c true, if this Data Type is semantically equal to @a rhs, i.e. if
     * @b DataType matches @c false otherwise
     */
    [[nodiscard]] bool isEqual(const ISerializable& rhs) const override;

private:
    CDataType m_DataType;
};

/**
 * @brief Overload for debugging CDataTypeLists
 */
SILA_CPP_EXPORT QDebug operator<<(QDebug dbg, const CDataTypeList& rhs);
SILA_CPP_EXPORT std::ostream& operator<<(std::ostream& os,
                                         const CDataTypeList& rhs);
}  // namespace SiLA2::codegen::fdl

#endif  // CODEGEN_DATATYPELIST_H
