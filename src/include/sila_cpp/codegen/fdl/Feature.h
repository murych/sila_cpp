/**
 ** This file is part of the sila_cpp project.
 ** Copyright (c) 2021 SiLA2
 **
 ** Permission is hereby granted, free of charge, to any person obtaining a copy
 ** of this software and associated documentation files (the "Software"), to deal
 ** in the Software without restriction, including without limitation the rights
 ** to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 ** copies of the Software, and to permit persons to whom the Software is
 ** furnished to do so, subject to the following conditions:
 **
 ** The above copyright notice and this permission notice shall be included in all
 ** copies or substantial portions of the Software.
 **
 ** THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 ** IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 ** FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 ** AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 ** LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 ** OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 ** SOFTWARE.
 **/

//============================================================================
/// \file   Feature.h
/// \author Florian Meinicke (florian.meinicke@cetoni.de)
/// \date   26.01.2021
/// \brief  Declaration of the CFeature class
//============================================================================
#ifndef CODEGEN_FEATURE_H
#define CODEGEN_FEATURE_H

//============================================================================
//                                  INCLUDES
//============================================================================
#include <sila_cpp/codegen/fdl/Command.h>
#include <sila_cpp/codegen/fdl/DataTypeDefinition.h>
#include <sila_cpp/codegen/fdl/DefinedExecutionError.h>
#include <sila_cpp/codegen/fdl/Metadata.h>
#include <sila_cpp/codegen/fdl/Property.h>
#include <sila_cpp/codegen/fdl/SiLAElement.h>
#include <sila_cpp/global.h>

namespace SiLA2::codegen::fdl
{
/**
 * @brief The CFeature class represents a SiLA 2 Feature that was defined in a FDL
 * file
 */
class SILA_CPP_EXPORT CFeature final : public CSiLAElement
{
public:
    /**
     * @brief Get the name of this object
     *
     * @return The object's name
     */
    [[nodiscard]] QString name() const override { return "Feature"; }

    /**
     * @brief Convert this object to its @c QVariant representation
     *
     * @return A QVariant instance for this object
     */
    [[nodiscard]] QVariant toVariant() const override;

    /**
     * @brief Initializes this object with the values given in the @c QVariant
     * @a CommandVariant
     *
     * @param from The QVariant instance that represents an object of this class
     */
    void fromVariant(const QVariant& from) override;

    /**
     * @brief Get this Feature's Originator
     *
     * @return The Originator of this Feature
     */
    [[nodiscard]] QString originator() const { return m_Originator; }

    /**
     * @brief Get this Feature's Category
     *
     * @return The Category of this Feature
     */
    [[nodiscard]] QString category() const { return m_Category; }

    /**
     * @brief Get this Feature's Version
     *
     * @return The Version of this Feature
     */
    [[nodiscard]] QString featureVersion() const { return m_FeatureVersion; }

    /**
     * @brief Get this Feature's SiLA 2 Version
     *
     * @return The SiLA 2 Version of this Feature
     */
    [[nodiscard]] QString sila2Version() const { return m_SiLA2Version; }

    /**
     * @brief Get this Feature's Commands
     *
     * @return All of the Commands of this Feature
     */
    [[nodiscard]] QList<CCommand> commands() const { return m_Commands; }

    /**
     * @brief Get this Feature's Properties
     *
     * @return All of the Properties of this Feature
     */
    [[nodiscard]] QList<CProperty> properties() const { return m_Properties; }

    /**
     * @brief Get this Feature's Defined Execution Errors
     *
     * @return All of the Defined Execution Errors of this Feature
     */
    [[nodiscard]] QList<CDefinedExecutionError> errors() const
    {
        return m_Errors;
    }

    /**
     * @brief Get this Feature's Metadata definitions
     *
     * @return All of the Metadata definitions of this Feature
     */
    [[nodiscard]] QList<CMetadata> metadata() const { return m_Metadata; }

    /**
     * @brief Get this Feature's Data Type Definitions
     *
     * @return All of the Data Type Definitions of this Feature
     */
    [[nodiscard]] QList<CDataTypeDefinition> dataTypeDefinitions() const
    {
        return m_DataTypeDefinitions;
    }

protected:
    /**
     * @brief Helper function for the equality comparison operators
     *
     * @param rhs The Feature to compare this Feature to
     *
     * @return @c true, if this Feature is semantically equal to @a rhs, i.e. if
     * @b Originator, @b Category, @b FeatureVersion, @b SiLA2Version,
     * @b Commands, @b Properties, @b Errors, @b Metadata, and
     * @b DataTypeDefinitions match, @c false otherwise
     *
     * @sa CSiLAElement::isEqual(const ISerializable&)
     */
    [[nodiscard]] bool isEqual(const ISerializable& rhs) const override;

private:
    QString m_Originator;
    QString m_Category;
    QString m_FeatureVersion;
    QString m_SiLA2Version;

    QList<CCommand> m_Commands;
    QList<CProperty> m_Properties;
    QList<CDefinedExecutionError> m_Errors;
    QList<CMetadata> m_Metadata;
    QList<CDataTypeDefinition> m_DataTypeDefinitions;
};

/**
 * @brief Overload for debugging CFeatures
 */
SILA_CPP_EXPORT QDebug operator<<(QDebug dbg, const CFeature& rhs);
SILA_CPP_EXPORT std::ostream& operator<<(std::ostream& os, const CFeature& rhs);
}  // namespace SiLA2::codegen::fdl

#endif  // CODEGEN_FEATURE_H
