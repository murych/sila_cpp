/**
 ** This file is part of the sila_cpp project.
 ** Copyright 2021 SiLA2
 **
 ** Permission is hereby granted, free of charge, to any person obtaining a copy
 ** of this software and associated documentation files (the "Software"), to deal
 ** in the Software without restriction, including without limitation the rights
 ** to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 ** copies of the Software, and to permit persons to whom the Software is
 ** furnished to do so, subject to the following conditions:
 **
 ** The above copyright notice and this permission notice shall be included in all
 ** copies or substantial portions of the Software.
 **
 ** THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 ** IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 ** FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 ** AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 ** LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 ** OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 ** SOFTWARE.
 **/

//============================================================================
/// \file   ProtobufFile.h
/// \author Florian Meinicke (florian.meinicke@cetoni.de)
/// \date   11.02.2021
/// \brief  Declaration of the CProtobufFile class
//============================================================================
#ifndef CODEGEN_PROTOBUFFILE_H
#define CODEGEN_PROTOBUFFILE_H

//============================================================================
//                                  INCLUDES
//============================================================================
#include <sila_cpp/global.h>

#include <QString>

namespace SiLA2::codegen::proto
{
/**
 * @brief The CProtobufFile class represents a valid Protocol Buffer File that has
 * the correct file name and protobuf package name according to SiLA 2 Part B.
 */
class SILA_CPP_EXPORT CProtobufFile
{
public:
    /**
     * @brief Construct a new @c CProtobufFile with the given @a FileName and
     * @a Content
     *
     * @param FileName The name of the protobuf file
     * @param Content The content of the file (i.e. the protobuf code) including
     * all necessary directives ("syntax", "import" and "package")
     */
    CProtobufFile(QString FileName, QString Content);

    /**
     * @brief Generate a @c CProtobufFile from the given @a Protobuf. This
     * function will add all of the necessary directives ("syntax", "import" and
     * "package") and automatically generate a unique file name. Since this
     * function will only be necessary to use for protobuf files that don't
     * contain a full SiLA 2 Feature the protobuf package name and the file name
     * will be arbitrary.
     *
     * @param Protobuf The raw protobuf code that can represent only a single
     * message for example
     * @return A valid @c CProtobufFile with an arbitrary but unique file name and
     * the protobuf code with all necessary directives
     */
    [[nodiscard]] static CProtobufFile fromRawProtobuf(const QString& Protobuf);

    /**
     * @brief Get the file name of this protobuf file
     *
     * @return This file's name
     */
    [[nodiscard]] QString fileName() const { return m_FileName; }

    /**
     * @brief Get the content of this protobuf file
     *
     * @return This file's content
     */
    [[nodiscard]] QString content() const { return m_Content; }

private:
    QString m_FileName;
    QString m_Content;

    static size_t m_DynamicFileCount;
};
}  // namespace SiLA2::codegen::proto
#endif  // CODEGEN_PROTOBUFFILE_H
