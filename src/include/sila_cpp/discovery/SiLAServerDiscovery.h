/**
 ** This file is part of the sila_cpp project.
 ** Copyright 2021 SiLA2
 **
 ** Permission is hereby granted, free of charge, to any person obtaining a copy
 ** of this software and associated documentation files (the "Software"), to deal
 ** in the Software without restriction, including without limitation the rights
 ** to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 ** copies of the Software, and to permit persons to whom the Software is
 ** furnished to do so, subject to the following conditions:
 **
 ** The above copyright notice and this permission notice shall be included in all
 ** copies or substantial portions of the Software.
 **
 ** THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 ** IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 ** FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 ** AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 ** LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 ** OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 ** SOFTWARE.
 **/

//============================================================================
/// \file   SiLAServerDiscovery.h
/// \author Florian Meinicke (florian.meinicke@cetoni.de)
/// \date   20.01.2021
/// \brief  Declaration of the CSiLAServerDiscovery class
//============================================================================
#ifndef SILASERVERDISCOVERY_H
#define SILASERVERDISCOVERY_H

//============================================================================
//                                  INCLUDES
//============================================================================
#include <sila_cpp/global.h>

#include <QObject>

#include <polymorphic_value.h>

namespace SiLA2
{
//============================================================================
//                            FORWARD DECLARATIONS
//============================================================================
class CServerAddress;
class CServerInformation;

/**
 * @brief The CSiLAServerDiscovery class encompasses the SiLA Server Discovery
 * mechanism, i.e. it provides functionality to register a SiLA Server in the
 * network and to browse for available Servers
 */
class SILA_CPP_EXPORT CSiLAServerDiscovery : public QObject
{
    Q_OBJECT

    class PrivateImpl;
    using PrivateImplPtr = isocpp_p0201::polymorphic_value<PrivateImpl>;

public:
    /**
     * @brief C'tor
     */
    explicit CSiLAServerDiscovery(QObject* parent = nullptr,
                                  PrivateImplPtr priv = {});

    /**
     * @brief D'tor
     */
    ~CSiLAServerDiscovery() override;

    /**
     * @brief Publish a server running on @a ServerPort with the given
     * @a ServerInfo on the network
     *
     * @param ServerInfo The information (such as name, description, UUID, ...) of
     * the server to publish
     * @param ServerPort The port on which the server is running
     * @param RootCA Optional certificate of the root CA that signed the
     * certificate of the Server (will be put into the TXT records of the
     * discovery entry)
     */
    void publishServer(const CServerInformation& ServerInfo, int ServerPort,
                       const QString& RootCA = "");

    /**
     * @brief Stop advertising the currently published server. If there is no
     * server currently published this function does nothing.
     */
    void unpublishServer();

    /**
     * @brief Starts browsing for available SiLA Servers on the network
     */
    void startDiscovery();

    /**
     * @brief Stops browsing for available SiLA Servers on the network
     */
    void stopDiscovery();

    /**
     * @brief Whether the Discovery is currently active
     *
     * @returns @c true, if Discovery is active, @c false otherwise
     */
    [[nodiscard]] bool isActive() const;

signals:
    /**
     * @brief This signal will be emitted for each SiLA Server that is found on
     * the network. Start searching for available servers by calling
     * @c startDiscovery().
     *
     * @param UUID The UUID of the server
     * @param Address The IP address and port of the server
     * @param ServerInfo Additional information about the discovered Server that
     * was read from TXT records (As of SiLA 2 v1.1 a Server adds additional
     * information like its name, version and description as TXT records to its
     * DNS entry. If the Server that was discovered does not implement this
     * behaviour yet, the corresponding @a ServerInfo fields will be empty and can
     * be discarded. In this case you need to connect to the Server first in order
     * to get this info.)
     * @param CACertificate The certificate of the CA (Certificate Authority) that
     * signed the certificate of the Server (As of SiLA 2 v1.1 if a Server uses an
     * untrusted certificate it must include the certificate of the CA that signed
     * the Server's certificate in the TXT records of its DNS entry. For Servers
     * using SiLA 2 v1.0 or Servers that don't have an untrusted certificate this
     * value will be empty.)
     */
    void serverAdded(const QUuid& UUID, const CServerAddress& Address,
                     const CServerInformation& ServerInfo,
                     const QString& CACertificate) const;

    /**
     * @brief This signal will be emitted when the zeroconf info for an already
     * discovered SiLA Server changed
     *
     * @param UUID The UUID of the server
     * @param Address The IP address and port of the server
     * @param ServerInfo Additional information about the discovered Server that
     * was read from TXT records
     * @param CACertificate The certificate of the CA (Certificate Authority) that
     * signed the certificate of the Server
     *
     * @sa serverAdded()
     */
    void serverUpdated(const QUuid& UUID, const CServerAddress& Address,
                       const CServerInformation& ServerInfo,
                       const QString& CACertificate) const;

    /**
     * @brief This signal will be emitted for each SiLA Server that is removed
     * from the network.
     *
     * @param UUID The UUID of the server
     */
    void serverRemoved(const QUuid& UUID) const;

    /**
     * @brief This signal will be emitted when an error occurred during publishing
     * a Server or browsing for Servers
     *
     * @param Message The error message
     */
    void error(const QString& Message);

protected:
    PrivateImplPtr d_ptr;

private:
    PIMPL_DECLARE_PRIVATE(CSiLAServerDiscovery)
};
}  // namespace SiLA2
#endif  // SILASERVERDISCOVERY_H
