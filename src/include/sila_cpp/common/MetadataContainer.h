/*******************************************************************************
 ** This file is part of the sila_cpp project.
 ** Copyright 2022 SiLA 2
 **
 ** Permission is hereby granted, free of charge, to any person obtaining a copy
 ** of this software and associated documentation files (the "Software"), to deal
 ** in the Software without restriction, including without limitation the rights
 ** to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 ** copies of the Software, and to permit persons to whom the Software is
 ** furnished to do so, subject to the following conditions:
 **
 ** The above copyright notice and this permission notice shall be included in all
 ** copies or substantial portions of the Software.
 **
 ** THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 ** IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 ** FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 ** AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 ** LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 ** OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 ** SOFTWARE.
 *****************************************************************************/

//============================================================================
/// \file   MetadataContainer.h
/// \author Florian Meinicke (florian.meinicke@cetoni.de)
/// \date   25.01.2022
/// \brief  Declaration of the CMetadataContainer class
//============================================================================
#ifndef METADATACONTAINER_H
#define METADATACONTAINER_H

//============================================================================
//                                  INCLUDES
//============================================================================
#include <sila_cpp/common/FullyQualifiedMetadataID.h>
#include <sila_cpp/global.h>
#include <sila_cpp/internal/type_traits.h>

#include <polymorphic_value.h>

#include <map>

namespace grpc
{
class string_ref;
class ClientContext;
}  // namespace grpc

namespace SiLA2
{
/**
 * @brief The CMetadataContainer class is a simple container for Metadata received
 * from a SiLA 2 Client along with a Command Call or Property Request.
 */
class SILA_CPP_EXPORT CMetadataContainer :
    public std::map<CFullyQualifiedMetadataID, std::string>
{
public:
    using grpc_metadata_container_t =
        std::multimap<grpc::string_ref, grpc::string_ref>;

    /**
     * @brief C'tor
     *
     * Constructs an empty container
     */
    CMetadataContainer();

    /**
     * @brief C'tor
     *
     * Constructs a new @c CMetadataContainer from the given SiLA Client
     * @a Metadata
     */
    explicit CMetadataContainer(
        std::map<CFullyQualifiedMetadataID, std::string> Metadata);

    /**
     * @brief C'tor
     *
     * Construct a new @c CMetadataContainer from the given list of pairs of Fully
     * Qualified Metadata Identifiers and Metadata values (i.e. protobuf
     * @c Messages)
     */
    CMetadataContainer(
        std::initializer_list<
            std::pair<CFullyQualifiedMetadataID, const google::protobuf::Message&>>
            Metadata);

    /**
     * @brief D'tor
     */
    ~CMetadataContainer();

    /**
     * @brief Get a single deserialized Client Metadata for the given
     * @a MetadataID
     *
     * @tparam T The type of the Metadata (e.g. @c Metadata_MyMetadata if you've
     * defined 'MyMetadata' as part of your Feature
     *
     * @param MetadataID The Fully Qualified Identifier of the Metadata value to
     * get
     *
     * @return The Metadata for @a MetadataID already deserialized into a protobuf
     * Message of type @a T
     *
     * @throws std::out_of_range if there is no Metadata value for the given
     * @a MetadataID
     *
     * @sa metadata()
     */
    template<typename T, typename = std::enable_if_t<internal::is_real_base_of_v<
                             google::protobuf::Message, T>>>
    [[nodiscard]] T value(const CFullyQualifiedMetadataID& MetadataID) const;

    /**
     * @brief Adds all Metadata in this container to the given gRPC Client
     * @a Context
     *
     * @param Context The gRPC Client Context that should have this container's
     * Metadata
     */
    void addToContext(grpc::ClientContext* Context) const;

    /**
     * @brief Constructs a new container from the given @a RawMetadata container
     * received from gRPC
     *
     * @param RawMetadata The raw metadata received in the gRPC call context
     *
     * @return A new @c CMetadataContainer with all SiLA Client Metadata from
     * @a RawMetadata
     */
    static CMetadataContainer fromRawMetadata(
        const grpc_metadata_container_t& RawMetadata);
};

//============================================================================
template<typename T, typename>
T CMetadataContainer::value(const CFullyQualifiedMetadataID& MetadataID) const
{
    T Message;
    try
    {
        Message.ParseFromString(at(MetadataID));
    }
    catch ([[maybe_unused]] const std::out_of_range& err)
    {
        using namespace std::string_literals;
        throw std::out_of_range{"No Metadata with Identifier \""s
                                + MetadataID.toStdString()
                                + "\" in received Metadata ist!"};
    }
    return Message;
}

/**
 * @brief Overload for printing @c CMetadataContainer
 */
SILA_CPP_EXPORT std::ostream& operator<<(std::ostream& os,
                                         const CMetadataContainer& rhs);
}  // namespace SiLA2
#endif  // METADATACONTAINER_H
