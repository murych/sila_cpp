/**
 ** This file is part of the sila_cpp project.
 ** Copyright (c) 2021 SiLA2
 **
 ** Permission is hereby granted, free of charge, to any person obtaining a copy
 ** of this software and associated documentation files (the "Software"), to deal
 ** in the Software without restriction, including without limitation the rights
 ** to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 ** copies of the Software, and to permit persons to whom the Software is
 ** furnished to do so, subject to the following conditions:
 **
 ** The above copyright notice and this permission notice shall be included in all
 ** copies or substantial portions of the Software.
 **
 ** THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 ** IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 ** FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 ** AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 ** LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 ** OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 ** SOFTWARE.
 **/

//============================================================================
/// \file   FullyQualifiedPropertyID.h
/// \author Florian Meinicke (florian.meinicke@cetoni.de)
/// \date   26.01.2021
/// \brief  Declaration of the CFullyQualifiedPropertyID class
//============================================================================
#ifndef FULLYQUALIFIEDPROPERTYID_H
#define FULLYQUALIFIEDPROPERTYID_H

//============================================================================
//                                  INCLUDES
//============================================================================
#include <sila_cpp/common/FullyQualifiedFeatureID.h>

namespace SiLA2
{
/**
 * @brief The CFullyQualifiedPropertyID represents a Fully Qualified Property
 * Identifier as specified by the SiLA 2 standard
 */
class SILA_CPP_EXPORT CFullyQualifiedPropertyID : public CFullyQualifiedFeatureID
{
public:
    /**
     * @brief C'tor
     *
     * @param FeatureID The Fully Qualified Feature Identifier of the Property's
     * Feature
     * @param Identifier The Property's Identifier
     */
    CFullyQualifiedPropertyID(const CFullyQualifiedFeatureID& FeatureID,
                              std::string_view Identifier);
    CFullyQualifiedPropertyID(const CFullyQualifiedFeatureID& FeatureID,
                              QStringView Identifier);

    /**
     * @brief Default c'tor
     *
     * Constructs an empty (invalid) Fully Qualified Property Identifier
     */
    CFullyQualifiedPropertyID() = default;

    /**
     * @brief Construct a CFullyQualifiedPropertyID from the given string @a from.
     * This assumes that @a from is already in the form of a Fully Qualified
     * Identifier and just needs to be converted to the correct type.
     *
     * @param from A Fully Qualified Property Identifier as a @c std::string
     * @return The corresponding Fully Qualified Property Identifier or an empty
     * Identifier if the given string @a from was invalid
     *
     * @sa isValid()
     */
    [[nodiscard]] static CFullyQualifiedPropertyID fromString(const QString& from);
    [[nodiscard]] static CFullyQualifiedPropertyID fromStdString(
        const std::string& from);

    /**
     * @brief Check if this Fully Qualified Identifier is equal to @a rhs
     * according to the <i>Uniqueness of Identifiers</i> defined in the standard
     * (i.e. Identifiers are case-insensitive)
     *
     * @param rhs The Identifier to compare to
     * @returns true, if *this == rhs, false otherwise
     */
    bool operator==(const CFullyQualifiedPropertyID& rhs) const;

    /**
     * @override
     * @brief Get the Property Identifier without any additional information from
     * the Fully Qualified Feature Identifier
     *
     * @return The Property Identifier
     *
     * @sa CFullyQualifiedFeatureID::identifier()
     */
    [[nodiscard]] QString identifier() const override;

    /**
     * @override
     * @brief Check if this Fully Qualified Property Identifier is valid. Most of
     * the time this is @c true, but when constructing a Fully Qualified Property
     * Identifier from a string using @c fromStdString() it might happen that the
     * returned Identifier is invalid if the given string was invalid.
     *
     * @returns true, if the Identifier is valid, false otherwise
     *
     * @sa fromStdString()
     */
    [[nodiscard]] bool isValid() const override;

    /**
     * @brief Convert this Fully Qualified Feature Identifier to a @c QString
     *
     * @return The Fully Qualified Feature Identifier as a @c QString
     */
    [[nodiscard]] QString toString() const override;

    /**
     * @brief Convert this Fully Qualified Property Identifier to a @c std::string
     *
     * @return The Fully Qualified Property Identifier as a @c std::string
     */
    [[nodiscard]] std::string toStdString() const override;

    /**
     * @brief Convert this Fully Qualified Property Identifier into a fully
     * qualified gRPC method name
     *
     * @param Observable Indicate whether the Property is observable or not. Since
     * SiLA 2 Identifiers don't include this information this
     * @c CFullyQualifiedPropertyID has no way of knowing this. But since the
     * mapping to gRPC method names is different for Unobservable and Observable
     * Properties we need to get this information here.
     * @return The gRPC method name for this Fully Qualified Property Identifier
     */
    [[nodiscard]] std::string toMethodName(bool Observable = false) const;

    /**
     * @brief Get the fully qualified gRPC message name for the Parameters of the
     * Property identified by this Fully Qualified Property Identifier
     *
     * @param Observable Indicate whether the Property is observable or not. Since
     * SiLA 2 Identifiers don't include this information this
     * @c CFullyQualifiedPropertyID has no way of knowing this. But since the
     * mapping to gRPC message names is different for Unobservable and Observable
     * Properties we need to get this information here.
     * @return The gRPC message name for the Parameter message of this Fully
     * Qualified Property Identifier
     */
    [[nodiscard]] std::string parameterMessageName(bool Observable = false) const;

    /**
     * @brief Get the fully qualified gRPC message name for the Responses of the
     * Property identified by this Fully Qualified Property Identifier
     *
     * @param Observable Indicate whether the Property is observable or not. Since
     * SiLA 2 Identifiers don't include this information this
     * @c CFullyQualifiedPropertyID has no way of knowing this. But since the
     * mapping to gRPC message names is different for Unobservable and Observable
     * Properties we need to get this information here.
     * @return The gRPC message name for the Response message of this Fully
     * Qualified Property Identifier
     */
    [[nodiscard]] std::string responseMessageName(bool Observable = false) const;

private:
    QString m_Identifier{};
};
}  // namespace SiLA2

#endif  // FULLYQUALIFIEDPROPERTYID_H
