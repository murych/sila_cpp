/**
 ** This file is part of the sila_cpp project.
 ** Copyright (c) 2021 SiLA2
 **
 ** Permission is hereby granted, free of charge, to any person obtaining a copy
 ** of this software and associated documentation files (the "Software"), to deal
 ** in the Software without restriction, including without limitation the rights
 ** to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 ** copies of the Software, and to permit persons to whom the Software is
 ** furnished to do so, subject to the following conditions:
 **
 ** The above copyright notice and this permission notice shall be included in all
 ** copies or substantial portions of the Software.
 **
 ** THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 ** IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 ** FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 ** AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 ** LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 ** OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 ** SOFTWARE.
 **/

//============================================================================
/// \file   FullyQualifiedCommandID.h
/// \author Florian Meinicke (florian.meinicke@cetoni.de)
/// \date   22.01.2021
/// \brief  Declaration of the CFullyQualifiedCommandID class
//============================================================================
#ifndef FULLYQUALIFIEDCOMMANDID_H
#define FULLYQUALIFIEDCOMMANDID_H

//============================================================================
//                                  INCLUDES
//============================================================================
#include <sila_cpp/common/FullyQualifiedFeatureID.h>

namespace SiLA2
{
/**
 * @brief The CFullyQualifiedCommandID  represents a Fully Qualified Command
 * Identifier as specified by the SiLA 2 standard
 */
class SILA_CPP_EXPORT CFullyQualifiedCommandID : public CFullyQualifiedFeatureID
{
public:
    /**
     * @brief The RPCType enum defines the different types of RPCs an Observable
     * Command Execution can go through
     */
    enum class RPCType
    {
        Invalid,  ///< Command is not Observable
        Initiation,
        Info,
        IntermediateResult,
        Result,
    };

    /**
     * @brief C'tor
     *
     * @param FeatureID The Fully Qualified Feature Identifier of the Command's
     * Feature
     * @param Identifier The Command's Identifier
     */
    CFullyQualifiedCommandID(const CFullyQualifiedFeatureID& FeatureID,
                             std::string_view Identifier);
    CFullyQualifiedCommandID(const CFullyQualifiedFeatureID& FeatureID,
                             QStringView Identifier);

    /**
     * @brief Default c'tor
     *
     * Constructs an empty (invalid) Fully Qualified Command Identifier
     */
    CFullyQualifiedCommandID() = default;

    /**
     * @brief Construct a CFullyQualifiedCommandID from the given string @a from.
     * This assumes that @a from is already in the form of a Fully Qualified
     * Identifier and just needs to be converted to the correct type.
     *
     * @param from A Fully Qualified Command Identifier as a @c std::string
     * @return The corresponding Fully Qualified Command Identifier or an empty
     * Identifier if the given string @a from was invalid
     *
     * @sa isValid()
     */
    [[nodiscard]] static CFullyQualifiedCommandID fromString(const QString& from);
    [[nodiscard]] static CFullyQualifiedCommandID fromStdString(
        const std::string& from);

    /**
     * @brief Check if this Fully Qualified Identifier is equal to @a rhs
     * according to the <i>Uniqueness of Identifiers</i> defined in the standard
     * (i.e. Identifiers are case-insensitive)
     *
     * @param rhs The Identifier to compare to
     * @returns true, if *this == rhs, false otherwise
     */
    bool operator==(const CFullyQualifiedCommandID& rhs) const;

    /**
     * @brief Get the Fully Qualified Command Identifier
     *
     * @return The Fully Qualified Command Identifier part of this Fully Qualified
     * Identifier
     */
    [[nodiscard]] CFullyQualifiedCommandID commandIdentifier() const;

    /**
     * @override
     * @brief Get the Command Identifier without any additional information from
     * the Fully Qualified Feature Identifier
     *
     * @return The Command Identifier
     *
     * @sa CFullyQualifiedFeatureID::identifier()
     */
    [[nodiscard]] QString identifier() const override;

    /**
     * @override
     * @brief Check if this Fully Qualified Command Identifier is valid. Most of
     * the time this is @c true, but when constructing a Fully Qualified Command
     * Identifier from a string using @c fromStdString() it might happen that the
     * returned Identifier is invalid if the given string was invalid.
     *
     * @returns true, if the Identifier is valid, false otherwise
     *
     * @sa fromStdString()
     */
    [[nodiscard]] bool isValid() const override;

    /**
     * @brief Convert this Fully Qualified Command Identifier to a @c QString
     *
     * @return The Fully Qualified Command Identifier as a @c QString
     */
    [[nodiscard]] QString toString() const override;

    /**
     * @brief Convert this Fully Qualified Command Identifier to a @c std::string
     *
     * @return The Fully Qualified Command Identifier as a @c std::string
     */
    [[nodiscard]] std::string toStdString() const override;

    /**
     * @brief Convert this Fully Qualified Command Identifier into a fully
     * qualified gRPC method name
     *
     * @param Type The type of the RPC method to construct the name for
     * @return The gRPC method name for this Fully Qualified Command Identifier
     */
    [[nodiscard]] std::string toMethodName(RPCType Type = RPCType::Invalid) const;

    /**
     * @brief Get the fully qualified gRPC message name for the Parameters of the
     * Command identified by this Fully Qualified Command Identifier
     *
     * @return The gRPC message name for the Parameter message of this Fully
     * Qualified Command Identifier
     */
    [[nodiscard]] std::string parameterMessageName() const;

    /**
     * @brief Get the fully qualified gRPC message name for the Responses of the
     * Command identified by this Fully Qualified Command Identifier
     *
     * @param Type The type of the RPC method to construct the name for
     * @return The gRPC message name for the Response message of this Fully
     * Qualified Command Identifier
     */
    [[nodiscard]] std::string responseMessageName(
        RPCType Type = RPCType::Invalid) const;

protected:
    /**
     * @brief Convert the given RPC type @a Type into its string representation.
     * This includes a leading '_' since this function will be used by the
     * functions that convert this Command Identifier into gRPC message names.
     *
     * @param Type The Type to convert to a string
     * @return The string representation of the given @a Type that can be used to
     * construct gRPC message names
     */
    [[nodiscard]] static QString typeToString(RPCType Type);

private:
    QString m_Identifier{};
};

/**
 * @brief The CFullyQualifiedCommandParameterID  represents a Fully Qualified
 * Command Parameter Identifier as specified by the SiLA 2 standard
 */
class SILA_CPP_EXPORT CFullyQualifiedCommandParameterID :
    public CFullyQualifiedCommandID
{
public:
    /**
     * @brief C'tor
     *
     * @param CommandID The Fully Qualified Command Identifier of the
     * Parameter's Command
     * @param Identifier The Parameter's Identifier
     */
    CFullyQualifiedCommandParameterID(const CFullyQualifiedCommandID& CommandID,
                                      std::string_view Identifier);
    CFullyQualifiedCommandParameterID(const CFullyQualifiedCommandID& CommandID,
                                      QStringView Identifier);

    /**
     * @brief Default c'tor
     *
     * Constructs an empty (invalid) Fully Qualified Command Parameter
     * Identifier
     */
    CFullyQualifiedCommandParameterID() = default;

    /**
     * @brief Construct a CFullyQualifiedCommandParameterID from the given string
     * @a from. This assumes that @a from is already in the form of a Fully
     * Qualified Identifier and just needs to be converted to the correct
     * type.
     *
     * @param from A Fully Qualified Command Parameter Identifier as a @c
     * std::string
     * @return The corresponding Fully Qualified Command Parameter Identifier or
     * an empty Identifier if the given string @a from was invalid
     *
     * @sa isValid()
     */
    [[nodiscard]] static CFullyQualifiedCommandParameterID fromString(
        const QString& from);
    [[nodiscard]] static CFullyQualifiedCommandParameterID fromStdString(
        const std::string& from);

    /**
     * @brief Check if this Fully Qualified Identifier is equal to @a rhs
     * according to the <i>Uniqueness of Identifiers</i> defined in the
     * standard (i.e. Identifiers are case-insensitive)
     *
     * @param rhs The Identifier to compare to
     * @returns true, if *this == rhs, false otherwise
     */
    bool operator==(const CFullyQualifiedCommandParameterID& rhs) const;

    /**
     * @brief Get the Fully Qualified Command Parameter Identifier
     *
     * @return The Fully Qualified Command Parameter Identifier part of this Fully
     * Qualified Identifier
     */
    [[nodiscard]] CFullyQualifiedCommandParameterID parameterIdentifier() const;

    /**
     * @override
     * @brief Get the Parameter Identifier without any additional information
     * from the Fully Qualified Command Identifier
     *
     * @return The Parameter Identifier
     *
     * @sa CFullyQualifiedCommandID::identifier()
     */
    [[nodiscard]] QString identifier() const override;

    /**
     * @override
     * @brief Check if this Fully Qualified Command Parameter Identifier is valid.
     * Most of the time this is @c true, but when constructing a Fully Qualified
     * Command Parameter Identifier from a string using @c fromStdString() it
     * might happen that the returned Identifier is invalid if the given string
     * was invalid.
     *
     * @returns true, if the Identifier is valid, false otherwise
     *
     * @sa fromStdString()
     */
    [[nodiscard]] bool isValid() const override;

    /**
     * @brief Convert this Fully Qualified Command Parameter Identifier to a
     * @c std::string
     *
     * @return The Fully Qualified Command Parameter Identifier as a
     * @c std::string
     */
    [[nodiscard]] std::string toStdString() const override;

    /**
     * @brief Convert this Fully Qualified Command Parameter Identifier to a
     * @c QString
     *
     * @return The Fully Qualified Command Parameter Identifier as a @c QString
     */
    [[nodiscard]] QString toString() const override;

protected:
    friend class CValidationError;

private:
    QString m_Identifier{};
};

/**
 * @brief The CFullyQualifiedCommandResponseID  represents a Fully Qualified
 * Command Response Identifier as specified by the SiLA 2 standard
 */
class SILA_CPP_EXPORT CFullyQualifiedCommandResponseID :
    public CFullyQualifiedCommandID
{
public:
    /**
     * @brief C'tor
     *
     * @param CommandID The Fully Qualified Command Identifier of the
     * Response's Command
     * @param Identifier The Response's Identifier
     */
    CFullyQualifiedCommandResponseID(const CFullyQualifiedCommandID& CommandID,
                                     std::string_view Identifier);
    CFullyQualifiedCommandResponseID(const CFullyQualifiedCommandID& CommandID,
                                     QStringView Identifier);

    /**
     * @brief Default c'tor
     *
     * Constructs an empty (invalid) Fully Qualified Command Response
     * Identifier
     */
    CFullyQualifiedCommandResponseID() = default;

    /**
     * @brief Construct a CFullyQualifiedCommandResponseID from the given string
     * @a from. This assumes that @a from is already in the form of a Fully
     * Qualified Identifier and just needs to be converted to the correct
     * type.
     *
     * @param from A Fully Qualified Command Response Identifier as a @c
     * std::string
     * @return The corresponding Fully Qualified Command Response Identifier or
     * an empty Identifier if the given string @a from was invalid
     *
     * @sa isValid()
     */
    [[nodiscard]] static CFullyQualifiedCommandResponseID fromString(
        const QString& from);
    [[nodiscard]] static CFullyQualifiedCommandResponseID fromStdString(
        const std::string& from);

    /**
     * @brief Check if this Fully Qualified Identifier is equal to @a rhs
     * according to the <i>Uniqueness of Identifiers</i> defined in the
     * standard (i.e. Identifiers are case-insensitive)
     *
     * @param rhs The Identifier to compare to
     * @returns true, if *this == rhs, false otherwise
     */
    bool operator==(const CFullyQualifiedCommandResponseID& rhs) const;

    /**
     * @brief Get the Fully Qualified Command Response Identifier
     *
     * @return The Fully Qualified Command Response Identifier part of this Fully
     * Qualified Identifier
     */
    [[nodiscard]] CFullyQualifiedCommandResponseID responseIdentifier() const;

    /**
     * @override
     * @brief Get the Response Identifier without any additional information
     * from the Fully Qualified Command Identifier
     *
     * @return The Response Identifier
     *
     * @sa CFullyQualifiedCommandID::identifier()
     */
    [[nodiscard]] QString identifier() const override;

    /**
     * @override
     * @brief Check if this Fully Qualified Command Response Identifier is valid.
     * Most of the time this is @c true, but when constructing a Fully Qualified
     * Command Response Identifier from a string using @c fromStdString() it
     * might happen that the returned Identifier is invalid if the given string
     * was invalid.
     *
     * @returns true, if the Identifier is valid, false otherwise
     *
     * @sa fromStdString()
     */
    [[nodiscard]] bool isValid() const override;

    /**
     * @brief Convert this Fully Qualified Command Response Identifier to a
     * @c std::string
     *
     * @return The Fully Qualified Command Response Identifier as a
     * @c std::string
     */
    [[nodiscard]] std::string toStdString() const override;

    /**
     * @brief Convert this Fully Qualified Command Response Identifier to a
     * @c QString
     *
     * @return The Fully Qualified Command Response Identifier as a @c QString
     */
    [[nodiscard]] QString toString() const override;

private:
    QString m_Identifier{};
};

/**
 * @brief The CFullyQualifiedCommandIntermediate ResponseID  represents a Fully
 * Qualified Command Intermediate Response Identifier as specified by the SiLA 2
 * standard
 */
class SILA_CPP_EXPORT CFullyQualifiedCommandIntermediateResponseID :
    public CFullyQualifiedCommandID
{
public:
    /**
     * @brief C'tor
     *
     * @param CommandID The Fully Qualified Command Identifier of the
     * IntermediateResponse's Command
     * @param Identifier The IntermediateResponse's Identifier
     */
    CFullyQualifiedCommandIntermediateResponseID(
        const CFullyQualifiedCommandID& CommandID, std::string_view Identifier);
    CFullyQualifiedCommandIntermediateResponseID(
        const CFullyQualifiedCommandID& CommandID, QStringView Identifier);

    /**
     * @brief Default c'tor
     *
     * Constructs an empty (invalid) Fully Qualified Command Intermediate
     * Response Identifier
     */
    CFullyQualifiedCommandIntermediateResponseID() = default;

    /**
     * @brief Construct a CFullyQualifiedCommandIntermediateResponseID from the
     * given string
     * @a from. This assumes that @a from is already in the form of a Fully
     * Qualified Identifier and just needs to be converted to the correct
     * type.
     *
     * @param from A Fully Qualified Command Intermediate Response Identifier as a
     * @c std::string
     * @return The corresponding Fully Qualified Command Intermediate Response
     * Identifier or an empty Identifier if the given string @a from was invalid
     *
     * @sa isValid()
     */
    [[nodiscard]] static CFullyQualifiedCommandIntermediateResponseID fromString(
        const QString& from);
    [[nodiscard]] static CFullyQualifiedCommandIntermediateResponseID
    fromStdString(const std::string& from);

    /**
     * @brief Check if this Fully Qualified Identifier is equal to @a rhs
     * according to the <i>Uniqueness of Identifiers</i> defined in the
     * standard (i.e. Identifiers are case-insensitive)
     *
     * @param rhs The Identifier to compare to
     * @returns true, if *this == rhs, false otherwise
     */
    bool operator==(const CFullyQualifiedCommandIntermediateResponseID& rhs) const;

    /**
     * @brief Get the Fully Qualified Command Intermediate Response Identifier
     *
     * @return The Fully Qualified Command Intermediate Response Identifier part
     * of this Fully Qualified Identifier
     */
    [[nodiscard]] CFullyQualifiedCommandIntermediateResponseID
    intermediateResponseIdentifier() const;

    /**
     * @override
     * @brief Get the Intermediate Response Identifier without any additional
     * information from the Fully Qualified Command Identifier
     *
     * @return The Intermediate Response Identifier
     *
     * @sa CFullyQualifiedCommandID::identifier()
     */
    [[nodiscard]] QString identifier() const override;

    /**
     * @override
     * @brief Check if this Fully Qualified Command Intermediate Response
     * Identifier is valid. Most of the time this is @c true, but when
     * constructing a Fully Qualified Command Intermediate Response Identifier
     * from a string using @c fromStdString() it might happen that the returned
     * Identifier is invalid if the given string was invalid.
     *
     * @returns true, if the Identifier is valid, false otherwise
     *
     * @sa fromStdString()
     */
    [[nodiscard]] bool isValid() const override;

    /**
     * @brief Convert this Fully Qualified Command Intermediate Response
     * Identifier to a
     * @c std::string
     *
     * @return The Fully Qualified Command Intermediate Response Identifier as a
     * @c std::string
     */
    [[nodiscard]] std::string toStdString() const override;

    /**
     * @brief Convert this Fully Qualified Command Intermediate Response
     * Identifier to a
     * @c QString
     *
     * @return The Fully Qualified Command Intermediate Response Identifier as a
     * @c QString
     */
    [[nodiscard]] QString toString() const override;

private:
    QString m_Identifier{};
};
}  // namespace SiLA2

#endif  // FULLYQUALIFIEDCOMMANDID_H
