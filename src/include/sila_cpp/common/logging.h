/**
 ** This file is part of the sila_cpp project.
 ** Copyright 2020 SiLA2
 **
 ** Permission is hereby granted, free of charge, to any person obtaining a copy
 ** of this software and associated documentation files (the "Software"), to deal
 ** in the Software without restriction, including without limitation the rights
 ** to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 ** copies of the Software, and to permit persons to whom the Software is
 ** furnished to do so, subject to the following conditions:
 **
 ** The above copyright notice and this permission notice shall be included in all
 ** copies or substantial portions of the Software.
 **
 ** THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 ** IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 ** FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 ** AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 ** LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 ** OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 ** SOFTWARE.
 **/

//============================================================================
/// \file   logging.h
/// \author Florian Meinicke (florian.meinicke@cetoni.de)
/// \date   08.01.2020
/// \brief  Definition of helper functions for logging
//============================================================================
#ifndef SILA_CPP_LOGGING_H
#define SILA_CPP_LOGGING_H

//============================================================================
//                                  INCLUDES
//============================================================================
#include <sila_cpp/global.h>

#include <QLoggingCategory>
#include <QString>
#include <QtDebug>

#include <grpcpp/impl/codegen/status_code_enum.h>

#include <iostream>
#include <memory>

//=============================================================================
//                            FORWARD DECLARATIONS
//=============================================================================
namespace grpc
{
class Status;
class string_ref;
}  // namespace grpc

namespace google::protobuf
{
class Message;
}  // namespace google::protobuf

/**
 * @brief The logging categories used for all log messages within the sila_cpp
 * libraries
 */
SILA_CPP_DISABLE_WARNING_PUSH
// the extra ';' at the end of each line triggers a warning but without it
// clang-format gets confused and indents all subsequent lines wrong
SILA_CPP_DISABLE_WARNING_PEDANTIC
/// "sila_cpp"
SILA_CPP_EXPORT Q_DECLARE_LOGGING_CATEGORY(sila_cpp);
/// "sila_cpp.client"
SILA_CPP_EXPORT Q_DECLARE_LOGGING_CATEGORY(sila_cpp_client);
/// "sila_cpp.client.heartbeat"
SILA_CPP_EXPORT Q_DECLARE_LOGGING_CATEGORY(sila_cpp_heartbeat);
/// "sila_cpp.codegen"
SILA_CPP_EXPORT Q_DECLARE_LOGGING_CATEGORY(sila_cpp_codegen);
/// "sila_cpp.common"
SILA_CPP_EXPORT Q_DECLARE_LOGGING_CATEGORY(sila_cpp_common);
/// "sila_cpp.discovery"
SILA_CPP_EXPORT Q_DECLARE_LOGGING_CATEGORY(sila_cpp_discovery);
/// "sila_cpp.framework.constraints"
SILA_CPP_EXPORT Q_DECLARE_LOGGING_CATEGORY(sila_cpp_constraints);
/// "sila_cpp.framework.data_types"
SILA_CPP_EXPORT Q_DECLARE_LOGGING_CATEGORY(sila_cpp_data_types);
/// "sila_cpp.framework.errors"
SILA_CPP_EXPORT Q_DECLARE_LOGGING_CATEGORY(sila_cpp_errors);
/// "sila_cpp.internal"
SILA_CPP_EXPORT Q_DECLARE_LOGGING_CATEGORY(sila_cpp_internal);
/// "sila_cpp.server"
SILA_CPP_EXPORT Q_DECLARE_LOGGING_CATEGORY(sila_cpp_server);
SILA_CPP_DISABLE_WARNING_POP

namespace SiLA2::logging
{
/**
 * @brief The CLogManager class handles the global logging setup within the
 * sila_cpp library
 */
class SILA_CPP_EXPORT CLogManager final
{
public:
    enum class LoggingLevel : uint8_t
    {
        Debug,     ///< equivalent to @c QtDebugMsg
        Info,      ///< equivalent to @c QtInfoMsg
        Warning,   ///< equivalent to @c QtWarningMsg
        Critical,  ///< equivalent to @c QtCriticalMsg
        Fatal,     ///< equivalent to @c QtFatalMsg
    };

    /**
     * @brief Get a shared_ptr to the @c CLogManager instance
     *
     * @return A shared_ptr to the @c CLogManager instance
     */
    static std::shared_ptr<CLogManager> instance() noexcept;

    /**
     * @brief D'tor
     *
     * Re-installs the previous message handler that was used before the last call
     * to @c installMessageHandler
     */
    ~CLogManager();

    /**
     * @brief Installs the custom message handler @a CustomHandler or
     * sila_cpp's default message handler if @a CustomHandler is @c nullptr
     *
     * @param CustomHandler A custom message handler that should be used
     * instead of the default one of the sila_cpp library. Passing @c nullptr
     * will reset the handler to the default handler that ships with sila_cpp
     *
     * @note If you want to use the
     *   default handler again you need to call this function again with no
     *   arguments (or passing @c nullptr which does the same). If you've
     *   installed a custom handler (maybe for your custom logging categories)
     *   through Qt's @c qInstallMessageHandler then this function will use
     * this handler for all messages that don't belong to sila_cpp logging
     * categories (see above). In this case you have to call this function
     * again with no arguments so that the @c CLogManager can properly use
     * your custom handler to delegate all non-sila_cpp related messages.
     *
     * @return The previously installed message handler
     */
    QtMessageHandler installMessageHandler(
        QtMessageHandler CustomHandler = nullptr);

    /**
     * @brief Set the logging level for messages of the 'sila_cpp' logging
     * categories (see above) according to the environment variable
     * `SILA_CPP_LOGGING_LEVEL`. This will affect all of the logging
     * categories used in sila_cpp. For finer control over individual
     * categories use the
     * @c setLoggingLevel(QString,LoggingLevel) method. If you want to change
     * the logging level for your application, either set the environment
     * variable or call this function at the beginning of your @c main.
     *
     * @param DefaultLevel The default logging level if not set via the
     * environment variable
     *
     * @sa setLoggingLevelForCategory
     */
    static void setLoggingLevel(LoggingLevel DefaultLevel = LoggingLevel::Info);

    /**
     * @brief Set the logging level for messages of the logging category with
     * the given name @a CategoryName according to the environment variable
     * `SILA_CPP_<CategoryName>_LOGGING_LEVEL`
     * (e.g. if @a CategoryName is 'sila_cpp.framework.errors' the
     * corresponding environment variable is
     * `SILA_CPP_FRAMEWORK_ERRORS_LOGGING_LEVEL` or
     * `SILA_CPP_SERVER_LOGGING_LEVEL` for @a CategoryName is
     * 'sila_cpp.server'). If you want to change the logging level for your
     * application, either set the environment variable or call this function
     * at the beginning of your
     * @c main.
     *
     * @param CategoryName The name of the logging category to set the logging
     * level for (see above for possible values). Will be converted to upper
     * case.
     * @param DefaultLevel The default logging level if not set via the
     * corresponding environment variable. If the `SILA_CPP_LOGGING_LEVEL`
     * environment variable has been set as well then its value will be used
     * as the default value.
     *
     * @sa setLoggingLevel
     */
    static void setLoggingLevel(const QString& CategoryName,
                                LoggingLevel DefaultLevel = LoggingLevel::Info);

    /**
     * @brief Get the previously used message handler (this will be the
     * default Qt message handler in most cases)
     *
     * @return The previous message handler
     */
    [[nodiscard]] QtMessageHandler previousMessageHandler() const
    {
        return m_PreviousMessageHandler;
    }

private:
    /**
     * @brief C'tor
     */
    CLogManager() = default;

    /**
     * @brief Installs sila_cpp's default message handler and sets the logging
     * level
     *
     * @sa installMessageHandler()
     * @sa setLoggingLevel()
     */
    void initialize();

    /**
     * @brief Get the previously used logging category filter (this will be
     * the default Qt logging category filter)
     *
     * @return The previous logging category filter
     */
    [[nodiscard]] QLoggingCategory::CategoryFilter previousFilter() const
    {
        return m_PreviousFilter;
    }

    /**
     * @brief Convert the @c LoggingLevel @a level to the equivalent @c
     * QtMsgType
     *
     * @param level The @c LoggingLevel to convert
     *
     * @return The equivalent @c QtMsgType
     */
    [[nodiscard]] static QtMsgType loggingLevelToQtMsgType(LoggingLevel level);

    friend LoggingLevel operator++(LoggingLevel& level);

    friend void loggingCategoryFilter(QLoggingCategory* category);

    static std::shared_ptr<CLogManager> m_Instance;
    QtMessageHandler m_PreviousMessageHandler{nullptr};
    QLoggingCategory::CategoryFilter m_PreviousFilter{nullptr};
};

/**
 * @brief Macro to conveniently access the @c CLogManager instance
 */
#define SiLALogManager SiLA2::logging::CLogManager::instance()
}  // namespace SiLA2::logging

//============================================================================
//                            FORWARD DECLARATIONS
//============================================================================
namespace SiLA2
{
class CDynamicSiLAClient;
}  // namespace SiLA2

/**
 * @brief Forward declarations for sila_cpp's `operator<<` overloads
 */
// defined in DynamicSiLAClient.cpp
QDebug operator<<(QDebug dbg, const SiLA2::CDynamicSiLAClient& rhs);

/**
 * @brief `QDebug` `operator<<` overloads for commonly used `std`, `grpc`,
 * `google::protobuf` types
 */
SILA_CPP_EXPORT QDebug operator<<(QDebug dbg, const std::string& rhs);

SILA_CPP_EXPORT QDebug operator<<(QDebug dbg, const grpc::StatusCode& rhs);
SILA_CPP_EXPORT QDebug operator<<(QDebug dbg, const grpc::Status& rhs);

SILA_CPP_EXPORT QDebug operator<<(QDebug dbg,
                                  const google::protobuf::Message& rhs);
SILA_CPP_EXPORT QDebug operator<<(QDebug dbg,
                                  const google::protobuf::Message* rhs);

SILA_CPP_EXPORT QDebug operator<<(QDebug dbg, const grpc::string_ref& rhs);

/**
 * @brief Overload to debug `unique_ptr`s and `shared_ptr`s
 */
template<typename T>
QDebug operator<<(QDebug dbg, const std::unique_ptr<T>& rhs);
template<typename T>
QDebug operator<<(QDebug dbg, const std::shared_ptr<T>& rhs);

/**
 * @brief Overload to print @c QStrings
 */
SILA_CPP_EXPORT std::ostream& operator<<(std::ostream& os, const QString& rhs);

/**
 * @brief Overload for printing @c google::protobuf::Messages
 */
SILA_CPP_EXPORT std::ostream& operator<<(std::ostream& os,
                                         const google::protobuf::Message& rhs);
SILA_CPP_EXPORT std::ostream& operator<<(std::ostream& os,
                                         const google::protobuf::Message* rhs);

SILA_CPP_EXPORT std::ostream& operator<<(std::ostream& os,
                                         const grpc::string_ref& rhs);

/**
 * @brief Overload for debugging vectors
 */
template<typename T>
std::ostream& operator<<(std::ostream& os, const std::vector<T>& rhs);

/**
 * @brief Overload for debugging QLists with C++ @c std::ostream
 */
template<typename T>
std::ostream& operator<<(std::ostream& os, const QList<T>& rhs);

/**
 * @brief Overload to print unique_ptrs and shared_ptrs
 */
template<typename T>
std::ostream& operator<<(std::ostream& os, const std::unique_ptr<T>& rhs);
template<typename T>
std::ostream& operator<<(std::ostream& os, const std::shared_ptr<T>& rhs);

//============================================================================
template<typename T>
QDebug operator<<(QDebug dbg, const std::unique_ptr<T>& rhs)
{
    return rhs ? dbg << *rhs : dbg;
}

//============================================================================
template<typename T>
QDebug operator<<(QDebug dbg, const std::shared_ptr<T>& rhs)
{
    return rhs ? dbg << *rhs : dbg;
}

//============================================================================
template<typename T>
std::ostream& operator<<(std::ostream& os, const std::vector<T>& rhs)
{
    os << "std::vector(";
    auto it = std::begin(rhs);
    auto end = std::end(rhs);
    if (it != end)
    {
        os << *it;
        ++it;
    }
    while (it != end)
    {
        os << ", " << *it;
        ++it;
    }
    return os << ')';
}

//============================================================================
template<typename T>
std::ostream& operator<<(std::ostream& os, const QList<T>& rhs)
{
    os << '(';
    auto it = std::begin(rhs);
    auto end = std::end(rhs);
    if (it != end)
    {
        os << *it;
        ++it;
    }
    while (it != end)
    {
        os << ", " << *it;
        ++it;
    }
    return os << ')';
}

//============================================================================
template<typename T>
std::ostream& operator<<(std::ostream& os, const std::unique_ptr<T>& rhs)
{
    return rhs ? os << *rhs : os;
}

//============================================================================
template<typename T>
std::ostream& operator<<(std::ostream& os, const std::shared_ptr<T>& rhs)
{
    return rhs ? os << *rhs : os;
}
#endif  // SILA_CPP_LOGGING_H
