/**
 ** This file is part of the sila_cpp project.
 **
 ** Permission is hereby granted, free of charge, to any person obtaining a copy
 ** of this software and associated documentation files (the "Software"), to deal
 ** in the Software without restriction, including without limitation the rights
 ** to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 ** copies of the Software, and to permit persons to whom the Software is
 ** furnished to do so, subject to the following conditions:
 **
 ** The above copyright notice and this permission notice shall be included in all
 ** copies or substantial portions of the Software.
 **
 ** THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 ** IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 ** FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 ** AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 ** LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 ** OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 ** SOFTWARE.
 **/

//============================================================================
/// \file    test_error_handling.cpp
/// \authors Florian Meinicke
/// \date    28.04.2020
/// \brief   main file for the standalone unit test application
//============================================================================
#include <sila_cpp/common/ServerAddress.h>

#include "interoperability/InteroperabilityTest/InteroperabilityTestServer.h"
#include "interoperability/InteroperabilityTest/test_globals.h"

#define CATCH_CONFIG_RUNNER
#include <catch2/catch.hpp>

// variables for the server that the interoperability tests shall connect to
std::string ServerIP = "localhost";
std::string ServerPort = "50051";
const std::string NeedsServerTag = "[needs-server]";

/**
 * @brief Checks if the string @a val contains the string @a to_find
 *
 * @param val The string to search in
 * @param to_find The string to find
 *
 * @returns @c true, if @a val contains @a to_find. qc false otherwise
 */
bool contains(const std::string& val, const std::string& to_find)
{
    return std::regex_search(val, std::regex{to_find});
}

/**
 * @brief Checks if the given @a TestsOrTags need the InteroperabilityTestServer
 *
 * @param TestsOrTags The selected tests or tags to run
 *
 * @returns @c true, if the @a TestsOrTags require the InteroperabilityTestServer,
 * @c false otherwise
 */
bool needsServer(const std::vector<std::string>& TestsOrTags)
{
    if (TestsOrTags.empty())
    {
        // no tests or tags specified means "run all tests" so we need the server
        return true;
    }

    using std::any_of, std::cbegin, std::cend;

    // final regex: \\?\[needs-server\\?\]
    const auto NeedsServerTagRegex =
        R"(\\?\)" + NeedsServerTag.substr(0, NeedsServerTag.size() - 1)
        + R"(\\?\])";
    const auto ExcludedNeedsServerTagRegex = "~" + NeedsServerTagRegex;

    return any_of(cbegin(TestsOrTags), cend(TestsOrTags),
                  [&ExcludedNeedsServerTagRegex,
                   &NeedsServerTagRegex](const auto& TestOrTag) {
                      return !contains(TestOrTag, ExcludedNeedsServerTagRegex)
                             && contains(TestOrTag, NeedsServerTagRegex);
                  });
}

//============================================================================
int main(int argc, const char* const argv[])
{
    Catch::Session Session;  // There must be exactly one instance

    auto RetCode = Session.applyCommandLine(argc, argv);
    if (RetCode != 0)  // Indicates a command line error
    {
        return RetCode;
    }

    if (const auto& ConfigData = Session.configData();
        !ConfigData.showHelp && !ConfigData.listReporters
        && !ConfigData.listTestNamesOnly && !ConfigData.listTests
        && !ConfigData.listTags && needsServer(ConfigData.testsOrTags))
    {
        std::cout << "Starting Test Server\n";
        // The server to which the clients in the interoperability tests can
        // connect
        auto TestServer = InteroperabilityTestServer{
            {"InteroperabilityTest", "TestServer"}, {"::", ServerPort}};
        TestServer.runInsecure(false);

        RetCode = Session.run();

        TestServer.shutdown();
    }
    else
    {
        RetCode = Session.run();
    }
    return RetCode;
}
