/**
 ** This file is part of the sila_cpp project.
 ** Copyright (c) 2021 SiLA2
 **
 ** Permission is hereby granted, free of charge, to any person obtaining a copy
 ** of this software and associated documentation files (the "Software"), to deal
 ** in the Software without restriction, including without limitation the rights
 ** to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 ** copies of the Software, and to permit persons to whom the Software is
 ** furnished to do so, subject to the following conditions:
 **
 ** The above copyright notice and this permission notice shall be included in all
 ** copies or substantial portions of the Software.
 **
 ** THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 ** IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 ** FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 ** AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 ** LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 ** OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 ** SOFTWARE.
 **/

//============================================================================
/// \file   test_proto.cpp
/// \author Florian Meinicke (florian.meinicke@cetoni.de)
/// \date   05.02.2021
/// \brief  Unit tests for the Protobuf generation implementation
//============================================================================

//============================================================================
//                                  INCLUDES
//============================================================================
#include <sila_cpp/codegen/fdl/FDLSerializer.h>
#include <sila_cpp/codegen/proto/ProtobufGenerator.h>
#include <sila_cpp/common/logging.h>

#include <QFile>

//============================================================================
std::ostream& operator<<(std::ostream& os, const QByteArray& rhs)
{
    return os << '"' << rhs.toStdString() << '"';
}

#include <catch2/catch.hpp>

using namespace SiLA2::codegen::fdl;
using namespace SiLA2::codegen::proto;

//============================================================================
SCENARIO("Convert a Feature to proto", "[proto]")
{
    QLoggingCategory::setFilterRules("*.debug=true\n*.info=true");

    GIVEN("A Feature Definition read from a file and deserialized into a "
          "CFeature object")
    {
        const auto FileName = GENERATE(
            as<QString>{}, "Empty", "UnobservableCommand", "UnobservableCommands",
            "ObservableCommand", "ObservableCommands", "Property", "Metadata",
            "DataTypeDefinition");
        auto FDLFile = QFile{":/fdl/" + FileName + ".sila.xml"};
        REQUIRE(FDLFile.open(QFile::ReadOnly | QFile::Text));

        CFeature Feature;
        CFDLSerializer::deserialize(Feature, FDLFile.readAll());

        AND_GIVEN("the expected protobuf code")
        {
            auto ProtoFile = QFile{":/proto/" + FileName + ".proto"};
            REQUIRE(ProtoFile.open(QFile::ReadOnly | QFile::Text));

            WHEN("this Feature is converted into protobuf code")
            {
                const auto Protobuf =
                    CProtobufGenerator::generate(Feature).toStdString();

                THEN("the code is exactly as the expected code")
                {
                    REQUIRE_THAT(
                        Protobuf,
                        Catch::Contains(ProtoFile.readAll().toStdString()));
                }
            }
        }
    }
}
