cmake_minimum_required(VERSION 3.13)
project(sila_cpp_tests LANGUAGES CXX)

set(CMAKE_AUTOMOC ON)
set(CMAKE_AUTORCC ON)
set(CMAKE_CXX_STANDARD 17)
set(CMAKE_CXX_STANDARD_REQUIRED ON)

if(WIN32 AND CMAKE_COMPILER_IS_GNUCXX)
    # fix gcc error: File too big / too many sections
    set(CMAKE_CXX_FLAGS "${CMAKE_CXX_FLAGS} -Wa,-mbig-obj")
endif()

find_package(Catch2 REQUIRED)

# Find sila_cpp library
if(NOT TARGET sila_cpp)
    find_package(sila_cpp CONFIG REQUIRED)
else()
    include(${SILA_CPP_SOURCE_DIR}/cmake/sila_cppConfig.cmake)
endif()

get_target_property(SILA_CPP_BUILD_TYPE sila2::sila_cpp TYPE)

message(STATUS "sila_cpp: ${SILA_CPP_BUILD_TYPE}")

if(NOT "${SILA_CPP_BUILD_TYPE}" STREQUAL "SHARED_LIBRARY")
    message(WARNING "sila_cpp: Building the tests is currently only supported if sila_cpp was built as a shared library")
    return()
endif()

##############################################################################
#                            Interoperability Test
##############################################################################
add_subdirectory(interoperability/InteroperabilityTest)

##############################################################################
#                                 catch_main
##############################################################################
# a library just for the `main` where Catch2 is compiled in completely to reduce
# build times of the individual tests
add_library(catch_main SHARED catch_main.cpp)
target_link_libraries(catch_main PUBLIC
    -Wl,--whole-archive Catch2::Catch2
    -Wl,--no-whole-archive InteroperabilityTestServer
    )
set_target_properties(catch_main PROPERTIES INTERPROCEDURAL_OPTIMIZATION FALSE)

target_include_directories(catch_main PRIVATE
    ${CMAKE_CURRENT_BINARY_DIR}/interoperability/InteroperabilityTest/DataTypeProvider/grpc
    )

##############################################################################
#                               sila_cpp_tests
##############################################################################
add_executable(${PROJECT_NAME}
    test_sila_service.cpp
    test_data_types.cpp
    test_error_handling.cpp
    test_fully_qualified_identifiers.cpp
    test_fdl.cpp
    test_proto.cpp
    test_logging.cpp
    test.qrc
    )
target_link_libraries(${PROJECT_NAME} PRIVATE
    catch_main
    project_options
    # project_warnings
    InteroperabilityTest
    )

# copy the libs to the directory of the executable so that it can find them
add_custom_command(TARGET ${PROJECT_NAME} POST_BUILD
    COMMAND ${CMAKE_COMMAND} -E copy
    $<TARGET_FILE:sila2::sila_cpp>
    $<TARGET_FILE:sila2::sila_cpp_common>
    $<TARGET_FILE:sila2::sila_cpp_codegen>
    $<TARGET_FILE:QtZeroConf>
    $<TARGET_FILE:InteroperabilityTestFeatures>
    $<TARGET_FILE:InteroperabilityTestServer>
    $<TARGET_FILE:InteroperabilityTestClient>
    $<TARGET_FILE_DIR:${PROJECT_NAME}>
    )

##############################################################################
#                                    CTest
##############################################################################
include(CTest)
include(Catch)
catch_discover_tests(${PROJECT_NAME})

##############################################################################
#                                  Coverage
##############################################################################
if(SILA_CPP_ENABLE_COVERAGE)
    file(DOWNLOAD https://raw.githubusercontent.com/bilke/cmake-modules/40787eff7923f4532ba9bb4805eadb1924f8ad8f/CodeCoverage.cmake ${CMAKE_CURRENT_BINARY_DIR}/CodeCoverage.cmake)
    include(${CMAKE_CURRENT_BINARY_DIR}/CodeCoverage.cmake)

    set(GCOVR_ADDITIONAL_ARGS
        --print-summary
        )
    # html output for local report
    setup_target_for_coverage_gcovr_html(
        NAME coverage_html
        EXECUTABLE ctest --output-on-failure
        DEPENDENCIES ${PROJECT_NAME}
        BASE_DIRECTORY ..
        EXCLUDE "examples/*" "test/*" "third_party/*" "*build*/*" ".conan/*"
    )
    # xml output for GitLab
    setup_target_for_coverage_gcovr_xml(
        NAME coverage_xml
        EXECUTABLE ctest --output-on-failure
        DEPENDENCIES ${PROJECT_NAME}
        BASE_DIRECTORY ..
        EXCLUDE "examples/*" "test/*" "third_party/*" "*build*/*" ".conan/*"
    )
endif()
