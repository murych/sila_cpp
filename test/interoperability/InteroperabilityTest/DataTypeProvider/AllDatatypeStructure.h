/**
 ** This file is part of the sila_cpp project.
 **
 ** Permission is hereby granted, free of charge, to any person obtaining a copy
 ** of this software and associated documentation files (the "Software"), to deal
 ** in the Software without restriction, including without limitation the rights
 ** to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 ** copies of the Software, and to permit persons to whom the Software is
 ** furnished to do so, subject to the following conditions:
 **
 ** The above copyright notice and this permission notice shall be included in all
 ** copies or substantial portions of the Software.
 **
 ** THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 ** IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 ** FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 ** AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 ** LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 ** OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 ** SOFTWARE.
 **/

//============================================================================
/// \file    AllDatatypeStructure.h
/// \authors Florian Meinicke
/// \date    2020-08-25
/// \brief   Definition of the AllDatatypeStructure Data Type for the
///          DataTypeProvider Feature
//============================================================================
#ifndef ALLDATATYPESTRUCTURE_H
#define ALLDATATYPESTRUCTURE_H

//============================================================================
//                                  INCLUDES
//============================================================================
#include "DataTypeProvider.grpc.pb.h"

/**
 * @brief Convenience type for the DataType_AllDatatypeStructure Protobuf Message
 */
struct AllDatatypeStructure
{
    SiLA2::CString StringStructureElement{};
    SiLA2::CInteger IntegerStructureElement{};
    SiLA2::CReal RealStructureElement{};
    SiLA2::CBoolean BooleanStructureElement{};
    SiLA2::CBinary BinaryStructureElement{};
    SiLA2::CDate DateStructureElement{};
    SiLA2::CTime TimeStructureElement{};
    SiLA2::CTimestamp TimestampStructureElement{};

    [[nodiscard]] sila2::org::silastandard::test::datatypeprovider::v1::
        DataType_AllDatatypeStructure::AllDatatypeStructure_Struct*
        makeNestedMessage() const
    {
        using sila2::org::silastandard::test::datatypeprovider::v1::
            DataType_AllDatatypeStructure;
        auto* Result =
            new DataType_AllDatatypeStructure::AllDatatypeStructure_Struct{};
        Result->set_allocated_stringstructureelement(
            StringStructureElement.toProtoMessagePtr());
        Result->set_allocated_integerstructureelement(
            IntegerStructureElement.toProtoMessagePtr());
        Result->set_allocated_realstructureelement(
            RealStructureElement.toProtoMessagePtr());
        Result->set_allocated_booleanstructureelement(
            BooleanStructureElement.toProtoMessagePtr());
        Result->set_allocated_binarystructureelement(
            BinaryStructureElement.toProtoMessagePtr());
        Result->set_allocated_datestructureelement(
            DateStructureElement.toProtoMessagePtr());
        Result->set_allocated_timestructureelement(
            TimeStructureElement.toProtoMessagePtr());
        Result->set_allocated_timestampstructureelement(
            TimestampStructureElement.toProtoMessagePtr());
        return Result;
    }

    /**
     * @brief Convert this convenience type to a SiLAFramework type, i.e. the
     * Protobuf Message
     *
     * @return The SiLAFramework equivalent of this type as a Protobuf Message
     */
    [[nodiscard]] sila2::org::silastandard::test::datatypeprovider::v1::
        DataType_AllDatatypeStructure
        toProtoMessage() const
    {
        using sila2::org::silastandard::test::datatypeprovider::v1::
            DataType_AllDatatypeStructure;
        auto Result = DataType_AllDatatypeStructure{};
        Result.set_allocated_alldatatypestructure(makeNestedMessage());
        return Result;
    }

    /**
     * @brief Convert this convenience type to a SiLAFramework type, i.e. the
     * Protobuf Message
     *
     * @return The SiLAFramework equivalent of this type as a Protobuf Message
     * pointer
     */
    [[nodiscard]] sila2::org::silastandard::test::datatypeprovider::v1::
        DataType_AllDatatypeStructure*
        toProtoMessagePtr() const
    {
        using sila2::org::silastandard::test::datatypeprovider::v1::
            DataType_AllDatatypeStructure;
        auto* Result = new DataType_AllDatatypeStructure{};
        Result->set_allocated_alldatatypestructure(makeNestedMessage());
        return Result;
    }
};

#endif  // ALLDATATYPESTRUCTURE_H
