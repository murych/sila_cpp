/**
 ** This file is part of the sila_cpp project.
 ** Copyright 2020 SiLA2
 **
 ** Permission is hereby granted, free of charge, to any person obtaining a copy
 ** of this software and associated documentation files (the "Software"), to deal
 ** in the Software without restriction, including without limitation the rights
 ** to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 ** copies of the Software, and to permit persons to whom the Software is
 ** furnished to do so, subject to the following conditions:
 **
 ** The above copyright notice and this permission notice shall be included in all
 ** copies or substantial portions of the Software.
 **
 ** THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 ** IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 ** FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 ** AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 ** LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 ** OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 ** SOFTWARE.
 **/

//============================================================================
/// \file   test_globals.h
/// \author Florian Meinicke (florian.meinicke@cetoni.de)
/// \date   26.08.2020
/// \brief  Declaration of global variables used in the interoperability unit
///         tests
//============================================================================
#ifndef TEST_GLOBALS_H
#define TEST_GLOBALS_H

#include <sila_cpp/client/SiLAClient.h>

#include <string>

// these come from the source files that contain the `main()` of the unit test
// applications
extern std::string ServerIP;
extern std::string ServerPort;
// Each test case or scenario that needs a SiLA 2 Server to run or to connect to,
// needs to have this tag in its name *and* its tags. Otherwise, the `main`
// function won't start the server.
extern const std::string NeedsServerTag;

class CSiLATestClient : public SiLA2::CSiLAClient
{
public:
    using CSiLAClient::CSiLAClient;

    [[nodiscard]] bool acceptUntrustedServerCertificate(
        [[maybe_unused]] const QSslCertificate& Cert) const override
    {
        qInfo() << "Accepting insecure certificate";
        return true;
    }
};

#endif  // TEST_GLOBALS_H
