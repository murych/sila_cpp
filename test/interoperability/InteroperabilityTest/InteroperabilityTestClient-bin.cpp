/**
 ** This file is part of the sila_cpp project.
 **
 ** Permission is hereby granted, free of charge, to any person obtaining a copy
 ** of this software and associated documentation files (the "Software"), to deal
 ** in the Software without restriction, including without limitation the rights
 ** to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 ** copies of the Software, and to permit persons to whom the Software is
 ** furnished to do so, subject to the following conditions:
 **
 ** The above copyright notice and this permission notice shall be included in all
 ** copies or substantial portions of the Software.
 **
 ** THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 ** IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 ** FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 ** AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 ** LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 ** OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 ** SOFTWARE.
 **/

//============================================================================
/// \file    InteroperabilityTestClient-bin.cpp
/// \authors Florian Meinicke
/// \date    2020-08-24
/// \brief   Standalone InteroperabilityTest client application
//============================================================================

//============================================================================
//                                  INCLUDES
//============================================================================
#include <sila_cpp/common/ServerAddress.h>

#include "InteroperabilityTestClient.h"

#include <QCommandLineParser>
#include <QCoreApplication>

/**
 * @brief Looking for command line arguments
 *
 * @param ServerAddress The default server address that will be used if not given
 * on the command line
 *
 * @return tupleCServerAddress, bool, CSSLCredentials> A tuple with
 * the server's address (IP and port), a @c bool indicating whether to use
 * unencrypted communication, and SSL credentials to use for encrypted
 * communication
 */
std::tuple<SiLA2::CServerAddress, bool, SiLA2::CSSLCredentials> parseCommandLine(
    SiLA2::CServerAddress ServerAddress)
{
    QCommandLineParser Parser;
    Parser.setApplicationDescription("A SiLA2 client: InteroperabilityTest");
    Parser.addHelpOption();
    Parser.addVersionOption();

    Parser.addOptions(
        {{{"s", "server-host"},
          "The IP address or hostname of the SiLA server to connect "
          "with\n(default: "
              + ServerAddress.ip() + ')',
          "ip-or-hostname",
          ServerAddress.ip()},
         {{"p", "server-port"},
          "The port on which the SiLA server runs\n(default: "
              + ServerAddress.port() + ')',
          "port",
          ServerAddress.port()},
         {{"r", "root-ca"},
          "Root certificate authority that signed the \033[3mserver's\033[0m "
          "certificate. If the server has a self-signed certificate that wasn't "
          "signed by a CA then use the server's certificate here. If left empty "
          "the client will try to automatically fetch the server's certificate "
          "and use that as the root certificate.\n(default: empty)",
          "filename"},
         {{"c", "encryption-cert"},
          "SSL certificate filename, e.g. 'client-ssl.crt' Note that the client "
          "doesn't necessarily require a certificate.\n(default: empty)",
          "filename"},
         {{"k", "encryption-key"},
          "SSL key filename, e.g. 'client-ssl.key' Note that the client doesn't "
          "necessarily require a key.\n(default: empty)",
          "filename"},
         {{"i", "force-insecure"},
          "Forces the client to connect to the given SiLA server using insecure "
          "(i.e. unencrypted) communication. Note that this should not be used "
          "in production but for debugging only!"}});

    Parser.process(QCoreApplication::arguments());

    // Server Address / Hostname
    if (Parser.isSet("server-host"))
    {
        ServerAddress.setIP(Parser.value("server-host"));
    }
    if (Parser.isSet("server-port"))
    {
        ServerAddress.setPort(Parser.value("server-port"));
    }

    // SSL Certificate
    SiLA2::CSSLCredentials Credentials;

    const auto RootCA = Parser.value("root-ca");
    const auto Certificate = Parser.value("encryption-cert");
    const auto Key = Parser.value("encryption-key");
    const auto HasRootCA = RootCA.isEmpty();
    const auto HasCertificate = Certificate.isEmpty();
    const auto HasKey = Key.isEmpty();

    if (Parser.isSet("insecure") && (HasRootCA || HasCertificate || HasKey))
    {
        throw std::invalid_argument{
            "Cannot use '--insecure' in combination with '--root-ca', "
            "'--encryption-cert' or '--encryption-key'"};
    }

    if ((HasCertificate && !HasKey) || (!HasCertificate && HasKey))
    {
        throw std::invalid_argument{"Either provide both of '--encryption-cert' "
                                    "and '--encryption-key' or none of them"};
    }

    if (HasRootCA)
    {
        Credentials.setRootCAFromFile(RootCA);
    }
    if (HasCertificate)
    {
        Credentials.setCertificateFromFile(Certificate);
    }
    if (HasKey)
    {
        Credentials.setKeyFromFile(Key);
    }

    return {ServerAddress, Parser.isSet("force-insecure"), Credentials};
}
//============================================================================
int main(int argc, char* argv[])
{
    QCoreApplication App{argc, argv};
    QCoreApplication::setApplicationName("InteroperabilityTestClient");
    QCoreApplication::setApplicationVersion("0.0.1");

    using namespace std::string_literals;

    const auto [ServerAddress, ForceInsecure, Credentials] =
        parseCommandLine({"127.0.0.1"s, "50051"s});

    // Create and start the client
    auto SiLAClient = InteroperabilityTestClient{ServerAddress};
    if (ForceInsecure)
    {
        SiLAClient.connectInsecure();
    }
    else if (!Credentials.isEmpty())
    {
        SiLAClient.connect(Credentials);
    }
    else
    {
        SiLAClient.connect();
    }

    // Commands
    std::ignore = SiLAClient.EchoStringValue();
    std::ignore = SiLAClient.EchoIntegerValue();
    std::ignore = SiLAClient.EchoRealValue();
    std::ignore = SiLAClient.EchoBooleanValue();
    std::ignore = SiLAClient.EchoBinaryValue();
    std::ignore = SiLAClient.EchoDateValue();
    std::ignore = SiLAClient.EchoTimeValue();
    std::ignore = SiLAClient.EchoTimeStampValue();
    std::ignore = SiLAClient.EchoStringList();
    std::ignore = SiLAClient.EchoIntegerList();
    std::ignore = SiLAClient.EchoStructureValues();
    std::ignore = SiLAClient.EchoMultipleMixedValues();
    std::ignore = SiLAClient.GetMultipleMixedValues();

    // Properties
    std::ignore = SiLAClient.Get_StringValue();
    std::ignore = SiLAClient.Get_IntegerValue();
    std::ignore = SiLAClient.Get_RealValue();
    std::ignore = SiLAClient.Get_BooleanValue();
    std::ignore = SiLAClient.Get_BinaryValue();
    std::ignore = SiLAClient.Get_DateValue();
    std::ignore = SiLAClient.Get_TimeValue();
    std::ignore = SiLAClient.Get_TimeStampValue();
    std::ignore = SiLAClient.Get_StringList();
    std::ignore = SiLAClient.Get_IntegerList();
    std::ignore = SiLAClient.Get_StructureValues();

    return 0;
}
