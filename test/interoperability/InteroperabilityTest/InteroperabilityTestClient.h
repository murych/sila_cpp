/**
 ** This file is part of the sila_cpp project.
 **
 ** Permission is hereby granted, free of charge, to any person obtaining a copy
 ** of this software and associated documentation files (the "Software"), to deal
 ** in the Software without restriction, including without limitation the rights
 ** to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 ** copies of the Software, and to permit persons to whom the Software is
 ** furnished to do so, subject to the following conditions:
 **
 ** The above copyright notice and this permission notice shall be included in all
 ** copies or substantial portions of the Software.
 **
 ** THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 ** IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 ** FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 ** AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 ** LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 ** OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 ** SOFTWARE.
 **/

//============================================================================
/// \file    InteroperabilityTestClient.h
/// \authors Florian Meinicke
/// \date    2020-08-24
/// \brief   Declaration of the InteroperabilityTestClient class
//============================================================================
#ifndef INTEROPERABILITYTESTCLIENT_H
#define INTEROPERABILITYTESTCLIENT_H

//============================================================================
//                                  INCLUDES
//============================================================================
#include <sila_cpp/client/SiLAClient.h>
#include <sila_cpp/data_types.h>

// include the gRPC classes for the SiLA Features
#include "DataTypeProvider.grpc.pb.h"
#include "DataTypeProvider/AllDatatypeStructure.h"

/**
 * @brief A service with all standard test features that can be used to test
 * sila_cpp against the other reference implementations
 */
class InteroperabilityTestClient : public SiLA2::CSiLAClient
{
public:
    explicit InteroperabilityTestClient(const SiLA2::CServerAddress& Address);

    void connect() override;

    using CSiLAClient::connect;

    [[nodiscard]] bool acceptUntrustedServerCertificate(
        const QSslCertificate&) const override;

    /**
     * @brief Call the Unobservable command EchoStringValue on the server
     *
     * @param StringValue The string value to echo.
     *
     * @return EchoStringValue_Responses The Command Response
     * It contains the following fields:
     * @li ReceivedStringValue A message containing the string value that has
     * been received.
     */
    sila2::org::silastandard::test::datatypeprovider::v1::EchoStringValue_Responses
    EchoStringValue(const SiLA2::CString& StringValue = {}) const;

    /**
     * @brief Call the Unobservable command EchoIntegerValue on the server
     *
     * @param IntegerValue The integer value to echo.
     *
     * @return EchoIntegerValue_Responses The Command Response
     * It contains the following fields:
     * @li ReceivedIntegerValue A message containing the integer value that
     * has been received.
     */
    sila2::org::silastandard::test::datatypeprovider::v1::EchoIntegerValue_Responses
    EchoIntegerValue(const SiLA2::CInteger& IntegerValue = {}) const;

    /**
     * @brief Call the Unobservable command EchoRealValue on the server
     *
     * @param RealValue The real value to be echo.
     *
     * @return EchoRealValue_Responses The Command Response
     * It contains the following fields:
     * @li ReceivedRealValue A message containing the real value that has
     * been received.
     */
    sila2::org::silastandard::test::datatypeprovider::v1::EchoRealValue_Responses
    EchoRealValue(const SiLA2::CReal& RealValue = {}) const;

    /**
     * @brief Call the Unobservable command EchoBooleanValue on the server
     *
     * @param BooleanValue The boolean value to echo.
     *
     * @return EchoBooleanValue_Responses The Command Response
     * It contains the following fields:
     * @li ReceivedBooleanValue A message containing the boolean value that
     * has been received.
     */
    sila2::org::silastandard::test::datatypeprovider::v1::EchoBooleanValue_Responses
    EchoBooleanValue(const SiLA2::CBoolean& BooleanValue = {}) const;

    /**
     * @brief Call the Unobservable command EchoBinaryValue on the server
     *
     * @param BinaryValue The Binary value to be echo.
     *
     * @return EchoBinaryValue_Responses The Command Response
     * It contains the following fields:
     * @li ReceivedBinaryValue A message containing the Binary value that has
     * been received.
     */
    sila2::org::silastandard::test::datatypeprovider::v1::EchoBinaryValue_Responses
    EchoBinaryValue(const SiLA2::CBinary& BinaryValue = {}) const;

    /**
     * @brief Call the Unobservable command EchoDateValue on the server
     *
     * @param DateValue The date value to echo.
     *
     * @return EchoDateValue_Responses The Command Response
     * It contains the following fields:
     * @li ReceivedDateValue A message containing the date value that has
     * been received.
     */
    sila2::org::silastandard::test::datatypeprovider::v1::EchoDateValue_Responses
    EchoDateValue(const SiLA2::CDate& DateValue = {}) const;

    /**
     * @brief Call the Unobservable command EchoTimeValue on the server
     *
     * @param TimeValue The time value to echo.
     *
     * @return EchoTimeValue_Responses The Command Response
     * It contains the following fields:
     * @li ReceivedTimeValue A message containing the time value that has
     * been received.
     */
    sila2::org::silastandard::test::datatypeprovider::v1::EchoTimeValue_Responses
    EchoTimeValue(const SiLA2::CTime& TimeValue = {}) const;

    /**
     * @brief Call the Unobservable command EchoTimeStampValue on the server
     *
     * @param TimestampValue The time stamp value to echo.
     *
     * @return EchoTimeStampValue_Responses The Command Response
     * It contains the following fields:
     * @li ReceivedValue A message containing the time stamp value that has
     * been received.
     */
    sila2::org::silastandard::test::datatypeprovider::v1::EchoTimeStampValue_Responses
    EchoTimeStampValue(const SiLA2::CTimestamp& TimestampValue = {}) const;

    /**
     * @brief Call the Unobservable command EchoStringList on the server
     *
     * @param StringList The list of String values to echo.
     *
     * @return EchoStringList_Responses The Command Response
     * It contains the following fields:
     * @li ReceivedValues A message containing the string values that have
     * been received.
     */
    sila2::org::silastandard::test::datatypeprovider::v1::EchoStringList_Responses
    EchoStringList(const std::vector<SiLA2::CString>& StringList = {}) const;

    /**
     * @brief Call the Unobservable command EchoIntegerList on the server
     *
     * @param IntegerList The list of integer values to echo.
     *
     * @return EchoIntegerList_Responses The Command Response
     * It contains the following fields:
     * @li ReceivedValues A message containing the integer values that have
     * been received.
     */
    sila2::org::silastandard::test::datatypeprovider::v1::EchoIntegerList_Responses
    EchoIntegerList(const std::vector<SiLA2::CInteger>& IntegerList = {}) const;

    /**
     * @brief Call the Unobservable command EchoStructureValues on the server
     *
     * @param ReceivedValues A structure containing values of different types
     * that have been received
     *
     * @return EchoStructureValues_Responses The Command Response
     * It contains the following fields:
     * None
     */
    sila2::org::silastandard::test::datatypeprovider::v1::EchoStructureValues_Responses
    EchoStructureValues(const AllDatatypeStructure& ReceivedValues = {}) const;

    /**
     * @brief Call the Unobservable command EchoMultipleMixedValues on the server
     *
     * @param StringValue The string value to echo.
     * @param IntegerValue The integer value to echo.
     * @param RealValue The real value to echo.
     * @param BooleanValue The boolean value to echo.
     * @param DateValue The date value to echo.
     * @param TimeValue The time value to echo.
     * @param TimestampValue The time stamp value to echo.
     *
     * @return EchoMultipleMixedValues_Responses The Command Response
     * It contains the following fields:
     * @li ReceivedValues A message containing the values that have been
     * received.
     */
    sila2::org::silastandard::test::datatypeprovider::v1::
        EchoMultipleMixedValues_Responses
        EchoMultipleMixedValues(
            const SiLA2::CString& StringValue = {},
            const SiLA2::CInteger& IntegerValue = {},
            const SiLA2::CReal& RealValue = {},
            const SiLA2::CBoolean& BooleanValue = {},
            const SiLA2::CDate& DateValue = {},
            const SiLA2::CTime& TimeValue = {},
            const SiLA2::CTimestamp& TimestampValue = {}) const;

    /**
     * @brief Call the Unobservable command GetMultipleMixedValues on the server
     *
     * @param Parameters The following Command Parameters:
     * None
     *
     * @return GetMultipleMixedValues_Responses The Command Response
     * It contains the following fields:
     * @li StringValue Returns the string value 'SiLA2_Test_String_Value'.
     * @li IntegerValue Returns the integer value '5124'.
     * @li RealValue Returns the real value '3.1415926'.
     * @li BooleanValue Returns the boolean value 'true'.
     * @li DateValue Returns the date value '2018-08-24' in ISO 8601 format,
     * corresponding to US '08/24/2018'.
     * @li TimeValue Returns the time value '12:34:56.789'.
     * @li TimeStampValue Returns the time stamp value '2018-08-24 12:34:56'.
     */
    sila2::org::silastandard::test::datatypeprovider::v1::
        GetMultipleMixedValues_Responses
        GetMultipleMixedValues() const;

    /**
     * @brief Request the Unobservable Property StringValue from the server
     *
     * @return The value of the Property
     */
    sila2::org::silastandard::test::datatypeprovider::v1::Get_StringValue_Responses
    Get_StringValue() const;

    /**
     * @brief Request the Unobservable Property IntegerValue from the server
     *
     * @return The value of the Property
     */
    sila2::org::silastandard::test::datatypeprovider::v1::Get_IntegerValue_Responses
    Get_IntegerValue() const;

    /**
     * @brief Request the Unobservable Property RealValue from the server
     *
     * @return The value of the Property
     */
    sila2::org::silastandard::test::datatypeprovider::v1::Get_RealValue_Responses
    Get_RealValue() const;

    /**
     * @brief Request the Unobservable Property BooleanValue from the server
     *
     * @return The value of the Property
     */
    sila2::org::silastandard::test::datatypeprovider::v1::Get_BooleanValue_Responses
    Get_BooleanValue() const;

    /**
     * @brief Request the Unobservable Property BinaryValue from the server
     *
     * @return The value of the Property
     */
    sila2::org::silastandard::test::datatypeprovider::v1::Get_BinaryValue_Responses
    Get_BinaryValue() const;

    /**
     * @brief Request the Unobservable Property DateValue from the server
     *
     * @return The value of the Property
     */
    sila2::org::silastandard::test::datatypeprovider::v1::Get_DateValue_Responses
    Get_DateValue() const;

    /**
     * @brief Request the Unobservable Property TimeValue from the server
     *
     * @return The value of the Property
     */
    sila2::org::silastandard::test::datatypeprovider::v1::Get_TimeValue_Responses
    Get_TimeValue() const;

    /**
     * @brief Request the Unobservable Property TimeStampValue from the server
     *
     * @return The value of the Property
     */
    sila2::org::silastandard::test::datatypeprovider::v1::Get_TimeStampValue_Responses
    Get_TimeStampValue() const;

    /**
     * @brief Request the Unobservable Property StringList from the server
     *
     * @return The value of the Property
     */
    sila2::org::silastandard::test::datatypeprovider::v1::Get_StringList_Responses
    Get_StringList() const;

    /**
     * @brief Request the Unobservable Property IntegerList from the server
     *
     * @return The value of the Property
     */
    sila2::org::silastandard::test::datatypeprovider::v1::Get_IntegerList_Responses
    Get_IntegerList() const;

    /**
     * @brief Request the Unobservable Property StructureValues from the server
     *
     * @return The value of the Property
     */
    sila2::org::silastandard::test::datatypeprovider::v1::Get_StructureValues_Responses
    Get_StructureValues() const;

private:
    std::unique_ptr<sila2::org::silastandard::test::datatypeprovider::v1::
                        DataTypeProvider::Stub>
        m_DataTypeProviderStub;
};

#endif  // INTEROPERABILITYTESTCLIENT_H
