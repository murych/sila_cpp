<!--- Provide a general summary of your changes in the Title above -->

## Description
<!--- Describe your changes in detail -->


## What is the current behavior?
<!-- You can also link to an open issue here -->


## Does this MR introduce a breaking changes?
<!-- What changes might users need to make in their application due to this MR? -->


## Other information
<!-- Anything else that is important -->


## Checklist:
<!--- Go over all the following points, and put an `x` in all the boxes that apply. -->
<!--- If you're unsure about any of these, don't hesitate to ask. We're here to help! -->
- [ ] The commit messages follow our guidelines
- [ ] Tests have been added so that this bug will be caught in the future
- [ ] Docs have been added / updated
- [ ] A new changelog fragment with a **Fixed** section has been added to the [changelog.d](changelog.d) directory (we use [scriv](https://pypi.org/project/scriv/))

/assign_reviewer @FMeinicke
/target_branch develop
