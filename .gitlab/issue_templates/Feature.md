## Description
<!-- (Describe what the new feature should do concisely) -->


## Use cases / Benefits
<!-- (Why do you think the new feature could be useful) -->


## Proposals
<!-- (Do you already have an idea of how to implement the new feature or
what new things might be required for the feature to work properly) -->


/label ~Feature
/assign @FMeinicke
