## Summary
<!-- (Summarize the bug encountered concisely) -->


## Context
<!-- (Please provide any relevant information about your setup.
This is important in case the issue is not reproducible except for under certain conditions) -->

- `sila_cpp` version:
- Operating System + version:
- Compiler + version:


## Steps to reproduce
<!-- (How one can reproduce the issue - this is very important) -->


## What is the current bug behavior?
<!-- (What actually happens) -->


## What is the expected correct behavior?
<!-- (What you should see instead) -->


## Relevant logs and/or screenshots
<!-- (Paste any relevant logs - please use code blocks (```) to format console output,
logs, and code as it's very hard to read otherwise.
Also, set the environment variable `SILA_CPP_LOGGING_LEVEL` to `debug` prior to running your application(s).) -->


## Possible fixes
<!-- (If you can, link to the line of code that might be responsible for the problem) -->


/label ~Bug
/assign @FMeinicke
