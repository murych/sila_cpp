cmake_minimum_required(VERSION 3.19)

# set a default CXX standard for the tools and targets that do not specify them.
# If commented, the latest supported standard for your compiler is automatically set.
set(CMAKE_CXX_STANDARD 17)

list(APPEND CMAKE_MODULE_PATH ${CMAKE_CURRENT_SOURCE_DIR}/cmake/modules)

if(NOT CONAN_EXPORTED)
    file(DOWNLOAD "https://gist.githubusercontent.com/FMeinicke/102ae2fb4c9c7a3459932f7e06b1be03/raw/9b3ec61ec1719b5612e0b8f18bef8b5924bdf1a2/hex.cmake" ${CMAKE_CURRENT_BINARY_DIR}/hex.cmake)
    include(${CMAKE_CURRENT_BINARY_DIR}/hex.cmake)
    include(GetGitRevisionDescription)

    git_describe(GitTagVersion --tags --abbrev=8)
    string(REGEX REPLACE "^v([0-9]+)\\..*" "\\1" VERSION_MAJOR "${GitTagVersion}")
    string(REGEX REPLACE "^v[0-9]+\\.([0-9]+).*" "\\1" VERSION_MINOR "${GitTagVersion}")
    string(REGEX REPLACE "^v[0-9]+\\.[0-9]+\\.([0-9]+).*" "\\1" VERSION_PATCH "${GitTagVersion}")
    string(REGEX REPLACE "^v[0-9]+\\.[0-9]+\\.[0-9]+-([0-9]+).*" "\\1" VERSION_COMMITS "${GitTagVersion}")
    string(REGEX REPLACE "^v[0-9]+\\.[0-9]+\\.[0-9]+-[0-9]+-g([a-z0-9]+)$" "\\1" VERSION_HASH "${GitTagVersion}")

    set(VERSION_SHORT "${VERSION_MAJOR}.${VERSION_MINOR}.${VERSION_PATCH}")
    if("${VERSION_COMMITS}" STREQUAL "${GitTagVersion}" AND "${VERSION_HASH}" STREQUAL "${GitTagVersion}")
        set(VERSION_LONG "${VERSION_SHORT}")
    else()
        from_hex("0x${VERSION_HASH}" VERSION_HASH_DEC)
        set(VERSION_SHORT "${VERSION_SHORT}.${VERSION_HASH_DEC}")
        set(VERSION_LONG "${VERSION_SHORT}-${VERSION_COMMITS}-g${VERSION_HASH}")
    endif()
    message(STATUS "sila_cpp: version ${VERSION_LONG}")
endif()

project(sila_cpp LANGUAGES CXX VERSION ${VERSION_SHORT})

set(SILA_CPP_SOURCE_DIR "${PROJECT_SOURCE_DIR}")

if("${PROJECT_SOURCE_DIR}" STREQUAL "${CMAKE_SOURCE_DIR}")
    set(SILA_CPP_BUILD_STANDALONE ON)
else()
    # we're included as a submodule in another project
    set(SILA_CPP_BUILD_STANDALONE OFF)
endif()

# Library version
##############################################################################
add_library(project_version INTERFACE)

string(TIMESTAMP CURRENT_YEAR "%Y")

if(WIN32)
    include(GenerateProductVersion.cmake/GenerateProductVersion)

    generateproductversion(
        Win32VersionInfoFiles
        NAME "${PROJECT_NAME}"
        COMMENTS "${PROJECT_NAME} v${VERSION_LONG}"
        COMPANY_NAME "SiLA"
        COMPANY_COPYRIGHT "SiLA (C) 2020-${CURRENT_YEAR}"
        FILE_DESCRIPTION "Official SiLA 2 C++ reference implementation"
    )

    add_library(project_version_obj OBJECT ${Win32VersionInfoFiles})
    target_sources(project_version INTERFACE $<TARGET_OBJECTS:project_version_obj>)
endif()

set_target_properties(project_version
    PROPERTIES
    VERSION ${VERSION_LONG}
    SOVERSION ${VERSION_MAJOR}
    COMPATIBLE_INTERFACE_STRING "${PROJECT_NAME}_MAJOR_VERSION"
    "INTERFACE_${PROJECT_NAME}_MAJOR_VERSION" ${VERSION_MAJOR}
    )

# Options
##############################################################################
option(BUILD_SHARED_LIBS "Build sila_cpp as a shared library" ON)
option(BUILD_TESTING "Build tests" OFF)
option(SILA_CPP_BUILD_EXAMPLES "Build examples" ${SILA_CPP_BUILD_STANDALONE})
option(SILA_CPP_ENABLE_COVERAGE "Enable coverage reporting for gcc/clang" OFF)

option(SILA_CPP_USE_CONAN "Enable the usage of Conan package manager for all third party dependencies" OFF)
option(SILA_CPP_CATCH2_PROVIDER "Whether to use an already installed Catch2 from the 'system' (the default) or install it from 'conan'" "system")
option(SILA_CPP_GRPC_PROVIDER "Whether to use an already installed gRPC from the 'system' (the default) or install it from 'conan'" "system")
option(SILA_CPP_PROTOBUF_PROVIDER "Whether to use an already installed protobuf from the 'system' (the default) or install it from 'conan'" "system")
option(SILA_CPP_QT_PROVIDER "Whether to use an already installed Qt from the 'system' (the default) or install it from 'conan'" "system")
option(SILA_CPP_XERCESC_PROVIDER "Whether to use an already installed XercesC from the 'system' (the default) or install it from 'conan'" "system")

# QtZeroConf dependency
##############################################################################
set(SAVED_BUILD_SHARED_LIBS ${BUILD_SHARED_LIBS})
set(BUILD_SHARED_LIBS ON) # force shared build for QtZeroConf
add_subdirectory(third_party/QtZeroConf)
set_target_properties(QtZeroConf PROPERTIES DEBUG_POSTFIX "d")
set(BUILD_SHARED_LIBS ${SAVED_BUILD_SHARED_LIBS})

# Add project_options
# https://github.com/aminya/project_options
##############################################################################
include(FetchContent)
fetchcontent_declare(_project_options URL https://github.com/aminya/project_options/archive/refs/tags/v0.24.1.zip)
fetchcontent_makeavailable(_project_options)
include(${_project_options_SOURCE_DIR}/Index.cmake)

# Add dynamic CMake options
include(${_project_options_SOURCE_DIR}/src/DynamicProjectOptions.cmake)

# default values for dynamic CMake options
set(ENABLE_COVERAGE_DEFAULT ${SILA_CPP_ENABLE_COVERAGE})
set(ENABLE_CACHE_DEFAULT OFF)
# disable these for now
set(WARNINGS_AS_ERRORS_DEVELOPER_DEFAULT OFF)
set(ENABLE_CLANG_TIDY_DEVELOPER_DEFAULT OFF)
set(ENABLE_CPPCHECK_DEVELOPER_DEFAULT OFF)

set(ENABLE_CONAN_DEFAULT ${SILA_CPP_USE_CONAN} OR ${CONAN_WITH_CATCH2}
    OR ${CONAN_WITH_GRPC} OR ${CONAN_WITH_PROTOBUF} OR ${CONAN_WITH_QT}
    OR ${CONAN_WITH_XERCESC}
    )

# Conan options
set(CONAN_WITH_CATCH2 "False")
if(SILA_CPP_CATCH2_PROVIDER STREQUAL "conan" OR ${SILA_CPP_USE_CONAN})
    set(CONAN_WITH_CATCH2 "True")
endif()
set(CONAN_WITH_GRPC "False")
if(SILA_CPP_GRPC_PROVIDER STREQUAL "conan" OR ${SILA_CPP_USE_CONAN})
    set(CONAN_WITH_GRPC "True")
endif()
set(CONAN_WITH_PROTOBUF "False")
if(SILA_CPP_PROTOBUF_PROVIDER STREQUAL "conan" OR ${SILA_CPP_USE_CONAN})
    set(CONAN_WITH_PROTOBUF "True")
endif()
set(CONAN_WITH_QT "False")
if(SILA_CPP_QT_PROVIDER STREQUAL "conan" OR ${SILA_CPP_USE_CONAN})
    set(CONAN_WITH_QT "True")
endif()
set(CONAN_WITH_XERCESC "False")
if(SILA_CPP_XERCESC_PROVIDER STREQUAL "conan" OR ${SILA_CPP_USE_CONAN})
    set(CONAN_WITH_XERCESC "True")
endif()
set(CONAN_SHARED_BUILD "False")
if(${BUILD_SHARED_LIBS})
    set(CONAN_SHARED_BUILD "True")
endif()

# Initialize project_options variable related to this project
# This overwrites `project_options` and sets `project_warnings`
# uncomment to enable the options. Some of them accept one or more inputs:
dynamic_project_options(
    # WARNINGS_AS_ERRORS
    # ENABLE_COVERAGE
    # ENABLE_CPPCHECK
    # ENABLE_CLANG_TIDY
    # ENABLE_VS_ANALYSIS
    # ENABLE_INCLUDE_WHAT_YOU_USE
    # ENABLE_CACHE
    # ENABLE_PCH
    # ENABLE_CONAN
    # ENABLE_VCPKG
    # ENABLE_DOXYGEN
    # ENABLE_INTERPROCEDURAL_OPTIMIZATION
    # ENABLE_NATIVE_OPTIMIZATION
    # ENABLE_USER_LINKER
    # ENABLE_BUILD_WITH_TIME_TRACE
    # ENABLE_UNITY
    # ENABLE_SANITIZER_ADDRESS
    # ENABLE_SANITIZER_LEAK
    # ENABLE_SANITIZER_UNDEFINED_BEHAVIOR
    # ENABLE_SANITIZER_THREAD
    # ENABLE_SANITIZER_MEMORY
    # LINKER <linker>
    # VS_ANALYSIS_RULESET <rules>
    # CONAN_PROFILE <profile>  # passes a profile to conan: see https://docs.conan.io/en/latest/reference/profiles.html
    # DOXYGEN_THEME <theme...>
    # MSVC_WARNINGS <warnings...>
    # CLANG_WARNINGS <warnings...>
    # GCC_WARNINGS <warnings...>
    # CUDA_WARNINGS <warnings...>
    # CPPCHECK_OPTIONS <options...>
    # PCH_HEADERS <headers...>
    CONAN_OPTIONS with_catch2=${CONAN_WITH_CATCH2}
    with_grpc=${CONAN_WITH_GRPC}
    with_protobuf=${CONAN_WITH_PROTOBUF}
    with_qt=${CONAN_WITH_QT}
    with_xercesc=${CONAN_WITH_XERCESC}
    *:shared=${CONAN_SHARED_BUILD}
)

if(CMAKE_CXX_COMPILER_ID STREQUAL "GNU")
    # disable -Wuseless-cast as this gives too many warnings on generated files
    target_compile_options(project_warnings INTERFACE -Wno-useless-cast)
endif()

# project code
##############################################################################
add_subdirectory(src)

# example code
##############################################################################
if(SILA_CPP_BUILD_EXAMPLES)
    add_subdirectory(examples)
endif()

# unit tests
##############################################################################
if(BUILD_TESTING)
    enable_testing()
    add_subdirectory(test)
endif()

# summary
##############################################################################
include(FeatureSummary)
feature_summary(WHAT ALL)
