#include <sila_cpp/client/SiLAClient.h>
#include <sila_cpp/server/SiLAServer.h>
#include <sila_cpp/common/ServerAddress.h>
#include <sila_cpp/common/ServerInformation.h>

#include <iostream>

using namespace SiLA2;

int main(int argc, char* argv[])
{
    auto SiLAServer = CSiLAServer{{"ConanTestServer"}};
    SiLAServer.run(false);

    // The client will automatically request most of the SiLAService Feature's
    // Properties.
    auto SiLAClient = CSiLAClient{{"localhost"}};

    return 0;
}
